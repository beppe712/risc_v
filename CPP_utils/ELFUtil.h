#if !defined(ELF_UTILS_H)
#define ELF_UTILS_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#if 0   /// 6/19/20
#include <string>
#include <iostream>
#include <sstream>
#else
#include "String.h"
#include "MyTemplates.h"
#endif

#if 1   /// 6/19/20
#define MESSAGE(msg)    printf("%s", msg.GetString())
#endif

#define EI_MAG0 0
#define EI_MAG1 1
#define EI_MAG2 2
#define EI_MAG3 3
#define EI_CLASS 4
#define EI_DATA 5
#define EI_VERSION 6
#define EI_OSABI 7
#define EI_ABIVERSION 8
#define EI_PAD 9
#define EI_NIDENT 16

///	e_ident[EI_CLASS]
#define ELFCLASSNONE	0		//  invalid class
#define ELFCLASS32		1		//  32-bit object
#define ELFCLASS64		2		//  64-bit object

///	e_ident[EI_DATA]
#define ELFDATANONE		0		//  invalid data encoding
#define ELFDATA2LSB		1		//  LSB-first (little endian)
#define ELFDATA2MSB		2		//	MSB-first (big endian)

///	e_type:
#define ET_COUNT	9

///	e_machine:
#define EM_COUNT		101

///	e_version:
#define EV_NONE	 0				//  invalid version
#define EV_CURRENT	 1			//  current version

#define ELFOSABI_COUNT	15
// 	64-255	Architecture-specific value range

#define SHT_COUNT	23
#define SHF_COUNT	12
#define SHN_COUNT	10

#define STB_COUNT	7
#define STT_COUNT	11
#define STV_COUNT	4

#define DT_COUNT	38

#define PT_COUNT	12
#define PF_COUNT	5

typedef unsigned long long  U64;
typedef unsigned int        U32;
typedef unsigned short      U16;
typedef unsigned char       U8;
typedef long long           S64;
typedef int                 S32;
typedef short               S16;
typedef char                S8;

typedef U32 Elf32_Addr;
typedef U16 Elf32_Half;
typedef U32 Elf32_Off;
typedef U32 Elf32_Sword;
typedef U32 Elf32_Word;

typedef U64 Elf64_Addr;
typedef U16 Elf64_Half;
typedef U64 Elf64_Off;
typedef U32 Elf64_Sword;
typedef U32 Elf64_Word;
typedef U64 Elf64_Xword;
typedef U64 Elf64_Sxword;

class ELF_Parser
{
public:
    class Info;
    enum ErrorStatus
    {
#define ES_ENUM
#include "ElfStatus.h"
#undef ES_ENUM
        ES_COUNT,
    };
    static const char * errorStatusName[ES_COUNT];
    static const char * GetErrorStatus(int errorStatus) {
        CRASH(errorStatus < 0 || errorStatus >= ES_COUNT);
        return errorStatusName[errorStatus];
    }
    class Status
    {
    public:
        int errorFlag, pos, val;
        Status() { Reset(); }
        void Reset() {
            errorFlag = ES_NO_ERROR;
            pos = 0;
            val = 0;
        }
    };
    class Value
    {
    public:
        const char * name;
        unsigned int value;
        const char * comment;
        int isLowRange, tmpValue;
        ///	9/30/15
        void RepairStrings(bool swapNameComment) {
            if (name == 0) { name = ""; }
            if (comment == 0) { comment = ""; }
            if (swapNameComment) {
                const char * tmp = name;
                name = comment;
                comment = tmp;
            }
        }
        void PrintInfo(String & s, const char * t) {
            if (isLowRange) { s.Printf("%12s = %3d [%s:RANGE](%s):\n", t, tmpValue, name, comment); }
            else { s.Printf("%12s = %3d [%s](%s):\n", t, value, name, comment); }
        }
    };
    static Value ehtypeArray[ET_COUNT], osabiArray[ELFOSABI_COUNT], machineArray[EM_COUNT];
    static Value shtypeArray[SHT_COUNT], shflagArray[SHF_COUNT], shnumArray[SHN_COUNT];
    static Value stbindArray[STB_COUNT], sttypeArray[STT_COUNT], stvisArray[STV_COUNT];
    static Value dtagArray[DT_COUNT], ptypeArray[PT_COUNT], pflagArray[PF_COUNT];
    class ValueGroup {
    public:
        unsigned count, maxNameLength;
        Value * value;
        ///	9/30/15
        ValueGroup(int c, Value * v, bool swapNameComment = false) {
            count = c;
            value = v;
            unsigned i;
            maxNameLength = 0;
            for (i = 0; i < count; i++) {
                value[i].RepairStrings(swapNameComment);
#if 1   /// 7/1/20
                unsigned l = (unsigned)strlen(value[i].name);
#else
                unsigned l = strlen(value[i].name);
#endif
                if (maxNameLength < l) { maxNameLength = l; }
            }
        }
        Value * GetValue(unsigned v) {
            unsigned i;
            for (i = 0; i < count; i++) {
                if (value[i].isLowRange) {
                    if (v >= value[i].value && v <= value[i + 1].value) { value[i].tmpValue = v; return &value[i]; }
                    i++;
                }
                else if (value[i].value == v) { return &value[i]; }
            }
            return 0;
        }
        Value * GetValue(const char * name) {
            unsigned i;
            for (i = 0; i < count; i++) { if (strcmp(value[i].name, name) == 0) { return &value[i]; } }
            CRASH("Error in ValueGroup::GetValue!!!");
            return 0;
        }
        ///	10/1/15
        String & GetValueMask(int v, String & s, bool addSpacing = false) {
            s = "";
            unsigned i;
            for (i = 0; i < count; i++) {
                if (value[i].value & v) {
                    s += value[i].name;
                    if (addSpacing) { s += " "; }
                }
            }
            return s;
        }
        int GetValueVector(const char * flags) {
#if 1   /// 7/1/20
            unsigned len = (unsigned)strlen(flags), i, j, vector = 0;
#else
            unsigned len = strlen(flags), i, j, vector = 0;
#endif
            for (i = 0; i < count; i++) {
                for (j = 0; j < len; j++) { if (strcmp(value[i].name, flags + j) == 0) { vector |= value[i].value; } }
            }
            return vector;
        }
    };
    static ValueGroup ehtypeGroup, osabiGroup, machineGroup;
    static ValueGroup shtypeGroup, shflagGroup, shnumGroup, stbindGroup, sttypeGroup, stvisGroup, dtagGroup, ptypeGroup, pflagGroup;
    class Elf32_Ehdr
    {
    public:
        Elf32_Half e_type;
        Elf32_Half e_machine;
        Elf32_Word e_version;
        Elf32_Addr e_entry;
        Elf32_Off e_phoff;
        Elf32_Off e_shoff;
        Elf32_Word e_flags;
        Elf32_Half e_ehsize;
        Elf32_Half e_phentsize;
        Elf32_Half e_phnum;
        Elf32_Half e_shentsize;
        Elf32_Half e_shnum;
        Elf32_Half e_shstrndx;
    };
    class Elf64_Ehdr
    {
    public:
        Elf64_Half e_type;
        Elf64_Half e_machine;
        Elf64_Word e_version;
        Elf64_Addr e_entry;
        Elf64_Off e_phoff;
        Elf64_Off e_shoff;
        Elf64_Word e_flags;
        Elf64_Half e_ehsize;
        Elf64_Half e_phentsize;
        Elf64_Half e_phnum;
        Elf64_Half e_shentsize;
        Elf64_Half e_shnum;
        Elf64_Half e_shstrndx;
    };
    class Elf_Ehdr : public Elf64_Ehdr
    {
    public:
#define ECPY(m)	e_##m = h->e_##m
#define ECPY_ALL	\
	ECPY(type);		ECPY(machine);		ECPY(version);	ECPY(entry);		ECPY(phoff);	ECPY(shoff);	ECPY(flags);	\
	ECPY(ehsize);	ECPY(phentsize);	ECPY(phnum);	ECPY(shentsize);	ECPY(shnum);	ECPY(shstrndx);
        void Copy(Elf32_Ehdr * h) { ECPY_ALL }
        void Copy(Elf64_Ehdr * h) { ECPY_ALL }
    };
    class Elf32_Shdr
    {
    public:
        Elf32_Word	sh_name;
        Elf32_Word	sh_type;
        Elf32_Word	sh_flags;
        Elf32_Addr	sh_addr;
        Elf32_Off	sh_offset;
        Elf32_Word	sh_size;
        Elf32_Word	sh_link;
        Elf32_Word	sh_info;
        Elf32_Word	sh_addralign;
        Elf32_Word	sh_entsize;
    };
    class Elf64_Shdr
    {
    public:
        Elf64_Word	sh_name;
        Elf64_Word	sh_type;
        Elf64_Xword	sh_flags;
        Elf64_Addr	sh_addr;
        Elf64_Off	sh_offset;
        Elf64_Xword	sh_size;
        Elf64_Word	sh_link;
        Elf64_Word	sh_info;
        Elf64_Xword	sh_addralign;
        Elf64_Xword	sh_entsize;
    };
    class Image
    {
    public:
        unsigned char * mem;
        unsigned int size, addr;
        Image(int sz, int ad) {
            addr = ad;
            size = ((sz + 3) >> 2) << 2;
            mem = new unsigned char[size];
            memset(mem, 0, size);
        }
        ~Image() { delete[] mem; }
        bool IsValidAddr(unsigned int addr0) { return (unsigned int)addr <= addr0 && (unsigned int)(addr + size) > addr0; }
    };
    class Elf_Shdr : public Elf64_Shdr
    {
    public:
        const char * sh_name_str;
        Image *image;
        class Field {
        public:
            unsigned sh_alloc   : 1;
            unsigned executable : 1;
            Field() : sh_alloc(0), executable(0) { }
            void Reset() { sh_alloc = 0; executable = 0; }
        } field;
        //int sh_alloc;

        Elf_Shdr() : sh_name_str(0)/*, sh_alloc(0)*/, image(0) { }
        ~Elf_Shdr() { if (image) { delete image; } }

#define SCPY(m)	sh_##m = s->sh_##m
#define SCPY_ALL	\
	SCPY(name);	SCPY(type);	SCPY(flags);	SCPY(addr);			SCPY(offset);	\
	SCPY(size);	SCPY(link);	SCPY(info);		SCPY(addralign);	SCPY(entsize);	\
	sh_name_str = 0; image = 0; field.Reset(); 
        void Copy(Elf32_Shdr * s) { SCPY_ALL }
        void Copy(Elf64_Shdr * s) { SCPY_ALL }
        void PrintInfo(String & s, int id, int maxStringLength);
        static int PrintHeader(String & s, int maxStringLength);
        void PrintImage(FILE *fp, int maxNameLength);
    };
    class ShdrTable
    {
    public:
        Status status;
        Elf_Shdr * shdr, *symbolSection, *dynSymSection, *dynSection;
        LinkedList<Elf_Shdr> relocSectionList, sectionList;
        String sectionStr, symbolStr, dynSymStr;
        int sectionCount, maxSectionStringLength;
        ShdrTable() : relocSectionList(1), sectionList(1) {
            shdr = 0;
            Reset();
        }
        bool Parse(Info * elfInfo, FILE * fp);
        ~ShdrTable() { Reset(); }
        void Reset() {
            if (shdr) { delete[] shdr; }
            shdr = 0;
            symbolSection = 0;
            dynSymSection = 0;
            dynSection = 0;
            relocSectionList.FlushAll();
            sectionList.FlushAll();
            sectionStr = "";
            symbolStr = "";
            dynSymStr = "";
            sectionCount = 0;
            maxSectionStringLength = 0;
            status.Reset();
        }
        bool CreateSections(FILE *fp, FILE *fout);
    };

    class Ehdr;

#define ELF_ST_BIND(i)			((i)>>4)
#define ELF_ST_TYPE(i)			((i)&0xf)
#define ELF_ST_INFO(b,t)		(((b)<<4)+((t)&0xf))
#define ELF_ST_VISIBILITY(o)	((o)&0x3)

    class Elf32_Sym {
    public:
        Elf32_Word	st_name;
        Elf32_Addr	st_value;
        Elf32_Word	st_size;
        unsigned char	st_info;
        unsigned char	st_other;
        Elf32_Half	st_shndx;
    };
    class Elf64_Sym {
    public:
        Elf64_Word	st_name;
        unsigned char	st_info;
        unsigned char	st_other;
        Elf64_Half	st_shndx;
        Elf64_Addr	st_value;
        Elf64_Xword	st_size;
    };
    class Elf_Sym : public Elf64_Sym
    {
    public:
        const char *	st_name_str;
        unsigned char	st_bind;
        unsigned char	st_type;
        unsigned char	st_vis;

#define MCPY(m)	st_##m = s->st_##m
#define MCPY_ALL											\
	MCPY(name);		MCPY(value);	MCPY(size);	MCPY(info);	\
	MCPY(other);	MCPY(shndx);							\
	st_bind = ELF_ST_BIND(st_info);							\
	st_type = ELF_ST_TYPE(st_info);							\
	st_vis = ELF_ST_VISIBILITY(st_other);					\
	st_name_str = 0;
        void Copy(Elf32_Sym * s) { MCPY_ALL }
        void Copy(Elf64_Sym * s) { MCPY_ALL }
        void PrintInfo(String & s, int id, int maxStringLength, ShdrTable * sectionTable);
        static int PrintHeader(String & s, int maxStringLength, Elf_Shdr * symbolSection);
    };
    class SymTable
    {
    public:
        Status status;
        Elf_Sym * symTable;
        Elf_Shdr * symbolSection;
        int symbolCount, maxSymbolStringLength;
        SymTable() { symTable = 0; Reset(); }
        bool Parse(Info * elfInfo, FILE * fp, Elf_Shdr * symSect, const char * symbolStringTable, bool showAll);
        ~SymTable() { Reset(); }
        void Reset() {
            if (symTable) { delete[] symTable; }
            symTable = 0;
            symbolCount = 0;
            symbolSection = 0;
            maxSymbolStringLength = 0;
            status.Reset();
        }
    };
    class Elf32_Rel
    {
    public:
        Elf32_Addr	r_offset;
        Elf32_Word	r_info;
    };
    class Elf32_Rela
    {
    public:
        Elf32_Addr	r_offset;
        Elf32_Word	r_info;
        Elf32_Sword	r_addend;
    };
    class Elf64_Rel
    {
    public:
        Elf64_Addr	r_offset;
        Elf64_Xword	r_info;
    };
    class Elf64_Rela
    {
    public:
        Elf64_Addr		r_offset;
        Elf64_Xword		r_info;
        Elf64_Sxword	r_addend;
    };
    class Elf_Rel : public Elf64_Rela
    {
    public:
        bool addend_enabled;
        U32 r_sym;
        U32 r_type;

#define ELF32_R_SYM(i)	((i)>>8)
#define ELF32_R_TYPE(i)   ((unsigned char)(i))
#define ELF32_R_INFO(s,t) (((s)<<8)+(unsigned char)(t))

#define ELF64_R_SYM(i)    ((i)>>32)
#define ELF64_R_TYPE(i)   ((i)&0xffffffffL)
#define ELF64_R_INFO(s,t) (((s)<<32)+((t)&0xffffffffL))

        void Copy(Elf32_Rel * r) {
            r_sym = ELF32_R_SYM(r->r_info);
            r_type = ELF32_R_TYPE(r->r_info);
            r_info = r->r_info;
            r_offset = r->r_offset;
            addend_enabled = false;
        }
        void Copy(Elf32_Rela * r) {
            r_sym = ELF32_R_SYM(r->r_info);
            r_type = ELF32_R_TYPE(r->r_info);
            r_info = r->r_info;
            r_offset = r->r_offset;
            r_addend = r->r_addend;
            addend_enabled = true;
        }
        void Copy(Elf64_Rel * r) {
            r_sym = ELF64_R_SYM(r->r_info);
            r_type = ELF64_R_TYPE(r->r_info);
            r_info = r->r_info;
            r_offset = r->r_offset;
            addend_enabled = false;
        }
        void Copy(Elf64_Rela * r) {
            r_sym = ELF64_R_SYM(r->r_info);
            r_type = ELF64_R_TYPE(r->r_info);
            r_info = r->r_info;
            r_offset = r->r_offset;
            r_addend = r->r_addend;
            addend_enabled = true;
        }
        void PrintInfo(String & s, int id, SymTable * symTable);
        static int PrintHeader(String & s, Elf_Shdr * relocSection);
    };
    class RelocTable
    {
    public:
        Status status;
        Elf_Rel * relocTable;
        Elf_Shdr * relocSection;
        SymTable * symTable;
        int relocCount;
        bool Parse(Info * elfInfo, FILE * fp, Elf_Shdr * relSect);
        RelocTable() { relocTable = 0; Reset(); }
        void Reset() {
            //			if(relocTable){ delete relocTable;}
            if (relocTable) { delete[] relocTable; }
            relocTable = 0;
            relocSection = 0;
            relocCount = 0;
            symTable = 0;
            status.Reset();
        }
        ~RelocTable() { Reset(); }
    };
    class Elf32_Dyn
    {
    public:
        Elf32_Sword	d_tag;
        union {
            Elf32_Word	d_val;
            Elf32_Addr	d_ptr;
        } d_un;
    };
    class Elf64_Dyn
    {
    public:
        Elf64_Sxword	d_tag;
        union {
            Elf64_Xword	d_val;
            Elf64_Addr	d_ptr;
        } d_un;
    };
    class Elf_Dyn : public Elf64_Dyn
    {
    public:
        const char * name;
        void Copy(Elf32_Dyn * d) {
            d_tag = d->d_tag;
            d_un.d_val = d->d_un.d_val;
            name = 0;
        }
        void Copy(Elf64_Dyn * d) {
            d_tag = d->d_tag;
            d_un.d_val = d->d_un.d_val;
            name = 0;
        }
        void PrintInfo(String & s, int id);
        static int PrintHeader(String & s, Elf_Shdr * dynSection);
    };
    class DynTable
    {
    public:
        Status status;
        Elf_Dyn * dynTable;
        Elf_Shdr * dynSection;
        int dynCount;
        bool Parse(Info * elfInfo, FILE * fp, Elf_Shdr * dynSect, const char * symbolStringTable);
        DynTable() { dynTable = 0; Reset(); }
        void Reset() {
            if (dynTable) { delete dynTable; }
            dynTable = 0;
            dynSection = 0;
            dynCount = 0;
            status.Reset();
        }
        ~DynTable() { Reset(); }
    };
    class Elf32_Phdr
    {
    public:
        Elf32_Word	p_type;
        Elf32_Off	p_offset;
        Elf32_Addr	p_vaddr;
        Elf32_Addr	p_paddr;
        Elf32_Word	p_filesz;
        Elf32_Word	p_memsz;
        Elf32_Word	p_flags;
        Elf32_Word	p_align;
    };
    class Elf64_Phdr
    {
    public:
        Elf64_Word	p_type;
        Elf64_Word	p_flags;
        Elf64_Off	p_offset;
        Elf64_Addr	p_vaddr;
        Elf64_Addr	p_paddr;
        Elf64_Xword	p_filesz;
        Elf64_Xword	p_memsz;
        Elf64_Xword	p_align;
    };
    class Elf_Phdr : public Elf64_Phdr
    {
    public:
#define PCPY(m)	p_##m = p->p_##m
#define PCPY_ALL	\
			PCPY(type);		PCPY(flags);	PCPY(offset);	PCPY(vaddr);	\
			PCPY(paddr);	PCPY(filesz);	PCPY(memsz);	PCPY(align);
        void Copy(Elf32_Phdr * p) { PCPY_ALL }
        void Copy(Elf64_Phdr * p) { PCPY_ALL }
        void PrintInfo(String & s, int id, ShdrTable * sectionTable);
        static int PrintHeader(String & s);
    };
    class PhdrTable
    {
    public:
        Status status;
        Elf_Phdr * phdr;
        int segmentCount;
        char * interpName;
        PhdrTable() { phdr = 0; interpName = 0; Reset(); }
        bool Parse(Info * elfInfo, FILE * fp);
        ~PhdrTable() { Reset(); }
        void Reset() {
            if (phdr) { delete[] phdr; }
            if (interpName) { delete[] interpName; }
            phdr = 0;
            interpName = 0;
            segmentCount = 0;
            status.Reset();
        }
    };
    class Ehdr
    {
    public:
        Status status;
        unsigned char e_ident[EI_NIDENT];
        Elf_Ehdr ehdr;
        bool IsElf32() { return e_ident[EI_CLASS] == ELFCLASS32; }
        bool IsElf64() { return e_ident[EI_CLASS] == ELFCLASS64; }
        bool IsElfNone() { return e_ident[EI_CLASS] == ELFCLASSNONE; }
        bool Parse(FILE * fp);
        void PrintMagicNumber(String & s) {
            s.Printf("%12s = ", "Magic");
            int i;
            for (i = 0; i < EI_NIDENT; i++) { s.Printf("%02x ", e_ident[i]); }
            s.Printf("\n");
        }
        void PrintClass(String & s) {
            s.Printf("%12s = ", "Class");
            if (IsElf32()) { s.Printf("ELF32\n"); }
            else if (IsElf64()) { s.Printf("ELF64\n"); }
            else { s.Printf("<unknown-ELF-class>\n"); }
        }
        void PrintByteOrder(String & s) {
            s.Printf("%12s = ", "Byte-Order");
            if (e_ident[EI_DATA] == ELFDATA2LSB) { s.Printf("Little-endian\n"); }
            else if (e_ident[EI_DATA] == ELFDATA2MSB) { s.Printf("Big-endian\n"); }
            else { s.Printf("<unknown-endian>\n"); }
        }
        void PrintFileVersion(String & s) {
            s.Printf("%12s = ", "File-Version");
            if (e_ident[EI_VERSION] == EV_CURRENT) { s.Printf("Current-version:\n"); }
            else { s.Printf("<invalid-version>:\n"); }
        }
        void PrintABIVersion(String & s) {
            s.Printf("%12s = %3d\n", "ABI-Version", e_ident[EI_ABIVERSION]);
        }
        void PrintInfo(String & s);
        void Reset() { status.Reset(); }
        ~Ehdr() { Reset(); }
#define PRINT_ELF_PARAM(s, n)		s.Printf("%12s = %10d (%08x):\n", #n, ehdr.n, ehdr.n)
#define PRINT_ELF_PARAM_LONG(s, n)	s.Printf("%12s = %10lld (%08llx):\n", #n, ehdr.n, ehdr.n)
    };
    class MemoryInfo
    {
    public:
        unsigned char *mem;
        unsigned size;
        int printfAddrWidth, printfDataWidth, segmentCount;
        FILE *fp[4];
        MemoryInfo() {
            mem = 0; size = 0; printfAddrWidth = 0; printfDataWidth = 0; segmentCount = 0;
            fp[0] = 0; fp[1] = 0; fp[2] = 0; fp[3] = 0;
        }
        void Set(unsigned char *m, unsigned sz, int aw, int dw, int sc, FILE *fp0[4]) {
            mem = m; size = sz; printfAddrWidth = aw; printfDataWidth = dw; segmentCount = sc;
            int i;
            for (i = 0; i < 4; ++i) { fp[i] = fp0[i]; }
        }
        void Close() {
            int i;
#if 1
			for (i = 0; i < segmentCount; ++i) {
				if (fp[i]) { fclose(fp[i]); }
			}
#else
            for (i = 0; i < segmentCount; ++i) { CRASH(fp[i] == 0); fclose(fp[i]); }
#endif
        }
    };
    class Info
    {
    public:
        Status status;
        MemoryInfo dmemInfo, pmemInfo;

        String projectDescriptor;

        Ehdr elfHeader;
        ShdrTable sectionTable;
        PhdrTable segmentTable;
        SymTable symbolTable, dynSymTable;
        DynTable dynTable;
        LinkedList<RelocTable> relocTableList;
        Info() { fout = 0; }
        bool Parse(FILE * fp);
        bool InstallMemory(int memDumpFlag);
        bool InstallSection(Elf_Shdr * section, MemoryInfo *mi);
        void Reset() {
            elfHeader.Reset();
            sectionTable.Reset();
            segmentTable.Reset();
            symbolTable.Reset();
            dynSymTable.Reset();
            dynTable.Reset();
            relocTableList.DeleteAll();
        }
        void ShowErrorStatus(String & s) {
            ///	11/21/13
            s.Printf("error status: info(%s), ehdr(%s), shdr(%s, pos = %d, val = %d(%x))\n",
                GetErrorStatus(status.errorFlag),
                GetErrorStatus(elfHeader.status.errorFlag),
                GetErrorStatus(sectionTable.status.errorFlag),
                sectionTable.status.pos, sectionTable.status.val, sectionTable.status.val);
            s.Printf("error status: sym(%s, pos = %d, val = %d(%x)), dynSym(%s, pos = %d, val = %d(%x)), dyn(%s, pos = %d, val = %d(%x))\n",
                GetErrorStatus(symbolTable.status.errorFlag),
                symbolTable.status.pos, symbolTable.status.val, symbolTable.status.val,
                GetErrorStatus(dynSymTable.status.errorFlag),
                dynSymTable.status.pos, dynSymTable.status.val, dynSymTable.status.val,
                GetErrorStatus(dynTable.status.errorFlag),
                dynTable.status.pos, dynTable.status.val, dynTable.status.val);
            if (fout) { fprintf(fout, "%s", s.GetString()); }
        }
        static bool CompareUnsigned(unsigned int a, unsigned b) { return a > b; }
    };
    static bool CompareSection(Elf_Shdr * a, Elf_Shdr * b) { return a->sh_addr > b->sh_addr; }
    static bool CompareSymbol(Elf_Sym * a, Elf_Sym * b) { return a->st_value > b->st_value; }
    static FILE * fout;
    static Info info;
    static void PrintTail(String & s, int len) {
        int ll = len;
        while (ll > 0) { s += "="; ll--; }
        s += "\n";
    }
};

extern ELF_Parser::Info elfInfo;

#endif  // ELF_UTILS_H
