
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include <vector>


typedef struct ST_Timer
{
	const char * name;
	long t, cycles;
} Timer;

int check_code(const char * code, unsigned int * val);

#define USE_NEW_PROFILER

//#if defined(USE_NEW_PROFILER)
#define MAX_PROFILE_FUNC_NAME_LENGTH	60
#define MAX_SECTION_NAME_LENGTH			10
#if 1	///	9/27/17 : use Profile classes!!!
struct ProfileInfo {
	unsigned int entryAddr;
	long cycles, subCycles, calls;
	char name[MAX_PROFILE_FUNC_NAME_LENGTH + 1];
	char sectName[MAX_SECTION_NAME_LENGTH + 1];
	static int maxProfileFuncNameLength;
	static bool CompareProfile(ProfileInfo *a, ProfileInfo *b);
	void PrintProfileInfo(FILE * fp, double totalCycles);
};

typedef struct ST_ProfileInfoInstance
{
	long cycles, subCycles;
	ProfileInfo * pi;
} ProfileInfoInstance;

//#define MAX_PROFILE_FUNC_COUNT	1000
struct Profiler
{
#ifdef MAX_PROFILE_FUNC_COUNT
	ProfileInfo profileInfoArray[MAX_PROFILE_FUNC_COUNT];
	ProfileInfoInstance profileInfoStack[MAX_PROFILE_FUNC_COUNT];
#else
	std::vector<ProfileInfo> profileInfoArray;
	std::vector<ProfileInfoInstance> profileInfoStack;
#endif
	int profileCount, sp, extraCalls;
	ProfileInfoInstance * curProfInfo;
	long skippedCycles;
	int id;
//	int profileEnabled;
	Profiler(int idx) : profileCount(0), sp(0), extraCalls(0), skippedCycles(0), id(idx) {}
	void SetupProfiler(const char * prog_file_name);
	void SetupElfProfiler();
	void ProfilerPush(unsigned int targetAddr);
	void ProfilerPop();
	void ProfilerUpdate();
	void ProfilerSkip();
	void ProfilerStart();
	void InitializeProfileInfoInstance();
	void ProfilerPrint();
	void PushProfileInfoInstance(ProfileInfo * pi);
	void PopProfileInfoInstance();
};

struct ProfilerGroup
{
	std::vector<Profiler> prof;
	Profiler *curProf;
	ProfilerGroup() : curProf(NULL) {}
	void ProfilerPush(unsigned int targetAddr) { if (curProf) { curProf->ProfilerPush(targetAddr); } }
	void ProfilerPop() { if (curProf) { curProf->ProfilerPop(); } }
	void ProfilerUpdate() { if (curProf) { curProf->ProfilerUpdate(); } }
	void ProfilerSkip() { if (curProf) { curProf->ProfilerSkip(); } }
	void ProfilerPrint() { if (curProf) { curProf->ProfilerPrint(); } }
	void ProfilerPrint(int idx) {
		if (idx < (int)prof.size()) { prof[idx].ProfilerPrint(); }
	}
	void SetupProfiler(const char * prog_file_name) {
#if 1   /// 7/1/20
        prof.emplace_back((int)prof.size());
#else
		prof.emplace_back(prof.size());
#endif
		curProf = &prof.back();
		curProf->SetupProfiler(prog_file_name);
#if 1   /// 7/1/20
        for (int i = 0, e = (int)prof.size(); i < e; ++i) { prof[i].InitializeProfileInfoInstance(); }
#else
		for (int i = 0, e = prof.size(); i < e; ++i) { prof[i].InitializeProfileInfoInstance(); }
#endif
	}
	void SetupElfProfiler() {
#if 1   /// 7/1/20
        prof.emplace_back((int)prof.size());
#else
		prof.emplace_back(prof.size());
#endif
		curProf = &prof.back();
		curProf->SetupElfProfiler();
#if 1   /// 7/1/20
        for (int i = 0, e = (int)prof.size(); i < e; ++i) { prof[i].InitializeProfileInfoInstance(); }
#else
		for (int i = 0, e = prof.size(); i < e; ++i) { prof[i].InitializeProfileInfoInstance(); }
#endif
	}
	void SetProfiler(int idx) {
		if (idx < (int)prof.size()) { curProf = &prof[idx]; }
		else { curProf = NULL; }
	}
};

extern ProfilerGroup g_prof;

//extern std::vector<Profiler> g_prof;

//extern Profiler g_prof;

#define USE_ELF_FILE

#if !defined(__LLVM_C2RTL__)
#define PROFILER_CALL(pc)	g_prof.ProfilerPush(pc);
#define PROFILER_CALL_COND(pc, cond)	if (cond) g_prof.ProfilerPush(pc);
#define PROFILER_RETURN		g_prof.ProfilerPop();
#define PROFILER_UPDATE		g_prof.ProfilerUpdate();
#define PROFILER_SKIP		g_prof.ProfilerSkip(); // for sleep support through wait instruction
//#define PROFILER_START		g_prof.ProfilerStart();
#define PROFILER_PRINT		g_prof.ProfilerPrint();

#define MP_PROFILER_CALL(pc)	prof->ProfilerPush(pc);
#define MP_PROFILER_CALL_COND(pc, cond)	if (cond) prof->ProfilerPush(pc);
#define MP_PROFILER_RETURN		prof->ProfilerPop();
#define MP_PROFILER_UPDATE		prof->ProfilerUpdate();
#define MP_PROFILER_SKIP		prof->ProfilerSkip(); // for sleep support through wait instruction
//#define PROFILER_START		g_prof.ProfilerStart();
#else
#define PROFILER_CALL(pc)
#define PROFILER_CALL_COND(pc, cond)
#define PROFILER_RETURN
#define PROFILER_UPDATE
#define PROFILER_START
#define PROFILER_PRINT

#define MP_PROFILER_CALL(pc)
#define MP_PROFILER_CALL_COND(pc, cond)
#define MP_PROFILER_RETURN
#define MP_PROFILER_UPDATE
#define MP_PROFILER_SKIP
#endif
#else
typedef struct ST_ProfileInfo
{
	unsigned int entryAddr;
	long cycles, subCycles, calls;
	char name[MAX_PROFILE_FUNC_NAME_LENGTH + 1];
	char sectName[MAX_SECTION_NAME_LENGTH + 1];
	static int maxProfileFuncNameLength;
} ProfileInfo;

bool CompareProfile(ProfileInfo *a, ProfileInfo *b);

typedef struct ST_ProfileInfoInstance
{
	long cycles, subCycles;
	ProfileInfo * pi;
} ProfileInfoInstance;
#define MAX_PROFILE_FUNC_COUNT	1000
typedef struct ST_Profiler
{
	ProfileInfo profileInfoArray[MAX_PROFILE_FUNC_COUNT];
	ProfileInfoInstance profileInfoStack[MAX_PROFILE_FUNC_COUNT];
	int profileCount, sp, extraCalls;
	ProfileInfoInstance * curProfInfo;
	long skippedCycles;
    int profileEnabled;
} Profiler;

extern Profiler g_prof;

void SetupProfiler(const char * prog_file_name);
void SetupElfProfiler();


void ProfilerPush(unsigned int targetAddr);
void ProfilerPop();
void ProfilerUpdate();
void ProfilerSkip(); 
void ProfilerStart();
void ProfilerPrint();

#define USE_ELF_FILE

#if !defined(__LLVM_C2RTL__) //&& !defined(USE_ELF_FILE)
#define PROFILER_CALL(pc)	ProfilerPush(pc);
#define PROFILER_CALL_COND(pc, cond)	if (cond) ProfilerPush(pc);
#define PROFILER_RETURN		ProfilerPop();
#define PROFILER_UPDATE		ProfilerUpdate();
#define PROFILER_SKIP		ProfilerSkip(); // for sleep support through wait instruction
#define PROFILER_START		ProfilerStart();
#define PROFILER_PRINT		ProfilerPrint();
#else
#define PROFILER_CALL(pc)
#define PROFILER_CALL_COND(pc, cond)
#define PROFILER_RETURN
#define PROFILER_UPDATE
#define PROFILER_START
#define PROFILER_PRINT
#define PROFILER_SKIP
#endif
#endif

void get_timer(Timer * tm);

extern FILE * fp_log;

void open_log_file(const char * n);
void close_log_file();

void print_format_double(double val, FILE * fp);
void print_format_int(long val, FILE * fp);
char * print_format_double_string(double val);
char * print_format_int_string(long val);

//typedef unsigned int UINT32;

extern const char *progFileName;
extern const char *elfFileName;

void install_program_tct(unsigned * dmem, unsigned * pmem, unsigned dmem_size, unsigned pmem_size, int memDumpFlag, int skipProfile = 0, char * file_name=(char *)progFileName);
void install_program_elf(unsigned * dmem, unsigned * pmem, unsigned dmem_size, unsigned pmem_size,
    unsigned *pc, unsigned *sp, int memDumpFlag, int pc_offset = 0, int skipProfile = 0, char * file_name=(char *)elfFileName);

void openMemDumpFile(int type, FILE **fp, int *segmentCount, int *printfAddrWidth, int *printfDataWidth);
