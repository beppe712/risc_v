// String.cpp: String クラスのインプリメンテーション
//
//////////////////////////////////////////////////////////////////////

#include "String.h"

FILE * String::stringWorkspaceFile = 0;

//////////////////////////////////////////////////////////////////////
// 構築/消滅
//////////////////////////////////////////////////////////////////////

#define INITIAL_STRING_ALLOC_SIZE 10
//#define STRING_CONSTRUCTOR	data(new char[INITIAL_STRING_ALLOC_SIZE]), length(0), allocSize(INITIAL_STRING_ALLOC_SIZE)
#define STRING_CONSTRUCTOR	data(0), length(0), allocSize(0)

int String::maxAllocSize = 0;

String::String() : STRING_CONSTRUCTOR
{
	*this = "";
}

String::String(const char * s, int l) : STRING_CONSTRUCTOR
{
	Set(s, l);
}

String::String(const char * s) : STRING_CONSTRUCTOR
{
	operator=(s);
}

String::String(const String & s) : STRING_CONSTRUCTOR
{
	operator=(s.GetString());
}

String::~String()
{
	delete [] data;
}

void String::Set(const char * s, int l)
{
	if(s){
		int ll = strlen(s);
		if(l > ll) l = ll;
		length = l;
		char * newdata = AllocateMemory();
		memmove(newdata, s, length);
		newdata[length] = 0;
		if(data != newdata){
			delete [] data;
			data = newdata;
		}
	}
	else *this = "";
}

char String::Val(int i)
{
	if(length <= i) return 0;
	return data[i];
}


void String::operator=(const char * s)
{
	if(s){
		if(*s || data == 0){
			length = strlen(s);
			char * newdata = AllocateMemory();
			memmove(newdata, s, length);
			newdata[length] = 0;
			if(data != newdata){
				delete [] data;
				data = newdata;
			}
		}
		else{	///	empty string
			data[0] = 0;
			length = 0;
		}
	}
	else{ *this = "";}
}

void String::operator+=(const char * s)
{
	if(s){
		int oldLength = length;
		int slength = strlen(s);
		length += slength;
		if(allocSize < length + 1){
			int maxLength = (oldLength > slength) ? oldLength : slength;
			allocSize = (maxLength << 1) + 1;
			char * temp = new char[allocSize];
			memcpy(temp, data, oldLength);
			memcpy(temp + oldLength, s, slength);
			temp[length] = 0;
			delete [] data;
			data = temp;
		}
		else{
			memcpy(data + oldLength, s, slength);
			data[length] = 0;
		}
	}
}

void String::operator+=(int i){ char s[1000]; sprintf(s, "%d", i); operator+=(s);}
void String::operator=(int i){ char s[1000]; sprintf(s, "%d", i); operator=(s);}

void String::operator+=(const char c)
{
	///	3/1/09
	int oldLength = length;
	int slength = 1;
	length += slength;
	if(allocSize < length + 1){
		int maxLength = (oldLength > slength) ? oldLength : slength;
		allocSize = (maxLength << 1) + 1;
		char * temp = new char[allocSize];
		memcpy(temp, data, oldLength);
		temp[length - 1] = c;
		temp[length] = 0;
		delete [] data;
		data = temp;
	}
	else{
		data[length - 1] = c;
		data[length] = 0;
	}
}

bool String::IsSame(const char * a, const char * b, int l)
{
	while(l --){
		char aa = *(a ++);
		char bb = *(b ++);
		if(aa != bb)
			return false;
		if(aa + bb == 0)
			return true;
	}
	return true;
}

bool String::operator==(const char * s)
{
	if(s == 0) return false;
	return IsSame(data, s);
}

bool String::Match(const char * s, int l)
{
	if(s == 0) return false;
	return (GetLength() == l && IsSame(data, s, l));
}

int String::GetMatchingPosition(const char * s)
{
	if(s == 0) return -1;
	char * p = data;
	int len = strlen(s);
	while(*p){
		if(IsSame(p, s, len)) return p - data;
		p ++;
	}
	return -1;
}

int String::GetLongestMatchingLength(String & a, String & b)
{
	const char * pa = a.GetString(), * pb = b.GetString();
	int len = (a.GetLength() < b.GetLength()) ? a.GetLength() : b.GetLength();
	while(len > 0){ if(IsSame(pa, pb, len)){ return len;} len --;}
	return len;
}

bool String::PartMatch(const char * s, int l)
{
	if(s == 0) return false;
	return IsSame(data, s, l);
}

bool String::MatchSuffix(const char * suffix)
{
	int l = strlen(suffix);
	if(length < l)
		return false;
	return (strcmp(data + length - l, suffix) == 0);
}

bool String::Includes(const char * s)
{
	///	11/25/10
	if(s == 0)
		return false;
	int l = strlen(s);
	if(length < l)
		return false;
	char * p = data;
	while(*(p + l - 1)){
		if(IsSame(p, s, l)) return true;
		p ++;
	}
	return false;
}

bool String::operator >(String & s)
{
	int len0 = GetLength();
	int len1 = s.GetLength();
	int len = (len0 < len1) ? len0 : len1;
	int i;
	for(i = 0; i < len; i ++){
		if(Val(i) > s.Val(i)) return true;
		else if(Val(i) < s.Val(i)) return false;
	}
	return (len0 < len1);
}

bool String::operator <(String & s)
{
	int len0 = GetLength();
	int len1 = s.GetLength();
	int len = (len0 < len1) ? len0 : len1;
	int i;
	for(i = 0; i < len; i ++){
		if(Val(i) > s.Val(i)) return false;
		else if(Val(i) < s.Val(i)) return true;
	}
	return (len0 < len1);
}

int String::Count(const char * s)
{
	if(data == 0) return 0;
	char * p = data;
	int slength = strlen(s);
	int cc = 0;
	while(*p){
		cc += IsSame(p, s, slength);
		p ++;
	}
	return cc;
}

int String::Count(char c)
{
	if(data == 0) return 0;
	char * p = data;
	int count = 0;
	while(*p){ count += (*(p ++) == c);}
	return count;
}

void String::ChangeToChar(char c)
{
	char * p = data;
	while(*p)
		*(p ++) = c;
}

void String::SetChar(char c, int length)
{
	if(length < 0)
		length = 0;
	char * p = new char[length + 1];
	p[length] = 0;
	int i;
	for(i = 0; i < length; i ++)
		p[i] = c;
	*this = p;
	delete [] p;
}

void String::ConvertIndent(String & s)
{
	char * p = data + strlen(data) - 1;
	int i = 0;
	while(p > data && *p != '\n'){ p --; i ++;}
	p ++;
	char * pp = new char[i + 1];
	char * ppp = pp;
	int j;
	for(j = 0; j < i; j ++){
		if(p[j] == '\t') ppp[j] = '\t';
		else ppp[j] = ' ';
	}
	ppp[j] = 0;
	s = ppp;
	delete [] pp;
}

#include "LinkedList.h"

char __formatInteger[1000];

#define FORMAT_STR_COUNT	20
static String formatStr[FORMAT_STR_COUNT];
static int formatStrPtr = 0;

const char * String::GetFormatInteger(unsigned long long val)
{
	char s[1000];
	sprintf(s, "%lld", val);
	int l = strlen(s), l0 = l;
	char * p = s, * pp = __formatInteger;
	for(; *p; p ++, pp ++, l --){
		if(*p == '-'){
			*pp = '-';
			continue;
		}
		if(l != l0 && (l % 3 == 0)){
			*(pp ++) = ',';
		}
		*pp = *p;
	}
	*pp = 0;
	///	12/5/13
	String * str = &formatStr[formatStrPtr ++];
	if(formatStrPtr >= FORMAT_STR_COUNT){ formatStrPtr = 0;}
	*str = __formatInteger;
	return str->GetString();
}

const char * String::GetFormatIntegerFromDouble(double val)
{
	char s[1000];
	sprintf(s, "%.f", val);
	int l = strlen(s), l0 = l;
	char * p = s, * pp = __formatInteger;
	for(; *p; p ++, pp ++, l --){
		if(*p == '-'){
			*pp = '-';
			continue;
		}
		if(l != l0 && (l % 3 == 0)){
			*(pp ++) = ',';
		}
		*pp = *p;
	}
	*pp = 0;
	///	12/5/13
	String * str = &formatStr[formatStrPtr ++];
	if(formatStrPtr >= FORMAT_STR_COUNT){ formatStrPtr = 0;}
	*str = __formatInteger;
	return str->GetString();
}

void String::ReplaceChar(char from, char to)
{
	char * p = data;
	while(*p){
		if(*p == from)
			*p = to;
		p ++;
	}
}

	///	3/9/14

#define MAX_LENGTH 20000
static char String_buf[MAX_LENGTH + 1];

int String::GetVPrintfLength(const char * format, va_list arglist)
{
    int ret = vsnprintf(String_buf, MAX_LENGTH, format, arglist);
    if (ret >= 0) { return ret + 1; }

    CRASH(stringWorkspaceFile == 0);
    rewind(stringWorkspaceFile);
    vfprintf(stringWorkspaceFile, format, arglist);
    fseek(stringWorkspaceFile, 0, SEEK_END);
    ret = ftell(stringWorkspaceFile);
    //	fclose(stringWorkspaceFile);
    return ret + 1;
}


#if 0   /// 11/28/16 : problem on ubuntu...
int String::Printf(const char * format, ...)
{
    va_list arglist;
    va_start(arglist, format);

    int length = GetVPrintfLength(format, arglist);
    va_end(arglist);
    *this += String_buf;
    return length;
}
#else
int String::Printf(const char * format, ...)
{
	va_list arglist;
	va_start(arglist, format);

//#define DBG_STRING
	int len = GetVPrintfLength(format, arglist);
#if defined(DBG_STRING)
    printf("String::Printf : length = %d\n", len);
    printf("String_buf = %s\n", String_buf);
#endif
	String s;
	char * str = s.AllocateMemory(len);
#if defined(DBG_STRING)
    printf("str = %llx\n", (long long)str);
    for (int i = 0; i < len; ++i) { str[i] = 0; }
    printf("str[%d] = %d\n", len - 1, str[len - 1]);
#endif
    va_start(arglist, format);
#if 0
    int ret = vsnprintf(str, len, format, arglist);
#else
    int ret = vsprintf(str, format, arglist);
#endif
//    va_end(arglist);
#if defined(DBG_STRING)
    printf("String::Printf : ret = %d\n", length);
#endif
    *this += s;
	return ret;
}
#endif

char String::Termination()
{
	if(data)
		return data[length - 1];
	return 0;
}

void String::RemoveTermination(char c)
{
	if(data){
		if(data[length - 1] == c){
			data[length - 1] = 0;
			length --;
		}
	}
}

bool String::RemoveTermination(const char * tail)
{
	if(data){
		int l = strlen(tail);
		if(length < l)
			return false;
		int i;
		for(i = 0; i < l; i ++){
			if(data[length - l + i] != tail[i])
				return false;
		}
		data[length - l] = 0;
		length -= l;
		return true;
	}
	return false;
}

bool String::RemovePrefix(const char * header)
{
	if(data){
		int l = strlen(header);
		int i;
		for(i = 0; i < l; i ++){
			if(data[i] != header[i])
				return false;
		}
		////for(i = 0; i < l; i ++)
		////	data[i] = data[i + l];
		////data[l] = 0;
		////length -= l;
		length -= l;
		for(i = 0; i < length; i ++)
			data[i] = data[i + l];
		data[length] = 0;
		return true;
	}
	return false;
}


int String::GetEscapeSequenceChar(const char * str, int * length)
{
	int ll;
	if(length == 0)
		length = &ll;
	*length = 0;
	if(str[0] != '\\')
		return -1;	///	not an escape sequence
	int octCount = 0;
	int hexCount = 0;
	*length = 2;
	switch(str[1]){
		case 'n': return '\n'; 
		case 't': return '\t'; 
		case 'v': return '\v'; 
		case 'b': return '\b'; 
		case 'r': return '\r'; 
		case 'f': return '\f'; 
		case 'a': return '\a'; 
		case '\\': return '\\'; 
		case '\'': return '\''; 
		case '\"': return '\"';
		case '0': case '1': case '2': case '3': case '4': case '5': case '6': case '7':
			octCount ++;
			break;
		case 'x':
			hexCount ++;
			break;
		default:
			CRASH("Invalid escape code!!!");
			break;
	}
	CRASH(octCount + hexCount != 1);
	int i = 2;
	String escapeStr;
	if(octCount){
		escapeStr += "0";
		escapeStr += str[1];
		while(octCount < 3){
			switch(str[i]){
				case '0': case '1': case '2': case '3': case '4': case '5': case '6': case '7':
					escapeStr += str[i];
					octCount ++;
					i ++;
					continue;
				default:
					break;
			}
			break;
		}
		*length = i;
	}
	else{
		escapeStr += "0x";
		while(1){
			switch(str[i]){
				case '0': case '1': case '2': case '3': case '4': case '5': case '6': case '7': case '8': case '9':
				case 'a': case 'b': case 'c': case 'd': case 'e': case 'f':
				case 'A': case 'B': case 'C': case 'D': case 'E': case 'F':
					escapeStr += str[i];
					hexCount ++;
					i ++;
					continue;
				default:
					break;
			}
			break;
		}
		CRASH(hexCount < 2);
		*length = i;
	}
	int escapeVal;
	sscanf(escapeStr.GetString(), "%i", &escapeVal);
	return escapeVal;
}

void String::PutEscapeSequenceChar(unsigned char charVal)
{
	switch(charVal){
		case '\n': *this += "\\n"; break; 
		case '\t': *this += "\\t"; break; 
		case '\v': *this += "\\v"; break; 
		case '\b': *this += "\\b"; break; 
		case '\r': *this += "\\r"; break; 
		case '\f': *this += "\\f"; break; 
		case '\a': *this += "\\a"; break; 
		case '\'': *this += "\\\'"; break; 
		case '\"': *this += "\\\""; break;
		default:	///	write as \octal
			{
				int oct0 = charVal & 0x7;
				int oct1 = (charVal >> 3) & 0x7;
				int oct2 = (charVal >> 6) & 0x7;
				*this += "\\";
				if(oct2)
					*this += oct2;
				if(oct1 || oct2)
					*this += oct1;
				*this += oct0;
			}
			break;
	}
}

int String::Interpret_c_char(const char * str)
{
	int val = GetEscapeSequenceChar(str);
	if(val >= 0)
		return val;
	else
		return str[0];
}

void String::RemoveEscapeCodesFromStringLiteral(const char * str)
{
	const char * p0 = str, * p1 = p0;
	String str0;
	while(*p1){
		int l;
		int escapeVal = GetEscapeSequenceChar(p1, &l);
		if(escapeVal >= 0){
			str0.Set(p0, p1 - p0);
			*this += str0;
			*this += (const char) escapeVal;
			p1 += l;
			p0 = p1;
		}
		else
			p1 ++;
	}
	str0.Set(p0, p1 - p0);
	*this += str0;
}

///	3/8/09
void String::RestoreEscapeCodesToStringLiteral()
{
	String & s = *this;
	const char * n = s.GetString();
	const char * p = n, * pp = p;
	String s0;
	while(*p){
		char c = 0;
		switch(*p){
		case '\n': c = 'n'; break;
		case '\t': c = 't'; break;
		case '\v': c = 'v'; break;
		case '\b': c = 'b'; break;
		case '\r': c = 'r'; break;
		case '\f': c = 'f'; break;
		case '\a': c = 'a'; break;
		case '\\': c = '\\'; break;
		case '\"': c = '\"'; break;
		}
		if(c){
			if(p - pp){ String ss(pp, p - pp); s0 += ss;}
			s0 += '\\'; s0 += c;
			pp = p + 1;
		}
		p ++;
	}
	if(p - pp){ String ss(pp, p - pp); s0 += ss;}
	s = s0;
}

////void String::RemoveEscapeCodesFromStringLiteral()
////{
////	CRASH("Don't call this!! (RemoveEscapeCodesFromStringLiteral)");
////	String & s = *this;
////	const char * n = s.GetString();
////	const char * p = n, * pp = p;
////	int len = s.GetLength();
////	String s0;
////	while(*p){
////		if(p - n >= len) break;
////		if(*p == '\\'){
////			if(p - pp){ String ss(pp, p - pp); s0 += ss;}
////			p ++;
////			pp = p + 1;
////			switch(*p){
////			case 'n': s0 += '\n'; break;
////			case 't': s0 += '\t'; break;
////			case 'v': s0 += '\v'; break;
////			case 'b': s0 += '\b'; break;
////			case 'r': s0 += '\r'; break;
////			case 'f': s0 += '\f'; break;
////			case 'a': s0 += '\a'; break;
////			case '\\': s0 += '\\'; break;
////			case '\'': s0 += '\''; break;
////			case '\"': s0 += '\"'; break;
////			}
////		}
////		p ++;
////	}
////	if(p - pp){ String ss(pp, p - pp); s0 += ss;}
////	s = s0;
////}
////
////void String::RestoreEscapeCodesToStringLiteral()
////{
////	CRASH("Don't call this!! (RestoreEscapeCodesToStringLiteral)");
////	String & s = *this;
////	const char * n = s.GetString();
////	const char * p = n, * pp = p;
////	String s0;
////	while(*p){
////		char c = 0;
////		switch(*p){
////		case '\n': c = 'n'; break;
////		case '\t': c = 't'; break;
////		case '\v': c = 'v'; break;
////		case '\b': c = 'b'; break;
////		case '\r': c = 'r'; break;
////		case '\f': c = 'f'; break;
////		case '\a': c = 'a'; break;
////		case '\\': c = '\\'; break;
////		case '\"': c = '\"'; break;
////		}
////		if(c){
////			if(p - pp){ String ss(pp, p - pp); s0 += ss;}
////			s0 += '\\'; s0 += c;
////			pp = p + 1;
////		}
////		p ++;
////	}
////	if(p - pp){ String ss(pp, p - pp); s0 += ss;}
////	s = s0;
////}


int String::GetPrintfLength(const char * format, ...)
{
	va_list arglist;
	va_start(arglist, format);
	return GetVPrintfLength(format, arglist);
}

const char * String::ExtractTail(char charBeforeTail)
{
	const char * p0 = GetString();
	const char * p = p0 + GetLength();
	while(p != p0){
		if(*p == charBeforeTail)
			return p + 1;
		p --;
	}
	return 0;
}

void String::RemoveFileExtension()
{
	char * p = data + length;
	while(p != data){
		if(*p == '.'){
			*p = 0;
			length = strlen(data);
			return;
		}
		p --;
	}
}

void String::ConvertToUpperCase()
{
	char * p = data;
	while(*p){
		if(*p >= 'a' && *p <= 'z')
			*p -= 'a' - 'A';
		p ++;
	}
}

bool String::IsInt()
{
	const char * p = data;
	if(*p == '0')
		return *(p + 1) == 0;	///	"0"
	else{
		while(*p){
			int d = *p - '0';
			if(d < 0 || d > 9)
				return false;
			p ++;
		}
		return true;
	}
}

int String::GetHexCharValue(char c)
{
	int d = c - '0';
	if(d >= 0 && d <= 9)
		return d;
	d = c - 'a';
	if(d >= 0 && d <= 5)
		return d + 10;
	d = c - 'A';
	if(d >= 0 && d <= 5)
		return d + 10;
	return -1;
}

bool String::IsHex()
{
	if(length < 3)
		return false;
	const char * p = data;
	if(p[0] != '0' || (p[1] != 'x' && p[1] != 'X'))
		return false;
	p += 2;
	while(*p){
		if(GetHexCharValue(*p) < 0)
			return false;
		p ++;
	}
	return true;
}

int String::GetHexValue()
{
	CRASH(!IsHex());
	int val = 0;
	const char * p = data + 2;
	while(*p){
		int d = GetHexCharValue(*p);
		CRASH(d < 0);
		val = (val << 4) | d;
		p ++;
	}
	return val;
}

#if 1	///	3/1/09 : added by kawachi

int String::ChangeToInt()
{
	char * p = data;
	int i;
	i = atoi(p);
	return i;
}

int String::BitInt(int l)
{
	char p = data[l];
	int i;
	i = atoi(&p);
	return i;
}

void String::Cut(int l)
{
	if(data){
		if(length > l){
			data[l] = 0;
			length = l;
		}
	}
}
#endif
