// LinkedList.h: LinkedList クラスのインターフェイス
//
//////////////////////////////////////////////////////////////////////

#if !defined(__INCLUDE_LINKEDLIST_H__)
#define __INCLUDE_LINKEDLIST_H__

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#if 1
#define CRASH(n)    do{ if (n) { printf ("CRASH!!: %s (hit return..)\n", #n); getchar(); exit(-1);}} while(n)
#else
void __CRASH(const char * s);// for detecting bugs

#define CRASH(n) if(n) __CRASH(#n)

void MyMessageBox(const char * s);
bool MyQuestionBox(const char * s0, const char * s1);
void MyMessageLog(const char * s);
#if 1	///	1/3/16
void MySimCycleLog(long long simCycle);
#elif 1	///	10/22/11 : int is not enough!!!
void MySimCycleLog(double simCycle);
#else
void MySimCycleLog(int simCycle);
#endif
void MyStringLog(const char * s);
#endif

class LINKER;

class BaseLinkedList
{
public:

	class List;
	class Linker
	{
	public:
		List * list;
		Linker * prev, * next;
		void * element;
		Linker(void *, List *);
		virtual ~Linker();
		void Cleanup();
		void Reset(void *, List *);
	};

	class List
	{
	protected:
		int deleteElementsDuringCleanup;
	public:
		int count;
		Linker * head, * tail;
		List(int tempFlag = 0);
		virtual ~List(){}
		bool IsTemporal(){ return deleteElementsDuringCleanup == 0;}
		void MakeTemporal(){ deleteElementsDuringCleanup = 0;}
		void DisableTemporal(){ deleteElementsDuringCleanup = 1;}
		void PushTail(void *);
		void PushHead(void *);
		bool PopElement(LINKER & lk, void * e);
		bool PopElement(void * e);
		void MoveCurrentToElement(LINKER & lk, void *);
		void MoveMarkedToCurrentElements(LINKER & mk, LINKER & lk, List *);
		void InsertListToCurrent(LINKER & lk, List *, bool updateLinkerFlag = false);
		void InsertListToTail(List *);
		void InsertToCurrent(LINKER & lk, void *);
		void ReplaceCurrent(LINKER & lk, void *);
		bool ReplaceIndexed(int, void *);
		int GetIndex(void * e);
		void Copy(List * list);
		void CopyInReverseOrder(List * list);
		int GetCount(void * e);
		void * b_PopHead();
		void * b_PopTail();
		void * b_PopCurrent(LINKER & lk, int);
		void * b_Head(LINKER & lk);
		void * b_Next(LINKER & lk);
		void * b_Next(LINKER & lk, int);
		void * b_Tail(LINKER & lk);
		void * b_Prev(LINKER & lk);
		void * b_Prev(LINKER & lk, int);
		void * b_Current(LINKER & lk);
		void * b_Current(LINKER & lk, int);
		void * b_Get(int);
		void * b_SearchElement(void *);
		void * b_PushNewElementToHead(void * e);
		void * b_PushNewElementToTail(void * e);
		void DeleteFromMarkedToCurrent(LINKER & mk, LINKER & lk);
		void PopFromMarkedToCurrent(LINKER & mk, LINKER & lk);
		int GetIndex(LINKER & lk);
		virtual void DeleteElement(Linker * linker) = 0;
		virtual bool DeleteAll();
		virtual bool FlushAll();
		virtual void operator<<(List &);
		virtual void operator>>(List &);
		void ReverseOrder();
	};
	static bool IsIdentical(List * listA, List * listB);
	static bool IsIdenticalFromHead(List * listA, List * listB);
	static bool IsIdenticalFromTail(List * listA, List * listB);
	static Linker * CreateLinker(void * e, List * l);
	static void StoreDeletedLinker(Linker * linker);
};

/**
LINKER is essentially an iterator for scanning the linked list
*/
#if 1
class LINKER	///	no constructors, no virtual functions
{
public:
	BaseLinkedList::Linker * linker;
};
#else
////class LINKER
////{
////public:
////	BaseLinkedList::Linker * linker;
////	LINKER() : linker(0){}
////	LINKER(LINKER & lk) : linker(lk.linker){}
////	void operator=(LINKER & lk){ linker = lk.linker;}
////};
#endif

////#define ASSERT_FAST_LINKER

class FastLinkedList
{
public:
	class List;
	class Element
	{
	public:
		List * list;
		Element * next, * prev;
		Element(){ Reset();}
		virtual ~Element(){}
		void Reset(){ list = 0; next = 0; prev = 0;}
		void RemoveFromList(){
#ifdef ASSERT_FAST_LINKER
			CRASH(list == 0);
#endif
			list->count --;
			Element * n = next;
			Element * p = prev;
			if(list->head == this){ list->head = n;}
			if(list->tail == this){ list->tail = p;}
			if(n){
				n->prev = p;
				next = 0;
			}
			if(p){
				p->next = n;
				prev = 0;
			}
			list = 0;
		}
		void Cleanup(){ if(list){ RemoveFromList();}}
	};
	class List
	{
	public:
		int count;
		Element * head, * tail;
		List(){ Reset();}
		void Reset(){ count = 0; head = 0; tail = 0;}
		virtual ~List(){ DeleteAll();}
#ifdef ASSERT_FAST_LINKER
#define INSERT_ELEMENT(e)	\
	CRASH(e->list);			\
	e->list = this;			\
	count ++
#else
#define INSERT_ELEMENT(e)	\
	e->list = this;			\
	count ++
#endif

		void RemoveElement(Element * e){
#ifdef ASSERT_FAST_LINKER
			CRASH(e->list != this);
#endif
			e->list = 0;
			count --;
			Element * n = e->next;
			Element * p = e->prev;
			if(head == e){ head = n;}
			if(tail == e){ tail = p;}
			if(n){
				n->prev = p;
				e->next = 0;
			}
			if(p){
				p->next = n;
				e->prev = 0;
			}
		}
		void PushHead(Element * e){
			INSERT_ELEMENT(e);
			if(head == 0){ tail = e;}
			else{
				head->prev = e;
				e->next = head;
			}
			head = e;
		}
		void PushTail(Element * e){
			INSERT_ELEMENT(e);
			if(tail == 0){ head = e;}
			else{
				tail->next = e;
				e->prev = tail;
			}
			tail = e;
		}
		void InsertToCurrent(Element * cur, Element * e){
#ifdef ASSERT_FAST_LINKER
			CRASH(cur == 0);
#endif
			INSERT_ELEMENT(e);
	///	4/14/09
			Element * cp = cur->prev;
			cur->prev = e;
			if((e->prev = cp)){ cp->next = e;}
			e->next = cur;
			if(head == cur){ head = e;}
		}
		Element * PopHead(){
			Element * e;
			if((e = head)){
#ifdef ASSERT_FAST_LINKER
				CRASH(e->list != this || e->prev);
#endif
				e->list = 0;
				count --;
				if(tail == e){ tail = 0;}
				if((head = e->next)){
					head->prev = 0;
					e->next = 0;
				}
				return e;
			}
			else{ return 0;}
		}
		Element * PopTail(){
			Element * e;
			if((e = tail)){
#ifdef ASSERT_FAST_LINKER
				CRASH(e->list != this || e->next);
#endif
				e->list = 0;
				count --;
				if(head == e){ head = 0;}
				if((tail = e->prev)){
					tail->next = 0;
					e->prev = 0;
				}
				return e;
			}
			else{ return 0;}
		}
		Element * SearchElement(Element * e){
			Element * e0;
			for(e0 = head; e0; e0 = e0->next){ if(e0 == e){ return e0;}}
			return 0;
		}
		Element * Get(int index);
		void DeleteAll(){ while(head){ PopHead();}}
		void FlushAll(){ while(head){ PopHead();}}
	};
};

#undef INSERT_ELEMENT
#undef ASSERT_FAST_LINKER

#endif 
