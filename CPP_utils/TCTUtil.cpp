
#include "TCTUtil.h"

#define MEGA_DOUBLE	1000000.0
#define TOO_BIG_DOUBLE	(MEGA_DOUBLE * MEGA_DOUBLE * 10)

#if 0//defined(__LLVM_C2RTL__)
#define C2RTL_IGNORE __attribute__((C2RTL_ignore_function))
#else
#define C2RTL_IGNORE
#endif



void print_format_double(double val, FILE * fp)
{
	char s[20];
	int digits, i;
	if(val > TOO_BIG_DOUBLE){ fprintf(fp, "%.3f (too big to format...)", val);}
	sprintf(s, "%.3f", val);
	digits = strlen(s) - 4;
	for(i = 0; i < digits; i ++){
		//putchar(s[i]);
		fputc(s[i], fp);
		if((digits - i) % 3 == 1 && i < digits - 1){ fputc(',', fp);}
	}
	for(i = 0; i < 4; i ++){ fputc(s[i + digits], fp);}
}

void print_format_int(long val, FILE * fp)
{
	char s[20];
	int digits, i;
	sprintf(s, "%ld", val);
	digits = strlen(s);
	for(i = 0; i < digits; i ++){
		fputc(s[i], fp);
		if((digits - i) % 3 == 1 && i < digits - 1){ fputc(',', fp);}
	}
}

FILE * fp_log = 0;

void print_timer(Timer * tm, long t0, FILE * fp)
{
	double elapsedSeconds = (double) (tm->t - t0) / CLOCKS_PER_SEC;
	fprintf(fp, "timer(%s) : ", tm->name);
	print_format_double(elapsedSeconds, fp);
	fprintf(fp, " sec, ");
	print_format_double((double) tm->cycles / elapsedSeconds, fp);
	fprintf(fp, " cycles/sec, ");
	print_format_int(tm->cycles, fp);
	fprintf(fp, " cycles\n");
}

void get_timer(Timer * tm)
{
	long t0 = tm->t;
	tm->t = clock();
	if(t0){
		print_timer(tm, t0, stdout);
		if(fp_log){ print_timer(tm, t0, fp_log);}
	}
	else	{ printf("timer(%s) initialized\n", tm->name);}
}

int check_code(const char * code, unsigned int * val)
{
	int v = 0;
	if(strlen(code) != 8){ return 0;}
	while(*code){
		int d = *code - '0';
		int h = *code - 'a';
		v <<= 4;
		if(d >= 0 && d <= 9)		{ v += d;}
		else if(h >= 0 && h <= 6)	{ v += h + 10;}
		else						{ return 0;}
		code ++;
	}
	*val = v;
	return 1;
}

void open_log_file(const char * n)
{
	if(fp_log){ fclose(fp_log);}
	fp_log = fopen(n, "w");
}

void close_log_file(){ if(fp_log){ fclose(fp_log);} fp_log = 0;}

//#if defined(USE_NEW_PROFILER)
//Profiler g_prof;
ProfilerGroup g_prof;
/// 9/18/17
int ProfileInfo::maxProfileFuncNameLength = 0;
#define MAX_LINE 1000

void ErrorMsg(const char * msg)
{
	printf("ASSERT FAILED!!! : %s (hit return:) ", msg);
	getchar();
}

#define ASSERT_PROFILER(n)	if(!(n)) ErrorMsg(#n)


#if 1	///	9/27/17 : use Profile classes!!!
void Profiler::SetupProfiler(const char * prog_file_name)
{
#if 1   ////    10/28/20
    profileInfoArray.resize(1000);
    profileInfoStack.resize(1000);
#endif

	FILE * fp = fopen(prog_file_name, "r");
	int i, j;
	char s[MAX_LINE], code[MAX_LINE], hh[5][MAX_LINE];
	/// 9/18/17
	ProfileInfo::maxProfileFuncNameLength = 0;
	ProfileInfo * pi = &profileInfoArray[profileCount++];
	strcpy(pi->name, "_entry");
	pi->entryAddr = 0;
	if (fp == 0) {
		printf("Cannot open %s...\n", prog_file_name);
		getchar();
		exit(0);
	}
	for (i = 0; i < 3; i++) {
		///	i = 0: data memory
		///	i = 1: program memory
		///	i = 2: end-of-data
		int tag = 0, addr = 0, size = 0, count = 0, state = 0;
		//		int printfAddrWidth, printfDataWidth; /// for hex-printf
		int min_addr, max_addr;
		while (fgets(s, MAX_LINE, fp)) {
			unsigned int val;
			int hf, rf;
			hf = sscanf(s, "%s %s %s %s %s", hh[0], hh[1], hh[2], hh[3], hh[4]);
			if (hf == 5 && strcmp(hh[2], "function") == 0 && strcmp(hh[3], ":") == 0) {
				if (strcmp(hh[1], "START") == 0) {
					pi = &profileInfoArray[profileCount++];
					min_addr = addr << 2;
					printf("(%s): min_addr = %08x << 2\n", hh[4], addr);
					hh[4][MAX_PROFILE_FUNC_NAME_LENGTH] = 0;
					strcpy(pi->name, hh[4]);
					pi->entryAddr = min_addr;
				}
				else if (strcmp(hh[1], "END") == 0) {
					max_addr = addr << 2;
					printf("(%s): max_addr = %08x << 2\n", hh[4], addr);
				}
			}
			rf = sscanf(s, "%s", code);
			if (rf == 1) {
				if (strcmp(code, "ZERO") == 0) {
					if (state != 3) { printf("ERROR!! ZERO appeared in state(%d)\n", state); exit(0); }
					rf = sscanf(s + 5, "%s", code);
					if (rf == 1 && check_code(code, &val)) {
#if 0
						printf("ZERO (%d)\n", val);
#endif
						///	1/7/14
						for (j = 0; j < (int)val; j++) {
							addr++;
						}
						count += val;
					}
					else { printf("ERROR after ZERO...\n"); exit(0); }
					if (count >= size) {
						state = 0;
						break;
					}
				}
				else if (check_code(code, &val)) {
					switch (state) {
					case 0:	tag = val;	break;
					case 1:	addr = val;	break;
					case 2:	size = val;	break;
					}
					if (state == 3) {
						addr++;
						count++;
						if (count == size) {
							state = 0;
							break;
						}
					}
					else {
						if (i == 2 && state == 0) {
							if (tag != 0xffffffff) { printf("ERROR while reading end-of-data\n"); exit(0); }
							break;
						}
						else if (state == 2) {
							if (addr & 0x3) { printf("ERROR!!! start address (%08x) must be multiple of 4\n", addr); exit(0); }
							printf("tag = %08x, addr = %08x, size = %08x\n", tag, addr, size);
							addr >>= 2;
						}
						state++;
					}
				}
			}
		}
	}
	fclose(fp);
	ProfilerStart();
}

void Profiler::PushProfileInfoInstance(ProfileInfo * pi)
{
	curProfInfo = &profileInfoStack[sp++];
	curProfInfo->pi = pi;
	curProfInfo->cycles = 0;
	curProfInfo->subCycles = 0;
	if (sp == 1) { pi->calls++; }
}

void Profiler::PopProfileInfoInstance()
{
	ProfileInfoInstance * pii;
	ASSERT_PROFILER(sp > 0);
	pii = &profileInfoStack[--sp];
	pii->pi->cycles += pii->cycles;
	pii->pi->subCycles += pii->subCycles;
	if (sp > 0) {
		curProfInfo = &profileInfoStack[sp - 1];
		curProfInfo->subCycles += pii->cycles + pii->subCycles;
	}
	else {
		curProfInfo = 0;
		pii->pi->cycles--; /// adjusting last executed instruction count
	}
}

C2RTL_IGNORE
void Profiler::ProfilerPush(unsigned int targetAddr)
{
//	if (!profileEnabled) { return; }
	int ub = profileCount, lb = 0;
	ProfileInfo * piTarget = 0;
	while (1) {
		int idx = (ub + lb) >> 1;
		unsigned int piAddr;
		ProfileInfo * pi = &profileInfoArray[idx];
		piAddr = pi->entryAddr;
		if (piAddr == targetAddr) { piTarget = pi; break; }
#if 1	///	9/18/17 : sometimes targetAddr function may not be registered (internal call)... simply ignore this
		if (piAddr < targetAddr) {
			if (lb == idx) {
				extraCalls++;
				return; /// targetAddr is not registered in profileInfoArray
			}
			lb = idx;
		}
#else
		else if (piAddr < targetAddr) { lb = idx; }
#endif
		else { ub = idx; }
		if (ub == lb) { break; }
	}
	ASSERT_PROFILER(piTarget != 0);
	if (piTarget) { piTarget->calls++; }
#ifdef MAX_PROFILE_FUNC_COUNT
	if (piTarget && sp < MAX_PROFILE_FUNC_COUNT) { PushProfileInfoInstance(piTarget); }
#else
	if (piTarget && sp < (int)profileInfoStack.size()) { PushProfileInfoInstance(piTarget); }
#endif
	else { extraCalls++; }
}

C2RTL_IGNORE
void Profiler::ProfilerPop()
{
//	if (!profileEnabled) { return; }
	if (extraCalls) { extraCalls--; }
	else { PopProfileInfoInstance(); }
}

C2RTL_IGNORE
void Profiler::ProfilerUpdate()
{
	//    if (!g_prof.profileEnabled) { return; }
	if (curProfInfo) { curProfInfo->cycles++; }
}

C2RTL_IGNORE
void Profiler::ProfilerSkip()
{
	skippedCycles++;
}

void Profiler::ProfilerStart()
{
	int i;
	ASSERT_PROFILER(profileCount > 0);
//	profileEnabled = 1;
	printf("Profiler:\n");
	ProfileInfo::maxProfileFuncNameLength = 0;
	for (i = 0; i < profileCount; i++) {
		ProfileInfo * pi = &profileInfoArray[i];
		int len = strlen(pi->name);
		if (ProfileInfo::maxProfileFuncNameLength < len) { ProfileInfo::maxProfileFuncNameLength = len; }
	}
	for (i = 0; i < profileCount; i++) {
		ProfileInfo * pi = &profileInfoArray[i];
		printf("%*s [%05x]\n", ProfileInfo::maxProfileFuncNameLength, pi->name, pi->entryAddr);
		pi->cycles = 0;
		pi->subCycles = 0;
		pi->calls = 0;
	}
#if 1
	InitializeProfileInfoInstance();
#else
	sp = 0;
	extraCalls = 0;
	PushProfileInfoInstance(&profileInfoArray[0]);
#endif
}
void Profiler::InitializeProfileInfoInstance() {
	sp = 0;
	extraCalls = 0;
	profileInfoArray[0].calls = 0;
	PushProfileInfoInstance(&profileInfoArray[0]);
}
#else
void SetupProfiler(const char * prog_file_name)
{
    FILE * fp = fopen(prog_file_name, "r");
    int i, j;
    char s[MAX_LINE], code[MAX_LINE], hh[5][MAX_LINE];
	/// 9/18/17
	ProfileInfo::maxProfileFuncNameLength = 0;
	ProfileInfo * pi = &g_prof.profileInfoArray[g_prof.profileCount++];
    strcpy(pi->name, "_entry");
    pi->entryAddr = 0;
    if (fp == 0) {
        printf("Cannot open %s...\n", prog_file_name);
        getchar();
        exit(0);
    }
    for (i = 0; i < 3; i++) {
        ///	i = 0: data memory
        ///	i = 1: program memory
        ///	i = 2: end-of-data
        int tag = 0, addr = 0, size = 0, count = 0, state = 0;
        //		int printfAddrWidth, printfDataWidth; /// for hex-printf
        int min_addr, max_addr;
        while (fgets(s, MAX_LINE, fp)) {
            unsigned int val;
            int hf, rf;
            hf = sscanf(s, "%s %s %s %s %s", hh[0], hh[1], hh[2], hh[3], hh[4]);
            if (hf == 5 && strcmp(hh[2], "function") == 0 && strcmp(hh[3], ":") == 0) {
                if (strcmp(hh[1], "START") == 0) {
                    pi = &g_prof.profileInfoArray[g_prof.profileCount++];
                    min_addr = addr << 2;
                    printf("(%s): min_addr = %08x << 2\n", hh[4], addr);
                    hh[4][MAX_PROFILE_FUNC_NAME_LENGTH] = 0;
                    strcpy(pi->name, hh[4]);
                    pi->entryAddr = min_addr;
                }
                else if (strcmp(hh[1], "END") == 0) {
                    max_addr = addr << 2;
                    printf("(%s): max_addr = %08x << 2\n", hh[4], addr);
                }
            }
            rf = sscanf(s, "%s", code);
            if (rf == 1) {
                if (strcmp(code, "ZERO") == 0) {
                    if (state != 3) { printf("ERROR!! ZERO appeared in state(%d)\n", state); exit(0); }
                    rf = sscanf(s + 5, "%s", code);
                    if (rf == 1 && check_code(code, &val)) {
#if 0
                        printf("ZERO (%d)\n", val);
#endif
                        ///	1/7/14
                        for (j = 0; j < (int)val; j++) {
                            addr++;
                        }
                        count += val;
                    }
                    else { printf("ERROR after ZERO...\n"); exit(0); }
                    if (count >= size) {
                        state = 0;
                        break;
                    }
                }
                else if (check_code(code, &val)) {
                    switch (state) {
                    case 0:	tag = val;	break;
                    case 1:	addr = val;	break;
                    case 2:	size = val;	break;
                    }
                    if (state == 3) {
                        addr++;
                        count++;
                        if (count == size) {
                            state = 0;
                            break;
                        }
                    }
                    else {
                        if (i == 2 && state == 0) {
                            if (tag != 0xffffffff) { printf("ERROR while reading end-of-data\n"); exit(0); }
                            break;
                        }
                        else if (state == 2) {
                            if (addr & 0x3) { printf("ERROR!!! start address (%08x) must be multiple of 4\n", addr); exit(0); }
                            printf("tag = %08x, addr = %08x, size = %08x\n", tag, addr, size);
                            addr >>= 2;
                        }
                        state++;
                    }
                }
            }
        }
    }
    fclose(fp);
    ProfilerStart();
}

void PushProfileInfoInstance(ProfileInfo * pi)
{
    g_prof.curProfInfo = &g_prof.profileInfoStack[g_prof.sp++];
    g_prof.curProfInfo->pi = pi;
    g_prof.curProfInfo->cycles = 0;
    g_prof.curProfInfo->subCycles = 0;
    if (g_prof.sp == 1) { pi->calls++; }
}

void PopProfileInfoInstance()
{
    ProfileInfoInstance * pii;
    ASSERT_PROFILER(g_prof.sp > 0);
    pii = &g_prof.profileInfoStack[--g_prof.sp];
    pii->pi->cycles += pii->cycles;
    pii->pi->subCycles += pii->subCycles;
    if (g_prof.sp > 0) {
        g_prof.curProfInfo = &g_prof.profileInfoStack[g_prof.sp - 1];
        g_prof.curProfInfo->subCycles += pii->cycles + pii->subCycles;
    }
    else {
        g_prof.curProfInfo = 0;
        pii->pi->cycles--; /// adjusting last executed instruction count
    }
}

C2RTL_IGNORE
void ProfilerPush(unsigned int targetAddr)
{
    if (!g_prof.profileEnabled) { return; }
    int ub = g_prof.profileCount, lb = 0;
    ProfileInfo * piTarget = 0;
    while (1) {
        int idx = (ub + lb) >> 1;
        unsigned int piAddr;
        ProfileInfo * pi = &g_prof.profileInfoArray[idx];
        piAddr = pi->entryAddr;
        if (piAddr == targetAddr) { piTarget = pi; break; }
#if 1	///	9/18/17 : sometimes targetAddr function may not be registered (internal call)... simply ignore this
		if (piAddr < targetAddr) {
			if (lb == idx) {
				g_prof.extraCalls++;
				return; /// targetAddr is not registered in profileInfoArray
			}
			lb = idx;
		}
#else
        else if (piAddr < targetAddr) { lb = idx; }
#endif
        else { ub = idx; }
        if (ub == lb) { break; }
    }
    ASSERT_PROFILER(piTarget != 0);
    if (piTarget) { piTarget->calls++; }
#ifdef MAX_PROFILE_FUNC_COUNT
    if (piTarget && g_prof.sp < MAX_PROFILE_FUNC_COUNT) { PushProfileInfoInstance(piTarget); }
#else
    if (piTarget && g_prof.sp < profileInfoStack.size()) { PushProfileInfoInstance(piTarget); }
#endif
    else { g_prof.extraCalls++; }
}

C2RTL_IGNORE
void ProfilerPop()
{
    if (!g_prof.profileEnabled) { return; }
    if (g_prof.extraCalls) { g_prof.extraCalls--; }
    else { PopProfileInfoInstance(); }
}

C2RTL_IGNORE
void ProfilerUpdate()
{
    if (!g_prof.profileEnabled) { return; }
    if (g_prof.curProfInfo) { g_prof.curProfInfo->cycles++; }
}

C2RTL_IGNORE
void ProfilerSkip()
{
    if (!g_prof.profileEnabled) { return; }
    g_prof.skippedCycles++;
}

void ProfilerStart()
{
    int i;
    ASSERT_PROFILER(g_prof.profileCount > 0);
    g_prof.profileEnabled = 1;
    printf("Profiler:\n");
	ProfileInfo::maxProfileFuncNameLength = 0;
	for (i = 0; i < g_prof.profileCount; i++) {
		ProfileInfo * pi = &g_prof.profileInfoArray[i];
		int len = strlen(pi->name);
		if (ProfileInfo::maxProfileFuncNameLength < len) { ProfileInfo::maxProfileFuncNameLength = len; }
	}
    for (i = 0; i < g_prof.profileCount; i++) {
        ProfileInfo * pi = &g_prof.profileInfoArray[i];
#if 1
		printf("%*s [%05x]\n", ProfileInfo::maxProfileFuncNameLength, pi->name, pi->entryAddr);
#else
        printf("%*s [%05x]\n", MAX_PROFILE_FUNC_NAME_LENGTH, pi->name, pi->entryAddr);
#endif
        pi->cycles = 0;
        pi->subCycles = 0;
        pi->calls = 0;
    }
    g_prof.sp = 0;
    g_prof.extraCalls = 0;
    PushProfileInfoInstance(&g_prof.profileInfoArray[0]);
}
#endif

char tmp_print_format[1000];

char * print_format_double_string(double val)
{
    char s[20];
    int digits, i, ii = 0;
    if (val > TOO_BIG_DOUBLE) { sprintf(tmp_print_format, "%.3f (too big to format...)", val); return tmp_print_format; }
    sprintf(s, "%.3f", val);
    digits = strlen(s) - 4;
    for (i = 0; i < digits; i++) {
        tmp_print_format[ii++] = s[i];
        if ((digits - i) % 3 == 1 && i < digits - 1) { tmp_print_format[ii++] = ','; }
    }
    tmp_print_format[ii] = 0;
    return tmp_print_format;
}

char * print_format_int_string(long val)
{
    char s[20];
    int digits, i, ii = 0;
    sprintf(s, "%ld", val);
    digits = strlen(s);
    for (i = 0; i < digits; i++) {
        tmp_print_format[ii++] = s[i];
        if ((digits - i) % 3 == 1 && i < digits - 1) { tmp_print_format[ii++] = ','; }
    }
    tmp_print_format[ii] = 0;
    return tmp_print_format;
}

#if 1
void ProfileInfo::PrintProfileInfo(FILE * fp, double totalCycles)
{
	///	9/18/17
	fprintf(fp, "%*s [%05x]: calls = ", ProfileInfo::maxProfileFuncNameLength, name, entryAddr);
	fprintf(fp, "%9s", print_format_int_string(calls));
	fprintf(fp, ", clks(acc) = ");
	fprintf(fp, "%13s", print_format_int_string(cycles + subCycles));
	fprintf(fp, " [%7.3f%%], clks(local) = ", (cycles + subCycles) * 100.0 / totalCycles);
	fprintf(fp, "%13s", print_format_int_string(cycles));
	fprintf(fp, " [%7.3f%%]", (cycles) * 100.0 / totalCycles);
	fprintf(fp, ", clks/call = %13s\n", calls != 0 ? print_format_int_string(cycles / calls) : 0);
}
#else
void PrintProfileInfo(FILE * fp, ProfileInfo * pi, double totalCycles)
{
	///	9/18/17
	fprintf(fp, "%*s [%05x]: calls = ", ProfileInfo::maxProfileFuncNameLength, pi->name, pi->entryAddr);
	fprintf(fp, "%9s", print_format_int_string(pi->calls));
	fprintf(fp, ", clks(acc) = ");
	fprintf(fp, "%13s", print_format_int_string(pi->cycles + pi->subCycles));
	fprintf(fp, " [%7.3f%%], clks(local) = ", (pi->cycles + pi->subCycles) * 100.0 / totalCycles);
	fprintf(fp, "%13s", print_format_int_string(pi->cycles));
	fprintf(fp, " [%7.3f%%]", (pi->cycles) * 100.0 / totalCycles);
	fprintf(fp, ", clks/call = %13s\n", pi->calls != 0 ? print_format_int_string(pi->cycles / pi->calls) : 0);
}
#endif

bool ProfileInfo::CompareProfile(ProfileInfo *a, ProfileInfo *b) { return a->cycles + a->subCycles < b->cycles + b->subCycles; }

#include "ELFUtil.h"

#if 1
void Profiler::ProfilerPrint()
{
	int i;
	double activeCycles = 0;
	double totalCycles = 0;
	double skippedCycles1 = 0;
//	profileEnabled = 1;
#if 1
	while (sp > 0) {
		ProfilerPop();
	}
#else
	PopProfileInfoInstance();
#endif
	printf("Profiler result(%d):\n", id);
	for (i = 0; i < profileCount; i++) { activeCycles += profileInfoArray[i].cycles; }
	skippedCycles1 = (double)skippedCycles;
	totalCycles = activeCycles + skippedCycles1;
	if (activeCycles == 0.0) { activeCycles = 1.0; } /// avoid divide-by-zero

	fprintf(stdout, "total cycles = %13s", print_format_double_string(totalCycles));
	fprintf(stdout, " (active: %s, ", print_format_double_string(activeCycles));
	fprintf(stdout, "sleep: %s) ", print_format_double_string(skippedCycles1));
	double active_percent = (activeCycles / totalCycles) * 100;
	fprintf(stdout, "(active: %3.2f%%, sleep: %3.2f%%) \n\n", active_percent, 100 - active_percent);

	if (fp_log) {
		fprintf(fp_log, "total cycles = %13s", print_format_double_string(totalCycles));
		fprintf(fp_log, " (active: %s, ", print_format_double_string(activeCycles));
		fprintf(fp_log, "sleep: %s) ", print_format_double_string(skippedCycles1));
		fprintf(fp_log, "(active: %3.2f%%, sleep: %3.2f%%) \n\n", active_percent, 100 - active_percent);
	}
	LinkedList<ProfileInfo> profList(1);
	for (i = 0; i < profileCount; i++) {
		if (profileInfoArray[i].calls) {
			profList.PushTail(&profileInfoArray[i]);
		}
	}
	profList.Sort(ProfileInfo::CompareProfile);
	LINKER lk;
	ProfileInfo *pi;
	for (pi = profList.Head(lk); pi; pi = profList.Next(lk)) {
		pi->PrintProfileInfo(stdout, activeCycles);
		if (fp_log) { pi->PrintProfileInfo(fp_log, activeCycles); }
	}
}
#else
void ProfilerPrint()
{
    int i;
    double activeCycles = 0;
    double totalCycles = 0;
    double skippedCycles1 = 0;
    g_prof.profileEnabled = 1;
#if 1
	while (g_prof.sp > 0) {
		ProfilerPop();
	}
#else
    PopProfileInfoInstance();
#endif
    printf("Profiler result:\n");
    for (i = 0; i < g_prof.profileCount; i++) { activeCycles += g_prof.profileInfoArray[i].cycles; }
    skippedCycles1 = (double)g_prof.skippedCycles;
    totalCycles = activeCycles + skippedCycles1;
    if (activeCycles == 0.0) { activeCycles = 1.0; } /// avoid divide-by-zero

    fprintf(stdout, "total cycles = %13s", print_format_double_string(totalCycles));
    fprintf(stdout, " (active: %s, ", print_format_double_string(activeCycles));
    fprintf(stdout, "sleep: %s) ", print_format_double_string(skippedCycles1));
    double active_percent = (activeCycles/totalCycles)*100;
    fprintf(stdout, "(active: %3.2f%%, sleep: %3.2f%%) \n\n", active_percent, 100 - active_percent);
    
    if (fp_log) { 
        fprintf(fp_log, "total cycles = %13s", print_format_double_string(totalCycles));
        fprintf(fp_log, " (active: %s, ", print_format_double_string(activeCycles));
        fprintf(fp_log, "sleep: %s) ", print_format_double_string(skippedCycles1));
        fprintf(fp_log, "(active: %3.2f%%, sleep: %3.2f%%) \n\n", active_percent, 100 - active_percent);
    }
#if 1
    LinkedList<ProfileInfo> profList(1);
    for (i = 0; i < g_prof.profileCount; i++) {
        if (g_prof.profileInfoArray[i].calls) {
            profList.PushTail(&g_prof.profileInfoArray[i]);
        }
    }
    profList.Sort(CompareProfile);
    LINKER lk;
    ProfileInfo *pi;
    for (pi = profList.Head(lk); pi; pi = profList.Next(lk)) {
        PrintProfileInfo(stdout, pi, activeCycles);
        if (fp_log) { PrintProfileInfo(fp_log, pi, activeCycles); }
    }
#else
    for (i = 0; i < g_prof.profileCount; i++) {
        if (g_prof.profileInfoArray[i].calls) {
            PrintProfileInfo(stdout, &g_prof.profileInfoArray[i], activeCycles);
            if (fp_log) { PrintProfileInfo(fp_log, &g_prof.profileInfoArray[i], activeCycles); }
        }
    }
#endif
}

#endif

void install_program_elf(unsigned *dmem, unsigned *pmem, unsigned dmem_size, unsigned pmem_size,
    unsigned *pc, unsigned *sp, int memDumpFlag, int pc_offset, int skipProfile, char * file_name)
{
#if 0
    FILE *fp = fopen(file_name, "r");
#else
    FILE *fp = fopen(file_name, "rb");
#endif
    if (fp) {
        elfInfo.Parse(fp);
        FILE *fp_out[4] = { 0 };
        int segmentCount = 0, printfAddrWidth, printfDataWidth;
        if (memDumpFlag) { openMemDumpFile(0, fp_out, &segmentCount, &printfAddrWidth, &printfDataWidth); }
	if (dmem) {
		elfInfo.dmemInfo.Set((unsigned char *)dmem, dmem_size, printfAddrWidth, printfDataWidth, segmentCount, fp_out);
	}
        if (memDumpFlag) { openMemDumpFile(1, fp_out, &segmentCount, &printfAddrWidth, &printfDataWidth); }
	if (pmem) {
		elfInfo.pmemInfo.Set((unsigned char *)pmem,pmem_size, printfAddrWidth, printfDataWidth, segmentCount, fp_out);
	}
        bool flag = true;
	if (dmem || pmem) {
		flag = elfInfo.InstallMemory(memDumpFlag);
	}
	if (pc)
		*pc = (unsigned) elfInfo.elfHeader.ehdr.e_entry + pc_offset;
#if 1	///	10/10/17 : need some space below sp
#define STACK_MARGIN	0x100
	if (sp)
		*sp = (dmem_size << 2) - STACK_MARGIN;
#else
        *sp = (dmem_size << 2); /// set sp to the end of dmem
#endif
        fclose(fp);
        CRASH(!flag);
#if 1   /// 7/14/17
        if (memDumpFlag) {// && !skipProfile) {
            g_prof.SetupElfProfiler();
        }
#endif
    }
}

#if 1
void Profiler::SetupElfProfiler()
{
	ProfileInfo * pi;

	auto &symTable = elfInfo.symbolTable;
	auto &sectionTable = elfInfo.sectionTable;
	auto *table = elfInfo.symbolTable.symTable;
	LinkedList<ELF_Parser::Elf_Sym> symbolList(1);
	/// 9/18/17
	ProfileInfo::maxProfileFuncNameLength = 0;
	for (int i = 0, e = symTable.symbolCount; i < e; ++i) {
		auto &sym = table[i];
		auto *shndxVal = ELF_Parser::shnumGroup.GetValue(sym.st_shndx);
		if (shndxVal || sym.st_name_str[0] == 0) { continue; }
		auto *shdr = &sectionTable.shdr[sym.st_shndx];
		if (shdr->field.executable) {
			symbolList.PushTail(&sym);
		}
	}
#ifdef MAX_PROFILE_FUNC_COUNT
	if (symbolList.count > MAX_PROFILE_FUNC_COUNT) {
		printf("ERROR!!! profileCount exceeds limit (%d)...\n", MAX_PROFILE_FUNC_COUNT);
		printf("Please increase the value MAX_PROFILE_FUNC_COUNT in CPP_utils/TCTUtil.h\n");
		CRASH("byebye");
	}
#else
	profileInfoArray.resize(symbolList.count + 1);
	profileInfoStack.resize(symbolList.count + 1);
#endif
	symbolList.Sort(ELF_Parser::CompareSymbol);
	char sname[MAX_PROFILE_FUNC_NAME_LENGTH + 1];
	sname[MAX_PROFILE_FUNC_NAME_LENGTH] = 0;
	LINKER lk;
	ELF_Parser::Elf_Sym *sym;
	sym = symbolList.Get(0);
	if (sym->st_value != 0) {
		pi = &profileInfoArray[profileCount++];
		strcpy(pi->name, "_entry"); /// add _entry at address 0 if no symbol is available (just in case)
		strcpy(pi->sectName, "_boot");
		pi->entryAddr = 0;
	}
	for (sym = symbolList.Head(lk); sym; sym = symbolList.Next(lk)) {
		auto *shdr = &sectionTable.shdr[sym->st_shndx];
		int len = strlen(sym->st_name_str);
		if (len > MAX_PROFILE_FUNC_NAME_LENGTH) { len = MAX_PROFILE_FUNC_NAME_LENGTH; }
		pi = &profileInfoArray[profileCount++];
		pi->entryAddr = (unsigned)sym->st_value;
		strncpy(sname, sym->st_name_str, len);
		sname[len] = 0;
		strcpy(pi->name, sname);
#if 1	///	9/18/17
		len = strlen(shdr->sh_name_str);
		if (len > MAX_SECTION_NAME_LENGTH) { len = MAX_SECTION_NAME_LENGTH; }
		strncpy(pi->sectName, shdr->sh_name_str, len);
		pi->sectName[len] = 0;
#endif
	}
#if 1	///	9/18/17
	ProfileInfo::maxProfileFuncNameLength = 0;
	for (int i = 0; i < profileCount; ++i) {
		pi = &profileInfoArray[i];
		int len = strlen(pi->name);
		if (ProfileInfo::maxProfileFuncNameLength < len) {
			ProfileInfo::maxProfileFuncNameLength = len;
		}
	}
	for (int i = 0; i < profileCount; ++i) {
		pi = &profileInfoArray[i];
		printf("ElfProfile (%*s): addr = %08x : %s\n", ProfileInfo::maxProfileFuncNameLength,
			pi->name, pi->entryAddr, pi->sectName);

	}
#endif
	ProfilerStart();
}
#else
void SetupElfProfiler()
{
    ProfileInfo * pi;

    auto &symTable = elfInfo.symbolTable;
    auto &sectionTable = elfInfo.sectionTable;
    auto *table = elfInfo.symbolTable.symTable;
    LinkedList<ELF_Parser::Elf_Sym> symbolList(1);
	/// 9/18/17
	ProfileInfo::maxProfileFuncNameLength = 0;
    for (int i = 0, e = symTable.symbolCount; i < e; ++i) {
        auto &sym = table[i];
        auto *shndxVal = ELF_Parser::shnumGroup.GetValue(sym.st_shndx);
        if (shndxVal || sym.st_name_str[0] == 0) { continue; }
        auto *shdr = &sectionTable.shdr[sym.st_shndx];
        if (shdr->field.executable) {
            symbolList.PushTail(&sym);
        }
    }
    if (symbolList.count > MAX_PROFILE_FUNC_COUNT) {
        printf("ERROR!!! profileCount exceeds limit (%d)...\n", MAX_PROFILE_FUNC_COUNT);
        printf("Please increase the value MAX_PROFILE_FUNC_COUNT in CPP_utils/TCTUtil.h\n");
        CRASH("byebye");
    }
    symbolList.Sort(ELF_Parser::CompareSymbol);
    char sname[MAX_PROFILE_FUNC_NAME_LENGTH + 1];
    sname[MAX_PROFILE_FUNC_NAME_LENGTH] = 0;
    LINKER lk;
    ELF_Parser::Elf_Sym *sym;
    sym = symbolList.Get(0);
    if (sym->st_value != 0) {
        pi = &g_prof.profileInfoArray[g_prof.profileCount++];
        strcpy(pi->name, "_entry"); /// add _entry at address 0 if no symbol is available (just in case)
        pi->entryAddr = 0;
    }
    for (sym = symbolList.Head(lk); sym; sym = symbolList.Next(lk)) {
        auto *shdr = &sectionTable.shdr[sym->st_shndx];
        int len = strlen(sym->st_name_str);
        if (len > MAX_PROFILE_FUNC_NAME_LENGTH) { len = MAX_PROFILE_FUNC_NAME_LENGTH; }
        pi = &g_prof.profileInfoArray[g_prof.profileCount++];
        pi->entryAddr = (unsigned)sym->st_value;
        strncpy(sname, sym->st_name_str, len);
        sname[len] = 0;
        strcpy(pi->name, sname);
#if 1	///	9/18/17
		len = strlen(shdr->sh_name_str);
		if (len > MAX_SECTION_NAME_LENGTH) { len = MAX_SECTION_NAME_LENGTH; }
		strncpy(pi->sectName, shdr->sh_name_str, len);
		pi->sectName[len] = 0;
#endif
#if 0	///	9/18/17
        printf("ElfProfile (%*s): addr = %08x : %s\n", MAX_PROFILE_FUNC_NAME_LENGTH,
            pi->name, pi->entryAddr, shdr->sh_name_str);
#endif
    }
#if 1	///	9/18/17
	ProfileInfo::maxProfileFuncNameLength = 0;
	for (int i = 0; i < g_prof.profileCount; ++i) {
		pi = &g_prof.profileInfoArray[i];
		int len = strlen(pi->name);
		if (ProfileInfo::maxProfileFuncNameLength < len) {
			ProfileInfo::maxProfileFuncNameLength = len;
		}
	}
	for (int i = 0; i < g_prof.profileCount; ++i) {
		pi = &g_prof.profileInfoArray[i];
		printf("ElfProfile (%*s): addr = %08x : %s\n", ProfileInfo::maxProfileFuncNameLength,
			pi->name, pi->entryAddr, pi->sectName);

	}
#endif
    ProfilerStart();
}
#endif

#define HANDLE_UNIFIED_MEMORY   /// 2/24/17

void install_program_tct(unsigned *dmem, unsigned *pmem, unsigned dmem_size, unsigned pmem_size, int memDumpFlag, int skipProfile, char * file_name)
{
    FILE * fp = fopen(file_name, "r"), *fp_out[4] = { 0 };
    int i, j, k, segmentCount;
    char s[MAX_LINE], code[MAX_LINE];
#if 1   /// 3/23/17 : don't forget to initialize segmentCount (when memDumpFlag == 0) !!!
    segmentCount = 0;
#endif
    if (fp == 0) {
        printf("Cannot open %s...\n", file_name);
        getchar();
        exit(0);
    }
#if defined(HANDLE_UNIFIED_MEMORY)   /// 2/24/17
    bool isUnifiedMemory = (dmem == pmem);
#endif
    for (i = 0; i < 3; i++) {
        ///	i = 0: data memory
        ///	i = 1: program memory
        ///	i = 2: end-of-data
        unsigned *mem = (i == 0) ? dmem : pmem;
        int tag = 0, addr = 0, size = 0, count = 0, state = 0;
        int printfAddrWidth, printfDataWidth; /// for hex-printf
        while (fgets(s, MAX_LINE, fp)) {
            unsigned int val;
            int rf = sscanf(s, "%s", code);
            if (rf == 1) {
                if (strcmp(code, "ZERO") == 0) {
                    if (state != 3) { printf("ERROR!! ZERO appeared in state(%d)\n", state); exit(0); }
                    rf = sscanf(s + 5, "%s", code);
                    if (rf == 1 && check_code(code, &val)) {
                        ///	1/7/14
                        for (j = 0; j < (int)val; j++) {
                            mem[addr] = 0;
                            for (k = 0; k < segmentCount; k++) {
                                if (fp_out[k]) {
                                    fprintf(fp_out[k], "@%0*x %0*x\t ///\t%s",
                                        printfAddrWidth, addr, printfDataWidth, 0, s);
                                }
                            }
                            addr++;
                        }
                        count += val;
                    }
                    else { printf("ERROR after ZERO...\n"); exit(0); }
                    if (count >= size) {
                        state = 0;
                        for (j = 0; j < segmentCount; j++) {
                            if (fp_out[j]) { fclose(fp_out[j]); }
                            fp_out[j] = 0;
                        }
                        break;
                    }
                }
                else if (check_code(code, &val)) {
                    switch (state) {
                    case 0:	tag = val;	break;
#if defined(HANDLE_UNIFIED_MEMORY)   /// 2/24/17
                    case 1:
                        if (i == 0 && isUnifiedMemory) {
                            addr = val + (pmem_size << 2);
                        }
                        else {
                            addr = val;
                        }
                        break;
#else
                    case 1:	addr = val;	break;
#endif
                    case 2:	size = val;	break;
                    }
                    if (state == 3) {
                        mem[addr] = val;
                        for (j = 0; j < segmentCount; j++) {
                            unsigned int v = (printfDataWidth == 8) ?
                                val : (val >> (j << 3)) & ((1 << (printfDataWidth << 2)) - 1);
                            if (fp_out[j]) {
                                fprintf(fp_out[j], "@%0*x %0*x\t ///\t%s", printfAddrWidth, addr, printfDataWidth, v, s);
                            }
                        }
                        addr++;
                        count++;
                        if (count == size) {
                            state = 0;
                            for (j = 0; j < segmentCount; j++) { if (fp_out[j]) { fclose(fp_out[j]); } }
                            break;
                        }
                    }
                    else {
                        if (i == 2 && state == 0) {
                            if (tag != 0xffffffff) { printf("ERROR while reading end-of-data\n"); exit(0); }
                            break;
                        }
                        else if (state == 2) {
                            if (addr & 0x3) { printf("ERROR!!! start address (%08x) must be multiple of 4\n", addr); exit(0); }
                            //printf("i = %d, state = %d, tag = %08x, addr = %08x, size = %08x\n",
                            //    i, state, tag, addr, size);
                            addr >>= 2;
                            if (memDumpFlag) {
                                openMemDumpFile(i, fp_out, &segmentCount, &printfAddrWidth, &printfDataWidth);
                                for (j = 0; j < segmentCount; j++) {
                                    if (fp_out[j]) {
                                        fprintf(fp_out[j], "/////// generated from (%s) ///////\n", file_name);
                                    }
                                }
                            }
                            printf("i = %d, state = %d, tag = %08x, addr = %08x, size = %08x, segmentCount = %d, printfAddrWidth = %d, printfDataWidth = %d\n",
                                i, state, tag, addr, size, segmentCount, printfAddrWidth, printfDataWidth);
                        }
                        state++;
                    }
                }
            }
        }
    }
    fclose(fp);
    if (memDumpFlag) {// && !skipProfile) {
        g_prof.SetupProfiler(file_name);
        /////// 11/13/16 : test elfParser...
        ////fp = fopen(elfFileName, "rb");
        ////if (fp) { elfInfo.Parse(fp); fclose(fp); }
    }
}

