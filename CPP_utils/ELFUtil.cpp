
#include "ELFUtil.h"

ELF_Parser::Info ELF_Parser::info;

const char * ELF_Parser::errorStatusName[ES_COUNT] = {
#define ES_NAME
#include "ElfStatus.h"
#undef ES_NAME
};

ELF_Parser::Value ELF_Parser::ehtypeArray[ET_COUNT] = {
    { "NONE",	0,		"No file type" },
    { "REL",		1,		"Relocatable file" },
    { "EXEC",	2,		"Executable file" },
    { "DYN",		3,		"Shared object file" },
    { "CORE",	4,		"Core file" },
    { "LOOS",	0xfe00,	"Operating system-specific", 1 },
    { "HIOS",	0xfeff,	"Operating system-specific" },
    { "LOPROC",	0xff00,	"Processor-specific", 1 },
    { "HIPROC",	0xffff,	"Processor-specific" },
};
ELF_Parser::Value ELF_Parser::osabiArray[ELFOSABI_COUNT] = {
    { "NONE",	0,	"No extensions or unspecified" },
    { "HPUX",	1,	"Hewlett-Packard HP-UX" },
    { "NETBSD",	2,	"NetBSD" },
    { "LINUX",	3,	"Linux" },
    { "SOLARIS",	6,	"Sun Solaris" },
    { "AIX",		7,	"AIX" },
    { "IRIX",	8,	"IRIX" },
    { "FREEBSD",	9,	"FreeBSD" },
    { "TRU64",	10,	"Compaq TRU64 UNIX" },
    { "MODESTO",	11,	"Novell Modesto" },
    { "OPENBSD",	12,	"Open BSD" },
    { "OPENVMS",	13,	"Open VMS" },
    { "NSK",		14,	"Hewlett-Packard Non-Stop Kernel" },
    { "LOPROC",	64,	"Processor-specific", 1 },
    { "HIPROC",	255,"Processor-specific" },
    //64-255	Architecture-specific value range
};
ELF_Parser::Value ELF_Parser::machineArray[EM_COUNT] = {
    { "NONE",		0,	"No machine" },
    { "M32",			1,	"AT&T WE 32100" },
    { "SPARC",		2,	"SPARC" },
    { "386",			3,	"Intel 80386" },
    { "68K",			4,	"Motorola 68000" },
    { "88K",			5,	"Motorola 88000" },
    //reserved	6	Reserved for future use (was {"486)
    { "860",			7,	"Intel 80860" },
    { "MIPS",		8,	"MIPS I Architecture" },
    { "S370",		9,	"IBM System/370 Processor" },
    { "MIPS_RS3_LE",	10,	"MIPS RS3000 Little-endian" },
    //reserved	11-14	Reserved for future use
    { "PARISC",		15,	"Hewlett-Packard PA-RISC" },
    //reserved	16	Reserved for future use
    { "VPP500",		17,	"Fujitsu VPP500" },
    { "SPARC32PLUS",	18,	"Enhanced instruction set SPARC" },
    { "960",			19,	"Intel 80960" },
    { "PPC",			20,	"PowerPC" },
    { "PPC64",		21,	"64-bit PowerPC" },
    { "S390",		22,	"IBM System/390 Processor" },
    //reserved	23-35	Reserved for future use
    { "V800",		36,	"NEC V800" },
    { "FR20",		37,	"Fujitsu FR20" },
    { "RH32",		38,	"TRW RH-32" },
    { "RCE",			39,	"Motorola RCE" },
    { "ARM",			40,	"Advanced RISC Machines ARM" },
    { "ALPHA",		41,	"Digital Alpha" },
    { "SH",			42,	"Hitachi SH" },
    { "SPARCV9",		43,	"SPARC Version 9" },
    { "TRICORE",		44,	"Siemens TriCore embedded processor" },
    { "ARC",			45,	"Argonaut RISC Core, Argonaut Technologies Inc." },
    { "H8_300",		46,	"Hitachi H8/300" },
    { "H8_300H",		47,	"Hitachi H8/300H" },
    { "H8S",			48,	"Hitachi H8S" },
    { "H8_500",		49,	"Hitachi H8/500" },
    { "IA_64",		50,	"Intel IA-64 processor architecture" },
    { "MIPS_X",		51,	"Stanford MIPS-X" },
    { "COLDFIRE",	52,	"Motorola ColdFire" },
    { "68HC12",		53,	"Motorola M68HC12" },
    { "MMA",			54,	"Fujitsu MMA Multimedia Accelerator" },
    { "PCP",			55,	"Siemens PCP" },
    { "NCPU",		56,	"Sony nCPU embedded RISC processor" },
    { "NDR1",		57,	"Denso NDR1 microprocessor" },
    { "STARCORE",	58,	"Motorola Star*Core processor" },
    { "ME16",		59,	"Toyota ME16 processor" },
    { "ST100",		60,		"STMicroelectronics ST100 processor" },
    { "TINYJ",		61,	"Advanced Logic Corp. TinyJ embedded processor family" },
    { "X86_64",		62,	"AMD x86-64 architecture" },
    { "PDSP",		63,	"Sony DSP Processor" },
    { "PDP10",		64,	"Digital Equipment Corp. PDP-10" },
    { "PDP11",		65,	"Digital Equipment Corp. PDP-11" },
    { "FX66",		66,	"Siemens FX66 microcontroller" },
    { "ST9PLUS",		67,	"STMicroelectronics ST9+ 8/16 bit microcontroller" },
    { "ST7",			68,	"STMicroelectronics ST7 8-bit microcontroller" },
    { "68HC16",		69,	"Motorola MC68HC16 Microcontroller" },
    { "68HC11",		70,	"Motorola MC68HC11 Microcontroller" },
    { "68HC08",		71,	"Motorola MC68HC08 Microcontroller" },
    { "68HC05",		72,	"Motorola MC68HC05 Microcontroller" },
    { "SVX",			73,	"Silicon Graphics SVx" },
    { "ST19",		74,	"STMicroelectronics ST19 8-bit microcontroller" },
    { "VAX",			75,	"Digital VAX" },
    { "CRIS",		76,	"Axis Communications 32-bit embedded processor" },
    { "JAVELIN",		77,	"Infineon Technologies 32-bit embedded processor" },
    { "FIREPATH",	78,	"Element 14 64-bit DSP Processor" },
    { "ZSP",			79,	"LSI Logic 16-bit DSP Processor" },
    { "MMIX",		80,	"Donald Knuth's educational 64-bit processor" },
    { "HUANY",		81,	"Harvard University machine-independent object files" },
    { "PRISM",		82,	"SiTera Prism" },
    { "AVR",			83,	"Atmel AVR 8-bit microcontroller" },
    { "FR30",		84,	"Fujitsu FR30" },
    { "D10V",		85,	"Mitsubishi D10V" },
    { "D30V",		86,	"Mitsubishi D30V" },
    { "V850",		87,	"NEC v850" },
    { "M32R",		88,	"Mitsubishi M32R" },
    { "MN10300",		89,	"Matsushita MN10300" },
    { "MN10200",		90,	"Matsushita MN10200" },
    { "PJ",			91,	"picoJava" },
    { "OPENRISC",	92,	"OpenRISC 32-bit embedded processor" },
    { "ARC_A5",		93,	"ARC Cores Tangent-A5" },
    { "XTENSA",		94,	"Tensilica Xtensa Architecture" },
    { "VIDEOCORE",	95,	"Alphamosaic VideoCore processor" },
    { "TMM_GPP",		96,	"Thompson Multimedia General Purpose Processor" },
    { "NS32K",		97,	"National Semiconductor 32000 series" },
    { "TPC",			98,	"Tenor Network TPC processor" },
    { "SNP1K",		99,	"Trebia SNP 1000 processor" },
    { "ST200",		100,"STMicroelectronics (www.st.com) ST200 microcontroller" },
    { "TCT",			49344, "Titech/Isshiki-Lab TCT Processor" },
    { "LOPROC",		101,"Unknown processor", 1 },
    { "HIPROC",		0xffff,	"Unknown processor" },
};
ELF_Parser::Value ELF_Parser::shtypeArray[SHT_COUNT] = {
    { "NULL",			0 },
    { "PROGBITS",		1 },
    { "SYMTAB",			2 },
    { "STRTAB",			3 },
    { "RELA",			4 },
    { "HASH",			5 },
    { "DYNAMIC",			6 },
    { "NOTE",			7 },
    { "NOBITS",			8 },
    { "REL",				9 },
    { "SHLIB",			10 },
    { "DYNSYM",			11 },
    { "INIT_ARRAY",		14 },
    { "FINI_ARRAY",		15 },
    { "PREINIT_ARRAY",	16 },
    { "GROUP",			17 },
    { "SYMTAB_SHNDX",	18 },
    { "LOOS",	0x60000000, 0, 1 },
    { "HIOS",	0x6fffffff },
    { "LOPROC",	0x70000000, 0, 1 },
    { "HIPROC",	0x7fffffff },
    { "LOUSER",	0x80000000, 0, 1 },
    { "HIUSER",	0xffffffff },
};
ELF_Parser::Value ELF_Parser::shflagArray[SHF_COUNT] = {
    { "W",			0x1,		"WRITE" },
    { "A",			0x2,		"ALLOC" },
    { "X",			0x4,		"EXECINSTR" },
    { "M",			0x10,		"MERGE" },
    { "S",			0x20,		"STRINGS" },
    { "I",			0x40,		"INFO_LINK" },
    { "L",			0x80,		"LINK_ORDER" },
    { "E",			0x100,		"OS_NONCONFORMING" },
    { "G",			0x200,		"GROUP" },
    { "T",			0x400,		"TLS" },
    { "MASKOS",		0x0ff00000, "MASKOS" },
    { "MASKPROC",	0xf0000000,	"MASKPROC" },
};
ELF_Parser::Value ELF_Parser::shnumArray[SHN_COUNT] = {
    { "UNDEF",		0 },
    { "LORESERVE",	0xff00 },
    { "LOPROC",		0xff00, 0, 1 },
    { "HIPROC",		0xff1f },
    { "LOOS",		0xff20, 0, 1 },
    { "HIOS",		0xff3f },
    { "ABS",			0xfff1 },
    { "COMMON",		0xfff2 },
    { "XINDEX",		0xffff },
    { "HIRESERVE",	0xffff },
};
ELF_Parser::Value ELF_Parser::stbindArray[STB_COUNT] = {
    { "LOCAL",	0 },
    { "GLOBAL",	1 },
    { "WEAK",	2 },
    { "LOOS",	10, 0, 1 },
    { "HIOS",	12 },
    { "LOPROC",	13, 0, 1 },
    { "HIPROC",	15 },
};
ELF_Parser::Value ELF_Parser::sttypeArray[STT_COUNT] = {
    { "NOTYPE",	0 },
    { "OBJECT",	1 },
    { "FUNC",	2 },
    { "SECTION",	3 },
    { "FILE",	4 },
    { "COMMON",	5 },
    { "TLS",		6 },
    { "LOOS",	10, 0, 1 },
    { "HIOS",	12 },
    { "LOPROC",	13, 0, 1 },
    { "HIPROC",	15 },
};
ELF_Parser::Value ELF_Parser::stvisArray[STV_COUNT] = {
    { "DEFAULT",		0 },
    { "INTERNAL",	1 },
    { "HIDDEN",		2 },
    { "PROTECTED",	3 },
};
ELF_Parser::Value ELF_Parser::dtagArray[DT_COUNT] = {
    { "NULL",	0,	"ignored" },//	mandatory	mandatory
    { "NEEDED",	1,	"val" },//	optional	optional
    { "PLTRELSZ",	2,	"val" },//	optional	optional
    { "PLTGOT",	3,	"ptr" },//	optional	optional
    { "HASH",	4,	"ptr" },//	mandatory	mandatory
    { "STRTAB",	5,	"ptr" },//	mandatory	mandatory
    { "SYMTAB",	6,	"ptr" },//	mandatory	mandatory
    { "RELA",	7,	"ptr" },//	mandatory	optional
    { "RELASZ",	8,	"val" },//	mandatory	optional
    { "RELAENT",	9,	"val" },//	mandatory	optional
    { "STRSZ",	10,	"val" },//	mandatory	mandatory
    { "SYMENT",	11,	"val" },//	mandatory	mandatory
    { "INIT",	12,	"ptr" },//	optional	optional
    { "FINI",	13,	"ptr" },//	optional	optional
    { "SONAME",	14,	"val" },//	ignored	optional
    { "RPATH*",	15,	"val" },//	optional	ignored
    { "SYMBOLIC*",	16,	"ignored" },//	ignored	optional
    { "REL",	17,	"ptr" },//	mandatory	optional
    { "RELSZ",	18,	"val" },//	mandatory	optional
    { "RELENT",	19,	"val" },//	mandatory	optional
    { "PLTREL",	20,	"val" },//	optional	optional
    { "DEBUG",	21,	"ptr" },//	optional	ignored
    { "TEXTREL*",	22,	"ignored" },	//optional	optional
    { "JMPREL",	23,	"ptr" },//	optional	optional
    { "BIND_NOW*",	24,	"ignored" },//	optional	optional
    { "INIT_ARRAY",	25,	"ptr" },//	optional	optional
    { "FINI_ARRAY",	26,	"ptr" },//	optional	optional
    { "INIT_ARRAYSZ",	27,	"val" },//	optional	optional
    { "FINI_ARRAYSZ",	28,	"val" },//	optional	optional
    { "RUNPATH",	29,	"val" },//	optional	optional
    { "FLAGS",	30,	"val" },//	optional	optional
    { "ENCODING",	31,	"unspecified" },//	unspecified	unspecified
    { "PREINIT_ARRAY",	32,	"ptr" },//optional	ignored
    { "PREINIT_ARRAYSZ",	33,	"val" },//	optional	ignored
    { "LOOS",	0x6000000D,	"unspecified", 1 },//	unspecified	unspecified
    { "HIOS",	0x6fffffff,	"unspecified" },	//unspecified	unspecified
    { "LOPROC",	0x70000000,	"unspecified", 1 },//	unspecified	unspecified
    { "HIPROC",	0x7fffffff,	"unspecified" },//	unspecified	unspecified
};
ELF_Parser::Value ELF_Parser::ptypeArray[PT_COUNT] = {
    { "NULL",	0 },
    { "LOAD",	1 },
    { "DYNAMIC",	2 },
    { "INTERP",	3 },
    { "NOTE",	4 },
    { "SHLIB",	5 },
    { "PHDR",	6 },
    { "TLS",		7 },
    { "LOOS",	0x60000000, 0, 1 },
    { "HIOS",	0x6fffffff },
    { "LOPROC",	0x70000000, 0, 1 },
    { "HIPROC",	0x7fffffff },
};
ELF_Parser::Value ELF_Parser::pflagArray[PF_COUNT] = {
    { "X",			0x1,		"Execute" },
    { "W",			0x2,		"Write" },
    { "R",			0x4,		"Read" },
    { "MASKOS",		0x0ff00000,	"Unspecified" },
    { "MASKPROC",	0xf0000000,	"Unspecified" },
};

ELF_Parser::ValueGroup ELF_Parser::ehtypeGroup(ET_COUNT, ehtypeArray);
ELF_Parser::ValueGroup ELF_Parser::osabiGroup(ELFOSABI_COUNT, osabiArray);
ELF_Parser::ValueGroup ELF_Parser::machineGroup(EM_COUNT, machineArray);
ELF_Parser::ValueGroup ELF_Parser::shtypeGroup(SHT_COUNT, shtypeArray);
ELF_Parser::ValueGroup ELF_Parser::shflagGroup(SHF_COUNT, shflagArray);
ELF_Parser::ValueGroup ELF_Parser::shnumGroup(SHN_COUNT, shnumArray);
ELF_Parser::ValueGroup ELF_Parser::stbindGroup(STB_COUNT, stbindArray);
ELF_Parser::ValueGroup ELF_Parser::sttypeGroup(STT_COUNT, sttypeArray);
ELF_Parser::ValueGroup ELF_Parser::stvisGroup(STV_COUNT, stvisArray);
ELF_Parser::ValueGroup ELF_Parser::dtagGroup(DT_COUNT, dtagArray);
ELF_Parser::ValueGroup ELF_Parser::ptypeGroup(PT_COUNT, ptypeArray);
ELF_Parser::ValueGroup ELF_Parser::pflagGroup(PF_COUNT, pflagArray);

void ELF_Parser::Ehdr::PrintInfo(String & s)
{
//    printf("Ehdr::PrintInfo\n");
    s.Printf("%12s = %s\n", "File-Name", info.projectDescriptor.GetString());
//    printf("String : %s\n", s.GetString());
    PrintMagicNumber(s);
    PrintClass(s);
    PrintByteOrder(s);
    PrintFileVersion(s);
    osabiGroup.GetValue(e_ident[EI_OSABI])->PrintInfo(s, "OSABI");
    PrintABIVersion(s);
    ehtypeGroup.GetValue(ehdr.e_type)->PrintInfo(s, "TYPE");
    machineGroup.GetValue(ehdr.e_machine)->PrintInfo(s, "MACHINE");

    PRINT_ELF_PARAM(s, e_version);
    PRINT_ELF_PARAM_LONG(s, e_entry);
    PRINT_ELF_PARAM_LONG(s, e_phoff);
    PRINT_ELF_PARAM_LONG(s, e_shoff);
    PRINT_ELF_PARAM(s, e_flags);
    PRINT_ELF_PARAM(s, e_ehsize);
    PRINT_ELF_PARAM(s, e_phentsize);
    PRINT_ELF_PARAM(s, e_phnum);
    PRINT_ELF_PARAM(s, e_shentsize);
    PRINT_ELF_PARAM(s, e_shnum);
    PRINT_ELF_PARAM(s, e_shstrndx);
}

#define MAKE_SILENT

int ELF_Parser::Elf_Shdr::PrintHeader(String & s, int maxStringLength)
{
    s.Printf("Section Headers:\n");
    int len = s.GetLength();
    s.Printf("|No.|%*s|%*s|Flag| Address| Offset |  Size  |Link|Info|Align|ESize|\n",
        maxStringLength, "Name   ", shtypeGroup.maxNameLength, "Type   ");
    len = s.GetLength() - len;
    int ll = len;
    while (ll > 0) { s += "="; ll--; }
    s += "\n";
    return len;
}

void ELF_Parser::Elf_Shdr::PrintInfo(String & s, int id, int maxStringLength)
{
    String f;
    shflagGroup.GetValueMask((int)sh_flags, f);
    s.Printf("|%3d|%-*s|%-*s|%-4s|%08llx|%08llx|%08llx|%4d|%4d|%5lld|%5lld|\n",
        id, maxStringLength, sh_name_str, shtypeGroup.maxNameLength, shtypeGroup.GetValue(sh_type)->name, f.GetString(),
        sh_addr, sh_offset, sh_size, sh_link, sh_info, sh_addralign, sh_entsize);
}

int ELF_Parser::Elf_Sym::PrintHeader(String & s, int maxStringLength, Elf_Shdr * symbolSection)
{
    s.Printf("Symbol Table (%s):\n", symbolSection->sh_name_str);
    int len = s.GetLength();
    s.Printf("|No.|%*s| Address|  Size  |%*s|%*s|%*s|Section...\n",
        maxStringLength, "Name   ",
        stbindGroup.maxNameLength, "Bind ",
        sttypeGroup.maxNameLength, "Type ",
        stvisGroup.maxNameLength, "Vis  ");
    len = s.GetLength() - len;
    int ll = len;
    while (ll > 0) { s += "="; ll--; }
    s += "\n";
    return len;
}

void ELF_Parser::Elf_Sym::PrintInfo(String & s, int id, int maxStringLength, ShdrTable * sectionTable)
{
    Value * shndxVal = shnumGroup.GetValue(st_shndx);
    Elf_Shdr * shdr = (shndxVal) ? 0 : &sectionTable->shdr[st_shndx];
    CRASH(shndxVal == 0 && shdr == 0);
    s.Printf("|%3d|%-*s|%08llx|%08llx|%-*s|%-*s|%-*s|(%s)\n",
        id, maxStringLength, st_name_str, st_value, st_size,
        stbindGroup.maxNameLength, stbindGroup.GetValue(st_bind)->name,
        sttypeGroup.maxNameLength, sttypeGroup.GetValue(st_type)->name,	///	12/16/13
        stvisGroup.maxNameLength, stvisGroup.GetValue(st_vis)->name,
        (shndxVal) ? shndxVal->name : shdr->sh_name_str);
}

int ELF_Parser::Elf_Rel::PrintHeader(String & s, Elf_Shdr * relocSection)
{
    s.Printf("Relocation Table (%s):\n", relocSection->sh_name_str);
    const char * reloc_type_name = shtypeGroup.GetValue(relocSection->sh_type)->name;
    int isRel = (strcmp(reloc_type_name, "REL") == 0);
    int len = s.GetLength();
    if (isRel) { s.Printf("|No.| Offset |  Info  |Type|SymID| SymVal | SymName... \n"); }
    else { s.Printf("|No.| Offset |  Info  |Type|SymID| addend | SymVal | SymName... \n"); }
    len = s.GetLength() - len;
    int ll = len;
    while (ll > 0) { s += "="; ll--; }
    s += "\n";
    return len;
}
void ELF_Parser::Elf_Rel::PrintInfo(String & s, int id, ELF_Parser::SymTable * symTable)
{
    if (r_sym == 0) return;
    CRASH(r_sym < 0 || r_sym >= (unsigned)symTable->symbolCount);
    Elf_Sym * sym = &symTable->symTable[r_sym];
    if (addend_enabled) {
        s.Printf("|%3d|%08llx|%08llx|%04x|%5d|%08llx|%08llx|(%s)\n", id, r_offset, r_info, r_type, r_sym, r_addend,
            sym->st_value, sym->st_name_str);
    }
    else {
        s.Printf("|%3d|%08llx|%08llx|%04x|%5d|%08llx|(%s)\n", id, r_offset, r_info, r_type, r_sym,
            sym->st_value, sym->st_name_str);
    }
}

int ELF_Parser::Elf_Dyn::PrintHeader(String & s, Elf_Shdr * dynSection)
{
    s.Printf("Dynamic Table (%s):\n", dynSection->sh_name_str);
    const char * dyn_type_name = shtypeGroup.GetValue(dynSection->sh_type)->name;
    int len = s.GetLength();
    s.Printf("|No.|%*s|Name/Value|\n", dtagGroup.maxNameLength, "Tag   ");
    len = s.GetLength() - len;
    int ll = len;
    while (ll > 0) { s += "="; ll--; }
    s += "\n";
    return len;
}
void ELF_Parser::Elf_Dyn::PrintInfo(String & s, int id)
{
    Value * dtag = dtagGroup.GetValue((int)d_tag);
    s.Printf("|%3d|%-*s|", id, dtagGroup.maxNameLength, dtag->name);
    if (dtag->comment[0] == 'v') { s.Printf("[V] %08x", d_un.d_val); }
    else if (dtag->comment[0] == 'p') { s.Printf("[P] %08x", d_un.d_ptr); }
    else { s.Printf("[IGNORE]"); }
    if (name) { s.Printf(" (%s)", name); }
    s += "\n";
}

#define ELF_ERROR(n)	{ status.errorFlag = ES_##n; return false;}

bool ELF_Parser::Ehdr::Parse(FILE * fp)
{
//    printf("start ELF_Parser::Ehdr::Parse(%08llx)...\n", (long long)fp);
    int frval;
//    printf("rewind??\n");
//    rewind(fp);
//    printf("rewind ok...\n");
//    long ft = ftell(fp);
//    printf("ftell = %08lx\n", ft);
//    unsigned tmp[20];
//    int frval;
//    frval = fread(tmp, 4, 2, fp);
//    printf("tmp[0] = %08x, tmp[1] = %08x, tmp[2] = %08x, frval = %d\n", tmp[0], tmp[1], tmp[2], frval);
//    ft = ftell(fp);
//    printf("ftell = %08lx\n", ft);
//    rewind(fp);
//    printf("rewind ok...\n");
//    ft = ftell(fp);
//    printf("ftell = %08lx\n", ft);
//    printf("EI_NIDENT = %d\n", EI_NIDENT);
////    frval = fread(tmp, 4, 2, fp);
//    frval = fread(tmp, 1, EI_NIDENT, fp);
//    printf("frval = %d\n", frval);
//    printf("tmp = {");
//    for (unsigned i = 0; i < EI_NIDENT / 4; ++i) { if (i) { printf(","); } printf("(%08x)", tmp[i]); }
//    printf("}\n");
//    rewind(fp);
    if ((frval = fread(e_ident, 1, EI_NIDENT, fp)) != EI_NIDENT)								ELF_ERROR(IDENT)
    //    printf("tmp = {");
    //for (unsigned i = 0; i < EI_NIDENT; ++i) { if (i) { printf(","); } printf("(%08x)", e_ident[i]); }
    //printf("}\n");

    /// 1. check magic number : 0x7f 'E' 'L' 'F'
    if (e_ident[0] != 0x7f || e_ident[1] != 'E' || e_ident[2] != 'L' || e_ident[3] != 'F')		ELF_ERROR(MAGIC_NUM)
    //printf("MAGIC_NUM ok\n");
    if (IsElf32()) {
        Elf32_Ehdr h32;
        if ((frval = fread(&h32, 1, sizeof(Elf32_Ehdr), fp)) != sizeof(Elf32_Ehdr))			    ELF_ERROR(ELF32)
        ehdr.Copy(&h32);
    }
    else if (IsElf64()) {
        Elf64_Ehdr h64;
        if ((frval = fread(&h64, 1, sizeof(Elf64_Ehdr), fp)) != sizeof(Elf64_Ehdr))			    ELF_ERROR(ELF64)
        ehdr.Copy(&h64);
    }
    else																					    ELF_ERROR(CLASS)
    //printf("ELF_CLASS ok\n");
    if (e_ident[EI_DATA] <= ELFDATANONE || e_ident[EI_DATA] > ELFDATA2MSB)					    ELF_ERROR(DATA)
    if (e_ident[EI_VERSION] != EV_CURRENT)													    ELF_ERROR(VERSION)
    if (osabiGroup.GetValue(e_ident[EI_OSABI]) == 0)										    ELF_ERROR(OSABI)

    if (ehtypeGroup.GetValue(ehdr.e_type) == 0)												    ELF_ERROR(EHTYPE)
    if (machineGroup.GetValue(ehdr.e_machine) == 0)												ELF_ERROR(MACHINE)
    if (ehdr.e_ehsize != ((IsElf32()) ? sizeof(Elf32_Ehdr) : sizeof(Elf64_Ehdr)) + EI_NIDENT)	ELF_ERROR(EHSIZE)
    if (ehdr.e_shentsize != ((IsElf32()) ? sizeof(Elf32_Shdr) : sizeof(Elf64_Shdr)))		    ELF_ERROR(SHENTSIZE)
    if (ehdr.e_phentsize != ((IsElf32()) ? sizeof(Elf32_Phdr) : sizeof(Elf64_Phdr)))		    ELF_ERROR(PHENTSIZE)
    //printf("e_phentsize ok\n");

    String msg;
    PrintInfo(msg);
    //printf("PrintInfo(msg) ok\n");
    MESSAGE(msg);
    fprintf(fout, "%s", msg.GetString());
    return true;
}

#define ELF_ERROR_POS(n, i, data)	{ status.errorFlag = ES_##n; status.pos = i;	status.val = data;	return false;}


bool ELF_Parser::ShdrTable::Parse(ELF_Parser::Info * elfInfo, FILE * fp)
{
    Ehdr * ehdr = &elfInfo->elfHeader;
    CRASH(shdr != 0 || symbolSection != 0 ||
        dynSymSection != 0 || dynSection != 0 || relocSectionList.count != 0);
    CRASH(ehdr->ehdr.e_shnum == 0);
    sectionCount = ehdr->ehdr.e_shnum;
    shdr = new Elf_Shdr[sectionCount];
    int i;
    rewind(fp);
    fseek(fp, (long)ehdr->ehdr.e_shoff, SEEK_SET);
    int frval;
    Elf32_Shdr s32;
    Elf64_Shdr s64;
    int allocVec = shflagGroup.GetValueVector("A");
    for (i = 0; i < sectionCount; i++) {
        if (ehdr->IsElf32()) {
            if ((frval = fread(&s32, 1, sizeof(Elf32_Shdr), fp)) != sizeof(Elf32_Shdr))		ELF_ERROR(SH32)
            shdr[i].Copy(&s32);
        }
        else {
            if ((frval = fread(&s64, 1, sizeof(Elf64_Shdr), fp)) != sizeof(Elf64_Shdr))		ELF_ERROR(SH64)
            shdr[i].Copy(&s64);
        }
        if ((shtypeGroup.GetValue(shdr[i].sh_type)) == 0)									ELF_ERROR_POS(SHTYPE, i, shdr[i].sh_type)
        if (shdr[i].sh_flags & allocVec) { shdr[i].field.sh_alloc = 1; }
    }
    if (ehdr->ehdr.e_shstrndx >= sectionCount)												ELF_ERROR(STRTAB_0)
    Elf64_Shdr * shStrTabSection = &shdr[ehdr->ehdr.e_shstrndx];
    char * sectionStringTable = sectionStr.AllocateMemory((int)shStrTabSection->sh_size);
    rewind(fp);
    fseek(fp, (long)shStrTabSection->sh_offset, SEEK_SET);
    if (fread(sectionStringTable, 1, (int)shStrTabSection->sh_size, fp) != shStrTabSection->sh_size)	ELF_ERROR(STRTAB_1)
    maxSectionStringLength = 0;
    Elf_Shdr * symStrTabSection = 0, *dynSymStrTabSection = 0;
    for (i = 0; i < sectionCount; i++) {
        const char * sectionName = sectionStringTable + shdr[i].sh_name;
        shdr[i].sh_name_str = sectionName;
        int len = strlen(sectionName);
        if (maxSectionStringLength < len) { maxSectionStringLength = len; }
        const char * sh_type_name = shtypeGroup.GetValue(shdr[i].sh_type)->name;
        if (strcmp(sectionName, ".symtab") == 0 && strcmp(sh_type_name, "SYMTAB") == 0) { symbolSection = &shdr[i]; }
        else if (strcmp(sectionName, ".strtab") == 0 && strcmp(sh_type_name, "STRTAB") == 0) { symStrTabSection = &shdr[i]; }
        else if (strcmp(sectionName, ".dynsym") == 0 && strcmp(sh_type_name, "DYNSYM") == 0) { dynSymSection = &shdr[i]; }
        else if (strcmp(sectionName, ".dynamic") == 0 && strcmp(sh_type_name, "DYNAMIC") == 0) { dynSection = &shdr[i]; }
        else if (strcmp(sectionName, ".dynstr") == 0 && strcmp(sh_type_name, "STRTAB") == 0) { dynSymStrTabSection = &shdr[i]; }
        else if (strcmp(sh_type_name, "REL") == 0 || strcmp(sh_type_name, "RELA") == 0) { relocSectionList.PushTail(&shdr[i]); }
    }
    CRASH(symbolSection && symStrTabSection == 0);
    if (symStrTabSection) {
        char * symbolStringTable = symbolStr.AllocateMemory((int)symStrTabSection->sh_size);
        rewind(fp);
        fseek(fp, (long)symStrTabSection->sh_offset, SEEK_SET);
        if (fread(symbolStringTable, 1, (int)symStrTabSection->sh_size, fp) != symStrTabSection->sh_size)		ELF_ERROR(STRTAB_2)
    }
    CRASH(dynSymSection && dynSymStrTabSection == 0);
    if (dynSymStrTabSection) {
        char * dynSymStringTable = dynSymStr.AllocateMemory((int)dynSymStrTabSection->sh_size);
        rewind(fp);
        fseek(fp, (long)dynSymStrTabSection->sh_offset, SEEK_SET);
        if (fread(dynSymStringTable, 1, (int)dynSymStrTabSection->sh_size, fp) != dynSymStrTabSection->sh_size)	ELF_ERROR(STRTAB_2)
    }
    String msg;
    int len = Elf_Shdr::PrintHeader(msg, maxSectionStringLength);
    MESSAGE(msg);
    fprintf(fout, "%s", msg.GetString());
    for (i = 0; i < sectionCount; i++) {
        msg = "";
        shdr[i].PrintInfo(msg, i, maxSectionStringLength);
        MESSAGE(msg);
        fprintf(fout, "%s", msg.GetString());
    }
    msg = "";
    PrintTail(msg, len);
    MESSAGE(msg);
    fprintf(fout, "%s", msg.GetString());
    return true;
}

bool ELF_Parser::SymTable::Parse(ELF_Parser::Info * elfInfo, FILE * fp, Elf_Shdr * symSect, const char * stringTable, bool showAll)
{
    Ehdr * ehdr = &elfInfo->elfHeader;
    symbolSection = symSect;
    CRASH(symTable != 0 || symbolSection == 0 || stringTable == 0);
    int symSize = (ehdr->IsElf32()) ? sizeof(Elf32_Sym) : sizeof(Elf64_Sym);
    if (symbolSection->sh_entsize != symSize || symbolSection->sh_size % symSize != 0)		ELF_ERROR(SYM_0)
        symbolCount = (int)symbolSection->sh_size / symSize;
    symTable = new Elf_Sym[symbolCount];

    int i;
    rewind(fp);
    fseek(fp, (long)symbolSection->sh_offset, SEEK_SET);
    int frval;
    Elf32_Sym s32;
    Elf64_Sym s64;
    maxSymbolStringLength = 0;
    for (i = 0; i < symbolCount; i++) {
        if (ehdr->IsElf32()) {
            if ((frval = fread(&s32, 1, sizeof(Elf32_Sym), fp)) != sizeof(Elf32_Sym))		ELF_ERROR_POS(SYM_1, i, 0)
            symTable[i].Copy(&s32);
        }
        else {
            if ((frval = fread(&s64, 1, sizeof(Elf64_Sym), fp)) != sizeof(Elf64_Sym))		ELF_ERROR_POS(SYM_2, i, 0)
            symTable[i].Copy(&s64);
        }
        if (stbindGroup.GetValue(symTable[i].st_bind) == 0)									ELF_ERROR_POS(SYM_3, i, symTable[i].st_bind)
        if (sttypeGroup.GetValue(symTable[i].st_type) == 0)									ELF_ERROR_POS(SYM_4, i, symTable[i].st_name)
        const char * sym_name = stringTable + symTable[i].st_name;
        symTable[i].st_name_str = sym_name;
        int len = strlen(sym_name);
        if (maxSymbolStringLength < len) { maxSymbolStringLength = len; }
    }
    String msg;
    int len = Elf_Sym::PrintHeader(msg, maxSymbolStringLength, symbolSection);
#if !defined(MAKE_SILENT)
    MESSAGE(msg);
#endif
    fprintf(fout, "%s", msg.GetString());
    Value * typeFunc = sttypeGroup.GetValue("FUNC");
    Value * typeSect = sttypeGroup.GetValue("SECTION");
    Value * typeObj = sttypeGroup.GetValue("OBJECT");
    for (i = 0; i < symbolCount; i++) {
        msg = "";
        Elf_Sym * sym = &symTable[i];	///	4/1/14
        Value * val = sttypeGroup.GetValue(sym->st_type);
        sym->PrintInfo(msg, i, maxSymbolStringLength, &elfInfo->sectionTable);
#if !defined(MAKE_SILENT)
        if (showAll || (val == typeFunc || val == typeSect || val == typeObj)) { MESSAGE(msg); }
#endif
        fprintf(fout, "%s", msg.GetString());
    }
    msg = "";
    PrintTail(msg, len);
#if !defined(MAKE_SILENT)
    MESSAGE(msg);
#endif
    fprintf(fout, "%s", msg.GetString());
    return true;
}

bool ELF_Parser::RelocTable::Parse(ELF_Parser::Info * elfInfo, FILE * fp, Elf_Shdr * relSect)
{
    Ehdr * ehdr = &elfInfo->elfHeader;
    relocSection = relSect;
    symTable = &elfInfo->dynSymTable;
    CRASH(relocTable != 0 || relocSection == 0 || symTable == 0);
    const char * reloc_type_name = shtypeGroup.GetValue(relocSection->sh_type)->name;
    int isRel = (strcmp(reloc_type_name, "REL") == 0);
    int isRela = (strcmp(reloc_type_name, "RELA") == 0);
    CRASH(isRel + isRela != 1);
    int relSize = (ehdr->IsElf32()) ? ((isRel) ? sizeof(Elf32_Rel) : sizeof(Elf32_Rela)) : ((isRel) ? sizeof(Elf64_Rel) : sizeof(Elf64_Rela));

    if (relocSection->sh_entsize != relSize || relocSection->sh_size % relSize != 0)				ELF_ERROR(REL_0)
        relocCount = (int)relocSection->sh_size / relSize;
    relocTable = new Elf_Rel[relocCount];

    int i;
    rewind(fp);
    fseek(fp, (long)relocSection->sh_offset, SEEK_SET);
    int frval;
    Elf32_Rel r32;
    Elf64_Rel r64;
    Elf32_Rela r32a;
    Elf64_Rela r64a;
    for (i = 0; i < relocCount; i++) {
        if (ehdr->IsElf32()) {
            if (isRel) {
                if ((frval = fread(&r32, 1, sizeof(Elf32_Rel), fp)) != sizeof(Elf32_Rel))		ELF_ERROR_POS(REL_1, i, 0)
                    relocTable[i].Copy(&r32);
            }
            else {
                if ((frval = fread(&r32a, 1, sizeof(Elf32_Rela), fp)) != sizeof(Elf32_Rela))		ELF_ERROR_POS(REL_2, i, 0)
                    relocTable[i].Copy(&r32a);
            }
        }
        else {
            if (isRel) {
                if ((frval = fread(&r64, 1, sizeof(Elf64_Rel), fp)) != sizeof(Elf64_Rel))		ELF_ERROR_POS(REL_3, i, 0)
                    relocTable[i].Copy(&r64);
            }
            else {
                if ((frval = fread(&r64a, 1, sizeof(Elf64_Rela), fp)) != sizeof(Elf64_Rela))		ELF_ERROR_POS(REL_4, i, 0)
                    relocTable[i].Copy(&r64a);
            }
        }
    }
    String msg;
    int len = Elf_Rel::PrintHeader(msg, relocSection);
#if !defined(MAKE_SILENT)
    MESSAGE(msg);
#endif
    fprintf(fout, "%s", msg.GetString());
    for (i = 0; i < relocCount; i++) {
        msg = "";
        relocTable[i].PrintInfo(msg, i, symTable);
#if !defined(MAKE_SILENT)
        MESSAGE(msg);
#endif
        fprintf(fout, "%s", msg.GetString());
    }
    msg = "";
    PrintTail(msg, len);
#if !defined(MAKE_SILENT)
    MESSAGE(msg);
#endif
    fprintf(fout, "%s", msg.GetString());
    return true;
}

bool ELF_Parser::DynTable::Parse(ELF_Parser::Info * elfInfo, FILE * fp, Elf_Shdr * dynSect, const char * stringTable)
{
    Ehdr * ehdr = &elfInfo->elfHeader;
    dynSection = dynSect;
    CRASH(dynTable != 0 || dynSection == 0 || stringTable == 0);
    int dynSize = (ehdr->IsElf32()) ? sizeof(Elf32_Dyn) : sizeof(Elf64_Dyn);

    if (dynSection->sh_entsize != dynSize || dynSection->sh_size % dynSize != 0)			ELF_ERROR(DYN_0)
        dynCount = (int)dynSection->sh_size / dynSize;
    dynTable = new Elf_Dyn[dynCount];

    int i;
    rewind(fp);
    fseek(fp, (long)dynSection->sh_offset, SEEK_SET);
    int frval;
    Elf32_Dyn d32;
    Elf64_Dyn d64;
    for (i = 0; i < dynCount; i++) {
        if (ehdr->IsElf32()) {
            if ((frval = fread(&d32, 1, sizeof(Elf32_Dyn), fp)) != sizeof(Elf32_Dyn))	ELF_ERROR_POS(DYN_1, i, 0)
                dynTable[i].Copy(&d32);
        }
        else {
            if ((frval = fread(&d64, 1, sizeof(Elf64_Dyn), fp)) != sizeof(Elf64_Dyn))	ELF_ERROR_POS(DYN_2, i, 0)
                dynTable[i].Copy(&d64);
        }
        Value * tag = dtagGroup.GetValue((int)dynTable[i].d_tag);
        if (tag == 0)																	ELF_ERROR_POS(DYN_3, i, 0)
            if (strcmp(tag->name, "NEEDED") == 0) { dynTable[i].name = stringTable + dynTable[i].d_un.d_val; }
    }
    String msg;
    int len = Elf_Dyn::PrintHeader(msg, dynSection);
#if !defined(MAKE_SILENT)
    MESSAGE(msg);
#endif
    fprintf(fout, "%s", msg.GetString());
    for (i = 0; i < dynCount; i++) {
        msg = "";
        dynTable[i].PrintInfo(msg, i);
#if !defined(MAKE_SILENT)
        MESSAGE(msg);
#endif
        fprintf(fout, "%s", msg.GetString());
    }
    msg = "";
    PrintTail(msg, len);
#if !defined(MAKE_SILENT)
    MESSAGE(msg);
#endif
    fprintf(fout, "%s", msg.GetString());
    return true;
}

bool ELF_Parser::PhdrTable::Parse(ELF_Parser::Info * elfInfo, FILE * fp)
{
    Ehdr * ehdr = &elfInfo->elfHeader;
    CRASH(phdr != 0 || interpName != 0);
    if (ehdr->ehdr.e_phnum == 0) { return true; }
    segmentCount = ehdr->ehdr.e_phnum;
    phdr = new Elf_Phdr[segmentCount];
    int i;
    rewind(fp);
    fseek(fp, (long)ehdr->ehdr.e_phoff, SEEK_SET);
    int frval;
    Elf32_Phdr p32;
    Elf64_Phdr p64;
    Elf_Phdr * segInterp = 0;
    for (i = 0; i < segmentCount; i++) {
        if (ehdr->IsElf32()) {
            if ((frval = fread(&p32, 1, sizeof(Elf32_Phdr), fp)) != sizeof(Elf32_Phdr))				ELF_ERROR_POS(PH32, i, 0)
                phdr[i].Copy(&p32);
        }
        else {
            if ((frval = fread(&p64, 1, sizeof(Elf64_Phdr), fp)) != sizeof(Elf64_Phdr))				ELF_ERROR_POS(PH64, i, 0)
                phdr[i].Copy(&p64);
        }
        Value * type = ptypeGroup.GetValue(phdr[i].p_type);
        if ((type) == 0)																			ELF_ERROR_POS(PHTYPE, i, phdr[i].p_type)
        if (strcmp(type->name, "INTERP") == 0) { segInterp = &phdr[i]; }
    }
    if (segInterp) {
        rewind(fp);
        fseek(fp, (long)segInterp->p_offset, SEEK_SET);
        CRASH(segInterp->p_filesz > segInterp->p_memsz);
        interpName = new char[(int)segInterp->p_memsz];
        if ((frval = fread(interpName, 1, (int)segInterp->p_filesz, fp)) != segInterp->p_filesz)	ELF_ERROR(PH_INTERP)
        U64 i;
        for (i = segInterp->p_filesz; i < segInterp->p_memsz; i++) { interpName[i] = 0; }
    }
    String msg;
    int len = Elf_Phdr::PrintHeader(msg);
    MESSAGE(msg);
    fprintf(fout, "%s", msg.GetString());
    for (i = 0; i < segmentCount; i++) {
        msg = "";
        phdr[i].PrintInfo(msg, i, &elfInfo->sectionTable);
        if (&phdr[i] == segInterp) { msg.Printf("(interp = %s)\n", interpName); }
        MESSAGE(msg);
        fprintf(fout, "%s", msg.GetString());
    }
    msg = "";
    PrintTail(msg, len);
    MESSAGE(msg);
    fprintf(fout, "%s", msg.GetString());
    return true;
}

int ELF_Parser::Elf_Phdr::PrintHeader(String & s)
{
    s.Printf("Program Headers:\n");
    int len = s.GetLength();
    s.Printf("|No.|%*s| Offset | Vaddr  | Paddr  |Filesize|Memsize |Flags|Align|Sections...\n",
        ptypeGroup.maxNameLength, "Type  ");
    len = s.GetLength() - len;
    int ll = len;
    while (ll > 0) { s += "="; ll--; }
    s += "\n";
    return len;
}

void ELF_Parser::Elf_Phdr::PrintInfo(String & s, int id, ShdrTable * sectionTable)
{
    String f;
    pflagGroup.GetValueMask((int)p_flags, f);
    s.Printf("|%3d|%-*s|%08llx|%08llx|%08llx|%08llx|%08llx|%-5s| %04llx|",
        id, ptypeGroup.maxNameLength, ptypeGroup.GetValue(p_type)->name,
        p_offset, p_vaddr, p_paddr, p_filesz, p_memsz,
        f.GetString(), p_align);

    int i;
    for (i = 0; i < sectionTable->sectionCount; i++) {
        Elf64_Addr sh_addr = sectionTable->shdr[i].sh_addr;
        if (sh_addr >= p_vaddr && sh_addr < p_vaddr + p_memsz) {
            s.Printf("(%s)", sectionTable->sectionStr.GetString() + sectionTable->shdr[i].sh_name);
        }
    }
    s += "\n";
}

FILE * ELF_Parser::fout = 0;

ELF_Parser::Info elfInfo;

bool ELF_Parser::Info::Parse(FILE * fp)
{
    Reset();
    CRASH(fout != 0);
    fout = fopen("../../TestProc/elf_info.log", "w");
    CRASH(fout == 0);
    printf("------------ ELF_Parser::Info::Parser() ----------------\n");
    if (!elfHeader.Parse(fp)) { return false; }
    //printf("elfHeader.Parse ok\n");
    if (elfHeader.ehdr.e_shnum) {
        if (!sectionTable.Parse(this, fp)) { return false; }
        //printf("sectionTable.Parse ok\n");
        if (sectionTable.symbolSection) {
            CRASH(sectionTable.symbolStr.GetLength() == 0);
            if (!symbolTable.Parse(this, fp, sectionTable.symbolSection, sectionTable.symbolStr.GetString(), false)) { return false; }
            //printf("symbolTable.Parse ok\n");
        }
        if (sectionTable.dynSymSection) {
            CRASH(sectionTable.dynSymStr.GetLength() == 0);
            if (!dynSymTable.Parse(this, fp, sectionTable.dynSymSection, sectionTable.dynSymStr.GetString(), true)) { return false; }
            //printf("dynSymTable.Parse ok\n");
        }
        if (sectionTable.dynSection) {
            CRASH(sectionTable.dynSymStr.GetLength() == 0);
            if (!dynTable.Parse(this, fp, sectionTable.dynSection, sectionTable.dynSymStr.GetString())) { return false; }
            //printf("dynTable.Parse ok\n");
        }
        LINKER lk;
        Elf_Shdr * rsh;
        for (rsh = sectionTable.relocSectionList.Head(lk); rsh; rsh = sectionTable.relocSectionList.Next(lk)) {
            RelocTable * reltab = new RelocTable;
            relocTableList.PushTail(reltab);
            reltab->Parse(this, fp, rsh);
            //printf("RelocTable.Parse ok\n");
        }
    }
    //printf("segmentTable.Parse...\n");
    if (elfHeader.ehdr.e_phnum) { if (!segmentTable.Parse(this, fp)) { return false; } }
    if (!sectionTable.CreateSections(fp, fout)) { return false; }
    fclose(fout);
    fout = 0;
    return true;
}

bool ELF_Parser::ShdrTable::CreateSections(FILE *fp, FILE *fout) {
    int i;// , j;
    int frval;
    Elf_Shdr * section;
    int maxNameLength = 0;
    for (i = 0; i < sectionCount; i++) {
        if (shdr[i].field.sh_alloc) {
            sectionList.PushTail(&shdr[i]);
            int l = strlen(shdr[i].sh_name_str);
            if (maxNameLength < l) { maxNameLength = l; }
        }
    }
    sectionList.Sort(CompareSection);
    LINKER lk;
    int execVector = shflagGroup.GetValueVector("X");
    for (i = 0, section = sectionList.Head(lk); section; section = sectionList.Next(lk), i++) {
        if (section->sh_addr + section->sh_size >= 0x100000000LL)										ELF_ERROR(X_3)
        int sz = (int)section->sh_size;
        int addr = (int)section->sh_addr;
        if (strcmp(shtypeGroup.GetValue(section->sh_type)->name, "NOBITS") != 0) {
            section->image = new Image(sz, addr);
            rewind(fp);
            fseek(fp, (long)section->sh_offset, SEEK_SET);
            if ((frval = fread(section->image->mem, 1, sz, fp)) != sz)									ELF_ERROR(X_4)
        }
        section->field.executable = (execVector & section->sh_flags) != 0;
        section->PrintImage(fout, maxNameLength);
    }
    return true;
}

bool ELF_Parser::Info::InstallMemory(int memDumpFlag)
{
    LINKER lk;
    Elf_Shdr * section;
    LinkedList<Elf_Shdr> &sectionList = sectionTable.sectionList;

    bool flag = true;
//    int i, j;

    for (section = sectionList.Head(lk); section; section = sectionList.Next(lk)) {
        Elf64_Addr minAddr = section->sh_addr, maxAddr = section->sh_addr + section->sh_size;
        if (section->field.executable) {
            CRASH(section->image->mem == 0);
#if 1	///	9/22/17
			if (maxAddr >= (pmemInfo.size << 2)) {
				printf("section (%s : %08llx - %08llx) cannot fit into pmem(size = %08x)...\n",
					section->sh_name_str, minAddr, maxAddr, (pmemInfo.size << 2));
				flag = false;
#else
            if (maxAddr >= pmemInfo.size) {
				printf("section (%s : %08llx - %08llx) cannot fit into pmem(size = %08x)...\n",
					section->sh_name_str, minAddr, maxAddr, pmemInfo.size);
				flag = false;
#endif
            }
            else { memcpy(pmemInfo.mem + minAddr, section->image->mem, (size_t)section->sh_size); }
        }
#if 1	///	9/22/17
		if (maxAddr >= (dmemInfo.size << 2)) {
			printf("section (%s : %08llx - %08llx) cannot fit into dmem(size = %08x)...\n",
				section->sh_name_str, minAddr, maxAddr, (dmemInfo.size << 2));
			flag = false;
		}
#else
		if (maxAddr >= dmemInfo.size) {
            printf("section (%s : %08llx - %08llx) cannot fit into dmem(size = %08x)...\n",
                section->sh_name_str, minAddr, maxAddr, dmemInfo.size);
            flag = false;
        }
#endif
        else if (!section->image) { memset(dmemInfo.mem + minAddr, 0, (size_t)section->sh_size); }
        else { memcpy(dmemInfo.mem + minAddr, section->image->mem, (size_t)section->sh_size); }
    }
    if (!flag || !memDumpFlag) { return flag; }
    for (section = sectionList.Head(lk); section; section = sectionList.Next(lk)) {
        MemoryInfo *mi = (section->field.executable) ? &pmemInfo : &dmemInfo;
        InstallSection(section, mi);
        if (section->field.executable) { InstallSection(section, &dmemInfo); }
    }
    pmemInfo.Close();
    dmemInfo.Close();
    return flag;
}

bool ELF_Parser::Info::InstallSection(Elf_Shdr * section, MemoryInfo *mi) {
    Elf64_Addr minAddr = section->sh_addr, maxAddr = section->sh_addr + section->sh_size;
    CRASH((minAddr & 0x3) != 0 || (maxAddr & 0x3) != 0);
    unsigned *pval = (unsigned *)&mi->mem[minAddr];
    unsigned addr = ((unsigned)minAddr) >> 2;
    int i, j;
    for (i = 0; i < section->sh_size; i += 4, ++pval, ++addr) {
        unsigned val = *pval;
        for (j = 0; j < mi->segmentCount; j++) {
            unsigned int v = (mi->printfDataWidth == 8) ?
                val : (val >> (j << 3)) & ((1 << (mi->printfDataWidth << 2)) - 1);
            if (mi->fp[j]) {
                fprintf(mi->fp[j], "@%0*x %0*x\n", mi->printfAddrWidth, addr, mi->printfDataWidth, v);
            }
        }
    }
    return true;
}

void ELF_Parser::Elf_Shdr::PrintImage(FILE *fp, int maxNameLength)
{
    String f;
    shflagGroup.GetValueMask((int)sh_flags, f);
    CRASH(fp == 0);
    fprintf(fp, "-------------------------------------------------------\n");
    fprintf(fp, "Image(Section: %*s) : addr(%08llx - %08llx) [%4s]\n",
        maxNameLength, sh_name_str, sh_addr, sh_addr + sh_size, f.GetString());
    if (image == 0) { fprintf(fp, "ALL ZEROES...\n"); return; }
    unsigned char * mem = image->mem;
    /// 8 words (32 bytes) per line
    Elf64_Addr addr0 = sh_addr & ~0x1f, maxAddr = sh_addr + sh_size;
    unsigned i;
    if (sh_addr & 0x1f) {
        fprintf(fp, "%08llx:", addr0);
        for (i = 0; i < 32; ++i) {
            fprintf(fp, "%s", (i & 0x3) ? " " : "|");
            if (addr0 + i < sh_addr) { fprintf(fp, "--"); }
            else fprintf(fp, "%02x", mem[addr0 + i - sh_addr]);
        }
        fprintf(fp, "\n");
        addr0 += 32;
    }
    int zeroRows = 0;
    while (addr0 < maxAddr) {
        bool nonZero = false;
        for (i = 0; i < 32; ++i) {
            if (addr0 + i < maxAddr && mem[addr0 + i - sh_addr]) { nonZero = true; break; }
        }
        if (nonZero) {
            zeroRows = 0;
            fprintf(fp, "%08llx:", addr0);
            for (i = 0; i < 32; ++i) {
                fprintf(fp, "%s", (i & 0x3) ? " " : "|");
                if (addr0 + i >= maxAddr) { fprintf(fp, "--"); }
                else fprintf(fp, "%02x", mem[addr0 + i - sh_addr]);
            }
            fprintf(fp, "\n");
        }
        else {
            if (!zeroRows) { fprintf(fp, "%08llx: ... all 0s ...\n", addr0); }
            ++zeroRows;
        }
        addr0 += 32;
    }
}
