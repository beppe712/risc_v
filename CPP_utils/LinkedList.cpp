// LinkedList.cpp: LinkedList クラスのインプリメンテーション
//
//////////////////////////////////////////////////////////////////////

#include "LinkedList.h"
#include "stdio.h"
#include "stdlib.h"
#include "time.h"

//////////////////////////////////////////////////////////////////////
// 構築/消滅
//////////////////////////////////////////////////////////////////////

#if 0
void __CRASH(const char * s)
{
	FILE * fp = fopen("CRASH.log", "w");
	time_t ltime;
	time(&ltime);
	fprintf(fp, "Crashed at %s\n", ctime(&ltime));
	fprintf(fp, "%s\n", s);
	fclose(fp);
	char ss[1000];
	sprintf(ss, "%s\n!!!!!!!!!!!ERROR!!!!!!!!!!!\nAbout to crash the program......\n", s);
	MyMessageBox(ss);
#if defined(_DEBUG)
	int * j = 0;
	*j = 0; // write to address 0! CRASH!
#else
	exit(-1);
#endif
}
#endif

BaseLinkedList::Linker::Linker(void * e, BaseLinkedList::List * l)
{
	Reset(e, l);
}

void BaseLinkedList::Linker::Reset(void * e, BaseLinkedList::List * l)
{
	element = e;
	list = l;
	next = 0;
	prev = 0;
	if(list) list->count ++;
}

BaseLinkedList::Linker::~Linker()
{
	Cleanup();
}

void BaseLinkedList::Linker::Cleanup()
{
	CRASH(element != 0 && "BaseLinkedList::Linker::~Linker():");
	if(list){
		if(list->head == this) list->head = next;
		if(list->tail == this) list->tail = prev;
		list->count --;
	}
	if(next) next->prev = prev;
	if(prev) prev->next = next;
	list = 0;
	next = 0;
	prev = 0;
}

BaseLinkedList::List::List(int tempFlag)
{
	count = 0;
	head = 0;
	tail = 0;
	deleteElementsDuringCleanup = (tempFlag == 0);
}

bool BaseLinkedList::List::DeleteAll()
{
	bool flag = (head != 0);

	while(head){
		DeleteElement(head);
		StoreDeletedLinker(head);
	}
	return flag;
}

bool BaseLinkedList::List::FlushAll()
{
	bool flag = (head != 0);

	while(b_PopHead());
	return flag;
}

void * BaseLinkedList::List::b_Head(LINKER & lk)
{
	lk.linker = head;
	if(lk.linker) return lk.linker->element;
	else return 0;
}

void * BaseLinkedList::List::b_Next(LINKER & lk)
{
	if(lk.linker == 0) lk.linker = head;
	else lk.linker = lk.linker->next;
	if(lk.linker) return lk.linker->element;
	else return 0;
}

void * BaseLinkedList::List::b_Next(LINKER & lk, int count)
{
	///	2/6/08 : I think this was a bug...
	int i;
	if(lk.linker == 0){
		lk.linker = head;
		count --;	///	when this happens, this should count as 1 "Next" move, which was ignored previous
	}
	for(i = 0; i < count; i ++){
		if(lk.linker == 0){ return 0;}
		lk.linker = lk.linker->next;
	}
	if(lk.linker)	{ return lk.linker->element;}
	else			{ return 0;}
}

void * BaseLinkedList::List::b_Tail(LINKER & lk)
{
	lk.linker = tail;
	if(lk.linker)	{ return lk.linker->element;}
	else			{ return 0;}
}

void * BaseLinkedList::List::b_Prev(LINKER & lk)
{
	if(lk.linker == 0)	{ lk.linker = tail;}
	else				{ lk.linker = lk.linker->prev;}
	if(lk.linker)	{ return lk.linker->element;}
	else			{ return 0;}
}

void * BaseLinkedList::List::b_Prev(LINKER & lk, int count)
{
	///	2/6/08 : same bug as b_Next(LINKER & lk, int count)...
	int i;
	if(lk.linker == 0){
		lk.linker = tail;
		count --;	///	when this happens, this should count as 1 "Prev" move, which was ignored previous
	}
	for(i = 0; i < count; i ++){
		if(lk.linker == 0){ return 0;}
		lk.linker = lk.linker->prev;
	}
	if(lk.linker)	{ return lk.linker->element;}
	else			{ return 0;}
}

void * BaseLinkedList::List::b_Current(LINKER & lk)
{
	if(lk.linker)	{ return lk.linker->element;}
	else			{ return 0;}
}

void * BaseLinkedList::List::b_Current(LINKER & lk, int index)
{
	LINKER ll = lk;
	if(index == 0)		{ return b_Current(ll);}
	else if(index > 0)	{ return b_Next(ll, index);}
	else				{ return b_Prev(ll, -index);}
}

void * BaseLinkedList::List::b_Get(int index)
{
	int i;
	Linker * linker;
	if(index < 0)	{ for(linker = tail, i = -1; (linker != 0) && i > index; linker = linker->prev, i --){}}
	else			{ for(linker = head, i = 0; (linker != 0) && i < index; linker = linker->next, i ++){}}
	return (linker) ? linker->element : 0;
}

bool BaseLinkedList::List::ReplaceIndexed(int index, void * e)
{
	int i;
	Linker * linker;
	if(index < 0)	{ for(linker = tail, i = -1; (linker != 0) && i > index; linker = linker->prev, i --){}}
	else			{ for(linker = head, i = 0; (linker != 0) && i < index; linker = linker->next, i ++){}}
	if(linker)	{ linker->element = e; return true;}
	else		{ return false;}
}

void * BaseLinkedList::List::b_PopHead()
{
	if(head == 0){ return 0;}
	void * e = head->element;
	head->element = 0;
	StoreDeletedLinker(head);
	return e;
}

void * BaseLinkedList::List::b_PopTail()
{
	if(tail == 0){ return 0;}
	void * e = tail->element;
	tail->element = 0;
	StoreDeletedLinker(tail);
	return e;
}

void * BaseLinkedList::List::b_PopCurrent(LINKER & lk, int dir)
{
	if(lk.linker == 0){ return 0;}
	void * e = lk.linker->element;
	lk.linker->element = 0;
	Linker * linker = lk.linker;
	if(dir > 0)	{ lk.linker = lk.linker->next;}
	else		{ lk.linker = lk.linker->prev;}
	StoreDeletedLinker(linker);
	return e;
}

void * BaseLinkedList::List::b_SearchElement(void * e)
{
	Linker * linker;
	for(linker = head; linker; linker = linker->next){ if(linker->element == e){ return e;}}
	return 0;
}

void * BaseLinkedList::List::b_PushNewElementToHead(void * e)
{
	void * e0 = b_SearchElement(e);
	if(e0){ return e0;}
	PushHead(e);
	return e;
}

void * BaseLinkedList::List::b_PushNewElementToTail(void * e)
{
	void * e0 = b_SearchElement(e);
	if(e0){ return e0;}
	PushTail(e);
	return e;
}


///	11/16/07 : "b_" removed from function name

void BaseLinkedList::List::PushTail(void * e)
{
	Linker * l = CreateLinker(e, this);
	l->prev = tail;
	if(tail == 0)	{ head = l;}
	else			{ tail->next = l;}
	tail = l;
}

void BaseLinkedList::List::PushHead(void * e)
{
	Linker * l = CreateLinker(e, this);
	l->next = head;
	if(head == 0)	{ tail = l;}
	else			{ head->prev = l;}
	head = l;
}

void BaseLinkedList::List::InsertListToTail(BaseLinkedList::List * list)
{
	void * e;
	while((e = list->b_PopHead()) != 0){ PushTail(e);}
}

void BaseLinkedList::List::InsertListToCurrent(LINKER & lk, BaseLinkedList::List * list, bool updateLinkerFlag)
{
	//	2/19/07
	if(list->count == 0){ return;}
	Linker * linker;
	for(linker = list->head; linker; linker = linker->next){ linker->list = this;}

	if(lk.linker){
		list->head->prev = lk.linker->prev;
		lk.linker->prev = list->tail;
	}
	else{
		list->head->prev = tail;
		tail = list->tail;
	}
	if(list->head->prev){ list->head->prev->next = list->head;}
	list->tail->next = lk.linker;
	if(head == lk.linker){ head = list->head;}

	if(updateLinkerFlag){ lk.linker = list->head;}
	count += list->count;

	list->head = 0;
	list->tail = 0;
	list->count = 0;
}

void BaseLinkedList::List::InsertToCurrent(LINKER & lk, void * e)
{
	///	insert new element to left (prev) of lk pointer
	Linker * l;
	l = CreateLinker(e, this);
	if(lk.linker == 0){
		l->prev = tail;
		tail = l;
	}
	else{
		l->prev = lk.linker->prev;
		lk.linker->prev = l;
	}
	if(l->prev){ l->prev->next = l;}
	l->next = lk.linker;
	if(head == lk.linker){ head = l;}
}

void BaseLinkedList::List::ReplaceCurrent(LINKER & lk, void * e){ lk.linker->element = e;}

void BaseLinkedList::List::DeleteFromMarkedToCurrent(LINKER & mk, LINKER & lk)
{
	b_Next(lk);
	while(mk.linker && mk.linker != lk.linker){
		Linker * temp = mk.linker;
		mk.linker = mk.linker->next;
		DeleteElement(temp);
		StoreDeletedLinker(temp);
	}
}

void BaseLinkedList::List::PopFromMarkedToCurrent(LINKER & mk, LINKER & lk)
{
	b_Next(lk);
	while(mk.linker && mk.linker != lk.linker){
		Linker * temp = mk.linker;
		mk.linker = mk.linker->next;
		temp->element = 0;
		StoreDeletedLinker(temp);
	}
}

bool BaseLinkedList::List::PopElement(LINKER & lk, void * e0)
{
	void * e;
	b_Head(lk);
	while((e = b_Current(lk))){
		if(e == e0){
			b_PopCurrent(lk, -1);
			return true;
		}
		else{ b_Next(lk);}
	}
	return false;
}

bool BaseLinkedList::List::PopElement(void * e0)
{
#if 1	///	4/6/15
	LINKER lk;
	b_Head(lk);
	return PopElement(lk, e0);
#else
	void * e;
	LINKER lk;
	b_Head(lk);
	while((e = b_Current(lk))){
		if(e == e0){
			b_PopCurrent(lk, -1);
			return true;
		}
		else{ b_Next(lk);}
	}
	return false;
#endif
}

void BaseLinkedList::List::MoveCurrentToElement(LINKER & lk, void * e)
{
	Linker * current;
	for(current = head; current; current = current->next){ if(current->element == e){ break;}}
	lk.linker = current;
}

int BaseLinkedList::List::GetIndex(void * e)
{
	int index = 0;
	Linker * linker;
	for(linker = head; linker; linker = linker->next, index ++){ if(linker->element == e){ return index;}}
	return -1;
}

int BaseLinkedList::List::GetIndex(LINKER & lk)
{
	int index = 0;
	Linker * linker;
	for(linker = head; linker; linker = linker->next, index ++){ if(linker == lk.linker){ return index;}}
	return -1;
}

int BaseLinkedList::List::GetCount(void * e)
{
	int count = 0;
	Linker * linker;
	for(linker = head; linker; linker = linker->next){ if(linker->element == e){ count ++;}}
	return count;
}

void BaseLinkedList::List::MoveMarkedToCurrentElements(LINKER & mk, LINKER & lk, BaseLinkedList::List * l)
{
	//	2/19/07
	b_Next(lk);
	Linker * plinker = mk.linker->prev;
	mk.linker->prev = 0;
	if(lk.linker){ lk.linker->prev->next = 0;}
	if(plinker && lk.linker){
		plinker->next = lk.linker;
		lk.linker->prev = plinker;
	}
	else if(plinker){
		plinker->next = 0;
		tail = plinker;
	}
	else if(lk.linker){
		lk.linker->prev = 0;
		head = lk.linker;
	}
	else{
		head = 0;
		tail = 0;
	}
	Linker * l0;
	int count0 = 0;
	for(l0 = mk.linker; l0; l0 = l0->next){
		l0->list = l;
		l->tail = l0;
		count0 ++;
	}
	l->head = mk.linker;
	count -= count0;
	l->count = count0;
}

void BaseLinkedList::List::operator<<(BaseLinkedList::List & l)
{
	void * e;
	while((e = l.b_PopHead()) != 0){ PushTail(e);}
}

void BaseLinkedList::List::operator>>(BaseLinkedList::List & l)
{
	void * e;
	while((e = b_PopHead()) != 0){ l.PushHead(e);}
}

void BaseLinkedList::List::Copy(BaseLinkedList::List * list)
{
	///	5/25/12 : let's CRASH if !IsTemporal()
	CRASH(!IsTemporal());
	void * t1;
	LINKER lk;
	for(t1 = list->b_Head(lk); t1; t1 = list->b_Next(lk)){ PushTail(t1);}
}

void BaseLinkedList::List::CopyInReverseOrder(BaseLinkedList::List * list)
{
	///	5/25/12 : let's CRASH if !IsTemporal()
	CRASH(!IsTemporal());
	void * t1;
	LINKER lk;
	for(t1 = list->b_Head(lk); t1; t1 = list->b_Next(lk)){ PushHead(t1);}
}

void BaseLinkedList::List::ReverseOrder()
{
	void * t;
	Linker * l0 = head, * l1 = tail;
	int c = 0, cmax = count / 2;
	for(c = 0; c < cmax; c ++){
		t = l0->element;
		l0->element = l1->element;
		l1->element = t;
		l0 = l0->next;
		l1 = l1->prev;
	}
	CRASH(((count & 1) == 0 && l0->prev != l1 && l1->next != l0) || ((count & 1) == 1 && l0 != l1));
}

bool BaseLinkedList::IsIdentical(BaseLinkedList::List * listA, BaseLinkedList::List * listB)
{
	LINKER lk1, lk2;
	void * e1 = listA->b_Head(lk1), * e2 = listB->b_Head(lk2);
	while(e1 || e2){
		if(e1 != e2){ return false;}
		e1 = listA->b_Next(lk1);
		e2 = listB->b_Next(lk2);
	}
	return true;
}

bool BaseLinkedList::IsIdenticalFromHead(BaseLinkedList::List * listA, BaseLinkedList::List * listB)
{
	LINKER lk1, lk2;
	void * e1 = listA->b_Head(lk1), * e2 = listB->b_Head(lk2);
	while(e1 && e2){
		if(e1 != e2){ return false;}
		e1 = listA->b_Next(lk1);
		e2 = listB->b_Next(lk2);
	}
	return true;
}

bool BaseLinkedList::IsIdenticalFromTail(BaseLinkedList::List * listA, BaseLinkedList::List * listB)
{
	LINKER lk1, lk2;
	void * e1 = listA->b_Tail(lk1), * e2 = listB->b_Tail(lk2);
	while(e1 && e2){
		if(e1 != e2){ return false;}
		e1 = listA->b_Prev(lk1);
		e2 = listB->b_Prev(lk2);
	}
	return true;
}

#include "MyTemplates.h"

ObjectPool::Manager * ObjectPool::rootManager = 0;

ObjectPool::PoolBase::PoolBase(int psz, int bsz, const char * n) :
count(0), poolSize(psz), bucketSize(bsz), totalPoolCount(0), name(n)
{
	CRASH(rootManager == 0);
	rootManager->objectPoolList.PushTail(this);
}

ObjectPool::PoolBase::~PoolBase()
{}

void ObjectPool::Manager::PrintObjectPoolInfo(String & s)
{
	LINKER lk;
	PoolBase * pool;
	s += "-------------------------------------\n";
	for(pool = objectPoolList.Head(lk); pool; pool = objectPoolList.Next(lk)){
		pool->PrintObjectPoolInfo(s);
		s += "-------------------------------------\n";
	}
}


void BaseLinkedList::StoreDeletedLinker(BaseLinkedList::Linker * linker)
{
	if(ObjectPool::GetLinkerPool()){
		linker->Cleanup();
		ObjectPool::GetLinkerPool()->Push(linker);
	}
	else{ delete linker;}
}

BaseLinkedList::Linker * BaseLinkedList::CreateLinker(void * e, BaseLinkedList::List * l)
{
	Linker * linker = (ObjectPool::GetLinkerPool()) ? ObjectPool::GetLinkerPool()->Pop() : 0;
	if(linker)	{ linker->Reset(e, l);}
	else		{ linker = new Linker(e, l);}
	return linker;
}

FastLinkedList::Element * FastLinkedList::List::Get(int index)
{
	int i;
	Element * e;
	if(index < 0){
		{ for(e = tail, i = -1; (e != 0) && i > index; e = e->prev, i --);}
	}
	else{
		if(index > (count >> 1)){ for(e = tail, i = count - 1;  (e != 0) && i > index; e = e->prev, i --);}
		else					{ for(e = head, i = 0;			(e != 0) && i < index; e = e->next, i ++);}
	}
	return e;
}

