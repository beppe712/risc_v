// String.h: String クラスのインターフェイス
//
//////////////////////////////////////////////////////////////////////

#if !defined(__INCLUDE_STRING_H__)
#define __INCLUDE_STRING_H__

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>

class String  
{
	char * data;
	int length, allocSize;
	static int maxAllocSize;
	char * AllocateMemory(){
		if(allocSize < length + 1){
			allocSize = length + 1;
			if(maxAllocSize < allocSize)
				maxAllocSize = allocSize;
			return new char[allocSize];
		}
		return data;
	}
	static FILE * stringWorkspaceFile;
public:
	static void PrepareWorkspace();
	static void DestroyWorkspace();
	char * AllocateMemory(int size){
		length = size;
		char * mem = AllocateMemory();
		if(mem != data){
			delete [] data;
			data = mem;
		}
		return data;
	}
	static int GetMaxAllocSize(){ return maxAllocSize;}
#if 1   /// 7/1/20
    void UpdateLength() { length = (int)strlen(data); }
#else
	void UpdateLength(){ length = strlen(data);}
#endif
	String();
	String(const char *, int);
	String(const char *);
	String(const String &);
 	const char * GetString() const { return data;}
	char GetLastChar(){ return (length == 0) ? 0 : data[length - 1];}
	virtual ~String();
	void operator=(const char *);
	void operator=(const String & s) { operator=(s.GetString());}
	void operator=(int i);
	void operator=(unsigned int i){ operator=((int) i);}
	void operator+=(const char *);
	void operator+=(String & s){ operator+=(s.GetString());}
	void operator+=(const char);
	void operator+=(int i);
	void operator+=(unsigned int i){ operator+=((int) i);}
	bool operator==(const char *);
	bool operator==(String & s){ return operator==(s.GetString());}
	bool operator==(const char c){ char s[2]; s[0] = c; s[1] = 0; return operator==(s);}
	bool operator!=(const char * s){ return !operator==(s);}
	bool operator!=(String & s){ return !operator==(s);}
	bool operator>(String & s);
	bool operator<(String & s);
	bool operator>=(String & s){ return !operator<(s);}
	bool operator<=(String & s){ return !operator>(s);}
	void Set(const char *, int);
	int GetLength(){ return length;}
	///	1/8/16
	int * GetLengthPointer(){ return &length;}
	char Val(int i = 0);
	bool Match(const char *, int);
	bool PartMatch(const char *, int);
	bool Includes(const char *);
	int Count(const char *);
	int Count(char);
	char Termination();
	void RemoveTermination(char c);
	bool RemoveTermination(const char * tail);
	bool RemovePrefix(const char * header);
	void ConvertIndent(String &);
	int GetMatchingPosition(const char *);
	bool MatchSuffix(const char *);
	void ChangeToChar(char c);
	static int GetLongestMatchingLength(String & a, String & b);
	static const char * GetFormatInteger(unsigned long long val);
	static const char * GetFormatIntegerFromDouble(double val);
#if 1	///	9/18/14
	static bool IsSame(const char * a, const char * b, int l = -1);
#else
	static bool IsSame(const char * a, const char * b);
	static bool IsSame(const char * a, const char * b, int l);
#endif
	const char * GetString(int i) const {
		if(length >= i)
			return data + i;
		return 0;
	}
	static int GetEscapeSequenceChar(const char * str, int * length = 0);
	void PutEscapeSequenceChar(unsigned char charVal);
	static int Interpret_c_char(const char * str);
	void RemoveEscapeCodesFromStringLiteral(const char * str);
	void RestoreEscapeCodesToStringLiteral();
	void ReplaceChar(char from, char to);
	void SetChar(char c, int length);
	int Printf(const char * format, ...);
	static int GetVPrintfLength(const char * format, va_list arglist);
	static int GetPrintfLength(const char * format, ...);
	const char * ExtractTail(char charBeforeTail);
	void ConvertToUpperCase();
#if 1	///	3/1/09 : added by kawachi
	int ChangeToInt();
	void Cut(int l);
	int BitInt(int l);
#endif
	bool IsInt();
	bool IsHex();
	int GetHexValue();
	static int GetHexCharValue(char c);

	void RemoveFileExtension();
};

class StringArray
{
public:
	String * strArray;
	int size;
	StringArray(int sz) : strArray(0), size(sz){ if(size){ strArray = new String[size];}}
	~StringArray(){ if(strArray){ delete [] strArray;}}
	String * Get(int idx){
		if(idx < 0 || idx >= size)	{ return 0;}
		else						{ return &strArray[idx];}
	}
};

#define STRING_QUOTE(n)		"\"" n "\""

#endif 
