//	set path=C:\MinGW\bin;%path%

#define BBI_RGB  0
#define BBI_RLE8 1
#define BBI_RLE4 2
#define BBI_BITFIELD 3
	
#define WIN_OS2_OLD 12
#define WIN_NEW     40
#define OS2_NEW     64
	
struct PIX
{
	unsigned char R, G, B, I;
};

struct BMPIMG
{
	PIX * bitmap;
	PIX colormap[256];
	int colormap_size;
	int size_x, size_y, size_xy;
	int out_pix_bits;	/* valid values are: 24, 8, 4, 1 */
    BMPIMG(int pix_bits = 24);
    ~BMPIMG();
    int save(const char *fname);
    int open(const char *fname);
    int loadBMP1(FILE * fp, int w, int h);
    int loadBMP4(FILE * fp, int w, int h, int comp);
    int loadBMP8(FILE * fp, int w, int h, int comp);
    int loadBMP24(FILE * fp, int w, int h);
    int openBMP(FILE * fp);

    int createGrayscale64x64xN(int w, int h);
    int setGrayscale64x64(char *blk, int idx);
};

//void BMPIMG_initialize(BMPIMG * img, int pix_bits);
//void BMPIMG_destroy(BMPIMG * img);
//int BMPIMG_save(BMPIMG * img, const char *fname);
//int BMPIMG_open(BMPIMG * img, const char * fname);
