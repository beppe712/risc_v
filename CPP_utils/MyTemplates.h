#ifndef _MY_TEMPLATES_H_INCLUDED
#define _MY_TEMPLATES_H_INCLUDED

class TimeUnit
{
public:
	typedef double Time;
	static Time maxTime;
};

class C_LineInfo
{
public:
	int info, infoRaw;
	C_LineInfo(int i = -1, int r = -1){ info = i; infoRaw = r;}
};

#include "LinkedList.h"
#define CREATE_LINKED_LIST_TEMPLATE_DEFAULT_0(LIST)										\
	T * Head(LINKER & lk)					{ return (T *) b_Head(lk);}					\
	T * Next(LINKER & lk)					{ return (T *) b_Next(lk);}					\
	T * Prev(LINKER & lk)					{ return (T *) b_Prev(lk);}					\
	T * Tail(LINKER & lk)					{ return (T *) b_Tail(lk);}					\
	T * Current(LINKER & lk)				{ return (T *) b_Current(lk);}				\
	T * Current(LINKER & lk, int i)			{ return (T *) b_Current(lk, i);}			\
	virtual void DeleteElement(BaseLinkedList::Linker * linker){						\
		if(deleteElementsDuringCleanup){ delete (T *) linker->element;}					\
		linker->element = 0;															\
	}																					



#define CREATE_LINKED_LIST_TEMPLATE_DEFAULT(LIST)										\
	T * Get(int i)							{ return (T *) b_Get(i);}					\
	T * Next(LINKER & lk, int count)		{ return (T *) b_Next(lk, count);}			\
	T * PopHead()							{ return (T *) b_PopHead();}				\
	T * PopTail()							{ return (T *) b_PopTail();}				\
	T * PopCurrent(LINKER & lk, int dir = 0){ return (T *) b_PopCurrent(lk, dir);}		\
	T * Prev(LINKER & lk, int count)		{ return (T *) b_Prev(lk, count);}			\
/*	T * Tail(LINKER & lk)					{ return (T *) b_Tail(lk);}	*/				\
	T * SearchElement(T * e)				{ return (T *) b_SearchElement(e);}			\
	T * PushNewElementToHead(T * e)			{ return (T *) b_PushNewElementToHead(e);}	\
	T * PushNewElementToTail(T * e)			{ return (T *) b_PushNewElementToTail(e);}	\
CREATE_LINKED_LIST_TEMPLATE_DEFAULT_0(LIST)

#define CREATE_LINKED_LIST_TEMPLATE(LIST)												\
	LIST(int temporalFlag = 0) : BaseLinkedList::List(temporalFlag){}					\
	virtual ~LIST(){ DeleteAll();}														\
	CREATE_LINKED_LIST_TEMPLATE_DEFAULT(LIST)

#define SMALL_LIST_COUNT	200
template <class T> class LinkedList : public BaseLinkedList::List
{
	static void Swap(T ** a, T ** b){ T * t = *a; *a = *b; *b = t;}
	void BubbleSort(bool (* Compare)(T *, T *));
	void MergeSort(bool (* Compare)(T *, T *));
public:
	CREATE_LINKED_LIST_TEMPLATE(LinkedList)
	void Sort(bool (* Compare)(T *, T *)){ if(count < SMALL_LIST_COUNT){ BubbleSort(Compare);} else{ MergeSort(Compare);}}
};

template <class T> void LinkedList<T>::BubbleSort(bool (* Compare)(T *, T *))
{
	int i, j;
	LINKER lk;
	T * a, * b;
	for(i = count - 1; i >= 0; i --){
		j = i;
		a = Head(lk);
		for(b = Next(lk); j > 0; b = Next(lk), j --){
			CRASH(b == 0);
			if(Compare(a, b))	{ lk.linker->prev->element = b; lk.linker->element = a;}	///	swap a and b
			else				{ a = b;}
		}
	}
}

template <class T> void LinkedList<T>::MergeSort(bool (* Compare)(T *, T *))
{
	T ** elements = new T * [count], ** buf = new T * [count], * t;
	LINKER lk;
	int i, j;
	for(t = Head(lk), i = 0; t; t = Next(lk), i ++){ elements[i] = t;}
	for(i = 1; i < count; i <<= 1){	///	i = 1, 2, 4, ...
		int ii = i * 2;
		for(j = 0; j < count; j += ii){
			int k0 = j, k1 = j + i, m = 0;
			int l0 = (k1 < count) ? k1 : count, l1 = (j + ii < count) ? j + ii : count;
			while(k0 < l0 && k1 < l1){
				if(Compare(elements[k0], elements[k1]))	{ buf[m ++] = elements[k1 ++];}
				else									{ buf[m ++] = elements[k0 ++];}
			}
			while(k0 < l0){ buf[m ++] = elements[k0 ++];}
			while(k1 < l1){ buf[m ++] = elements[k1 ++];}
			for(k0 = 0; k0 < m; k0 ++){ elements[j + k0] = buf[k0];}
		}
	}
	for(t = Head(lk), i = 0; t; t = Next(lk), i ++){ lk.linker->element = elements[i];}
	delete [] elements;
	delete [] buf;
}

template <class T> class FastLinker : public FastLinkedList::Element
{
	void BubbleSort(bool (* Compare)(T *, T *));
	void MergeSort(bool (* Compare)(T *, T *));
public:
	T * target;
	virtual ~FastLinker(){}
	void Sort(bool (* Compare)(T *, T *)){ if(list->count < SMALL_LIST_COUNT){ BubbleSort(Compare);} else{ MergeSort(Compare);}}
};

template <class T> void FastLinker<T>::BubbleSort(bool (* Compare)(T *, T *))
{
	///	9/6/13
	int i, j;
	int count = list->count;
	FastLinker<T> * ea, * eb;
	for(i = count - 1; i >= 0; i --){
		j = i;
		ea = (FastLinker<T> *) list->head;
		for(eb = (FastLinker<T> *) ea->next; j > 0; eb = (FastLinker<T> *) eb->next, j --){
			CRASH(eb == 0);
			if(Compare(ea->target, eb->target)){
				///	swap a and b
				///	x -> a -> b -> y
				///	x -> b -> a -> y
				CRASH(ea->next != eb || eb->prev != ea);
				FastLinkedList::Element * x = ea->prev, * y = eb->next;
				eb->prev = x;
				if(x)	{ x->next = eb;}
				else	{ list->head = eb;}
				ea->next = y;
				if(y)	{ y->prev = ea;}
				else	{ list->tail = ea;}
				eb->next = ea;
				ea->prev = eb;
				eb = ea;
			}
			else{ ea = eb;}
		}
	}
}

template <class T> void FastLinker<T>::MergeSort(bool (* Compare)(T *, T *))
{
	int count = list->count;
	FastLinkedList::List * list0 = list;
	FastLinker<T> ** elements = new FastLinker <T> * [count], ** buf = new FastLinker <T> * [count];
	int i, j;
	FastLinkedList::Element * fe;
	i = 0;
	while((fe = list0->PopHead())){ elements[i ++] = (FastLinker<T> *) fe;}
	for(i = 1; i < count; i <<= 1){	///	i = 1, 2, 4, ...
		int ii = i * 2;
		for(j = 0; j < count; j += ii){
			int k0 = j, k1 = j + i, m = 0;
			int l0 = (k1 < count) ? k1 : count, l1 = (j + ii < count) ? j + ii : count;
			while(k0 < l0 && k1 < l1){
				if(Compare(elements[k0]->target, elements[k1]->target))	{ buf[m ++] = elements[k1 ++];}
				else													{ buf[m ++] = elements[k0 ++];}
			}
			while(k0 < l0){ buf[m ++] = elements[k0 ++];}
			while(k1 < l1){ buf[m ++] = elements[k1 ++];}
			for(k0 = 0; k0 < m; k0 ++){ elements[j + k0] = buf[k0];}
		}
	}
	for(i = 0; i < count; i ++){ list0->PushTail(elements[i]);}
	delete [] elements;
	delete [] buf;
}

#define __ARRAY_STACK_IS_NON_EMPTY__(s)		(s.topOfStack != s.stack)
#define __ARRAY_STACK_IS_EMPTY__(s)			(s.topOfStack == s.stack)

template <class T> class ArrayStack
{
public:
#define INITIAL_ARRAY_TYPE_STACK_SIZE	(1 << 10)
#define ASSERT_ARRAY_TYPE_STACK
	int size;
	T * stack, * topOfStack, * stackTail;
	ArrayStack(int sz = 0){
		size = (sz) ? sz : INITIAL_ARRAY_TYPE_STACK_SIZE;
		stack = new T[size];
		stackTail = stack + size;
		topOfStack = stack;
	}
	bool IsEmpty(){ return (topOfStack == stack);}
	T * Top(){ return (topOfStack == stack) ? 0 : topOfStack - 1;}
	T * Pop(){
#ifdef ASSERT_ARRAY_TYPE_STACK
		CRASH(topOfStack == stack);
#endif
		return (-- topOfStack);
	}
	void Push(T obj){
		*(topOfStack ++) = obj;
		if(topOfStack == stackTail){ Reallocate();}
	}
	int GetStackPos(){ return topOfStack - stack;}
	void Reallocate();
	virtual ~ArrayStack(){ delete [] stack;}
	void Reset(){ topOfStack = stack;}
	void PopElement(T obj);
#undef INITIAL_ARRAY_TYPE_STACK_SIZE
#undef ASSERT_ARRAY_TYPE_STACK
};

template <class T> void ArrayStack<T>::Reallocate()
{
	CRASH(topOfStack != stack + size || size == 1);
	int newSize = size + (size >> 1);
	T * newStack = new T[newSize];
	int i;
	for(i = 0; i < size; i ++){ newStack[i] = stack[i];}
	delete [] stack;
	topOfStack = newStack + size;
	size = newSize;
	stack = newStack;
	stackTail = stack + size;
}

template <class T> void ArrayStack<T>::PopElement(T obj)
{
	int i;
	for(i = 0; i < size; i ++)	{ if(stack[i] == obj){ break;}}
	CRASH(i == size);	///	obj is not in this ArrayStack!!!
	for(; i < size - 1; i ++)	{ stack[i] = stack[i + 1];}
	topOfStack --;
	size --;
}

///	1. LinkedList 2. array
template <class T> class Container
{
	///	first mode is LinkedList. After constructing LinkedList, convert it to array.
	///	After converting to array mode, you cannot modify the contents
public:
	union Link
	{
	public:
		LinkedList<T> * list;	///	LinkedList is memory-hungry: sizeof(LinkedList) = 5 words, sizeof(Linker) = 4 words
		T ** object;
		Link() : list(0){}
	} link;
	union Indexer
	{
	public:
		LINKER lk;
		int index;
	} cur;
	unsigned int mode		:	1;	//	mode = 0: LinkedList, mode = 1: object array
	unsigned int isTemporal	:	1;	//	temporal = 0: delete objects upon destruction
	int			count		:	30;
protected:
	void SyncCount(){
		CRASH(mode);
		count = link.list->count;
	}
public:
	T * GetHead(){
		if(mode == 0)	{ return link.list->Head(cur.lk);}
		else			{ return (count > (cur.index = 0)) ? link.object[cur.index] : 0;}
	}
	T * GetTail(){
		if(mode == 0)	{ return link.list->Tail(cur.lk);}
		else			{ return (0 <= (cur.index = count - 1)) ? link.object[cur.index] : 0;}
	}
	T * GetNext(){
		if(mode == 0)	{ return link.list->Next(cur.lk);}
		else			{ return (count > (++ cur.index)) ? link.object[cur.index] : 0;}
	}
	T * GetPrev(){
		if(mode == 0)	{ return link.list->Prev(cur.lk);}
		else			{ return (0 <= (-- cur.index)) ? link.object[cur.index] : 0;}
	}
	T * GetCurrent(){
		if(mode == 0)	{ return link.list->Current(cur.lk);}
		else			{ return (count > (cur.index)) ? link.object[cur.index] : 0;}
	}
	LinkedList<T> * GetList(){ return (mode == 0) ? link.list : 0;}
	Container(int tempFlag = 0) : mode(0), isTemporal(tempFlag), count(0){ InitializeToListMode();}
	void MakeTemporal(){
		isTemporal = 1;
		if(!mode){ link.list->MakeTemporal();}
	}
	virtual ~Container(){ Destroy();}
	void InitializeToListMode(){
		CRASH(link.list || mode || count);
		link.list = new LinkedList<T>(isTemporal);
	}
	void PushHead(T * e){
		CRASH(mode);
		link.list->PushHead(e);
		count = link.list->count;
	}
	void PushTail(T * e){
		CRASH(mode);
		link.list->PushTail(e);
		count = link.list->count;
	}
	T * PushNewElementToTail(T * e){
		CRASH(mode);
		e = link.list->PushNewElementToTail(e);
		count = link.list->count;
		return e;
	}
	void CopyToList(LinkedList<T> * list){
		if(mode == 0){
			LINKER lk;
			T * e;
			for(e = link.list->Head(lk); e; e = link.list->Next(lk)){ list->PushTail(e);}
		}
		else{
			int i;
			for(i = 0; i < count; i ++){ list->PushTail(link.object[i]);}
		}
	}
	T * PopHead(){
		CRASH(mode);
		T * e = link.list->PopHead();
		count = link.list->count;
		return e;
	}
	T * PopTail(){
		CRASH(mode);
		T * e = link.list->PopTail();
		count = link.list->count;
		return e;
	}
	T * SearchElement(T * e){
		CRASH(mode);
		return link.list->SearchElement(e);
	}
	T * PopCurrent(int dir = 0){
		CRASH(mode);
		T * ret = link.list->PopCurrent(cur.lk, dir);
		count --;
		return ret;
	}
	void InitializeToArrayMode(int size){
		CRASH(count || mode);
		delete link.list;
		mode = 1;
		count = size;
		link.object = new T *[count];
		int i;
		for(i = 0; i < count; i ++){ link.object[i] = 0;}
	}
	void ChangeToArrayMode(){
		CRASH(mode);
		T ** array = new T *[count];
		int i = 0;
		T * e;
		while((e = link.list->PopHead())){ array[i ++] = e;}
		CRASH(i != count);
		mode = 1;
		delete link.list;
		link.object = array;
	}
	void ChangeToListMode(){
		CRASH(!mode);
		T ** array = link.object;
		link.object = 0;
		int count0 = count;
		count = 0;
		mode = 0;
		InitializeToListMode();
		count = count0;
		int i;
		for(i = 0; i < count; i ++){ link.list->PushTail(array[i]);}
		delete [] array;
	}
	void Destroy(){
		if(mode == 0){ delete link.list;}
		else{
			if(!isTemporal){ int i; for(i = 0; i < count; i ++){ delete link.object[i];}}
			delete [] link.object;
		}
		mode = 0;
		count = 0;
		link.list = 0;
	}
	T * Get(int i){
		if(i < 0 || i >= count){ return 0;}
		if(mode == 0)	{ return link.list->Get(i);}
		else			{ return link.object[i];}
	}
	T * operator[](int i){ return Get(i);}
	int GetIndex(T * e){
		CRASH(!mode);
		int i;
		for(i = 0; i < count; i ++){ if(link.object[i] == e){ return i;}}
		return -1;
	}
	void Put(int i, T * e){ CRASH(!mode || i < 0 || i >= count); link.object[i] = e;}
	int GetCount(){ return count;}
};

template <class T> class PointerArray
{
	T ** array;
	int size;
public:
	PointerArray(int s) : size(s){
		array = new T * [size];
		int i;
		for(i = 0; i < size; i ++){ array[i] = 0;}
	}
	virtual ~PointerArray(){ delete [] array;}
	bool Set(T * e, int i){
		if(i < 0 || i >= size)	{ return false;}
		else					{ array[i] = e; return true;}
	}
	T * Get(int i){
		if(i < 0 || i >= size)	{ return 0;}
		else					{ return array[i];}
	}
};

#include "String.h"
///	class T : must be derived from class String
template <class T> class C_Dictionary : public BaseLinkedList::List
{
	LINKER linker[256];
public:
	///	3/14/11
	CREATE_LINKED_LIST_TEMPLATE_DEFAULT_0(C_Dictionary)
	C_Dictionary(int temporalFlag = 0) : BaseLinkedList::List(temporalFlag){ ResetEntry();}
	virtual ~C_Dictionary(){ DeleteAll();}
	void ResetEntry(){ int i; for(i = 0; i < 256; i ++){ linker[i].linker = 0;}}
	void Insert(T * element);
	T * Search(const char * s);
	void SearchMatching(const char * s, LinkedList<T> * list);
	void UpdateEntry(unsigned char header, LINKER lk);
	T * PopString(const char * s);
	bool CheckEntry(unsigned char header){ return linker[header].linker != 0;}
	T * PopHead();
	T * PopTail();
};

template <class T> void C_Dictionary<T>::Insert(T * element)
{
	String * str = element;	//	this will force type checking (T * element must be derived from String)
	unsigned char header = (unsigned char) str->Val(0); // first character in str
	int i;
	for(i = header; i < 256; i ++){
		if(linker[i].linker){
			InsertToCurrent(linker[i], element);
			linker[header].linker = linker[i].linker->prev;
			break;
		}
	}
	if(i == 256){
		// no element stored after "header" index
		PushTail(element);
		linker[header].linker = tail;
	}
	CRASH(Current(linker[header]) != str);
}

template <class T> T * C_Dictionary<T>::Search(const char * s)
{
	unsigned char header = (unsigned char) s[0]; // first character in str
	if(linker[header].linker == 0){ return 0;}	//	no entry for s
	LINKER lk(linker[header]);
	String * s0;
	for(s0 = Current(lk); s0; s0 = Next(lk)){
		unsigned char h0 = (unsigned char) s0->Val(0);
		if(h0 != header){ return 0;}	//	s not found
		if(*s0 == s)	{ return (T *) s0;}
	}
	return 0;	//	s not found
}

template <class T> void C_Dictionary<T>::SearchMatching(const char * s, LinkedList<T> * list)
{
	unsigned char header = (unsigned char) s[0]; // first character in str
	if(linker[header].linker == 0){ return;}	//	no entry for s
	int length = strlen(s);
	LINKER lk(linker[header]);
	T * s0;
	for(s0 = Current(lk); s0; s0 = Next(lk)){
		unsigned char h0 = (unsigned char) s0->Val(0);
		if(h0 != header){ return;}	//	s not found
		if(s0->PartMatch(s, length)){ list->PushTail(s0);}
	}
}

template <class T> void C_Dictionary<T>::UpdateEntry(unsigned char header, LINKER lk)
{
	if(linker[header].linker == lk.linker){
		String * s1 = Current(lk, 1);
		if(s1 && s1->Val(0) == header)	{ linker[header].linker = lk.linker->next;}
		else							{ linker[header].linker = 0;}
	}
}

///	2/23/09
template <class T> T * C_Dictionary<T>::PopString(const char * s)
{
	unsigned char header = (unsigned char) s[0]; // first character in str
	if(linker[header].linker == 0){ return 0;}	//	no entry for s
	LINKER lk(linker[header]);
	String * s0;
	for(s0 = Current(lk); s0; s0 = Next(lk)){
		unsigned char h0 = (unsigned char) s0->Val(0);
		if(h0 != header){ return 0;}	//	s not found
		if(*s0 == s){
			///	3/14/11
			UpdateEntry(header, lk);
			b_PopCurrent(lk, 0);
			return (T *) s0;	//	s found and deleted
		}
	}
	return 0;	//	s not found
}

template <class T> T * C_Dictionary<T>::PopHead()
{
	LINKER lk;
	String * s = Head(lk);
	if(s == 0){ return 0;}
	unsigned char header = (unsigned char) s->Val(0); // first character in str
	UpdateEntry(header, lk);
	return (T *) b_PopHead();
}

template <class T> T * C_Dictionary<T>::PopTail()
{
	LINKER lk;
	String * s = Tail(lk);
	if(s == 0){ return 0;}
	unsigned char header = (unsigned char) s->Val(0); // first character in str
	UpdateEntry(header, lk);
	return (T *) b_PopTail();
}

template <class T> class C_Tag
{
public:
	int tag;
	bool deleteObjectWhenDestroyed;
	T * object;
	C_Tag(T * d, int id, bool destroyObjectFlag = false) : tag(id), deleteObjectWhenDestroyed(destroyObjectFlag), object(d){}
	virtual ~C_Tag(){ if(deleteObjectWhenDestroyed){ delete object;}}
};

class NamedStringTagList
{
public:
	String name;
	LinkedList<C_Tag<String> > stringTagList;
	NamedStringTagList(const char * n){ name = n;}
};


template <class T> class HashedList
{
#define MAX_HASH_BIT_COUNT 16
	int GetHashKey(unsigned int id){
		int key = 0;
		while(id){ key ^= (id & (hashSize - 1)); id >>= hashBitCount;}
		CRASH(key >= hashSize);
		return key;
	}
public:
	int hashBitCount, hashSize;
	int totalCount;
	LinkedList<C_Tag<T> > * listArray;
	HashedList(int hashBitCount0){
		hashBitCount = hashBitCount0;
		CRASH(hashBitCount <= 0 || hashBitCount > MAX_HASH_BIT_COUNT);
		hashSize = (1 << hashBitCount);
		listArray = new LinkedList<C_Tag<T> >[hashSize];
		totalCount = 0;
	}
	~HashedList(){ delete [] listArray;}
	void Insert(C_Tag<T> * e){
		listArray[GetHashKey(e->tag)].PushTail(e);
		totalCount ++;
	}
	void DeleteAll(){
		int i;
		for(i = 0; i < hashSize; i ++){ listArray[i].DeleteAll();}
		totalCount = 0;
	}
	T * SearchElement(int tag){
		LinkedList<C_Tag<T> > * list = &listArray[GetHashKey(tag)];
		LINKER lk;
		C_Tag<T> * e;
		for(e = list->Head(lk); e; e = list->Next(lk)){ if(e->tag == tag){ return e->object;}}
		return 0;
	}
	T * SearchElement(bool (* matchCondition)(T *, int, int, int), int arg0, int arg1, int arg2){
		C_Tag<T> * e;
		LINKER lk;
		LinkedList<C_Tag<T> > * list;
		if(arg0){
			list = &listArray[GetHashKey(arg0)];
			for(e = list->Head(lk); e; e = list->Next(lk)){
				if(e->tag == arg0 && matchCondition(e->object, arg0, arg1, arg2)){ return e->object;}
			}
			return 0;
		}
		else{	///	no hash entry available
			int i;
			for(i = 0; i < hashSize; i ++){
				list = &listArray[i];
				for(e = list->Head(lk); e; e = list->Next(lk)){
					if(matchCondition(e->object, arg0, arg1, arg2)){ return e->object;}
				}
			}
			return 0;
		}
	}
	void ExecuteCallBackAtAllObjects(void (* callback)(T *)){
		int i;
		C_Tag<T> * e;
		for(i = 0; i < hashSize; i ++){
			LinkedList<C_Tag<T> > * list = &listArray[i];
			LINKER lk;
			for(e = list->Head(lk); e; e = list->Next(lk)){ callback(e->object);}
		}
	}
	void PrintListCount(String & s){
		s.Printf("totalCount = %d:\n", totalCount);
		int i;
		for(i = 0; i < hashSize; i ++){
			s.Printf("(%3d:%3d)", i, listArray[i].count);
			if(i % 8 == (8 - 1)){ s += "\n";}
		}
		s += "\n";
	}
};

class ObjectPool
{
public:
	template <class T> class Bucket
	{
	public:
		int count, maxCount, size;
		T ** bucket;
		Bucket(int sz) : count(0), maxCount(0), size(sz){ bucket = new T * [size];}
		virtual ~Bucket(){
			int i;
			for(i = 0; i < count; i ++){ delete bucket[i];}
			delete [] bucket;
		}
		T * Pop(){ CRASH(count == 0); return bucket[-- count];}
		void Push(T * e){
			CRASH(count == size);
			bucket[count ++] = e;
			if(maxCount < count){ maxCount = count;}
		}
	};
	class PoolBase
	{
	public:
		int count, poolSize, bucketSize, totalPoolCount;
		String name;
		PoolBase(int psz, int bsz, const char * n);
		virtual ~PoolBase();
		virtual void PrintObjectPoolInfo(String & s) = 0;
	};
	template <class T> class Pool : public PoolBase
	{
	public:
		Bucket<T> ** bucket;
		Pool(int psz, int bsz, const char * n) : PoolBase(psz, bsz, n){
			bucket = new Bucket<T> * [poolSize];
			int i;
			for(i = 0; i < poolSize; i ++){ bucket[i] = 0;}
		}
		virtual ~Pool(){ Destroy(); delete [] bucket;}
		T * Pop(){
			if(bucket[count] == 0 || bucket[count]->count == 0){ return 0;}
			T * e = bucket[count]->Pop();
			if(bucket[count]->count == 0){ if(count > 0){ count --;}}
			return e;
		}
		void Push(T * e){
			if(bucket[count] && bucket[count]->count == bucketSize){ count ++;}
			CRASH(count == poolSize);
			if(bucket[count] == 0){ bucket[count] = new Bucket<T>(bucketSize);}
			bucket[count]->Push(e);
			totalPoolCount ++;
		}
		virtual void PrintObjectPoolInfo(String & s){
			int activeBucketObjectCount = (bucket[count]) ? bucket[count]->count : 0;
			int i;
			for(i = 0; i < poolSize; i ++){ if(bucket[i] == 0){ break;}}
			int maxBucketCount = 0;
			if(i > 0){ maxBucketCount = bucket[i - 1]->maxCount;}
			s.Printf("Object-pool : %s (size = %d * %d)\n\t# total pooled = %d, # current pooled = %d (# max pooled = %d)\n",
				name.GetString(), poolSize, bucketSize, totalPoolCount, count * bucketSize + activeBucketObjectCount, (i - 1) * bucketSize + maxBucketCount);
		}
		void Destroy(){
			int i;
			for(i = 0; i < poolSize; i ++){
				if(bucket[i] == 0){ break;}
				delete bucket[i];
				bucket[i] = 0;
			}
		}
	};
#define LINKER_POOL_SIZE			(1 << 8)
#define LINKER_STATE_BUCKET_SIZE	(1 << 16)
public:
	class Manager
	{
	public:
		LinkedList<PoolBase> objectPoolList;
		Pool<BaseLinkedList::Linker> * linkerPool;
		Manager() : linkerPool(0){}
		void Create(){ linkerPool = new Pool<BaseLinkedList::Linker>(LINKER_POOL_SIZE, LINKER_STATE_BUCKET_SIZE, "linkerPool"); }
		virtual ~Manager(){
			objectPoolList.PopElement(linkerPool);	//	need to delete linkerPool last
			objectPoolList.DeleteAll();
			delete linkerPool;
		}
		void PrintObjectPoolInfo(String & s);
	};
	static void CreateRootManager(){
		CRASH(rootManager);
		rootManager = new Manager;
		rootManager->Create();
	}
	static Pool<BaseLinkedList::Linker> * GetLinkerPool(){
		if(rootManager == 0){ return 0;}
		return rootManager->linkerPool;
	}
	static void DestroyRootManager(){
		if(rootManager){ delete rootManager;}
		rootManager = 0;
	}
	static void PrintObjectPoolInfo(String & s){ if(rootManager){ rootManager->PrintObjectPoolInfo(s);}}
private:
	static Manager * rootManager;
};

#if 1	///	11/13/14
#if 1	///	11/28/14
extern void (*array_msg)(String & s);

template < class T > class Array
{
public:
#define INITIAL_ARRAY_SIZE	100
	int count, size;
	bool msgFlag;
	const char * name;
	T * elements;
	void Initialize(const char * n, int sz, bool mflag){
		name = n;
		count = 0;
		size = sz;
		if(size == 0){ elements = 0;}
		else{ elements = new T[size];}
		msgFlag = mflag;
	}
	Array(const char * n, int sz = INITIAL_ARRAY_SIZE){ Initialize(n, sz, false);}
	Array(int sz = INITIAL_ARRAY_SIZE){ Initialize("unnamed", sz, false);}
	void Reset(){ count = 0;}
	///	copy constructor...
	Array < T > & operator=(Array < T > & array){
		name = array.name;
		count = array.count;
		msgFlag = array.msgFlag;
		if(size != array.size){ Resize(array.size);}
		int i;
		for(i = 0; i < count; i ++){ elements[i] = array.elements[i];}
		return *this;
	}
	void Resize(int newSize){
		T * newElements = new T[newSize];
		if(size){
			int cpySize = (newSize < size) ? newSize : size;
			int i;
			for(i = 0; i < cpySize; i ++){ newElements[i] = elements[i];}
			delete [] elements;
		}
		elements = newElements;
		if(msgFlag && array_msg){
			String msg;
			msg.Printf("Array(%s): size: %d --> %d\n", name, size, newSize);
			array_msg(msg);
		}
		size = newSize;
	}
	virtual ~Array(void){ if(elements){ delete [] elements;}}
	void PreCreate(int elementCount){
		if(elements){ delete [] elements;}
		elements = 0; count = 0; size = 0;
		Resize(elementCount);
	}
	T * Create(int elementCount){
		if(elements){ delete [] elements;}
		///	11/22/13
		elements = 0; count = elementCount; size = 0;
		Resize(count);
		return elements;
	}
	T * Allocate(int elementCount = 1){
		int oldCount = count;
		count = oldCount + elementCount;
		if(count > size){ Resize(count << 1);}	///	increase array by 2 times
		return &elements[oldCount];
	}
	T * Head(){ return elements;}
	///	11/22/13
	T & operator[](int i){ CRASH(i < 0 || i >= count); return elements[i];}
	///	11/20/13
	T * Tail(){ if(count == 0){ return 0;} else{ return elements + count - 1;}}
	int GetIndex(T t){
		int i;
		for(i = 0; i < count; i ++){ if(elements[i] == t){ return i;}}
		return -1;
	}
	T * Finalize(int count0){
		count = count0;
		return Finalize();
	}
	T * Finalize(void){
		if(count > 0){ if(count != size){ Resize(count);}}
		return elements;
	}
	////////////// stack implementation ////////////////
	void Push(T t){
		if(count >= size){ Resize(count << 1);}	///	increase array by 2 times
		elements[count ++] = t;
	}
#if 1	///	5/12/15
	void PushNewElement(T t){ if(!Exists(t)){ Push(t);}}
#else
	void PushNewElement(T t){ if(Search(t) == 0){ Push(t);}}
#endif
	T Pop(void){
		if(count <= 0){ return 0;}
		return elements[-- count];
	}
	T TopOfStack(void){
		if(count <= 0){ return 0;}
		return elements[count - 1];
	}
#if 1	///	5/12/15 : don't use Search()!!! (problem if T is basic-type (such as integers)
	bool Exists(T t){
		int i;
		for(i = 0; i < count; i ++){ if(elements[i] == t){ return true;}}
		return false;
	}
#else
	T Search(T t){
		int i;
		for(i = 0; i < count; i ++){ if(elements[i] == t){ return t;}}
		return 0;
	}
#endif
	////////////// sort implementation ////////////////
	static void Swap(T * a, T * b){
		T t = *a;
		*a = *b;
		*b = t;
	}
	private:
	void BubbleSort(bool (* compare)(T a, T b)){
		T * t, * last = Tail();
		int i;
		for(i = 0; i < count; i ++){
			for(t = Head() + 1; t <= last - i; t ++){ if(compare(*(t - 1), *t)){ Swap(t - 1, t);}}
		}
	}
	void MergeSort(bool (* compare)(T a, T b))
	{
		//T ** elements = new T * [count], ** buf = new T * [count], * t;
		//LINKER lk;
		T * buf = new T [count];
		int i, j;
//		for(t = Head(lk), i = 0; t; t = Next(lk), i ++){ elements[i] = t;}
		for(i = 1; i < count; i <<= 1){	///	i = 1, 2, 4, ...
			int ii = i * 2;
			for(j = 0; j < count; j += ii){
				int k0 = j, k1 = j + i, m = 0;
				int l0 = (k1 < count) ? k1 : count, l1 = (j + ii < count) ? j + ii : count;
				while(k0 < l0 && k1 < l1){
					if(compare(elements[k0], elements[k1]))	{ buf[m ++] = elements[k1 ++];}
					else									{ buf[m ++] = elements[k0 ++];}
				}
				while(k0 < l0){ buf[m ++] = elements[k0 ++];}
				while(k1 < l1){ buf[m ++] = elements[k1 ++];}
				for(k0 = 0; k0 < m; k0 ++){ elements[j + k0] = buf[k0];}
			}
		}
		//for(t = Head(lk), i = 0; t; t = Next(lk), i ++){ lk.linker->element = elements[i];}
		//delete [] elements;
		delete [] buf;
	}
	public:
	void Sort(bool (* compare)(T, T)){ if(count < SMALL_LIST_COUNT){ BubbleSort(compare);} else{ MergeSort(compare);}}
	////////////// binary search implementation ////////////////
	T * BinarySearch(unsigned int key, int (* compare)(T * a, unsigned int key)){
		///	binary search!!!
		int maxRange = count - 1, minRange = 0;
		///	|-----------------X-----------------|
		///	|--------X--------|
		///	         |---X----|
		///	             |-X--|
		///              |X|
		while(maxRange - minRange >= 0){
			int searchOffset = (maxRange + minRange) >> 1;
			T * t = &elements[searchOffset];
			int flag = compare(t, key);
			if(flag == 0)		{ return t;}
			else if(flag < 0)	{ maxRange = searchOffset - 1;}
			else				{ minRange = searchOffset + 1;}
		}
		return 0;
	}
};

#define ARRAY_CTOR(n)				n(#n)
#define ARRAY_CTOR_SIZE(n,size)		n(#n,size)

template <class T> class Pool
{
	LINKER lk;
	LinkedList<Array<T> > arrayList;
	int arraySize;
	bool isActive;
public:
	int GetArraySize(){ return arraySize;}
	Pool(int size) : arraySize(size), isActive(false){ CRASH(arraySize == 0); lk.linker = 0;}
	void Activate(){
		CRASH(arraySize == 0);
		CRASH(isActive);
		isActive = true;
	}
	void Deactivate(){
		CRASH(arraySize == 0);
		CRASH(!isActive);
		Array<T> * array;
		for(array = arrayList.Head(lk); array; array = arrayList.Next(lk)){ array->count = 0;}
		arrayList.Head(lk);
		isActive = false;
	}
	T * New(int count = 1){
		CRASH(!isActive);
		Array<T> * a = arrayList.Current(lk);
		if(a == 0 || a->count + count > arraySize){
			a = arrayList.Next(lk);
			if(a){ CRASH(a->count);}
			else{
				arrayList.PushTail(new Array<T>(arraySize));
				a = arrayList.Tail(lk);
			}
		}
		T * element = &a->elements[a->count];
		a->count += count;
		return element;
	}
	int GetAllocatedCount(){ return arrayList.count * arraySize;}
};

#else
template <class T> class MyArray
{
public:
	T * array;
	int size, count;
	MyArray(int sz){ size = sz; array = new T[size]; count = 0;}
	~MyArray(){ delete [] array;}
};
template <class T> class Pool
{
	LINKER lk;
	LinkedList<MyArray<T> > arrayList;
	int arraySize;
	bool isActive;
public:
	int GetArraySize(){ return arraySize;}
	Pool(int size) : arraySize(size), isActive(false){ CRASH(arraySize == 0); lk.linker = 0;}
	void Activate(){
		CRASH(arraySize == 0);
		CRASH(isActive);
		isActive = true;
	}
	void Deactivate(){
		CRASH(arraySize == 0);
		CRASH(!isActive);
		MyArray<T> * array;
		for(array = arrayList.Head(lk); array; array = arrayList.Next(lk)){ array->count = 0;}
		arrayList.Head(lk);
		isActive = false;
	}
	T * New(int count = 1){
		CRASH(!isActive);
		MyArray<T> * array = arrayList.Current(lk);
		if(array == 0 || array->count + count > arraySize){
			array = arrayList.Next(lk);
			if(array){ CRASH(array->count);}
			else{
				arrayList.PushTail(new MyArray<T>(arraySize));
				array = arrayList.Tail(lk);
			}
		}
		T * element = &array->array[array->count];
		array->count += count;
		return element;
	}
	int GetAllocatedCount(){ return arrayList.count * arraySize;}
};
#endif
#endif


#if 1	///	these will be defined in MyTemplates.h to be more visible...
#define LINE_NUM_BITS	20
#define FILE_TAG_BITS	32 - LINE_NUM_BITS
#endif

#if 0 /// disable Heap --> error occurs on ubuntu:LLVM???? why????
#define HEAP_ASSERT_LEVEL	1
///	0 : disable all assertion
///	1 : enable all assertion macros, disable heap assertion
///	2 : enable all assertion macros and heap assertion
#if (HEAP_ASSERT_LEVEL > 0)
#define ASSERT_HEAP(n)	CRASH(n)
#else
#define ASSERT_HEAP(n)	(n)
#endif
template <class T> class HeapElement : public FastLinker<T>
{
public:
	HeapElement<T> * p, * lc, * rc;		///	p : parent, lc : left-child, rc : right-child
	HeapElement(){ target = 0; Reset();}
	virtual ~HeapElement(){}
	void Reset(){ FastLinker<T>::Reset(); p = 0; lc = 0; rc = 0;}
	void AssertReset(){ ASSERT_HEAP(list || next || prev || p || lc || rc);}
	void UpdateHeap();
	void RemoveFromHeap();
};

template <class T> class Heap : public FastLinkedList::List
{
public:
	int (* GetKey)(T *);
	void (* SetHeap)(T *, HeapElement<T> *);
	bool assertFlag;
	Heap(int (* getkey)(T *), void (* setheap)(T *, HeapElement<T> *)){
		GetKey = getkey; SetHeap = setheap;
#if (HEAP_ASSERT_LEVEL <= 1)
		assertFlag = false;
#else
		assertFlag = true;
#endif
		Reset();
	}
	virtual ~Heap(){ Reset();}
	virtual void ClearHeap(){
		FastLinkedList::Element * e, * e2;
		for(e = head; e;){ e2 = e; e = e->next; ((HeapElement<T> *) e2)->Reset();}
		Reset();
	}
	void SwapElement(HeapElement<T> * p, HeapElement<T> * c){
		T * pt = p->target, * ct = c->target;
		SetHeap(pt, c);	SetHeap(ct, p);
		p->target = ct;	c->target = pt;
	}
	T * GetTopElement(){
		ASSERT_HEAP(head == 0);
		return ((HeapElement<T> *) head)->target;
	}
	void AddToTail(HeapElement<T> * e){
		e->AssertReset();
		HeapElement<T> * oldTail = (HeapElement<T> *) tail;
		PushTail(e);
		HeapElement<T> * p;
		if(oldTail){
			p = oldTail->p;
			if(p == 0){
				ASSERT_HEAP(count != 2 || oldTail->lc || oldTail->rc);
				oldTail->lc = e;
				e->p = oldTail;
			}
			else{
				ASSERT_HEAP(p->lc == 0);
				if(p->rc == 0){ p->rc = e;}
				else{
					p = (HeapElement<T> *) p->next;
					ASSERT_HEAP(p->lc || p->rc);
					p->lc = e;
				}
				e->p = p;
			}
		}
		Update(e);
	}
	void Remove(HeapElement<T> * e){
		ASSERT_HEAP(e == 0 || e->list != this || tail == 0);
		bool flag = (e != tail);
		if(flag){ SwapElement(e, (HeapElement<T> *) tail);}
		HeapElement<T> * p = ((HeapElement<T> *) tail)->p;
		if(p){
			if(p->rc)	{ ASSERT_HEAP(p->rc != tail); p->rc = 0;}
			else		{ ASSERT_HEAP(p->lc != tail); p->lc = 0;}
		}
		((HeapElement<T> *) tail)->p = 0;
		PopTail();
		if(flag){ Update(e);}
	}
	void Update(HeapElement<T> * e){
		ASSERT_HEAP(e == 0 || e->list != this);
		HeapElement<T> * e0 = e, * pe, * ce;
		int key = GetKey(e->target);
		while((pe = e->p)){
			if(GetKey(pe->target) < key){ SwapElement(pe, e);}
			else						{ break;}
			e = pe;
		}
		if(e == e0){							/// check children
			while((ce = e->lc)){				///	choose left or right (with largest maxDist)
				HeapElement<T> * ce2 = e->rc;	///	left child
				int d = GetKey(ce->target), dright = (ce2) ? GetKey(ce2->target) : -1;
				if(d < dright){ ce = ce2; d = dright;}
				if(d > key)	{ SwapElement(e, ce);}
				else		{ break;}
				e = ce;
			}
		}
		if(assertFlag){ Assert();}
	}
	void Assert(void (* assertElement)(T *) = 0){
		HeapElement<T> * e;
		for(e = (HeapElement<T> *) head; e; e = (HeapElement<T> *) e->next){
			if(assertElement){ assertElement(e->target);}
			ASSERT_HEAP(e->list != this);
			ASSERT_HEAP(e->p && e->p->list != this);
			int d = GetKey(e->target);
			ASSERT_HEAP(e->p && GetKey(e->p->target) < d);
			ASSERT_HEAP(e->lc && GetKey(e->lc->target) > d);
			ASSERT_HEAP(e->rc && GetKey(e->rc->target) > d);
		}
	}
};

template <class T> void HeapElement<T>::UpdateHeap()	{ ASSERT_HEAP(list == 0); ((Heap<T> *) list)->Update(this);}
template <class T> void HeapElement<T>::RemoveFromHeap(){ ASSERT_HEAP(list == 0); ((Heap<T> *) list)->Remove(this);}

#endif
#endif

