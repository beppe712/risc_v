# makefile for LLVM-C2RTL
#
# how to use..
#
# PROJNAME = c2r_add_ss
# SRCS= test_add.c
# include ../Makefile.in

ifeq ($(strip $(shell echo)),)
SHELL_STYLE=Unix
else
SHELL_STYLE=DOS
endif

ifneq (clean,$(MAKECMDGOALS))
ifneq (tags,$(MAKECMDGOALS))
-include .TOP_FUNC
endif
endif

ifeq ($(strip $(TOP_FUNCS)),)
TOP_FUNCS = $(PROJNAME)
endif

TOP_FUNCS := $(sort $(TOP_FUNCS))

# If the following is defined, using "opt" to generate RTL.
#ifneq ($(OS),Windows_NT)
USE_C2RTL_OPT = 1
#endif

VPATH = $(sort $(dir $(SRCS)))

#####################
# dir name

BIN=bin
OBJ=obj
C2R_DIR=C2R_ROOT
C2R_RTL_C_DIR=$(C2R_DIR)/RTL_C
C2R_RTL_V_DIR=$(C2R_DIR)/RTL_V
C2R_RTL_TB_DIR=$(C2R_DIR)/RTL_V_TB

OBJRTLDIR=$(OBJ)/rtl
BINRTLDIR=$(BIN)/rtl
OBJSIMDIR=$(OBJ)/sim
BINSIMDIR=$(BIN)/sim

#####################
# TARGET name

ifeq ($(USE_SOC_MODEL), 1)
TARGET_RTL=$(MODULES:%=$(C2R_RTL_V_DIR)/%.RTL.v)
TARGET_VCD=$(MODULES:%=$(C2R_RTL_TB_DIR)/%_testbench.vcd)
TARGET_VSIM=$(MODULES:%=$(C2R_RTL_TB_DIR)/%.out)
SUB_MODULES=$(TOP_FUNCS:%=$(C2R_RTL_V_DIR)/%.RTL.v)
else
TARGET_RTL=$(TOP_FUNCS:%=$(C2R_RTL_V_DIR)/%.RTL.v)
TARGET_VCD=$(TOP_FUNCS:%=$(C2R_RTL_TB_DIR)/%_testbench.vcd)
TARGET_VSIM=$(TOP_FUNCS:%=$(C2R_RTL_TB_DIR)/%.out)
endif
TARGET_SIM=$(BINSIMDIR)/$(PROJNAME).exe

TARGET_EDIF=$(TOP_FUNCS:%=$(C2R_RTL_TB_DIR)/%.edif)

RTL_BCS=$(patsubst %.cpp, $(OBJRTLDIR)/%.bc, $(filter %.cpp,$(notdir $(SRCS))))
RTL_BCS+=$(patsubst %.c, $(OBJRTLDIR)/%.bc, $(filter %.c,$(notdir $(SRCS))))
DEPENDS = $(RTL_BCS:.bc=.d)

OBJS=$(patsubst %.cpp, $(OBJSIMDIR)/%.o, $(filter %.cpp,$(notdir $(SRCS))))
OBJS+=$(patsubst %.c, $(OBJSIMDIR)/%.o, $(filter %.c,$(notdir $(SRCS))))
DEPENDS += $(OBJS:.o=.d)

RTL_LINKED_BC = $(OBJRTLDIR)/$(PROJNAME).link.bc
RTL_LINKED_LL = $(OBJRTLDIR)/$(PROJNAME).link.ll

#####################
# flags

CC=clang
LLVMLINK=llvm-link
LLVMLINKFLAGS=
LLVMDIS=llvm-dis
OPT=opt
CC_SIM?=clang

ifeq ($(USE_C2RTL_OPT), 1)
CFLAGS_RTL= -O1 -D"_CRT_SECURE_NO_WARNINGS" -D"__TCT_COMPILER_USED__" -D"__LLVM_C2RTL__" -g $(CFLAGS)
else
CFLAGS_RTL= -D"_CRT_SECURE_NO_WARNINGS" $(CFLAGS)
endif

ifeq (,$(findstring -g,$(CFLAGS_SIM)))
CFLAGS_SIM += -O3
endif
CFLAGS_SIM += -D"_CRT_SECURE_NO_WARNINGS" $(CFLAGS)

ifneq ($(OS),Windows_NT)
CPPFLAGS = -std=c++11
LFLAGS += -lm -lstdc++
else
CPPFLAGS = -std=c++14
endif


OPTFLAGS_RTL= -C2R_Setup -C2R_IPConstProp -C2R_LoopUnroll -sccp -simplifycfg -dce -C2RTL
OPTFLAGS_SYS= -C2Sys_Setup -C2System
OPTFLAGS_OPT= -C2R_Setup -C2R_IPConstProp -C2R_LoopUnroll -sccp -simplifycfg -dce

VVP=vvp
IVERILOG=iverilog

#####################
.PHONY: all rtl sim runsim vsim vvp edif

#####################
# targets

all: vvp

rtl: $(TARGET_RTL)
	:

sim: $(TARGET_RTL) $(TARGET_SIM)

runsim: $(TARGET_SIM)
	$<

vsim: $(TARGET_VSIM)

vvp: $(TARGET_VCD)

edif: $(TARGET_EDIF)

#################
# rules for rtl

ifeq ($(USE_C2RTL_OPT), 1)
$(TOP_FUNCS:%=$(C2R_RTL_C_DIR)/%.RTL.h) \
$(TOP_FUNCS:%=$(C2R_RTL_C_DIR)/%.RTL.c) \
$(TOP_FUNCS:%=$(C2R_RTL_C_DIR)/%.RTL.BODY.c) \
$(TOP_FUNCS:%=$(C2R_RTL_V_DIR)/%.RTL.v) \
$(TOP_FUNCS:%=$(C2R_RTL_TB_DIR)/%.TESTBENCH.v) \
$(OBJRTLDIR)/$(PROJNAME).opt.bc \
$(OBJRTLDIR)/$(PROJNAME).opt.ll : $(RTL_LINKED_BC)
	@echo "opt...: $^ -> $@"
	$(OPT) $(OPTFLAGS_RTL) $^ -o $(OBJRTLDIR)/$(PROJNAME).opt.bc
	$(LLVMDIS) $(OBJRTLDIR)/$(PROJNAME).opt.bc

ifeq ($(USE_SOC_MODEL), 1)
.TOP_FUNC \
$(MODULES:%=$(C2R_RTL_C_DIR)/%.RTL.h) \
$(MODULES:%=$(C2R_RTL_C_DIR)/%.RTL.SYS.h) \
$(MODULES:%=$(C2R_RTL_C_DIR)/%.RTL.c) \
$(MODULES:%=$(C2R_RTL_C_DIR)/%.RTL.BODY.c) \
$(MODULES:%=$(C2R_RTL_V_DIR)/%.RTL.v) \
$(MODULES:%=$(C2R_RTL_TB_DIR)/%.TESTBENCH.v) \
$(OBJRTLDIR)/$(PROJNAME).opt.sys.bc \
$(OBJRTLDIR)/$(PROJNAME).opt.sys.ll : $(RTL_LINKED_BC) $(OBJRTLDIR)/$(PROJNAME).opt.bc
	$(OPT) $(OPTFLAGS_SYS) $< -o $(OBJRTLDIR)/$(PROJNAME).opt.sys.bc
	$(LLVMDIS) $(OBJRTLDIR)/$(PROJNAME).opt.sys.bc
endif

else
$(TARGET_RTL): $(SRCS)
	@echo "compile...: $^ -> $@"
	$(CC) -c2rtl $(CFLAGS_RTL) $^
endif

$(RTL_LINKED_BC) $(RTL_LINKED_LL): $(RTL_BCS)
	@echo "link(*.bc)...: $^ -> $@"
	$(LLVMLINK) $(LLVMLINKFLAGS) $^ -o $@
	$(LLVMDIS) $@

$(OBJRTLDIR)/%.bc: %.c
	@echo "compile(*.bc)...: $< -> $@"
ifeq ($(SHELL_STYLE),DOS)
	@[ -d $(OBJRTLDIR) ] || mkdir $(subst /,\,$(OBJRTLDIR))
else
	@[ -d $(OBJRTLDIR) ] || mkdir -p $(OBJRTLDIR)
endif
	$(CC) -c -emit-llvm $(CFLAGS_RTL) $< -o $@
	$(CC) -c -emit-llvm $(CFLAGS_RTL) $< -MQ $@ -MM > $(@:.bc=.d)

$(OBJRTLDIR)/%.bc: %.cpp
	@echo "compile(*.bc)...: $< -> $@"
ifeq ($(SHELL_STYLE),DOS)
	@[ -d $(OBJRTLDIR) ] || mkdir $(subst /,\,$(OBJRTLDIR))
else
	@[ -d $(OBJRTLDIR) ] || mkdir -p $(OBJRTLDIR)
endif
	$(CC) -c -emit-llvm $(CFLAGS_RTL) $(CPPFLAGS) $< -o $@
	$(CC) -c -emit-llvm $(CFLAGS_RTL) $(CPPFLAGS) $< -MQ $@ -MM > $(@:.bc=.d)

#################
# rules for sim

$(C2R_RTL_TB_DIR)/%_testbench.vcd : $(C2R_RTL_TB_DIR)/%.out
	cd $(dir $<) &&	$(VVP) $(notdir $<)

$(C2R_RTL_TB_DIR)/%.edif : $(C2R_RTL_V_DIR)/%.RTL.v
	@echo "compile...: $^ -> $@"
ifeq ($(USE_SOC_MODEL), 1)
	$(IVERILOG) -S -o $@ -I $(C2R_RTL_TB_DIR) -I $(C2R_RTL_V_DIR) $(filter %.v,$^)
else
	$(IVERILOG) -S -o $@ -I $(C2R_RTL_TB_DIR) $(filter %.v,$^)
endif

$(C2R_RTL_TB_DIR)/%.out : \
			$(C2R_RTL_V_DIR)/%.RTL.v \
			$(C2R_RTL_TB_DIR)/%.TESTBENCH.v \
			$(C2R_RTL_TB_DIR)/%_dump.log
	@echo "compile...: $^ -> $@"
ifeq ($(USE_SOC_MODEL), 1)
	$(IVERILOG) -o $@ -I $(C2R_RTL_TB_DIR) -I $(C2R_RTL_V_DIR) $(filter %.v,$^) $(SUB_MODULES)
else
	$(IVERILOG) -o $@ -I $(C2R_RTL_TB_DIR) $(filter %.v,$^)
endif

$(C2R_RTL_TB_DIR)/%_dump.log : $(BINSIMDIR)/$(PROJNAME).exe
	$<

$(BINSIMDIR)/$(PROJNAME).exe: $(OBJS)
	@echo "link...: $^ -> $@"
ifeq ($(SHELL_STYLE),DOS)
	@[ -d $(BINSIMDIR) ] || mkdir $(subst /,\,$(BINSIMDIR))
else
	@[ -d $(BINSIMDIR) ] || mkdir -p $(BINSIMDIR)
endif
	$(CC_SIM) $(LFLAGS) $^ -o $@

$(OBJSIMDIR)/%.o: %.c
	@echo "compile(*.o)...: $< -> $@"
ifeq ($(SHELL_STYLE),DOS)
	@[ -d $(OBJSIMDIR) ] || mkdir $(subst /,\,$(OBJSIMDIR))
else
	@[ -d $(OBJSIMDIR) ] || mkdir -p $(OBJSIMDIR)
endif
	$(CC_SIM) -c $(CFLAGS_SIM) $< -o $@
	$(CC_SIM) -c $(CFLAGS_SIM) $< -MQ $@ -MM > $(@:.o=.d)

$(OBJSIMDIR)/%.o: %.cpp
	@echo "compile(*.o)...: $< -> $@"
ifeq ($(SHELL_STYLE),DOS)
	@[ -d $(OBJSIMDIR) ] || mkdir $(subst /,\,$(OBJSIMDIR))
else
	@[ -d $(OBJSIMDIR) ] || mkdir -p $(OBJSIMDIR)
endif
	$(CC_SIM) -c $(CFLAGS_SIM) $(CPPFLAGS) $< -o $@
	$(CC_SIM) -c $(CFLAGS_SIM) $(CPPFLAGS) $< -MQ $@ -MM > $(@:.o=.d)

#################

clean:
	rm -rf bin obj C2R_ROOT

.PRECIOUS: $(C2R_RTL_TB_DIR)/%_dump.log

-include $(DEPENDS)
