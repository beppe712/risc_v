
#include "ELFUtil.h"
#include "C2RUtil.h"
#include "C2RProcUtil.h"

std::stringstream & operator << (std::stringstream &ss, const C2R::SF &sf) { return sf.operator<<(ss); }

static C2R::ProcUtil::Profiler __prof(0);
C2R::ProcUtil::Profiler *C2R::ProcUtil::prof = &__prof;
FILE *C2R::ProcUtil::fp_prof = NULL;

C2R::ProcUtil::Profiler::Profiler(int idx) : profileCount(0), sp(0), extraCalls(0), skippedCycles(0), id(idx) {
    if (!fp_prof) {
        fp_prof = fopen("profile.log", "w");
        C2R_ASSERT(fp_prof);
    }
}

void C2R::ProcUtil::install_program_elf(unsigned *dmem, unsigned *pmem, unsigned dmem_size, unsigned pmem_size,
    unsigned *pc, unsigned *sp, int memDumpFlag, int pc_offset, int skipProfile, char * file_name)
{
    FILE *fp = fopen(file_name, "rb");
    if (fp) {
        printf("install_program_elf : %s\n", file_name);
        ELF_Parser::Info elfInfo;
        elfInfo.Parse(fp);
        FILE *fp_out[4] = { 0 };
        int segmentCount = 0, printfAddrWidth, printfDataWidth;
        if (memDumpFlag) { openMemDumpFile(0, fp_out, &segmentCount, &printfAddrWidth, &printfDataWidth); }
        /// 7/1/20
        if (dmem) {
            elfInfo.dmemInfo.Set((unsigned char *)dmem, dmem_size, printfAddrWidth, printfDataWidth,
                segmentCount, fp_out);
        }
        if (memDumpFlag) { openMemDumpFile(1, fp_out, &segmentCount, &printfAddrWidth, &printfDataWidth); }
        /// 7/1/20
        if (pmem) {
            elfInfo.pmemInfo.Set((unsigned char *)pmem, pmem_size, printfAddrWidth, printfDataWidth,
                segmentCount, fp_out);
        }
        /// 7/1/20
        bool flag = (dmem && pmem) ? elfInfo.InstallMemory(memDumpFlag) : true;
        /// 7/1/20
        if (pc) { *pc = (unsigned)elfInfo.elfHeader.ehdr.e_entry + pc_offset; }
#define STACK_MARGIN	0x100
        /// 7/1/20
        if (sp) {
            *sp = (dmem_size << 2) - STACK_MARGIN;
        }
        fclose(fp);
        C2R_ASSERT(flag);
        /// 7/14/17
        if (memDumpFlag && prof) {
            prof->SetupElfProfiler(elfInfo);
        }
    }
}

#include <algorithm>

void C2R::ProcUtil::Profiler::SetupElfProfiler(ELF_Parser::Info &elfInfo)
{
    ProfileInfo * pi;

    auto &symTable = elfInfo.symbolTable;
    auto &sectionTable = elfInfo.sectionTable;
    auto *table = elfInfo.symbolTable.symTable;
    std::vector<ELF_Parser::Elf_Sym*> symbolList;
    /// 9/18/17
    ProfileInfo::maxProfileFuncNameLength = 0;
    for (size_t i = 0, e = symTable.symbolCount; i < e; ++i) {
        auto &sym = table[i];
        auto *shndxVal = ELF_Parser::shnumGroup.GetValue(sym.st_shndx);
        if (shndxVal || sym.st_name_str[0] == 0) { continue; }
        auto *shdr = &sectionTable.shdr[sym.st_shndx];
        if (shdr->field.executable) {
            symbolList.push_back(&sym);
        }
    }
    /// 7/1/20
    profileInfoArray.clear();
    profileInfoArray.resize(symbolList.size() + 1);
    profileInfoStack.resize(symbolList.size() + 1);
    std::sort(symbolList.begin(), symbolList.end(), ELF_Parser::CompareSymbol);
    ELF_Parser::Elf_Sym *sym;
    sym = symbolList[0];
    if (sym->st_value != 0) {
        pi = &profileInfoArray[profileCount++];
        /// 7/1/20
        pi->name = "_entry";
        pi->sectName = "_boot";
        pi->entryAddr = 0;
    }
    for (size_t i = 0, e = symbolList.size(); i < e; ++i) {
        sym = symbolList[i];
        auto *shdr = &sectionTable.shdr[sym->st_shndx];
        /// 7/1/20
        pi = &profileInfoArray[profileCount++];
        pi->entryAddr = (unsigned)sym->st_value;
        pi->name = sym->st_name_str;
        /// 7/1/20
        pi->sectName = shdr->sh_name_str;
    }
    ///	9/18/17
    ProfileInfo::maxProfileFuncNameLength = 0;
    for (int i = 0; i < profileCount; ++i) {
        pi = &profileInfoArray[i];
        /// 7/1/20
        size_t len = pi->name.size();
        if (ProfileInfo::maxProfileFuncNameLength < len) {
            ProfileInfo::maxProfileFuncNameLength = len;
        }
    }
    for (int i = 0; i < profileCount; ++i) {
        pi = &profileInfoArray[i];
        /// 7/1/20
        printf("ElfProfile (%*s): addr = %08x : %s\n", (int)ProfileInfo::maxProfileFuncNameLength,
            pi->name.c_str(), pi->entryAddr, pi->sectName.c_str());
    }
    ProfilerStart();
}


size_t C2R::ProcUtil::ProfileInfo::maxProfileFuncNameLength = 0;
#define MAX_LINE 1000

int C2R::ProcUtil::check_code(const char * code, unsigned int * val)
{
    int v = 0;
    if (strlen(code) != 8) { return 0; }
    while (*code) {
        int d = *code - '0';
        int h = *code - 'a';
        v <<= 4;
        if (d >= 0 && d <= 9) { v += d; }
        else if (h >= 0 && h <= 6) { v += h + 10; }
        else { return 0; }
        code++;
    }
    *val = v;
    return 1;
}

void C2R::ProcUtil::Profiler::PushProfileInfoInstance(ProfileInfo * pi)
{
    if (sp > (int)profileInfoStack.size()) {
        size_t sz = profileInfoStack.size();
        C2R_ASSERT(sz);
        profileInfoStack.resize(sz + sz / 2);
        printf("profileInfoStack.size : %d --> %d\n", (int)sz, (int)(sz + sz / 2));
    }
    curProfInfo = &profileInfoStack[sp++];
    curProfInfo->pi = pi;
    curProfInfo->cycles = 0;
    curProfInfo->subCycles = 0;
    if (sp == 1) { pi->calls++; }
}

void C2R::ProcUtil::Profiler::PopProfileInfoInstance()
{
    ProfileInfoInstance * pii;
    C2R_ASSERT(sp > 0);
    pii = &profileInfoStack[--sp];
    pii->pi->cycles += pii->cycles;
    pii->pi->subCycles += pii->subCycles;
    if (sp > 0) {
        curProfInfo = &profileInfoStack[sp - 1];
        curProfInfo->subCycles += pii->cycles + pii->subCycles;
    }
    else {
        curProfInfo = 0;
        pii->pi->cycles--; /// adjusting last executed instruction count
    }
}

void C2R::ProcUtil::Profiler::ProfilerPush(unsigned int targetAddr)
{
    //	if (!profileEnabled) { return; }
    int ub = profileCount, lb = 0;
    ProfileInfo * piTarget = 0;
    while (1) {
        int idx = (ub + lb) >> 1;
        unsigned int piAddr;
        ProfileInfo * pi = &profileInfoArray[idx];
        piAddr = pi->entryAddr;
        if (piAddr == targetAddr) { piTarget = pi; break; }
#if 1	///	9/18/17 : sometimes targetAddr function may not be registered (internal call)... simply ignore this
        if (piAddr < targetAddr) {
            if (lb == idx) {
                extraCalls++;
                return; /// targetAddr is not registered in profileInfoArray
            }
            lb = idx;
        }
#else
        else if (piAddr < targetAddr) { lb = idx; }
#endif
        else { ub = idx; }
        if (ub == lb) { break; }
    }
    C2R_ASSERT(piTarget != 0);
    if (piTarget) { piTarget->calls++; }
#if 1   /// 7/1/20
    PushProfileInfoInstance(piTarget);
#else
    if (piTarget && sp < MAX_PROFILE_FUNC_COUNT) { PushProfileInfoInstance(piTarget); }
    else { extraCalls++; }
#endif
}

void C2R::ProcUtil::Profiler::ProfilerPop()
{
    if (extraCalls) { extraCalls--; }
    else { PopProfileInfoInstance(); }
}

void C2R::ProcUtil::Profiler::ProfilerUpdate()
{
    if (curProfInfo) { curProfInfo->cycles++; }
}

void C2R::ProcUtil::Profiler::ProfilerSkip()
{
    skippedCycles++;
}

void C2R::ProcUtil::Profiler::ProfilerStart()
{
    int i;
    C2R_ASSERT(profileCount > 0);
    //	profileEnabled = 1;
    printf("Profiler:\n");
    ProfileInfo::maxProfileFuncNameLength = 0;
    for (i = 0; i < profileCount; i++) {
        ProfileInfo * pi = &profileInfoArray[i];
        /// 7/1/20
        size_t len = pi->name.size();
        if (ProfileInfo::maxProfileFuncNameLength < len) { ProfileInfo::maxProfileFuncNameLength = len; }
    }
    for (i = 0; i < profileCount; i++) {
        ProfileInfo * pi = &profileInfoArray[i];
        /// 7/1/20
        printf("%*s [%05x]\n", (int)ProfileInfo::maxProfileFuncNameLength, pi->name.c_str(), pi->entryAddr);
        pi->cycles = 0;
        pi->subCycles = 0;
        pi->calls = 0;
    }
    InitializeProfileInfoInstance();
}
void C2R::ProcUtil::Profiler::InitializeProfileInfoInstance() {
    sp = 0;
    extraCalls = 0;
    profileInfoArray[0].calls = 0;
    PushProfileInfoInstance(&profileInfoArray[0]);
}

char tmp_print_format[1000];

static char * print_format_double_string(double val)
{
    char s[20];
    int digits, i, ii = 0;
    if (val > TOO_BIG_DOUBLE) { sprintf(tmp_print_format, "%.3f (too big to format...)", val); return tmp_print_format; }
    sprintf(s, "%.3f", val);
    digits = (int)strlen(s) - 4;
    for (i = 0; i < digits; i++) {
        tmp_print_format[ii++] = s[i];
        if ((digits - i) % 3 == 1 && i < digits - 1) { tmp_print_format[ii++] = ','; }
    }
    tmp_print_format[ii] = 0;
    return tmp_print_format;
}

static char * print_format_int_string(long val)
{
    char s[20];
    int digits, i, ii = 0;
    sprintf(s, "%ld", val);
    digits = (int)strlen(s);
    for (i = 0; i < digits; i++) {
        tmp_print_format[ii++] = s[i];
        if ((digits - i) % 3 == 1 && i < digits - 1) { tmp_print_format[ii++] = ','; }
    }
    tmp_print_format[ii] = 0;
    return tmp_print_format;
}

void C2R::ProcUtil::ProfileInfo::PrintProfileInfo(FILE * fp, double totalCycles)
{
    ///	9/18/17
    /// 7/1/20
    fprintf(fp, "%*s [%05x]: calls = ", (int)ProfileInfo::maxProfileFuncNameLength, name.c_str(), entryAddr);
    fprintf(fp, "%9s", print_format_int_string(calls));
    fprintf(fp, ", clks(acc) = ");
    fprintf(fp, "%13s", print_format_int_string(cycles + subCycles));
    fprintf(fp, " [%7.3f%%], clks(local) = ", (cycles + subCycles) * 100.0 / totalCycles);
    fprintf(fp, "%13s", print_format_int_string(cycles));
    fprintf(fp, " [%7.3f%%]", (cycles) * 100.0 / totalCycles);
    fprintf(fp, ", clks/call = %13s\n", calls != 0 ? print_format_int_string(cycles / calls) : 0);
}

bool C2R::ProcUtil::ProfileInfo::CompareProfile(ProfileInfo *a, ProfileInfo *b) {
    return a->cycles + a->subCycles < b->cycles + b->subCycles;
}

void C2R::ProcUtil::Profiler::ProfilerPrint()
{
    int i, e;
    double activeCycles = 0;
    double totalCycles = 0;
    double skippedCycles1 = 0;
    //	profileEnabled = 1;
#if 1
    while (sp > 0) {
        ProfilerPop();
    }
#else
    PopProfileInfoInstance();
#endif
    printf("Profiler result(%d):\n", id);
    for (i = 0; i < profileCount; i++) { activeCycles += profileInfoArray[i].cycles; }
    skippedCycles1 = (double)skippedCycles;
    totalCycles = activeCycles + skippedCycles1;
    if (activeCycles == 0.0) { activeCycles = 1.0; } /// avoid divide-by-zero

    fprintf(stdout, "total cycles = %13s", print_format_double_string(totalCycles));
    fprintf(stdout, " (active: %s, ", print_format_double_string(activeCycles));
    fprintf(stdout, "sleep: %s) ", print_format_double_string(skippedCycles1));
    double active_percent = (activeCycles / totalCycles) * 100;
    fprintf(stdout, "(active: %3.2f%%, sleep: %3.2f%%) \n\n", active_percent, 100 - active_percent);

    if (fp_prof) {
        fprintf(fp_prof, "total cycles = %13s", print_format_double_string(totalCycles));
        fprintf(fp_prof, " (active: %s, ", print_format_double_string(activeCycles));
        fprintf(fp_prof, "sleep: %s) ", print_format_double_string(skippedCycles1));
        fprintf(fp_prof, "(active: %3.2f%%, sleep: %3.2f%%) \n\n", active_percent, 100 - active_percent);
    }
    std::vector<ProfileInfo*> profList;// (1);
    for (i = 0; i < profileCount; i++) {
        if (profileInfoArray[i].calls) {
            profList.push_back(&profileInfoArray[i]);
        }
    }
    //profList.Sort(ProfileInfo::CompareProfile);
    std::sort(profList.begin(), profList.end(), ProfileInfo::CompareProfile);
    for (i = 0, e = (int)profList.size(); i < e; ++i) {
        profList[i]->PrintProfileInfo(stdout, activeCycles);
        if (fp_prof) {
            profList[i]->PrintProfileInfo(fp_prof, activeCycles);
        }
    }
}
