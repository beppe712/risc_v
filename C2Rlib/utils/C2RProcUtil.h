/*
Copyright 2016 New System Vision Research and Development Institute.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#if !defined(C2R_PROC_UTIL_H)
#define C2R_PROC_UTIL_H

#include "ELFUtil.h"
#include <vector>

namespace C2R {
    struct ProcUtil {
        static const char *elfFileName;
        static int check_code(const char * code, unsigned int * val);
        static void openMemDumpFile(int type, FILE **fp, int *segmentCount, int *printfAddrWidth, int *printfDataWidth);
        static void install_program_elf(unsigned * dmem, unsigned * pmem, unsigned dmem_size, unsigned pmem_size,
            unsigned *pc, unsigned *sp, int memDumpFlag, int pc_offset = 0, int skipProfile = 0,
            char * file_name = (char *)elfFileName);
#if 0   /// 7/1/20
#define MAX_PROFILE_FUNC_NAME_LENGTH	60
#define MAX_SECTION_NAME_LENGTH			10
#define MAX_PROFILE_FUNC_COUNT	        1000
#endif
        struct ProfileInfo {
            unsigned int entryAddr;
            long cycles, subCycles, calls;
            /// 7/1/20
            std::string name, sectName;
            static size_t maxProfileFuncNameLength;
            static bool CompareProfile(ProfileInfo *a, ProfileInfo *b);
            void PrintProfileInfo(FILE * fp, double totalCycles);
        };
        struct ProfileInfoInstance {
            long cycles, subCycles;
            ProfileInfo * pi;
        };
        struct Profiler {
            /// 7/1/20
            std::vector<ProfileInfo> profileInfoArray;
            std::vector<ProfileInfoInstance> profileInfoStack;
            int profileCount, sp, extraCalls;
            ProfileInfoInstance * curProfInfo;
            long skippedCycles;
            int id;
            /// 7/1/20
            Profiler(int idx);
            void SetupProfiler(const char * prog_file_name);
            void SetupElfProfiler(ELF_Parser::Info &elfInfo);
            void ProfilerPush(unsigned int targetAddr);
            void ProfilerPop();
            void ProfilerUpdate();
            void ProfilerSkip();
            void ProfilerStart();
            void InitializeProfileInfoInstance();
            void ProfilerPrint();
            void PushProfileInfoInstance(ProfileInfo * pi);
            void PopProfileInfoInstance();
        };
        struct ProfilerGroup {
            std::vector<Profiler> prof;
            Profiler *curProf;
            ProfilerGroup() : curProf(NULL) {}
            void ProfilerPush(unsigned int targetAddr) { if (curProf) { curProf->ProfilerPush(targetAddr); } }
            void ProfilerPop() { if (curProf) { curProf->ProfilerPop(); } }
            void ProfilerUpdate() { if (curProf) { curProf->ProfilerUpdate(); } }
            void ProfilerSkip() { if (curProf) { curProf->ProfilerSkip(); } }
            void ProfilerPrint() { if (curProf) { curProf->ProfilerPrint(); } }
            void ProfilerPrint(int idx) {
                if (idx < (int)prof.size()) { prof[idx].ProfilerPrint(); }
            }
            void SetupProfiler(const char * prog_file_name) {
                prof.emplace_back((int)prof.size());
                curProf = &prof.back();
                curProf->SetupProfiler(prog_file_name);
                for (size_t i = 0, e = prof.size(); i < e; ++i) { prof[i].InitializeProfileInfoInstance(); }
            }
            void SetupElfProfiler(ELF_Parser::Info &elfInfo) {
                prof.emplace_back((int)prof.size());
                curProf = &prof.back();
                curProf->SetupElfProfiler(elfInfo);
                for (size_t i = 0, e = prof.size(); i < e; ++i) { prof[i].InitializeProfileInfoInstance(); }
            }
            void SetProfiler(int idx) {
                if (idx < (int)prof.size()) { curProf = &prof[idx]; }
                else { curProf = NULL; }
            }
        };
        static Profiler *prof;
        static FILE *fp_prof;
    };
};





#define USE_ELF_FILE

#if !defined(__LLVM_C2RTL__)
#define PROFILER_CALL(pc)	            C2R::ProcUtil::prof->ProfilerPush(pc);
#define PROFILER_CALL_COND(pc, cond)	do { if (cond) C2R::ProcUtil::prof->ProfilerPush(pc); } while(0)
#define PROFILER_RETURN		            C2R::ProcUtil::prof->ProfilerPop();
#define PROFILER_UPDATE		            C2R::ProcUtil::prof->ProfilerUpdate();
#define PROFILER_SKIP		            C2R::ProcUtil::prof->ProfilerSkip(); // for sleep support through wait instruction
#define PROFILER_PRINT		            C2R::ProcUtil::prof->ProfilerPrint();

#else
#define PROFILER_CALL(pc)
#define PROFILER_CALL_COND(pc, cond)
#define PROFILER_RETURN
#define PROFILER_UPDATE
#define PROFILER_START
#define PROFILER_PRINT

#endif

#endif //#if !defined(C2R_PROC_UTIL_H)
