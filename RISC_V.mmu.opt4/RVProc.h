
#if 1   /// 7/1/20
#include "../C2Rlib/utils/C2RUtil.h"
#include "../C2Rlib/utils/C2RProcUtil.h"
#else
#include "../CPP_utils/TCTUtil.h"
#endif
#include "RVProcArch.h"


//extern CPU cpu;

//void monitor_exception();

//void io_fsm(IO * io, UINT7 dc_reg_op, BIT active, UINT32 src0, UINT32 io_com, FIFO_PORT * dev_fifo_port, AXI4L::CH *axi);

void exception_message(int vec);
void sc_message();
void mts_message(UINT32 dst_id, UINT32 ex_out);

const char * insn_name();
void show_cpu();
int check_code(const char * code, unsigned int * val);
//void install_program(UINT32 * dmem, UINT32 * pmem, int setProfilerFlag);
//void set_profile_range(const char * func_name, long * min_addr, long * max_addr);

void set_output(UINT10 sw, UINT16 * out16, UINT7 * hex0, UINT7 * hex1, UINT7 * hex2, UINT7 * hex3);
void set_hex_output(UINT16 data, UINT7 * hex0, UINT7 * hex1, UINT7 * hex2, UINT7 * hex3);
UINT4 detect_key_input(UINT4 key, UINT4 * prev_key);

#define INV7(val)		(0x7f & ~(val))

#define INIT_PIPE_CTRL(pctl, stall)			pctl.stall_this = 0; pctl.stalled_flag = stall

#if defined(USE_RVINSN_DEC)
#define GET_BITS(data, msb, bw)	    (((data) >> ((msb) - (bw) + 1)) & ((1 << (bw)) - 1))
/// RISC-V
#define GET_RV_FLD(fid)     GET_BITS(ir, RVFLD[RVF_##fid].msb, RVFLD[RVF_##fid].bw)
#define RV_FLD(fid)         fid = GET_RV_FLD(fid)
#define RV_FLD_2(fid)       insn.fid = GET_RV_FLD(fid)
#elif 1
#define GET_BITS(data, msb, bw)	    (((data) >> ((msb) - (bw) + 1)) & ((1 << (bw)) - 1))
/// RISC-V
#define GET_RV_FLD(fid)     GET_BITS(ir, RVFLD[RVF_##fid].msb, RVFLD[RVF_##fid].bw)
#define RV_FLD(fid)         insn.fid = GET_RV_FLD(fid)
#define RV_FLD_2(fid)       fid = GET_RV_FLD(fid)
#else
#define GET_BITS(data, msbPos, bitLength)	(((data) >> ((msbPos) - (bitLength) + 1)) & ((1 << (bitLength)) - 1))
/// RISC-V
#define RV_GET_FIELD(fid)					(GET_BITS(ir, RVFLD[fid].msbPos, RVFLD[fid].bitLength) + RVFLD[fid].codeOffset)
#define RV_DEC_FLD(fid)						insn.fid = RV_GET_FIELD(RVF_##fid)
#endif

#if 1
#define FE_STALL	(dc.stall	| DC_STALL)
#define DC_STALL	(ex.stall	| EX_STALL)
#define EX_STALL	(me.stall | ME_STALL)
#define ME_STALL	(wb.stall | WB_STALL)
#define WB_STALL	(0)
#else
#define FE_STALL	(dc.pctl.stall_this	| DC_STALL)
#define DC_STALL	(ex.pctl.stall_this	| EX_STALL)
#define EX_STALL	(me.pctl.stall_this | ME_STALL)
#define ME_STALL	(wb.pctl.stall_this | WB_STALL)
#define WB_STALL	(0)
#endif

#define FE_SW_MODE	(dc.sw_mode | DC_SW_MODE)
#define DC_SW_MODE	(ex.sw_mode | EX_SW_MODE)
#define EX_SW_MODE	(me.sw_mode | ME_SW_MODE)
#define ME_SW_MODE	(wb.sw_mode | WB_SW_MODE)
#define WB_SW_MODE	(0)

#define FW_DC(PIPE, ID)		(PIPE.dst.valid && PIPE.dst.id == ID) ? PIPE.dst.out
#define SET_FB_DST(PIPE, PIPE_REF, OUT)		\
PIPE.dst.valid	= PIPE_REF##_r.dst_f;	\
PIPE.dst.id	= PIPE_REF##_r.dst_id;	\
PIPE.dst.out	= OUT
#define FW_EX(PIPE, IDX)		\
(PIPE.dst.valid && dc.r.src_f[IDX] && PIPE.dst.id == dc.r.src_id[IDX]) ? PIPE.dst.out

// for interrupt handling
#if defined(USE_CSR_STATUS_REG)
#define DF_CSR(addr, data)                                              \
	((me.csrc.we && (me.csrc.waddr == (addr))) ? me.csrc.wdata :        \
	((wb.csrc.we && (wb.csrc.waddr == (addr))) ? wb.csrc.wdata : data))
#else
#define DF_CSR(addr, data) \
	((me_stt.csr_we && (me_stt.csr_waddr == (addr)))?me_stt.csr_wdata: \
		((wb_stt.csr_we && (wb_stt.csr_waddr == (addr)))?wb_stt.csr_wdata:data))
#endif
#define	DF_MSTATUS	DF_CSR(CSR_mstatus, csr.mstatus)
#define	DF_MIE		DF_CSR(CSR_mie, csr.mie)
//#define	IRQ		    _irq(fb_mode, DF_MSTATUS, csr.mip & DF_MIE)

static inline UINT32 _irq(UINT2 mode, UINT32 mstatus, UINT32 intr) {
	switch (mode) {
	case 0:
		if (mstatus & 0x1)
			return intr & 0xfff;
		else
			return intr & 0xeee;
	case 1:
		if (mstatus & 0x2)
			return intr & 0xeee;
		else
			return intr & 0xccc;
	case 2:
		if (mstatus & 0x4)
			return intr & 0xccc;
		else
			return intr & 0x888;
	}
	if (mstatus & 0x8)
		return intr & 0x888;
	else
		return 0;
}

//#include "C2RTL_ProjInfo.h"

//#define C2ANY_DIR	"../../../../C2Any/"

#if !defined __LLVM_C2RTL__
#define C2R_DISABLE_USER_DISPLAY
#define C2R_DISABLE_VECTOR_OUTPUT
#endif

