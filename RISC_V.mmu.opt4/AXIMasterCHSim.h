#define DISABLE_VDISK

#if !defined(DISABLE_VDISK)
extern unsigned long vdisk_read(unsigned long addr);
extern unsigned long vdisk_write(unsigned long addr, unsigned long data);
#endif

#define WRITE_FILE

struct AXIMasterCHSim {
	AXI4L::CH	m_ch;

	unsigned int uart_status;
#define UART_BUF_SIZE	60
	char wbuf[UART_BUF_SIZE];
	int wbuf_cnt;
	char rbuf[UART_BUF_SIZE];
	int rbuf_wptr;
	int rbuf_rptr;
	int wflush_timer;
	bool no_header;
	bool no_msg;
#if defined (WRITE_FILE)
	FILE *fptr;
#endif

	unsigned int *raddr;
	unsigned int *waddr;

    void Reset(bool msg_flag) {
        uart_status = 0;
        wbuf_cnt = 0;
        rbuf_wptr = 0;
        rbuf_rptr = 0;
	wflush_timer = 0;
        no_header = msg_flag;
	no_msg = msg_flag;
        raddr = 0;
        waddr = 0;
        m_ch.resetSL(); m_ch.resetMA();
    }

	void KeyIn(char ch) {
		int next_rbuf_wptr = rbuf_wptr + 1;
		if (next_rbuf_wptr >= UART_BUF_SIZE)
			next_rbuf_wptr = 0;

		if (next_rbuf_wptr != rbuf_rptr) {
			rbuf[rbuf_wptr] = ch;
			rbuf_wptr = next_rbuf_wptr;
		}
	}

	void Read_Common(AXI4L::CH& io, ExtMem& xmem) {
		if (io.raddr.m.valid) {
			m_ch.raddr.m = io.raddr.m;
			if (io.raddr.m.addr & 0x80000000) {
				raddr = xmem.get_page(io.raddr.m.addr & 0x7fffffff)->m + (io.raddr.m.addr & 0xfff) / 4;
			}
		}

		if (m_ch.raddr.m.valid && io.rdat.m.ready) {

			switch (m_ch.raddr.m.addr) {
			case 0x10000000:
				if (rbuf_rptr != rbuf_wptr) {
					io.rdat.s.data = rbuf[rbuf_rptr++];
					if (rbuf_rptr >= UART_BUF_SIZE)
						rbuf_rptr = 0;
				}
				break;
			case 0x10000004:
				// (rx_error << 4) | (rx_empty << 3) | (rx_full << 2) | (tx_empty << 1) | (tx_full);
				if (rbuf_rptr != rbuf_wptr)
					io.rdat.s.data = 0x2;
				else
					io.rdat.s.data = 0xa;
				break;
			default:
#if !defined(DISABLE_VDISK)
                if ((m_ch.raddr.m.addr >= 0x10000100) &&
				    (m_ch.raddr.m.addr < 0x10000180)) {
					io.rdat.s.data = vdisk_read(m_ch.raddr.m.addr);
				}
#endif
				if (raddr)
					io.rdat.s.data = *raddr++;
				break;
			}

			io.rdat.s.resp = AXI4L::RSP_OK;
			io.rdat.s.valid = 1;
			io.rdat.s.last = (m_ch.raddr.m.len == 0);

			if (m_ch.raddr.m.len) {
				io.rdat.s.last = 0;
				m_ch.raddr.m.len--;
			} else {
				io.rdat.s.last = 1;
				m_ch.raddr.m.valid = 0;
				raddr = 0;
			}
		} else {
			io.rdat.s.valid = 0;
		}
	}

	void Read(AXI4L::CH& io, ExtMem& xmem) {
		Read_Common(io, xmem);

		io.raddr.s.ready = io.raddr.m.valid;
	}

	void Read_RTL(AXI4L::CH& io, ExtMem& xmem) {
		Read_Common(io, xmem);

		io.raddr.s.ready = !m_ch.raddr.m.valid;
	}

	////////////////
	int Write_Common(AXI4L::CH& io, ExtMem& xmem, unsigned int cycles) {
		int intr = 0;

		if (io.waddr.m.valid) {
			m_ch.waddr.m = io.waddr.m;
			if ((io.waddr.m.addr & 0xf0000000) != 0x10000000) {
				waddr = xmem.get_page(io.waddr.m.addr & 0x7fffffff)->m + (io.waddr.m.addr & 0xfff) / 4;
			}
		}

		if (io.wdat.m.valid) {
			m_ch.wdat.m = io.wdat.m;
		}

		bool uart_flush = false;

		if (m_ch.waddr.m.valid && m_ch.wdat.m.valid && io.wres.m.ready) {
			char ch = io.wdat.m.data;

			switch (m_ch.waddr.m.addr) {
			case 0x10000000:	// uart status
				if (wbuf_cnt == 0)
					wflush_timer = cycles + 1000;
				wbuf[wbuf_cnt++] = ch;
				if (ch == '\n')
					uart_flush = true;
				break;
			case 0x10000004:	// uart status
				uart_status = io.wdat.m.data;
				break;
			default:
#if !defined(DISABLE_VDISK)
                if ((m_ch.waddr.m.addr >= 0x10000100) &&
				    (m_ch.waddr.m.addr < 0x10000180)) {
					if (vdisk_write(m_ch.waddr.m.addr, io.wdat.m.data))
						intr |= 4;
				}
#endif
				if (waddr)
					*waddr++ = io.wdat.m.data;
				break;
			}

			io.wres.s.resp = AXI4L::RSP_OK;
			if (m_ch.waddr.m.len) {
				m_ch.waddr.m.len--;
				io.wres.s.valid = 0;
			} else {
				io.wres.s.valid = 1;
				m_ch.waddr.m.valid = 0;
				waddr = 0;
			}
			m_ch.wdat.m.valid = 0;
		} else {
			io.wres.s.valid = 0;
		}

		if (uart_flush ||
		   (wbuf_cnt >= UART_BUF_SIZE) ||
		   (wbuf_cnt && (wflush_timer < cycles))) {
			if (!no_header) {
#if defined(WRITE_FILE)
  				fprintf(fptr, "<UART_X.RX> msg = %.*s", wbuf_cnt, wbuf);
#endif
				printf("[%10d] <UART_X.RX> msg = %.*s", cycles, wbuf_cnt, wbuf);
			}
			else
#if defined(WRITE_FILE)
  				fprintf(fptr, "%.*s", wbuf_cnt, wbuf);
#endif
				printf("%.*s", wbuf_cnt, wbuf);
			wbuf_cnt = 0;
			no_header = !uart_flush | no_msg;
		}

		return intr;
	}

	unsigned Write(AXI4L::CH& io, ExtMem& xmem, unsigned int cycles) {
		unsigned intr = 0;

		intr = Write_Common(io, xmem, cycles);

		io.waddr.s.ready = io.waddr.m.valid;
		io.wdat.s.ready = io.wdat.m.valid;

		return intr;
	}

	unsigned Write_RTL(AXI4L::CH& io, ExtMem& xmem, unsigned int cycles) {
		unsigned intr = 0;

		intr = Write_Common(io, xmem, cycles);

		io.waddr.s.ready = !m_ch.waddr.m.valid;
		io.wdat.s.ready = !m_ch.wdat.m.valid;

		return intr;
	}

	static void DumpRead(const char *header, AXI4L::CH& io) {
		printf("%s: raddr 0x%08x-%d-%d-%02d-%d %d\n",
				header,
				io.raddr.m.addr,
				io.raddr.m.size,
				io.raddr.m.valid,
				io.raddr.m.len,
				io.raddr.m.prot,
				io.raddr.s.ready);
		printf("%s: rdat %d 0x%08x-%d-%d-%d\n",
				header,
				io.rdat.m.ready,
				io.rdat.s.data,
				io.rdat.s.resp,
				io.rdat.s.valid,
				io.rdat.s.last);
	}
	static void DumpWrite(const char *header, AXI4L::CH& io) {
		printf("%s: waddr 0x%08x-%d-%d-%02d-%d %d\n",
				header,
				io.waddr.m.addr,
				io.waddr.m.size,
				io.waddr.m.valid,
				io.waddr.m.len,
				io.waddr.m.prot,
				io.waddr.s.ready);
		printf("%s: wdat 0x%08x-0x%x-%d-%d %d\n",
				header,
				io.wdat.m.data,
				io.wdat.m.strobe,
				io.wdat.m.valid,
				io.wdat.m.last,
				io.wdat.s.ready);
		printf("%s: wres %d %d-%d\n",
				header,
				io.wres.m.ready,
				io.wres.s.resp,
				io.wres.s.valid);
	}
};
