#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdio.h>
#ifdef __linux__
#include <unistd.h>
#else
#include <io.h>
#define	open	_open
#define	close	_close
#define	lseek	_lseek
#define	write	_write
#define	read	_read
#endif // __linux__
#include "RVProc.h"
#include "ExtMem.h"

extern ExtMem	xmem;

static struct reg {
	unsigned long status;
	unsigned long command;
	unsigned long capacity;
	unsigned long blk_addr;
	unsigned long blk_count;
	struct {
		unsigned long addr;
		unsigned long count;
	} dma[16];
} reg;

struct VDisk {
	int	fd;

	VDisk() {
		fd = -1;
	}
	~VDisk() {
		if (fd >= 0)
			close(fd);
	}
	void init(const char *disk_image) {
		struct stat sb;
		fd = open(disk_image, O_RDWR);

		fstat(fd, &sb);
		reg.capacity = sb.st_size / 512;
	}
	void rw() {
		::lseek(fd, reg.blk_addr * 512, SEEK_SET);

		for (unsigned i = 0; i < reg.blk_count; i++) {
			unsigned addr = reg.dma[i].addr;
			unsigned count = reg.dma[i].count;
			void *rwaddr = xmem.get_page(addr & 0x7fffffff)->m +
						(addr & 0xfff) / 4;

			if (reg.command == 1)
				::read(fd, rwaddr, count);
			else if (reg.command == 2)
				::write(fd, rwaddr, count);
		}
	}
};

VDisk vd;

/// vdisk
void vdisk_init(const char *disk_image) {
	vd.init(disk_image);
}

unsigned long vdisk_read(unsigned long addr) {
	addr -= 0x10000100;

	unsigned long *lp = &reg.status + (addr / 4);

	return *lp;
}

unsigned long vdisk_write(unsigned long addr, unsigned long data) {
	addr -= 0x10000100;

	unsigned long *lp = &reg.status + (addr / 4);

	*lp = data;

	if (reg.command) {
		vd.rw();
		reg.command = 0;
		reg.status = 1;
	}

	return reg.status;
}
