
#if 0	///	9/12/14 : instruction extension
///	to support new instructions, modify /
01000_<rf0[4:0]>_<id_io[4:0]>_<op_io[1:0]>_<imm15[14:0]> : IO
///	40018373 // 009d : (01000_00000_00000_11__000_0__011__0__11__1001__1)     INTRINSIC (0x40018373 -> 0x40018373) D(xx), S0(xx), S1(xx) : uart_activate ()
///	40810000 // 000c : (01000_00010_00000_10__000_0__000__0__00__0000__0)     INTRINSIC (0x40010000 -> 0x40810000) D(xx), S0(R2), S1(xx) : uart_tx ( r2)
rf0[4:0] : 5 bits : source / dest reg - ID(0 - 31)
id_io[4:0] : 5 bits : io device id(0 - 31)
0 : UART_0
1 : UART_1
2 : I2C_0
3 : I2C_1
4 : I2S_0
5 : I2S_1
6 - 31 : reserved...
op_io[1:0] : 2 bits : io operation
0 : read_data
1 : read_status
2 : write_data
3 : write_command
imm15[14:0] : 15 bits : command parameter
UART :
imm15[0] : 1 bit : deactivate(1), deactivate(0)
imm15[4:1] : 4 bits : baud setting
0 : 50 bps
1 : 300 bps
2 : 1, 200 bps
3 : 2, 400 bps
4 : 4, 800 bps
5 : 9, 600 bps
6 : 19, 200 bps
7 : 38, 400 bps
8 : 57, 600 bps
9 : 115, 200 bps
10 - 15 : reserved(current set to 115, 200 bps)
imm15[6:5] : 2 bits : data length
0 : 5 bits
1 : 6 bits
2 : 7 bits
3 : 8 bits
imm15[7] : 1 bit : stop_bit length
0 : 1 bit
1 : 2 bit(6 / 7 / 8 - bit data), 1.5 bit(5 - bit data)
imm15[10:8] : 3 bits : parity setting
0 / 2 / 4 / 6 : no parity		->	0 : IOP_None
1 : odd parity(XNOR)	->	1 : IOP_Odd
3 : even parity(XOR)	->	3 : IOP_Even
5 : high parity(1)		->	5 : IOP_High
7 : low parity(0)		->	7 : IOP_Low
#endif

#define BUF_EMPTY(buf)	(!(buf)->not_empty)
#define BUF_FULL(buf)	((buf)->full)

#define CLK_FREQ	27000000	/// 27MHz
#define	BAUD_PERIOD(baud_rate)	((CLK_FREQ / baud_rate) - 1)	///	2812.50 --> 2812

#define ESC	0x1b	///	ESC code
#if 0
enum IO_Parity
{
	IOP_None = 0,
	IOP_Odd = 1,
	IOP_Even = 3,
	IOP_High = 5,
	IOP_Low = 7,
};

enum UART_BIT
{
	UART_BIT5 = 0,
	UART_BIT6 = 1,
	UART_BIT7 = 2,
	UART_BIT8 = 3,
};

enum UART_BAUD
{
	UART_BAUD_50 = 0,
	UART_BAUD_300 = 1,
	UART_BAUD_1200 = 2,
	UART_BAUD_2400 = 3,
	UART_BAUD_4800 = 4,
	UART_BAUD_9600 = 5,
	UART_BAUD_19200 = 6,
	UART_BAUD_38400 = 7,
	UART_BAUD_57600 = 8,
	UART_BAUD_115200 = 9,
	///	4/27/15 : added UART_BAUD_ULTRA_FAST
	UART_BAUD_ULTRA_FAST = 15,
};
#endif
enum IO_OP_STATE
{
	IOS_Idle = 0,
	IOS_Start = 1,
	IOS_Parity = 2,
	IOS_Stop = 3,
	IOS_Ack = 4,
};

enum SPI_OP_STATE
{
	SPI_S_Idle = 0,
	SPI_S_Active = 1
};

#if 0
enum SPI_COMMAND_ENUM
{
	SPI_ACK = 'a',
	SPI_NACK = 'n',
	SPI_TEXT_MODE = 't',
	SPI_BINARY_MOD = 'b',
};
#endif

struct UART_PORT
{
	IOBuf		buf;
	ST_UINT16	word;
	ST_UINT2	state;
	ST_BIT		bit_n, parity, error;
	ST_UINT4	bit_pos;
	ST_UINT16	tick;
	BIT			stalled;
	FB_BIT		irq;
	void reset_port() { state = IOS_Idle; error = 0; tick = 0; buf.reset(); }
	void activate(UINT32 w) { word = w; state = IOS_Start; bit_pos = 0; parity = 0; }
};

struct UARTPin
{
	DI_BIT tx, rx;
};

struct UART {
	UART_PORT	rx, tx;
	ST_BIT		activated, rx_bit_d_n, rx_bit_samp;
	ST_UINT16	baud_period_m1;
	ST_UINT4	bit_count_m1;
	ST_UINT2	stop_count;
	ST_UINT3	parity_type;
	ST_UINT3	pending_op;
	UINT32		data_in;
	BIT			active, stalled, data_latched;
	void fsm(BIT cancel_io_insn, UINT3 io_op_in, UINT32 src0, int devID);
	UINT32 read_status();
	void write_command(UINT15 io_param, int devID, UINT32 cycle);
	void do_tx(UINT2 io_op, UINT32 src0);
	UINT32 do_rx(UINT2 io_op);
	UINT32 adjust_word();
	BIT parity_bit(BIT parity);
	void set_pin(UARTPin *uart_pin);
};

struct SPIS_PORT  /// SPI-slave
{
	ST_UINT8	rx_buf;
	ST_UINT8	word, res_word;
	ST_UINT2	state;
	ST_BIT		bit_in_d, bit_out;
	ST_UINT4	bit_pos;
	UINT8       nxt_word;
	BIT			bit_in;   /// bit_in is unlatched input
	BIT         rx_stalled;
};

struct SPIM_PORT/// SPI-master
{
	ST_UINT8	rx_buf;
	ST_UINT8	word, res_word;
	ST_UINT2	state;
	ST_BIT		bit_in_d, bit_out;
	ST_UINT4	bit_pos;
	UINT8       nxt_word;
	BIT			bit_in;   /// bit_in is unlatched input
	BIT         rx_stalled;

	IOBuf		tx_buf;
	ST_BIT		bit_sck, error;
	ST_UINT16	tick;
	BIT			tx_stalled;
	FB_BIT		irq;
	void activate(UINT32 word, UINT32 tick_offset);
	void reset() { state = IOS_Idle; error = 0; tick = 0; tx_buf.reset(); rx_buf = 0; }
};

struct SPIPin
{
	DI_BIT miso;
	DI_BIT mosi;
	DI_BIT sck;
};

struct SPIM
{
	SPIM_PORT	tx_rx;
	ST_BIT		activated;
	ST_UINT16	baud_period_m1;
	ST_UINT4	bit_count; // fixed to 8
	ST_BIT			clock_polarity;
	ST_BIT			clock_phase;
	ST_BIT			interrupt_enable;
	ST_UINT3	pending_op;
	UINT32		data_in;
	BIT			active, stalled, data_latched;
	void fsm(BIT cancel_io_insn, UINT3 io_op_in, UINT32 src0);
	void do_tx(UINT2 io_op, UINT32 src0);
	UINT32 do_rx(UINT2 io_op, BIT signedChar);
	void write_command(UINT15 io_param, UINT32 cycle);
	UINT32 read_status();
	void set_pin(SPIPin *spi);
};

enum SPI_PROTOCOL_STATE_ENUM
{
	SPI_PS_INIT,
	SPI_PS_CMD,
	SPI_PS_BIN_OUT,
	SPI_PS_BIN_IN,
	SPI_PS_TEXT_OUT,
	SPI_PS_TEXT_PENDING,
};



struct UART_AXI4L : AXI4L::SlaveFSM {
	UART uart;
	BIT devRead(UINT32 *data, UINT32 addr, UINT3 size);
	BIT devWrite(UINT32 data, UINT32 addr, UINT3 size);
	void fsmUser();
#if defined(TEST_VIRTUAL_METHOD_C2R_FUNC)
	_C2R_FUNC(1)
		void step(AXI4L::CH *axi, UARTPin *uart_pin) {
		INIT_VIRTUAL_BUS_SLAVE(*this, axi)
			uart.set_pin(uart_pin);
		fsm(axi);
	}
#endif
};

struct SPIM_AXI4L : AXI4L::SlaveFSM { /// SPI-master
	SPIM spim;
	BIT devRead(UINT32 *data, UINT32 addr, UINT3 size);
	BIT devWrite(UINT32 data, UINT32 addr, UINT3 size);
	void fsmUser();
#if defined(TEST_VIRTUAL_METHOD_C2R_FUNC)
	_C2R_FUNC(1)
		void step(AXI4L::CH *axi, SPIPin *spi_pin) {
		INIT_VIRTUAL_BUS_SLAVE(*this, axi)
			spim.set_pin(spi_pin);
		fsm(axi);
	}
#endif
};

#include "ExtMem.h"

int uart_ext(BIT tx);
void uart_ext_switch_mode();
void uart_ext_flush_rx();
void uart_ext_tx(int ch);
void uart_ext_set_mode(int mode);

void spi_ext_slave(SPIPin *spi);
void spi_slave_ext_switch_mode();
void spi_slave_ext_flush_rx(int force_flush);
