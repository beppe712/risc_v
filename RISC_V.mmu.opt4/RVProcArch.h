#if !defined(TCT_PROC_ARCH_H)
#define TCT_PROC_ARCH_H
#define TEST_METHOD_C2R_FUNC --> /// 6/22/17 : class methods cannot be C2R_FUNC yet... --> 6/26/17 : NOW IT'S OK!!!
#define TEST_VIRTUAL_METHOD_C2R_FUNC ///    6/27/17 : probably ng yet... --> now it's ok...

#include <stdio.h>
#include <stdlib.h>
#include <string.h>


#define TEST_LLVM
#if defined(__LLVM_C2RTL__)
#define _BW(N)		__attribute__((C2RTL_bit_width(N)))
#define _BWT(N)     __attribute__((C2RTL_bit_width(#N)))
#define _T(N)		__attribute__((C2RTL_type(N)))
#define _C2R_FUNC(N)    __attribute__((C2RTL_function(N)))
#define _C2R_MODULE_    __attribute__((C2RTL_module))
#else
#define _BW(N)
#define _BWT(N)
#define _T(N)
#define _C2R_FUNC(N)
#define _C2R_MODULE_
#endif

//#define TEST_MPSOC
#define CPU_CPP


typedef unsigned char	UINT8 _BW(8), BIT _BW(1), UINT2 _BW(2), UINT3 _BW(3), UINT4 _BW(4), UINT5 _BW(5), UINT6 _BW(6), UINT7 _BW(7);
typedef BIT		FB_BIT, ST_BIT _T(state), DI_BIT _T(direct_signal), DO_BIT _T(direct_signal), D_BIT _T(direct_signal);

typedef UINT2		ST_UINT2 _T(state);
typedef UINT3		FB_UINT3, ST_UINT3 _T(state);
typedef UINT4		ST_UINT4 _T(state);
typedef UINT5		FB_UINT5, ST_UINT5 _T(state);
typedef UINT6		FB_UINT6, ST_UINT6 _T(state);
typedef UINT7		DO_UINT7 _T(direct_signal), ST_UINT7 _T(state);
typedef UINT8		ST_UINT8 _T(state), M_UINT8 _T(memory);
typedef signed char	SINT8 _BW(8);
typedef unsigned short	UINT16 _BW(16), UINT10 _BW(10), UINT12 _BW(12), UINT13 _BW(13), UINT15 _BW(15);
typedef UINT10		DI_UINT10 _T(direct_signal), ST_UINT10 _T(state), M_UINT10 _T(memory);
typedef UINT12		ST_UINT12 _T(state);
typedef UINT13		ST_UINT13 _T(state);
typedef UINT15		ST_UINT15 _T(state), M_UINT15 _T(memory);
typedef UINT16		FB_UINT16, M_UINT16 _T(memory), MX_UINT16 _T(memory), MXD_UINT16 _T(memory);
typedef UINT16		DO_UINT16 _T(direct_signal), ST_UINT16 _T(state);
typedef signed short	SINT16 _BW(16), SINT10 _BW(10), SINT12 _BW(12), SINT14 _BW(14);
typedef SINT14		ST_SINT14 _T(state);
typedef SINT16		ST_SINT16 _T(state), M_SINT16 _T(memory);
typedef unsigned int	UINT32 _BW(32), UINT18 _BW(18), UINT19 _BW(19), UINT20 _BW(20), UINT21 _BW(21), UINT24 _BW(24);
typedef UINT18		ST_UINT18 _T(state);
typedef UINT19		ST_UINT19 _T(state), M_UINT19 _T(memory);
typedef UINT20		ST_UINT20 _T(state);
typedef UINT21		M_UINT21 _T(memory);
typedef UINT24		ST_UINT24 _T(state);
typedef UINT32		M_UINT32 _T(memory), MI_UINT32 _T(memory_init), ST_UINT32 _T(state), RF_UINT32 _T(state), FB_UINT32, FC_UINT32 _T(force_bit_width), PR_UINT32 _T(state);
typedef signed int	SINT32 _BW(32), SINT17 _BW(17), SINT19 _BW(19), SINT20 _BW(20), SINT27 _BW(27);
typedef SINT19		ST_SINT19 _T(state);
typedef SINT20		ST_SINT20 _T(state), M_SINT20 _T(memory);
typedef SINT32		FC_SINT32 _T(force_bit_width), ST_SINT32 _T(state);

typedef unsigned long long UINT64 _BW(64);
typedef UINT64		ST_UINT64 _T(state);

typedef signed long long SINT64 _BW(64);
typedef SINT64		ST_SINT64 _T(state);

//#pragma _TCT_verilog_state				FEReg DCReg EXReg DIVReg PR_UINT32

typedef unsigned short	TEST15 _BW(16) _T(state); /// here, bit_width is ignored
_T(state) typedef unsigned int	TEST_A, TEST_B, TEST_C;	/// here, all typedefs will have state attribute

#if 0   /// RISC-V
RISC - V ISA format :

    R - type :
    funct7[31:25], rs2[24:20], rs1[19:15], funct3[14:12], rd[11:7], opcode[6:0]
    R4 - type : (FMADD.S, FMSUB.S, FNMSUB.S, FNMADD.S)
    rs3[31:27], funct2[26:25], rs2[24:20], rs1[19:15], funct3[14:12], rd[11:7], opcode[6:0]
    I - type :
    imm11_0[31:20], rs1[19:15], funct3[14:12], rd[11:7], opcode[6:0]
    S - type :
    imm11_5[31:25], rs2[24:20], rs1[19:15], funct3[14:12], imm4_0[11:7], opcode[6:0]
    SB - type :
    imm12.10_5[31:25], rs2[24:20], rs1[19:15], funct3[14:12], imm4_1.11[11:7], opcode[6:0]
    U - type :
    imm31_12[31:12], rd[11:7], opcode[6:0]
    UJ - type :
    imm20.10_1.11.19_12[31:12], rd[11:7], opcode[6:0]

		102e0 : 00008067          	ret
		imm11_0[31:20]	= 0000 0000 0000, 
		rs1[19:15]		= 0000 1, 
		funct3[14:12]	= 000, 
		rd[11:7]		= 0000 0, 
		opcode[6:0]		= 110 0111


    RV32I(Base Instruction Set) :
    LUI(U)      : opcode(0110111) : Load Upper Immediate
    AUIPC(U)    : opcode(0010111) : Add Upper Immediate to pc
    JAL(UJ)     : opcode(1101111)
    JALR(I)     : opcode(1100111), funct3(000)
    BEQ(SB)     : opcode(1100011), funct3(000)
    BNE(SB)     : opcode(1100011), funct3(001)
    BLT(SB)     : opcode(1100011), funct3(100)
    BGE(SB)     : opcode(1100011), funct3(101)
    BLTU(SB)    : opcode(1100011), funct3(110)
    BGEU(SB)    : opcode(1100011), funct3(111)
    LB(I)       : opcode(0000011), funct3(000)
    LH(I)       : opcode(0000011), funct3(001)
    LW(I)       : opcode(0000011), funct3(010)
    LBU(I)      : opcode(0000011), funct3(100)
    LHU(I)      : opcode(0000011), funct3(101)
    SB(S)       : opcode(0100011), funct3(000)
    SH(S)       : opcode(0100011), funct3(001)
    SW(S)       : opcode(0100011), funct3(010)
    ADDI(I)     : opcode(0010011), funct3(000)
    SLTI(I)     : opcode(0010011), funct3(010) : Set Less Than Immediate
    SLTIU(I)    : opcode(0010011), funct3(011) : Set Less Than Immediate
    XORI(I)     : opcode(0010011), funct3(100)
    ORI(I)      : opcode(0010011), funct3(110)
    ANDI(I)     : opcode(0010011), funct3(111)
    SLLI(I)     : opcode(0010011), funct3(001), imm11_5(0000000), imm4_0(shamt)
    SRLI(I)     : opcode(0010011), funct3(101), imm11_5(0000000), imm4_0(shamt)
    SRAI(I)     : opcode(0010011), funct3(101), imm11_5(0100000), imm4_0(shamt)
    ADD(R)      : opcode(0110011), funct3(000), funct7(0000000)
    SUB(R)      : opcode(0110011), funct3(000), funct7(0100000)
    SLL(R)      : opcode(0110011), funct3(001), funct7(0000000)
    SLT(R)      : opcode(0110011), funct3(010), funct7(0000000)
    SLTU(R)     : opcode(0110011), funct3(011), funct7(0000000)
    XOR(R)      : opcode(0110011), funct3(100), funct7(0000000)
    SRL(R)      : opcode(0110011), funct3(101), funct7(0000000)
    SRA(R)      : opcode(0110011), funct3(101), funct7(0100000)
    OR(R)       : opcode(0110011), funct3(110), funct7(0000000)
    AND(R)      : opcode(0110011), funct3(111), funct7(0000000)
    FENCE(I)    : opcode(0001111), funct3(000), rd(00000), rs1(00000), imm11_8(0000), imm7_4(pred), imm3_0(succ)
    FENCEI(I)   : opcode(0001111), funct3(001), rd(00000), rs1(00000), imm11_8(0000), imm7_4(0000), imm3_0(0000)
    ECALL(I)    : opcode(1110011), funct3(000), rd(00000), rs1(00000), imm11_0(000000000000)
    EBREAK(I)   : opcode(1110011), funct3(000), rd(00000), rs1(00000), imm11_0(000000000001)
    CSRRW(I)    : opcode(1110011), funct3(001), imm11_0(csr)
    CSRRS(I)    : opcode(1110011), funct3(010), imm11_0(csr)
    CSRRC(I)    : opcode(1110011), funct3(011), imm11_0(csr)
    CSRRWI(I)   : opcode(1110011), funct3(101), rs1(zimm), imm11_0(csr)
    CSRRSI(I)   : opcode(1110011), funct3(110), rs1(zimm), imm11_0(csr)
    CSRRCI(I)   : opcode(1110011), funct3(111), rs1(zimm), imm11_0(csr)


    RV32M(Standard Extension) :
    MUL(R)      : opcode(0110011), funct3(000), funct7(0000001)
    MULH(R)     : opcode(0110011), funct3(001), funct7(0000001)
    MULHSU(R)   : opcode(0110011), funct3(010), funct7(0000001)
    MULHU(R)    : opcode(0110011), funct3(011), funct7(0000001)
    DIV(R)      : opcode(0110011), funct3(100), funct7(0000001)
    DIVU(R)     : opcode(0110011), funct3(101), funct7(0000001)
    REM(R)      : opcode(0110011), funct3(110), funct7(0000001)
    REMU(R)     : opcode(0110011), funct3(111), funct7(0000001)

    RV32A(Standard Extension) :
    LR.W(R4)        : opcode(0101111), funct3(010), rs3(00010), funct2(aq | rl)
    SC.W(R4)        : opcode(0101111), funct3(010), rs3(00011), funct2(aq | rl)
    AMOSWAP.W(R4)   : opcode(0101111), funct3(010), rs3(00001), funct2(aq | rl)
    AMOADD.W(R4)    : opcode(0101111), funct3(010), rs3(00000), funct2(aq | rl)
    AMOXOR.W(R4)    : opcode(0101111), funct3(010), rs3(00100), funct2(aq | rl)
    AMOAND.W(R4)    : opcode(0101111), funct3(010), rs3(01100), funct2(aq | rl)
    AMOOR.W(R4)     : opcode(0101111), funct3(010), rs3(01000), funct2(aq | rl)
    AMOMIN.W(R4)    : opcode(0101111), funct3(010), rs3(10000), funct2(aq | rl)
    AMOMAX.W(R4)    : opcode(0101111), funct3(010), rs3(10100), funct2(aq | rl)
    AMOMINU.W(R4)   : opcode(0101111), funct3(010), rs3(11000), funct2(aq | rl)
    AMOMAXU.W(R4)   : opcode(0101111), funct3(010), rs3(11100), funct2(aq | rl)

    RV32F(Standard Extension) :
    FLW(I)          : opcode(0000111), funct3(010)
    FSW(S)          : opcode(0100111), funct3(010)
    FMADD.S(R4)     : opcode(1000011), funct3(rm), funct2(00)
    FMSUB.S(R4)     : opcode(1000111), funct3(rm), funct2(00)
    FNMSUB.S(R4)    : opcode(1001011), funct3(rm), funct2(00)
    FNMADD.S(R4)    : opcode(1001111), funct3(rm), funct2(00)
    FADD.S(R)       : opcode(1010011), funct3(rm), funct7(0000000)
    FSUB.S(R)       : opcode(1010011), funct3(rm), funct7(0000100)
    FMUL.S(R)       : opcode(1010011), funct3(rm), funct7(0001000)
    FDIV.S(R)       : opcode(1010011), funct3(rm), funct7(0001100)
    FSQRT.S(R)      : opcode(1010011), funct3(rm), funct7(0101100), rs2(00000)
    FSGNJ.S(R)      : opcode(1010011), funct3(000), funct7(0010000)
    FSGNJN.S(R)     : opcode(1010011), funct3(001), funct7(0010000)
    FSGNJX.S(R)     : opcode(1010011), funct3(010), funct7(0010000)
    FMIN.S(R)       : opcode(1010011), funct3(000), funct7(0010100)
    FMAX.S(R)       : opcode(1010011), funct3(001), funct7(0010100)
    FCVT.W.S(R)     : opcode(1010011), funct3(rm), funct7(1100000), rs2(00000)
    FCVT.WU.S(R)    : opcode(1010011), funct3(rm), funct7(1100000), rs2(00001)
    FMV.X.S(R)      : opcode(1010011), funct3(000), funct7(1110000), rs2(00000)
    FEQ.S(R)        : opcode(1010011), funct3(010), funct7(1010000)
    FLT.S(R)        : opcode(1010011), funct3(001), funct7(1010000)
    FLE.S(R)        : opcode(1010011), funct3(000), funct7(1010000)
    FCLASS.S(R)     : opcode(1010011), funct3(001), funct7(1110000), rs2(00000)
    FCVT.S.W(R)     : opcode(1010011), funct3(rm), funct7(1101000), rs2(00000)
    FCVT.S.WU(R)    : opcode(1010011), funct3(rm), funct7(1101000), rs2(00000)
    FMX.S.X(R)      : opcode(1010011), funct3(rm), funct7(1111000), rs2(00000)
#endif

#define USE_RISC_V

struct Field
{
#if 1
    UINT32 msb, bw;
#else
    SINT32 msbPos, bitLength, codeOffset, isSigned;
#endif
};

struct BranchStatus
{
    BIT		active;
    UINT32	addr;
    void init(BranchStatus &br2, BranchStatus &br1, BranchStatus &br0) {
        active = br2.active || br1.active || br0.active;
        addr = (br2.active) ? br2.addr : (br1.active) ? br1.addr : br0.addr;
    }
    void reset() { active = 0; addr = 0; }
};

struct DstStatus
{
    BIT		valid, ldm_f;
    UINT6	id;
    UINT32	out;
    void SetCond(DstStatus &ds, BIT cond) {
        valid = ds.valid && cond;
        id = ds.id;
        out = ds.out;
    }
    void SetData(DstStatus &ds, UINT32 data) {
        valid = ds.valid;
        id = ds.id;
        out = data;
    }
    void reset() { valid = 0; ldm_f = 0; id = 0; out = 0; }
};

struct CSRCtrl
{
    BIT we;
    UINT32 waddr;
    UINT32 wdata;
    CSRCtrl() : we(0), waddr(0), wdata(0) {}
    CSRCtrl(BIT we0, UINT32 wa0, UINT32 wd0) : we(we0), waddr(wa0), wdata(wd0) {}
    void reset() { we = 0; waddr = 0; wdata = 0; }
};

enum RVFieldEnum    /// RISC-V
{
    RVF_funct7,     /// 31:25
    RVF_funct3,     /// 14:12
    RVF_funct2,     /// 26:25
    RVF_rs3,        /// 31:27
    RVF_rs2,        /// 24:20
    RVF_rs1,        /// 19:15
    RVF_rd,         /// 11:7
    RVF_opcode,     /// 6:0
    RVF_imm12H,     /// 31:20
    RVF_imm5L,      /// 11:7
    RVF_imm20H,     /// 31:12
    RVF_count,
};

static const Field RVFLD[RVF_count] =    /// RISC-V
{
    { 31, 7 },		///	RVF_funct7
    { 14, 3 },		///	RVF_funct3
    { 26, 2 },		///	RVF_funct2
    { 31, 5 },		///	RVF_rs3
    { 24, 5 },		///	RVF_rs2
    { 19, 5 },		/// RVF_rs1
    { 11, 5 },		/// RVF_rd
    {  6, 7 },		/// RVF_opcode
    { 31, 12},		/// RVF_imm12H
    { 11, 5 },		///	RVF_imm5L
    { 31, 20},		///	RVF_imm20H
};

enum RVInsnEnum    /// RISC-V : opcode[6:0]
{
    RVI_lui     = 0x37,
    RVI_auipc   = 0x17,
    RVI_jal     = 0x6f,
    RVI_jalr    = 0x67, /// funct3:0x0
    RVI_br      = 0x63, /// funct3:0x0(BEQ), 0x1(BNE), 0x4(BLT), 0x5(BGE), 0x6(BLTU), 0x7(BGEU) 
    RVI_ld      = 0x03, /// funct3:0x0(LB), 0x1(LH), 0x2(LW), 0x4(LBU), 0x5(LHU)
    RVI_st      = 0x23, /// funct3:0x0(SB), 0x1(SH), 0x2(SW)
    RVI_compi   = 0x13, /// funct3:0x0(ADDI), 0x2(SLTI), 0x3(SLTIU), 0x4(XORI), 0x6(ORI), 0x7(ANDI)
                        ///         0x1(SLLI), 0x5(SRLI/SRAI)
    RVI_comp    = 0x33, /// funct7:0x20(SUB/SRA), 0x00(ADD/SLL/SLT/SLTU/XOR/SRL/OR/AND), 0x01(MUL*/DIV*/REM*)
                        /// funct3:0x0(ADD/SUB), 0x1(SLL), 0x2(SLT), 0x3(SLTU), 0x4(XOR), 0x5(SRL/SRA), 0x6(OR), 0x7(AND)
                        /// funct3:0x0(MUL), 0x1(MULH), 0x2(MULHSU), 0x3(MULHU), 0x4(DIV), 0x5(DIVU), 0x6(REM), 0x7(REMU)
    RVI_amo     = 0x2f, /// funct7:xxxyyy, aq, rl
                        /// funct3:0x2
    RVI_fence   = 0x0f, /// funct3:0x0(FENCE), 0x1(FENCEI)
    RVI_sys     = 0x73, /// funct3:0x0(ECALL/EBREAK), 0x1(CSRRW), 0x2(CSRRS), 0x3(CSRRC), 0x5(CSRRWI), 0x6(CSRRSI), 0x7(CSRRCI)
};

enum RVOpCodeEnum    /// RISC-V
{
    /// op[5:0] : 6 bits
    RVO_nop     = 0x00,
    RVO_lui     = 0x01, /// RVI_lui
    RVO_auipc   = 0x02, /// RVI_auipc
    RVO_jal     = 0x03, /// RVI_jal
    RVO_jalr    = 0x04, /// RVI_jalr
    RVO_br      = 0x05, /// RVI_br
    RVO_ld      = 0x06, /// RVI_ld
    RVO_st      = 0x07, /// RVI_st
    RVO_comp    = 0x08, /// RVI_compi | RVI_comp
    RVO_amo     = 0x09, /// RVI_amo
    RVO_fence   = 0x0a, /// RVI_fence
    RVO_sys     = 0x0b, /// RVI_sys, funct3 != 0
    RVO_priv    = 0x0c, /// RVI_sys, funct3 == 0
    RVO_trap    = 0x0d, /// Exception/Interrupt

    /// funct3[2:0] : 3 bits
    RVF3_beq    = 0x0,
    RVF3_bne    = 0x1,
    RVF3_blt    = 0x4,
    RVF3_bge    = 0x5,
    RVF3_bltu   = 0x6,
    RVF3_bgeu   = 0x7,

    RVF3_lb     = 0x0,
    RVF3_lh     = 0x1,
    RVF3_lw     = 0x2,
    RVF3_lbu    = 0x4,
    RVF3_lhu    = 0x5,
    RVF3_sb     = 0x0,
    RVF3_sh     = 0x1,
    RVF3_sw     = 0x2,

    RVF3_add    = 0x0,  /// with : subFlag = 0(add), 1(sub)
    RVF3_shl    = 0x1,
    RVF3_slt    = 0x2,
    RVF3_sltu   = 0x3,
    RVF3_xor    = 0x4,
    RVF3_shr    = 0x5,  /// with : sextFlag = 0(srl), 1(sra)
    RVF3_or     = 0x6,
    RVF3_and    = 0x7,

    RVF3_mul    = 0x0,  /// with : mdFlag = 1
    RVF3_mulh   = 0x1,  /// with : mdFlag = 1
    RVF3_mulhsu = 0x2,  /// with : mdFlag = 1
    RVF3_mulhu  = 0x3,  /// with : mdFlag = 1
    RVF3_div    = 0x4,  /// with : mdFlag = 1
    RVF3_divu   = 0x5,  /// with : mdFlag = 1
    RVF3_rem    = 0x6,  /// with : mdFlag = 1
    RVF3_remu   = 0x7,  /// with : mdFlag = 1

    AMO_lr      = 0x02,
    AMO_sc      = 0x03,
    AMO_swap    = 0x01,
    AMO_add     = 0x00,
    AMO_xor     = 0x04,
    AMO_and     = 0x0c,
    AMO_or      = 0x08,
    AMO_min     = 0x10,
    AMO_max     = 0x14,
    AMO_minu    = 0x18,
    AMO_maxu    = 0x1c,
};

#define RV_NOP_INST     0x00000013

#define GPR_COUNT	32

#define USE_BRANCH_EX

//#define DISABLE_MD          /// 1/28/17 : added : disable mul/div --> rv32i 
#define ENABLE_MUL_FUSION   /// 1/26/17 : added
#define ENABLE_DIV_FUSION   /// 1/26/17 : added

#define USE_DST_STATUS_REG  /// 7/10/20
#define USE_CSR_STATUS_REG  /// 7/10/20

//#define USE_CSR_COUNTERS    /// 7/14/20

//#define USE_CSR_STATE    /// 7/14/20

//#define FORMAT_WRITE_AT_EX
#define SIMPLIFY_IO_DECODE

#define USE_CACHE_TEMPLATE
#define USE_LRU_STRUCT
#define SEPARATE_ICACHE_ITLB

#define MERGE_DTLB_INTO_DCACHE

#define CHECK_TLB_TAG_IN_PREFETCH

#define MODIFY_DTLB_STATE_ENUM
#define MOVE_DTLB_FSM_INSIDE_DCACHE

#define MERGE_DTLB_FSM_INTO_DCACHE

#define MERGE_PRV_INST_IN_ICACHE

//#define ENABLE_TLB_STAT
//#define ENABLE_CACHE_STAT	1

#define MODIFY_DTLB_FLUSH


//#define NO_MMU

//#define DBG_SYS

//#if defined(NO_MMU) //&& !defined(__LLVM_C2RTL__)
//#define USE_TAG_REG
//#endif

/// USE_TAG_REG OFF:
// timer(RVProc) : 1.783 sec(elapsed), 1.783 sec(total), 9,414,501.402 cycles/sec, 16,786,056 cycles
// timer(RVProcRTL) : 10.286 sec(elapsed), 10.286 sec(total), 1,632,462.084 cycles/sec, 16,791,505 cycles

/// USE_TAG_REG ON:
// timer(RVProc) : 1.704 sec(elapsed), 1.704 sec(total), 9,850,971.831 cycles/sec, 16,786,056 cycles
// timer(RVProcRTL) : 10.995 sec(elapsed), 10.995 sec(total), 1,527,194.634 cycles/sec, 16,791,505 cycles

//#define DBG_WB
//#define DBG_ME
//#define DBG_EX
//#define DBG_DC
//#define DBG_FE

//#define USE_FIFO
//#define NO_SPI

#define CPU_ONLY
//#define TLB_STATS

/// total cycles =   126,181,974 (active: 126,181,974, sleep: 0) (active: 100.00%, sleep: 0.00%)
/// timer(RVProcRTL) : 297.811 sec(elapsed), 297.811 sec(total), 423,737.800 cycles/sec, 126,193,778 cycles

/// CHECK_TLB_TAG_IN_PREFETCH
/// total cycles =   126,181,719 (active: 126,181,719, sleep: 0) (active: 100.00%, sleep: 0.00%)
/// timer(RVProcRTL) : 269.219 sec(elapsed), 269.219 sec(total), 468,739.041 cycles/sec, 126,193,456 cycles

/// change in C2RTL
/// timer(RVProcRTL) : 286.982 sec(elapsed), 286.982 sec(total), 439,725.007 cycles/sec, 126,193,162 cycles

/// timer(RVProc) : 30.416 sec(elapsed), 30.416 sec(total), 4,148,531.003 cycles/sec, 126,181,719 cycles
/// timer(RVProcRTL) : 259.666 sec(elapsed), 259.666 sec(total), 485,983.748 cycles/sec, 126,193,456 cycles

/// test.rv.uart.O2.out
/// timer(RVProc) : 3.728 sec(elapsed), 3.728 sec(total), 4,513,332.350 cycles/sec, 16,825,703 cycles
/// timer(RVProcRTL) : 31.851 sec(elapsed), 31.851 sec(total), 528,262.755 cycles/sec, 16,825,697 cycles
/// timer(RVProcRTL+TV) : 34.442 sec(elapsed), 34.442 sec(total), 488,522.647 cycles/sec, 16,825,697 cycles
/// Synopsys VCS : CPU Time:    702.720 seconds; [23,493.671 cycles/sec]      Data structure size:  95.6Mb

/// CPU_ONLY:
/// timer(RVProc) : 2.325 sec(elapsed), 2.325 sec(total), 7,219,809.032 cycles/sec, 16,786,056 cycles
/// timer(RVProc) : 2.272 sec(elapsed), 2.272 sec(total), 7,388,228.873 cycles/sec, 16,786,056 cycles
/// timer(RVProc) : 2.234 sec(elapsed), 2.234 sec(total), 7,513,901.522 cycles/sec, 16,786,056 cycles

/// OLD-VERSION (before 8/26/2020)
/// timer(RVProcRTL) : 16.346 sec(elapsed), 16.346 sec(total), 1,027,254.680 cycles/sec, 16,791,505 cycles
/// timer(RVProcRTL) : 15.522 sec(elapsed), 15.522 sec(total), 1,081,787.463 cycles/sec, 16,791,505 cycles
/// timer(RVProcRTL+TV) : 19.529 sec(elapsed), 19.529 sec(total), 859,824.108 cycles/sec, 16,791,505 cycles
/// timer(RVProcRTL+TV) : 20.490 sec(elapsed), 20.490 sec(total), 819,497.560 cycles/sec, 16,791,505 cycles
/// Synopsys VCS : CPU Time:    622.790 seconds; [26,961.774 cycles/sec]      Data structure size: 610.7Mb

/// NEW-VERSION (8/26/2020) : regArray index write
/// timer(RVProcRTL) : 13.216 sec(elapsed), 13.216 sec(total), 1,270,543.659 cycles/sec, 16,791,505 cycles
/// timer(RVProcRTL) : 13.257 sec(elapsed), 13.257 sec(total), 1,266,614.242 cycles/sec, 16,791,505 cycles
/// EnableRTLCCollapsing :
/// timer(RVProcRTL) : 10.929 sec(elapsed), 10.929 sec(total), 1,536,417.330 cycles/sec, 16,791,505 cycles
/// timer(RVProcRTL) : 10.991 sec(elapsed), 10.991 sec(total), 1,527,750.432 cycles/sec, 16,791,505 cycles
/// timer(RVProcRTL+TV) : 13.911 sec(elapsed), 13.911 sec(total), 1,207,066.710 cycles/sec, 16,791,505 cycles
/// timer(RVProcRTL+TV) : 14.083 sec(elapsed), 14.083 sec(total), 1,192,324.434 cycles/sec, 16,791,505 cycles

/// Synopsys VCS : CPU Time:    622.790 seconds; [26,961.774 cycles/sec]      Data structure size: 610.7Mb
/// iverilog : 19:10:19.60 - 20:11:57.89

/// CPU_ONLY + NO_MMU
/// timer(RVProc) : 1.829 sec(elapsed), 1.829 sec(total), 9,177,723.346 cycles/sec, 16,786,056 cycles
/// timer(RVProcRTL) : 11.027 sec(elapsed), 11.027 sec(total), 1,522,762.764 cycles/sec, 16,791,505 cycles
/// Synopsys VCS : CPU Time:    471.640 seconds; [35,602.376 cycles/sec]      Data structure size: 610.6Mb
/// iverilog : 15:19:28.24 - 16:10:03.90 : 70:03.90 - 19:28.24 : (70 - 19)*60 + (3.90 - 28.24) = 3,035.66 sec
///         [5,531.418 cycles/sec]

/// bbl:
/// timer(RVProc) : 34.495 sec(elapsed), 34.495 sec(total), 3,657,970.025 cycles/sec, 126,181,676 cycles
/// timer(RVProcRTL) : 252.523 sec(elapsed), 252.523 sec(total), 499,730.476 cycles/sec, 126,193,439 cycles
/// timer(RVProcRTL+TV) : 292.324 sec(elapsed), 292.324 sec(total), 431,690.313 cycles/sec, 126,193,439 cycles
/// Synopsys VCS : CPU Time:   5253.520 seconds; [24,020.740 cycles/sec]      Data structure size: 381.7Mb

/// CPU_ONLY:
/// timer(RVProc) : 17.604 sec(elapsed), 17.604 sec(total), 6,636,919.507 cycles/sec, 116,836,331 cycles
/// timer(RVProc) : 18.444 sec(elapsed), 18.444 sec(total), 6,334,652.516 cycles/sec, 116,836,331 cycles
/// timer(RVProcRTL) : 111.895 sec(elapsed), 111.895 sec(total), 1,066,115.823 cycles/sec, 119,293,030 cycles
/// timer(RVProcRTL+TV) : 142.376 sec(elapsed), 142.376 sec(total), 837,873.167 cycles/sec, 119,293,030 cycles
/// Synopsys VCS : CPU Time:   4455.170 seconds; [26,776.313 cycles/sec]      Data structure size: 610.7Mb

#define FIX_TLB_SIZE

#define MODIFY_CACHE_HIT_LOGIC
//#define BREAK_IN_CHECK_TAG
//#define USE_CHECK_TAG_TREE

//#define USE_RANDOM_REPLACEMENT_TLB

#define PARAM_TLB_WAY_BITS      2//0//2
#define PARAM_CACHE_WAY_BITS    2

#define	ITLB_WAY_BITS	PARAM_TLB_WAY_BITS//2   /// 2^2 ways
#define	IC_WAY_BITS     PARAM_CACHE_WAY_BITS   /// 2^2 ways
#define	IC_OFFSET_BITS	4   /// 2^4 words/line
#define	IC_INDEX_BITS	6   /// 2^6 lines

#define	DTLB_WAY_BITS	PARAM_TLB_WAY_BITS//2
#define	DC_WAY_BITS     PARAM_CACHE_WAY_BITS
#define	DC_OFFSET_BITS	4
#define	DC_INDEX_BITS	6

#define IC_TAG_BITS     (32 - IC_INDEX_BITS - IC_OFFSET_BITS - BPW_BITS)
#define DC_TAG_BITS     (32 - DC_INDEX_BITS - DC_OFFSET_BITS - BPW_BITS)

enum WritebackEnum  /// TCT/RISC-V
{
    W_ex_out = 0x0,
    W_dout = 0x1,
};


#define GET_MEM_SIZE(addr_width)			(1 << (addr_width))
#define GET_PRINTF_ADDR_SIZE(addr_width)	(((addr_width) / 4) + (((addr_width) % 4) != 0))

#define PMEM_ADDR_WIDTH			16//18//16//12//16
#define PMEM_SIZE				GET_MEM_SIZE(PMEM_ADDR_WIDTH)
#define PMEM_PRINTF_ADDR_SIZE	GET_PRINTF_ADDR_SIZE(PMEM_ADDR_WIDTH)
#define DMEM_ADDR_WIDTH			17//16//18//24//22//20
#define DMEM_SIZE				GET_MEM_SIZE(DMEM_ADDR_WIDTH)
#define DMEM_PRINTF_ADDR_SIZE	GET_PRINTF_ADDR_SIZE(DMEM_ADDR_WIDTH)

#define DMEM_OFFSET				0x0000	///	need to put actual start address of data memory from the linker script...

struct CPU;

#define USE_RVINSN_DEC
//#define USE_RVINSN_DEC_2

struct RVInsn    /// RISC-V
{
    UINT32  opcode, funct3, funct7;
    UINT32  rd, rs1, rs2, rs3;
    UINT12  imm12H;     /// 31:20
    UINT5   imm5L;      /// 11:7
    UINT7   imm7H;      /// 31:25
    UINT20  imm20H;     /// 31:12

    /// decoded results
    UINT6   op;
    BIT     subFlag, sextFlag, mdFlag, csrFlag, csriFlag, csrwFlag;
    UINT2   br_type;
    BIT     memFlag, rs2Flag, linkFlag;
    SINT32  imm;
    void reset() {
        opcode = 0; funct3 = 0; funct7 = 0; rd = 0; rs1 = 0; rs2 = 0; rs3 = 0;
        imm12H = 0; imm5L = 0; imm7H = 0; imm20H = 0;
        op = 0; subFlag = 0; sextFlag = 0; mdFlag = 0; csrFlag = 0; csriFlag = 0; csrwFlag = 0;
        br_type = 0; memFlag = 0; rs2Flag = 0; linkFlag = 0;
    }
//#if defined(USE_RVINSN_DEC)
    void dec_U (unsigned ir, unsigned opval);
    void dec_UJ(unsigned ir, unsigned opval);
    void dec_I (unsigned ir, unsigned opval);
    void dec_S (unsigned ir, unsigned opval);
    void dec_SB(unsigned ir, unsigned opval);
    void dec_R (unsigned ir, unsigned opval);
#if defined(USE_RVINSN_DEC_2)
    void dec_SYS(unsigned ir, unsigned opval);
#endif
};

/// 7/30/20
struct FEState   /// TCT/RISC-V
{
    struct Reg {
        UINT32	pc;
        BIT	mmu_en, flush, pending, ready, sw_mode;
        BranchStatus	br;
    } r _T(state);
};

#define NO_NXT_PC

struct DCState   /// RISC-V
{
    BIT stall, sw_mode;
#if defined(MODIFY_DTLB_FLUSH)
    void resetSig() { stall = 0; sw_mode = 0; }
#else
    BIT	iflush, itflush;
    void resetSig() { stall = 0; sw_mode = 0; iflush = 0; itflush = 0; }
#endif
    struct DCReg {
        UINT32	src[3];
        UINT32	pc;
        UINT6   op;
        UINT3   funct3;
        BIT     subFlag, sextFlag, mdFlag;
        UINT5   dst_id, src_id[2];
        BIT     dst_f, src_f[2];
        UINT2   br_delay;
        BIT     disable_irq;
        /// 7/6/20
        BIT     ld_hazard;
        UINT5	amo_op;
        BIT	aq, rl;
        CSRCtrl csrc;
        BIT	sw_mode;
    } r _T(state);
};

#define USE_BR_DELAY
#define SEPARATE_ADDERS
#define USE_REG_ID_FUSION /// defined(SEPARATE_DIV_ST_1) && defined(SEPARATE_MUL)


//int get_data_size(UINT3 funct3);

enum RVRegEnum    /// RISC-V
{
    RVR_LK = 1, /// lk = r1 : link register
    RVR_SP = 2, /// sp = r2 : stack pointer
    RVR_GP = 3, /// gp = r3 : global pointer
};

struct PendingOp /// TCT/RISC-V
{
    BIT		is_pending;
    BIT		is_interruptible;
    BIT		resume_from_next_insn;
    void reset() { is_pending = 0; is_interruptible = 0; resume_from_next_insn = 0; }
};

//#if !defined(DISABLE_MD)
//#if defined(USE_REG_ID_FUSION)
struct MDReg /// TCT/RISC-V
{
    UINT2   pending;
    BIT		negate_q, negate_r, modulo_flag, terminate_condition;
    UINT32	f, g, q, r;
    UINT2   last_op;    /// (1 : mul, 2 : div/mod)
    UINT5   srcID[2];
} _T(state);

struct EXState /// TCT/RISC-V
{
    BIT             stall, sw_mode;
    PendingOp		pending_op;
    BranchStatus    br;
#if defined(MODIFY_DTLB_FLUSH)
    BIT	            iflush, itflush, itflush_vpn_valid;
    unsigned	    itflush_vpn;
    void resetSig() { stall = 0; sw_mode = 0; iflush = 0; itflush = 0; pending_op.reset(); br.reset(); }
#else
    void resetSig() { stall = 0; sw_mode = 0; pending_op.reset(); br.reset(); }
#endif
    struct Reg {
        UINT32	pc;
        UINT6	op;
        UINT3	funct3;
        DstStatus dst;
        UINT5	amo_op;
        CSRCtrl csrc;

        // stall status
        BIT	wait_flush;
        BIT	sw_mode;
#if defined(MODIFY_DTLB_FLUSH)
        BIT src_f0;
#endif
    } r _T(state);
};


struct MEState /// TCT/RISC-V
{
    BIT         stall, sw_mode;
    DstStatus	dst;
    UINT6	    op;
    CSRCtrl     csrc;
    void resetSig() { stall = 0; sw_mode = 0; dst.reset(); op = 0; csrc.reset(); }
    struct Reg {
        UINT32	pc;
        UINT6	op;
        DstStatus dst;

        BIT	    amo_st;
        UINT32	amo_data;
        BIT	    amo_reserved;
        UINT32	amo_reserved_address;

        CSRCtrl csrc;

        // stall status
        BIT	    output_pending;
        UINT32	output;
        BIT	    sw_mode;
    } r _T(state);
};


struct WBState /// TCT/RISC-V
{
    BIT             stall, sw_mode, mmu_en;
    UINT2           mode;
    DstStatus	    dst;
    BranchStatus    br; /// 7/21/20
    CSRCtrl csrc;
    void resetSig() { stall = 0; sw_mode = 0; mode = 0; mmu_en = 0; dst.reset(); csrc.reset(); }
    struct Reg {
        UINT32	pc;
        UINT2 mode;
    } r _T(state);
};

#define SET_EX_PENDING_OP(pending, interruptible, resume_from_next)			\
	ex.pending_op.is_pending = pending;								\
	ex.pending_op.is_interruptible = interruptible;					\
	ex.pending_op.resume_from_next_insn = resume_from_next

enum ExceptionCodeEnum
{
	EC_SCALL	= 0x0,		/* system call				*/
	EC_PRIV		= 0x1,		/* privileged instruction	*/
	EC_UNDEF	= 0x2,		/* undefined instruction	*/
	EC_ITLB		= 0x3,		/* instruction TLB miss		*/
	EC_DTLB		= 0x4,		/* data TLB miss			*/
	EC_FETCH	= 0x5,		/* fetch abort				*/
	EC_DATA		= 0x6,		/* data abort				*/
	EC_ZDIV		= 0x7,		/* zero division			*/
};

enum ExceptionVectorEnum
{
	EXCP_RESET	= 0x0,
	EXCP_SCALL	= 0x1,
	EXCP_CPU	= 0x2,
	EXCP_MMU	= 0x3,
	EXCP_IRQ	= 0x4,
};

enum MachineStatusMaskEnum
{
	MSR_USER	= (0x1	<< 0), /* user mode			*/
	MSR_MMU		= (0x1	<< 1), /* MMU enable		*/
	MSR_SUSER	= (0x1	<< 2), /* saved USER bit	*/
	MSR_SMMU	= (0x1	<< 3), /* saved MMU bit		*/
	MSR_EXCP	= (0x1	<< 4), /* exception enable	*/
	MSR_IRQ		= (0x1	<< 5), /* interrupt enable	*/
	MSR_UM		= (MSR_USER	| MSR_MMU),
	MSR_SUM		= (MSR_SUSER| MSR_SMMU),
	MSR_UMSUM	= (MSR_UM	| MSR_SUM),
};

enum InterruptRequestVectorEnum
{
	IRQ_UART_0_RX	= 0x00,
	IRQ_UART_0_TX	= 0x01,
	IRQ_UART_1_RX	= 0x02,
	IRQ_UART_1_TX	= 0x03,
	IRQ_I2C_0_RX	= 0x04,
	IRQ_I2C_0_TX	= 0x05,
	IRQ_I2C_1_RX	= 0x06,
	IRQ_I2C_2_TX	= 0x07,
	IRQ_I2S_0_RXTX	= 0x08,
	IRQ_I2S_1_RXTX	= 0x09,
};

struct Exception
{
	///	scall_pending enabled in DC-stage on SCALL instruction
	///	scall_detected enabled in EX-stage on ESR/EAR writeback
	ST_BIT	scall_pending;
	ST_BIT	cpu_excp_event, mmu_event;
	UINT8	excp_vector;
	BIT		excp_occurred;
	FB_BIT	excp_occurred_FE;
};

struct Hazard
{
	int dc;
	int io;
	int wait;
	int jpg;
	int jpg_quant_st1;
	int overlap;
};

#define ENABLE_CPU_STAT

enum RV_op_stat {
	RV_op_lui, RV_op_auipc, RV_op_jal, RV_op_jalr,
	RV_op_beq, RV_op_bne, RV_op_blt, RV_op_bge, RV_op_bltu, RV_op_bgeu,
	RV_op_lb, RV_op_lh, RV_op_lw, RV_op_lbu, RV_op_lhu,
	RV_op_sb, RV_op_sh, RV_op_sw,
	RV_op_addi, RV_op_slti, RV_op_sltiu, RV_op_xori, RV_op_ori, RV_op_andi,
	RV_op_slli, RV_op_srli, RV_op_srai,
	RV_op_add, RV_op_sub, RV_op_sll, RV_op_slt, RV_op_sltu, RV_op_xor,
	RV_op_srl, RV_op_sra, RV_op_or, RV_op_and,
	RV_op_fence, RV_op_fencei, RV_op_ecall, RV_op_ebreak,
	//RV32M
	RV_op_mul, RV_op_mulh, RV_op_mulhsu, RV_op_mulhu,
	RV_op_div, RV_op_divu, RV_op_rem, RV_op_remu,
	RV_op_invalid,
	RV_op_count
};


#if !defined(IGNORE_ARCH_INFO)
#include "CSR.h"

#include "RVProc_io.h"
#include "encryption.h"

struct CPU
{
#if !defined __LLVM_C2RTL__
	int enableProfile;
#endif
    ST_UINT32	cycle;          /// 0
	ST_BIT		halted;
	FIFO_PORT	dummy_fifo_port;
//#if defined(USE_CACHE_TEMPLATE)
    ICache<ITLB_WAY_BITS, IC_WAY_BITS, IC_OFFSET_BITS, IC_INDEX_BITS, IC_TAG_BITS>		icache;
    UINT32		ir;             /// 4
    RF_UINT32	gpr[GPR_COUNT];
	CSR		    csr;
    FEState		fe;
    RVInsn		insn;
    DCState     dc;
    EXState		ex;         /// 12
    MDReg		md;

    // data memory access
    UINT32	dm_addr;

    MEState		me;
    WBState		wb;
	IO			io;
#if !defined __LLVM_C2RTL__
	Hazard		hazard;
#endif
	// feed back
	// counters
	ST_UINT64 cycle64;
	ST_UINT64 instret64;

#if defined(CPU_CPP)
    void dec_SYS(BIT &mode_change);
	UINT32 div_st_1(UINT32 src0, UINT32 src1);
    UINT32 div_st_1b();
    UINT32 div_st_2();
#if defined(USE_REG_ID_FUSION)
    UINT32 mul_st_1(UINT32 src0, UINT32 src1);
    UINT32 mul_st_2();
#else
    UINT32 mul(UINT32 src0, UINT32 src1);
#endif
    UINT32 format_read_data(UINT32 dout, UINT2 byte_pos, UINT3 funct3);
	UINT4 byte_enable(UINT2 byte_pos, UINT3 funct3);
	UINT32 io_pipe_control();
    BIT detect_dc_hazard();

    // encryption key
    secure_mem key1;

	void fetch(AXI4L::CH *axi);
	void decode();
	void execute();
	void memory(AXI4L::CH *axi, UINT16 intr);
	void writeback();
#if !defined(__LLVM_C2RTL__) && defined(USE_RISC_V) && defined(ENABLE_CPU_STAT)
	int op_count[RV_op_count], op_count_stall[RV_op_count];
	void update_cpu_stat();
	void show_cpu_stat();
#endif
//#if defined(TEST_METHOD_C2R_FUNC)
    _C2R_FUNC(5)
#if defined(CPU_ONLY)
#if defined(USE_FIFO)
    int step(D_FIFO_PORT * in_fifo, D_FIFO_PORT * out_fifo, AXI4L::CH *axi_d, AXI4L::CH *axi_i, unsigned intr) {
        io.fifo.set_input_ports(in_fifo, out_fifo);
#else
    int step(AXI4L::CH *axi_d, AXI4L::CH *axi_i, unsigned intr) {
#endif
        fetch(axi_i);
        decode();
        execute();
        memory(axi_d, intr);
        writeback();
#if defined(USE_FIFO)
        io.fifo.set_output_ports(in_fifo, out_fifo);
#endif
#if !defined(__LLVM_C2RTL__) && defined(USE_RISC_V) && defined(ENABLE_CPU_STAT)
        update_cpu_stat();
#endif
        cycle++;
        return (halted == 1);
    }
#else
#if defined(USE_FIFO)
    int step(D_FIFO_PORT * in_fifo, D_FIFO_PORT * out_fifo, AXI4L::CH *axi_d, AXI4L::CH *axi_i, AXI4L::CH *uart) {
    io.fifo.set_input_ports(in_fifo, out_fifo);
#else
    int step(AXI4L::CH *axi_d, AXI4L::CH *axi_i, unsigned intr) {
#endif
		fetch(axi_i);
		decode();
		execute();
		memory(axi_d, intr);
		writeback();
#if defined(USE_FIFO)
		io.fifo.set_output_ports(in_fifo, out_fifo);
#endif
#if !defined(__LLVM_C2RTL__) && defined(USE_RISC_V) && defined(ENABLE_CPU_STAT)
		update_cpu_stat();
#endif
		cycle++;
		return (halted == 1);
	}
#endif
#endif

#if !defined(__LLVM_C2RTL__)
	// debug func
	UINT32 read_data;
	BIT read_pmem(UINT32 addr);
	BIT read_cmem(UINT32 addr);
	BIT read_vmem(UINT32 addr);
#endif
}/* CPU*/;

#endif

#endif  // !defined(TCT_PROC_ARCH_H)
