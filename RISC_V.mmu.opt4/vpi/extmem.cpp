#include <iverilog/vpi_user.h>
#include "../RVProcArch.h"
#include "../ExtMem.h"

ExtMem mem;

static int extmem_load_calltf(char *user_data)
{
	unsigned int pc, sp, a1;

	vpiHandle systfref, args_iter, argh;
	struct t_vpi_value argval;
	int i;

	// Obtain a handle to the argument list
	systfref = vpi_handle(vpiSysTfCall, NULL);
	args_iter = vpi_iterate(vpiArgument, systfref);

	// get values
	argh = vpi_scan(args_iter);
	argval.format = vpiStringVal;
	vpi_get_value(argh, &argval);
	mem.load_program(argval.value.str, &pc, &sp);

	argh = vpi_scan(args_iter);
	argval.format = vpiStringVal;
	vpi_get_value(argh, &argval);
	mem.load_dtb(argval.value.str, &a1);

	// set values
	argh = vpi_scan(args_iter);
	argval.format = vpiIntVal;
	argval.value.integer = pc;
	vpi_put_value(argh, &argval, NULL, vpiNoDelay);

	argh = vpi_scan(args_iter);
	argval.format = vpiIntVal;
	argval.value.integer = sp;
	vpi_put_value(argh, &argval, NULL, vpiNoDelay);

	argh = vpi_scan(args_iter);
	argval.format = vpiIntVal;
	argval.value.integer = a1;
	vpi_put_value(argh, &argval, NULL, vpiNoDelay);

	// Cleanup and return
	vpi_free_object(args_iter);

	return 0;
}

static int extmem_calltf(char *user_data)
{
	vpiHandle systfref, args_iter, argh;
	struct t_vpi_value argval;
	int value[7];
	int i;
	MEMCTLPin mem_pin;

	// Obtain a handle to the argument list
	systfref = vpi_handle(vpiSysTfCall, NULL);
	args_iter = vpi_iterate(vpiArgument, systfref);

	for (i = 0; i < 7; i++) {
		// Grab the value of the arguments
		argh = vpi_scan(args_iter);
		argval.format = vpiIntVal;
		vpi_get_value(argh, &argval);
		value[i] = argval.value.integer;
	}
	mem_pin.addr = value[0];
	mem_pin.din  = value[1];
	mem_pin.size = value[2];
	mem_pin.cs   = value[3];
	mem_pin.we   = value[4];
	mem_pin.ras  = value[5];
	mem_pin.cas  = value[6];

	mem.update(&mem_pin);

	argh = vpi_scan(args_iter);
	argval.format = vpiIntVal;
	argval.value.integer = mem_pin.dout;
	vpi_put_value(argh, &argval, NULL, vpiNoDelay);

	// Cleanup and return
	vpi_free_object(args_iter);
	return 0;
}

void extmem_register()
{
	s_vpi_systf_data	tf_data;

	tf_data.type = vpiSysTask;
	tf_data.tfname = "$extmem";
	tf_data.calltf = extmem_calltf;
	tf_data.compiletf = 0;
	tf_data.sizetf = 0;
	tf_data.user_data = 0;
	vpi_register_systf(&tf_data);

	tf_data.tfname = "$extmem_load";
	tf_data.calltf = extmem_load_calltf;
	vpi_register_systf(&tf_data);
}

void (*vlog_startup_routines[])() = {
	extmem_register,
	0,
};
