`ifndef LOAD_IMAGE
`define	LOAD_IMAGE	"../test/bbl"
`endif
`ifndef DEV_TREE
`define DEV_TREE	"../dtb/c2rtl.dtb"
`endif

module RVProcAXI_testbench;

parameter DISABLE_VCD_DUMP = 0;	//1;

/*input ports*/
reg          clk;
reg          rst_n;
reg          G_io_pins_spi_miso      ; /// <0,1> [U1]          ;
reg   [31:0] G_io_pins_mpin_dout     ; /// <0,4294967295> [U32];
reg          G_io_pins_in_fifo_strobe; /// <0,1> [U1]          ;
reg   [31:0] G_io_pins_in_fifo_data  ; /// <0,4294967295> [U32];
reg          G_io_pins_out_fifo_ready; /// <0,1> [U1]          ;

/*output ports*/
wire  [ 1:0] G_axi_bus_s_ch_1_intr         ; /// <0,3> [U2]          ;
wire         G_io_pins_spi_mosi            ; /// <0,1> [U1]          ;
wire         G_io_pins_spi_sck             ; /// <0,1> [U1]          ;
wire  [ 1:0] G_axi_bus_s_ch_2_intr         ; /// <0,3> [U2]          ;
wire  [30:0] G_io_pins_mpin_addr           ; /// <0,2147483647> [U31];
wire  [31:0] G_io_pins_mpin_din            ; /// <0,4294967295> [U32];
wire  [ 2:0] G_io_pins_mpin_size           ; /// <0,7> [U3]          ;
wire  [ 1:0] G_io_pins_mpin_cs             ; /// <0,3> [U2]          ;
wire         G_io_pins_mpin_we             ; /// <0,1> [U1]          ;
wire         G_io_pins_mpin_ras            ; /// <0,1> [U1]          ;
wire         G_io_pins_mpin_cas            ; /// <0,1> [U1]          ;
wire         G_io_pins_in_fifo_ready       ; /// <0,1> [U1]          ;
wire  [ 3:0] G_io_pins_in_fifo_bytes       ; /// <0,15> [U4]         ;
wire         G_io_pins_out_fifo_strobe     ; /// <0,1> [U1]          ;
wire         G_io_pins_out_fifo_chanID     ; /// <0,1> [U1]          ;
wire  [31:0] G_io_pins_out_fifo_data       ; /// <0,4294967295> [U32];
wire         G_axi_bus_m_ch_0_rdat_s_last  ; /// <0,1> [U1]          ;
wire  [31:0] G_axi_bus_m_ch_0_intr         ; /// <0,4294967295> [U32];
wire         G_axi_bus_m_ch_1_rdat_s_last  ; /// <0,1> [U1]          ;
wire  [ 2:0] G_axi_bus_s_ch_0_raddr_m_size ; /// <0,7> [U3]          ;
wire  [ 2:0] G_axi_bus_s_ch_0_raddr_m_prot ; /// <0,7> [U3]          ;
wire  [ 2:0] G_axi_bus_s_ch_0_waddr_m_size ; /// <0,7> [U3]          ;
wire  [ 2:0] G_axi_bus_s_ch_0_waddr_m_prot ; /// <0,7> [U3]          ;
wire  [ 3:0] G_axi_bus_s_ch_0_wdat_m_strobe; /// <0,15> [U4]         ;
wire         G_axi_bus_s_ch_0_wdat_m_last  ; /// <0,1> [U1]          ;
wire  [ 2:0] G_axi_bus_s_ch_1_raddr_m_size ; /// <0,7> [U3]          ;
wire  [ 2:0] G_axi_bus_s_ch_1_raddr_m_prot ; /// <0,7> [U3]          ;
wire  [ 2:0] G_axi_bus_s_ch_1_waddr_m_size ; /// <0,7> [U3]          ;
wire  [ 2:0] G_axi_bus_s_ch_1_waddr_m_prot ; /// <0,7> [U3]          ;
wire  [ 3:0] G_axi_bus_s_ch_1_wdat_m_strobe; /// <0,15> [U4]         ;
wire         G_axi_bus_s_ch_1_wdat_m_last  ; /// <0,1> [U1]          ;
wire  [ 2:0] G_axi_bus_s_ch_2_raddr_m_prot ; /// <0,7> [U3]          ;
wire  [ 2:0] G_axi_bus_s_ch_2_waddr_m_prot ; /// <0,7> [U3]          ;
wire  [ 3:0] G_axi_bus_s_ch_2_wdat_m_strobe; /// <0,15> [U4]         ;
wire         G_axi_bus_s_ch_2_wdat_m_last  ; /// <0,1> [U1]          ;
wire         G_RVProcAXI_OUT               ; /// <0,1> [U1]          ;

RVProcAXI M0 (
  /* global inputs*/
  clk, rst_n,

  /*inputs*/
  G_io_pins_spi_miso, G_io_pins_mpin_dout, G_io_pins_in_fifo_strobe, G_io_pins_in_fifo_data, 
  G_io_pins_out_fifo_ready, 

  /*outputs*/
  G_axi_bus_s_ch_1_intr, G_io_pins_spi_mosi, G_io_pins_spi_sck, G_axi_bus_s_ch_2_intr, 
  G_io_pins_mpin_addr, G_io_pins_mpin_din, G_io_pins_mpin_size, G_io_pins_mpin_cs, 
  G_io_pins_mpin_we, G_io_pins_mpin_ras, G_io_pins_mpin_cas, G_io_pins_in_fifo_ready, 
  G_io_pins_in_fifo_bytes, G_io_pins_out_fifo_strobe, G_io_pins_out_fifo_chanID, 
  G_io_pins_out_fifo_data, G_axi_bus_m_ch_0_rdat_s_last, G_axi_bus_m_ch_0_intr, 
  G_axi_bus_m_ch_1_rdat_s_last, G_axi_bus_s_ch_0_raddr_m_size, G_axi_bus_s_ch_0_raddr_m_prot, 
  G_axi_bus_s_ch_0_waddr_m_size, G_axi_bus_s_ch_0_waddr_m_prot, G_axi_bus_s_ch_0_wdat_m_strobe, 
  G_axi_bus_s_ch_0_wdat_m_last, G_axi_bus_s_ch_1_raddr_m_size, G_axi_bus_s_ch_1_raddr_m_prot, 
  G_axi_bus_s_ch_1_waddr_m_size, G_axi_bus_s_ch_1_waddr_m_prot, G_axi_bus_s_ch_1_wdat_m_strobe, 
  G_axi_bus_s_ch_1_wdat_m_last, G_axi_bus_s_ch_2_raddr_m_prot, G_axi_bus_s_ch_2_waddr_m_prot, 
  G_axi_bus_s_ch_2_wdat_m_strobe, G_axi_bus_s_ch_2_wdat_m_last, G_RVProcAXI_OUT

);

integer i;

initial begin
  if (!DISABLE_VCD_DUMP) begin
    $dumpfile("RVProcAXI_testbench.vcd");
    $dumpvars(0, RVProcAXI_testbench);
    $dumpoff;
  end /// if (!DISABLE_VCD_DUMP)
  clk = 0; rst_n = 0;
  for (i = 0; i < 10; i = i + 1) begin
    #50; clk = 1; #50; clk = 0;
  end
  rst_n = 1; #1000;

  M0.M0.M3.M0.M0.G__this_fe_reg_prev_inst = 32'h13;
  M0.M0.M3.M0.M0.G__this_mode = 32'h3;
  $extmem_load(
    `LOAD_IMAGE,
    `DEV_TREE,
    M0.M0.M3.M0.M0.G__this_fe_reg_next_pc,
    M0.M0.M3.M0.M0.G__this_gpr[2],
    M0.M0.M3.M0.M0.G__this_gpr[11]);

  while (!G_RVProcAXI_OUT) begin
//    if (M0.M0.M3.M0.M0.G__this_fe_reg_cur_pc == 'hc13e634c)
//        $dumpon;
//    if (M0.M0.M3.M0.M0.G__this_fe_reg_cur_pc == 'h80000004)
//        $dumpoff;
//    if (M0.M0.M3.M0.M0.G__this_fe_reg_cur_pc == 'hc1349314)
//        $finish;

    if (G_io_pins_mpin_cs)
      $extmem(
        G_io_pins_mpin_addr           , /// <0,2147483647> [U31];
        G_io_pins_mpin_din            , /// <0,4294967295> [U32];
        G_io_pins_mpin_size           , /// <0,7> [U3]          ;
        G_io_pins_mpin_cs             , /// <0,3> [U2]          ;
        G_io_pins_mpin_we             , /// <0,1> [U1]          ;
        G_io_pins_mpin_ras            , /// <0,1> [U1]          ;
        G_io_pins_mpin_cas            , /// <0,1> [U1]          ;
        G_io_pins_mpin_dout);

    #50; clk = 0; #50; clk = 1;
  end /// while(!...)
`ifdef __ICARUS__
  $finish_and_return(0);
`endif
end /// initial

endmodule /// (RVProcAXI_testbench)
