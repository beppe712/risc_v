#include <stdio.h>
#include <stdarg.h>

// UART status ((rx_error << 4) | (rx_empty << 3) | (rx_full << 2) | (tx_empty << 1) | (tx_full));
static volatile char *uart = (volatile char *)0x10000000;

static void uart_putch(char ch);

static void __attribute__((constructor)) _uart_init(void)
{
	//(activate) : baud_period(2), bit_count(8), stop_count(1), parity_type(3)
	*(unsigned short *)(uart + 4) = 1 | (15 << 1) | (3 << 5) | (0 << 7) | (3 << 8);
}

static void __attribute__((destructor)) _uart_fini(void)
{
	while ((*(unsigned int *)(uart + 4) & 2) == 0)
		;
}

static void uart_putch(char ch)
{
	while (*(unsigned int *)(uart + 4) & 1)
		;

	*uart = ch;
}

static void putline(const char *s)
{
	char ch;

	while (ch = *s++)
		uart_putch(ch);
}

int puts(const char *s)
{
	putline(s);
	uart_putch('\n');

	return 1;
}

int printf(const char *format, ...)
{
	int ret;
	va_list ap;
	char buffer[128];

	va_start(ap, format);
	ret = vsnprintf(buffer, sizeof(buffer), format, ap);
	va_end(ap);

	putline(buffer);

	return ret;
}
