#include <stdio.h>
#include <stdlib.h>

struct SYSCTRL {
	unsigned int clr_intr;
	unsigned int pad[3];
	unsigned long long mtime;
	unsigned long long mtimecmp;
};

volatile struct SYSCTRL *sysctrl = (struct SYSCTRL *)0x10001000;

void mhandler(unsigned a0,
		unsigned a1,
		unsigned a2,
		unsigned a3,
		unsigned a4,
		unsigned a5,
		unsigned a6,
		unsigned a7)
{
	unsigned int mepc;
	unsigned int mcause;

	sysctrl->mtimecmp = ~0;
	sysctrl->clr_intr = 0x00000080;

	asm volatile("csrr %[old], mepc" : [old] "=&r" (mepc) : );
	asm volatile("csrr %[old], mcause" : [old] "=&r" (mcause) : );

	printf("Machine Interrupt: mepc = 0x%08x, mcause = 0x%08x\n", mepc, mcause);

	if (mcause == 9) {
		if (a7 == 0x5d)
			asm volatile("ebreak" : : );

		sysctrl->mtimecmp = sysctrl->mtime + 10;
			
		mepc += 4;
		asm volatile("csrw mepc, %[new]" : : [new] "r" (mepc) );
	} else if (mcause == 0x80000007) {
		sysctrl->mtimecmp = ~0;
		asm volatile("csrs mip, %[new]" : : [new] "r" (0x00000020) );
	}
}

void shandler(void)
{
	unsigned int sepc;
	unsigned int scause;

	asm volatile("csrc sip, %[new]" : : [new] "r" (0x00000020) );
	asm volatile("csrr %[old], sepc" : [old] "=&r" (sepc) : );
	asm volatile("csrr %[old], scause" : [old] "=&r" (scause) : );
	printf("Supervisor interrupt: sepc = 0x%08x, scause = 0x%08x\n", sepc, scause);

	asm volatile("ebreak" : : );
}

void mcall(unsigned a0,
		unsigned a1,
		unsigned a2,
		unsigned a3,
		unsigned a4,
		unsigned a5,
		unsigned a6,
		unsigned a7)
{
	asm volatile("ecall" : : );
}

int main(int argc, char **argv)
{
	extern void _mhandler(void);
	extern void _shandler(void);
	extern void smode(void);

	printf("start\n");
	asm volatile("csrw mtvec, %[new]" : : [new] "r" (_mhandler) );
	asm volatile("csrs mie, %[new]" : : [new] "r" (0x00000080) );
	asm volatile("csrs mideleg, %[new]" : : [new] "r" (0x00000020) );
	sysctrl->mtimecmp = ~0;

	smode();

	printf("supervisor mode\n");
	asm volatile("csrw stvec, %[new]" : : [new] "r" (_shandler) );
	asm volatile("csrs sie, %[new]" : : [new] "r" (0x00000020) );
	asm volatile("csrs sstatus, 0x02" : : );

	mcall(0, 0, 0, 0, 0, 0, 0, 0);

	printf("sssssssssssssssssss\n");
	printf("sssssssssssssssssss\n");
	printf("sssssssssssssssssss\n");

	printf("end\n");

	return 0;
}
