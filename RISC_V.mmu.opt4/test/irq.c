#include <stdio.h>
#include <stdlib.h>

static __inline char h2a(char hex)
{
	if (hex < 10)
		return '0' + hex;
	else
		return 'a' - 10 + hex;
}

volatile unsigned int *sysctrl = (unsigned int *)0x10001000;

void handler(void)
{
	unsigned int mepc;
	char buf[10];

	*sysctrl = 0x00030000;

	asm volatile("csrr %[old], mepc" : [old] "=&r" (mepc) : );
	//asm volatile("csrr %[old], mcause" : [old] "=&r" (mepc) : );

	printf("\nInterrupt: mepc = 0x");
	buf[0] = h2a((mepc >> 28) & 0xf);
	buf[1] = h2a((mepc >> 24) & 0xf);
	buf[2] = h2a((mepc >> 20) & 0xf);
	buf[3] = h2a((mepc >> 16) & 0xf);
	buf[4] = h2a((mepc >> 12) & 0xf);
	buf[5] = h2a((mepc >>  8) & 0xf);
	buf[6] = h2a((mepc >>  4) & 0xf);
	buf[7] = h2a((mepc >>  0) & 0xf);
	buf[8] = 0;
	puts(buf);

	exit(-1);
}

int main(int argc, char **argv)
{
	unsigned int mask;
	unsigned int prev;


	mask = 0x8;
	asm volatile("csrw mstatus, %[new]"
			: 
			: [new] "r" (mask) );

	mask = (unsigned int)handler;
	asm volatile("csrw mtvec, %[new]"
			: 
			: [new] "r" (mask) );

	mask = 0x00030000;

	asm volatile("csrrs %[old], mie, %[new]"
			: [old] "=&r" (prev)
			: [new] "r" (mask) );
	printf("prev = 0x%08x\n", prev);

	return 0;
}
