#include <stdio.h>

void assert(int b)
{
	if (b)
		printf("OK\n");
	else
		printf("NG\n");
}

/////////////////
// forward @ ex.
void test1_1(void)
{
	int val;

	asm volatile(
		"li	a0, 10\n\t"
		"li	a1, 20\n\t"
		"li	a0, 1\n\t"
		"li	a1, 2\n\t"
		"add	%[rc], a0, a1"
			: [rc] "=&r" (val) : : "%a0", "%a1");
	
	assert(val == 3);
}

void test1_2(void)
{
	int val;

	asm volatile(
		"li	a0, 10\n\t"
		"li	a1, 20\n\t"
		"li	a0, 1\n\t"
		"li	a1, 2\n\t"
		"nop\n\t"
		"add	%[rc], a0, a1"
			: [rc] "=&r" (val) : : "%a0", "%a1");
	
	assert(val == 3);
}

void test1_3(void)
{
	int val;

	asm volatile(
		"li	a0, 10\n\t"
		"li	a1, 20\n\t"
		"li	a0, 1\n\t"
		"nop\n\t"
		"li	a1, 2\n\t"
		"add	%[rc], a0, a1"
			: [rc] "=&r" (val) : : "%a0", "%a1");
	
	assert(val == 3);
}

void test1_4(void)
{
	int val;

	asm volatile(
		"li	a0, 10\n\t"
		"li	a1, 20\n\t"
		"nop\n\t"
		"li	a0, 1\n\t"
		"li	a1, 2\n\t"
		"add	%[rc], a0, a1"
			: [rc] "=&r" (val) : : "%a0", "%a1");
	
	assert(val == 3);
}

/////////////////
// forward @ dc.
void test2_1(void)
{
	int val;

	asm volatile(
		"li	%[rc],0\n\t"
		"li	a0, 10\n\t"
		"li	a1, 20\n\t"
		"li	a1, 15\n\t"
		"li	a1, 10\n\t"
		"bne	a0, a1,1f\n\t"
		"li	%[rc],1\n"
		"1:"
			: [rc] "=&r" (val) : : "%a0", "%a1");
	
	assert(val == 1);
}

void test2_2(void)
{
	int val;

	asm volatile(
		"li	%[rc],0\n\t"
		"li	a0, 10\n\t"
		"li	a1, 20\n\t"
		"li	a1, 15\n\t"
		"li	a1, 10\n\t"
		"nop\n\t"
		"bne	a0, a1,1f\n\t"
		"li	%[rc],1\n"
		"1:"
			: [rc] "=&r" (val) : : "%a0", "%a1");
	
	assert(val == 1);
}

void test2_3(void)
{
	int val;

	asm volatile(
		"li	%[rc],0\n\t"
		"li	a0, 10\n\t"
		"li	a1, 20\n\t"
		"li	a1, 15\n\t"
		"li	a1, 10\n\t"
		"nop\n\t"
		"nop\n\t"
		"bne	a0, a1,1f\n\t"
		"li	%[rc],1\n"
		"1:"
			: [rc] "=&r" (val) : : "%a0", "%a1");
	
	assert(val == 1);
}

void test2_4(void)
{
	int val;

	asm volatile(
		"li	%[rc],0\n\t"
		"li	a0, 10\n\t"
		"li	a1, 20\n\t"
		"li	a1, 15\n\t"
		"li	a1, 10\n\t"
		"nop\n\t"
		"nop\n\t"
		"nop\n\t"
		"bne	a0, a1,1f\n\t"
		"li	%[rc],1\n"
		"1:"
			: [rc] "=&r" (val) : : "%a0", "%a1");
	
	assert(val == 1);
}

/////////////////
// mem hazard @ dc.
void test3_1(void)
{
	int data = 10;
	int val;

	asm volatile(
		"li	%[rc],0\n\t"
		"li	a0, 10\n\t"
		"li	a1, 20\n\t"
		"lw	a1, %[rb]\n\t"
		"bne	a0, a1,1f\n\t"
		"li	%[rc],1\n"
		"1:"
			: [rc] "=&r" (val) : [rb] "m" (data): "%a0", "%a1");
	
	assert(val == 1);
}

void test3_2(void)
{
	int data = 10;
	int val;

	asm volatile(
		"li	%[rc],0\n\t"
		"li	a0, 10\n\t"
		"li	a1, 20\n\t"
		"lw	a1, %[rb]\n\t"
		"nop\n\t"
		"bne	a0, a1,1f\n\t"
		"li	%[rc],1\n"
		"1:"
			: [rc] "=&r" (val) : [rb] "m" (data): "%a0", "%a1");
	
	assert(val == 1);
}

void test3_3(void)
{
	int data = 10;
	int val;

	asm volatile(
		"li	%[rc],0\n\t"
		"li	a0, 10\n\t"
		"li	a1, 20\n\t"
		"lw	a1, %[rb]\n\t"
		"nop\n\t"
		"nop\n\t"
		"bne	a0, a1,1f\n\t"
		"li	%[rc],1\n"
		"1:"
			: [rc] "=&r" (val) : [rb] "m" (data): "%a0", "%a1");
	
	assert(val == 1);
}

void test3_4(void)
{
	int data = 10;
	int val;

	asm volatile(
		"li	%[rc],0\n\t"
		"li	a0, 10\n\t"
		"li	a1, 20\n\t"
		"lw	a1, %[rb]\n\t"
		"nop\n\t"
		"nop\n\t"
		"nop\n\t"
		"bne	a0, a1,1f\n\t"
		"li	%[rc],1\n"
		"1:"
			: [rc] "=&r" (val) : [rb] "m" (data): "%a0", "%a1");
	
	assert(val == 1);
}

int main(int argc, char **argv)
{
	test1_1();
	test1_2();
	test1_3();
	test1_4();

	test2_1();
	test2_2();
	test2_3();
	test2_4();

	test3_1();
	test3_2();
	test3_3();
	test3_4();
	return 0;
}
