#include <stdio.h>

extern void enable_mmu();

void handler(void)
{
	unsigned int sepc;
	unsigned int scause;

	asm volatile("csrr %[old], sepc" : [old] "=&r" (sepc) : );
	asm volatile("csrr %[old], scause" : [old] "=&r" (scause) : );

	printf("Interrupt: sepc = 0x%08x, scause = 0x%08x\n", sepc, scause);

	sepc += 4;
	asm volatile("csrw sepc, %[new]" : : [new] "r" (sepc) );
}

int main(int argc, char **argv)
{
	extern void _handler(void);
	unsigned int status;

	printf("OK\n");
	enable_mmu();

	asm volatile("csrw stvec, %[new]" : : [new] "r" (_handler) );

	printf("hello\n");
	*(int *)0xffff0000 = 0;
	printf("hello\n");
	return 0;
}
