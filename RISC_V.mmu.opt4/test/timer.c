#include <stdio.h>
#include <stdlib.h>

struct SYSCTRL {
	unsigned int clr_intr;
	unsigned int pad[3];
	unsigned long long mtime;
	unsigned long long mtimecmp;
};

volatile struct SYSCTRL *sysctrl = (struct SYSCTRL *)0x10001000;

void handler(void)
{
	unsigned int mepc;
	unsigned int mcause;

	sysctrl->mtimecmp = ~0;
	sysctrl->clr_intr = 0x00000080;

	asm volatile("csrr %[old], mepc" : [old] "=&r" (mepc) : );
	asm volatile("csrr %[old], mcause" : [old] "=&r" (mcause) : );

	printf("Interrupt: mepc = 0x%08x, mcause = 0x%08x\n", mepc, mcause);
}

int main(int argc, char **argv)
{
	extern void _handler(void);

	printf("start\n");

	asm volatile("csrw mtvec, %[new]" : : [new] "r" (_handler) );
	asm volatile("csrs mie, %[new]" : : [new] "r" (0x00000080) );

	sysctrl->mtimecmp = ~0;
	asm volatile("csrs mstatus, 0x08" : : );

	//sysctrl->mtimecmp = sysctrl->mtime + 14;
	sysctrl->mtimecmp = sysctrl->mtime;
	asm volatile("nop" : : );

	asm volatile("csrc mstatus, 0x08" : : );

	printf("end\n");

	return 0;
}
