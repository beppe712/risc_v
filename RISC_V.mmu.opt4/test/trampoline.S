	.text
	.globl	_start
_start:
	/* enter supervisor mode
	 *	mstatus.mpp = 'b10
	 *	mstatus.mpie = 0
	 */
	csrr	t0,mstatus
	li	t1,0xffffe77f
	li	t2,0x00000800
	and	t0,t0,t1
	or	t0,t0,t2
	csrw	mstatus,t0
	la	t0,1f
	csrw	mepc,t0
	mret
1:
	/* map 0xc0000000-0xc0400000 to 0x80000000-0x80400000 */
	li	a0,0x40000000

	/* stvec point to virtual address of instruction after satp write */
	la	t0,2f
	add	t0,t0,a0
	csrw	stvec,t0

	li	t0, 0x1000
	csrw	medeleg, t0

	/* trampoline */
	la	t0, pg_dir
	srl	t0,t0,12
	li	t1, 0x80000000	/* MODE */
	or	t0,t0,t1
	sfence.vma
	csrw	satp,t0
2:
	or	t0,t0,t0
	la	a0,data
	lw	t1,0(a0)
	sw	x0,0(a0)
	nop
	ebreak

	.data
	.align	12
pg_dir:
#if	0
	/* PPN1 PPN0 RSW D A G U X W R V */
	/*  12   10   2  1 1 1 1 1 1 1 1 */

	/*  200  00   0  1 1 0 0 1 1 1 1 */
	.zero	0xc00
	.word	0x200000cf
	.zero	0x400-4
#else
	.zero	0xc00
	.word	(0x80022000 >> 2) + 0xc1
	.zero	0x400-4

	.zero	0x40
	.word	0x200040cf	// 0xc0010000-0xc0011000
	.zero	0x40 - 4
	.word	0		// 0xc0020000-0xc0021000
	.word	0		// 0xc0021000-0xc0022000
	.word	0		// 0xc0022000-0xc0023000
	.word	0x20008ccf	// 0xc0023000-0xc0024000
	.zero	0xf80 - 16
#endif
data:
	.word	0xdeadbeef
