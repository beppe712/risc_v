#include <stdio.h>

void test(void)
{
	unsigned int oldState;
	unsigned int newState;

	// test basic operations
	newState = 0x00000001;
	asm volatile("csrrw %[old], mscratch, %[new]"
			: [old] "=&r" (oldState)
			: [new] "r" (newState) );
	printf("old = 0x%08x, new = 0x%08x\n", oldState, newState);
	printf("%s\n", (oldState == 0)?"OK":"NG");

	newState = 0x00000002;
	asm volatile("csrrw %[old], mscratch, %[new]"
			: [old] "=&r" (oldState)
			: [new] "r" (newState) );
	printf("old = 0x%08x, new = 0x%08x\n", oldState, newState);
	printf("%s\n", (oldState == 1)?"OK":"NG");

	newState = 0x00000030;
	asm volatile("csrrs %[old], mscratch, %[new]"
			: [old] "=&r" (oldState)
			: [new] "r" (newState) );
	printf("old = 0x%08x, new = 0x%08x\n", oldState, newState);
	printf("%s\n", (oldState == 2)?"OK":"NG");

	newState = 0x0000000f;
	asm volatile("csrrc %[old], mscratch, %[new]"
			: [old] "=&r" (oldState)
			: [new] "r" (newState) );
	printf("old = 0x%08x, new = 0x%08x\n", oldState, newState);
	printf("%s\n", (oldState == 0x32)?"OK":"NG");

	newState = 0x00000000;
	asm volatile("csrrs %[old], mscratch, %[new]"
			: [old] "=&r" (oldState)
			: [new] "r" (newState) );
	printf("old = 0x%08x, new = 0x%08x\n", oldState, newState);
	printf("%s\n", (oldState == 0x30)?"OK":"NG");

	// test data forwarding
	newState = 0x12345678;
	asm volatile("csrrw x0, mscratch, %[new]\n"
		     "csrrw %[old], mscratch, x0\n"
			: [old] "=&r" (oldState)
			: [new] "r" (newState) );
	printf("old = 0x%08x, new = 0x%08x\n", oldState, newState);
	printf("%s\n", (oldState == 0x12345678)?"OK":"NG");
}

void test_df()
{
	unsigned int oldState;
	unsigned int newState;

	// test data forwarding
	newState = 0x12345678;

	asm volatile("csrrw x0, mscratch, %[new]\n\t"
		     "nop\n\t"
		     "csrrw %[old], mscratch, x0"
			: [old] "=&r" (oldState)
			: [new] "r" (newState) );
	printf("old = 0x%08x, new = 0x%08x\n", oldState, newState);
	printf("%s\n", (oldState == 0x12345678)?"OK":"NG");
}

int main(int argc, char **argv)
{
	test();
	test_df();

	return 0;
}
