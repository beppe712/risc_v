	.text
	.globl	mmuen
mmuen:
	/* stvec point to virtual address of instruction after satp write */
	la	t3,3f
	csrrw	t3,stvec,t3

	/* trampoline */
	la	t0, pg_dir
	srl	t0,t0,12
	li	t1, 0x80000000	/* MODE */
	or	t0,t0,t1
	sfence.vma
	csrw	satp,t0
3:
	csrw	stvec,t3
	ret

	.data
	.align	12
pg_dir:
	.zero	0x800
	.word	0x200000cf
	.zero	0x800-4
