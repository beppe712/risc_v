#include <stdio.h>

int main(int argc, char **argv)
{
	unsigned int src;
	unsigned int dest;
	unsigned int data;

	data = 0xdeadbeef;
	src = 0xa5a5a5a5;
	dest = 0x00000000;
	asm volatile("amoswap.w	 %0, %2, %1"
			: "=r" (dest), "+A" (data)
			: "r" (src)
			: "memory");
	printf("src = 0x%08x, dest = 0x%08x, data = 0x%08x\n", src, dest, data);
	printf("%s\n", (dest == 0xdeadbeef)?"OK":"NG");
	printf("%s\n", (data == 0xa5a5a5a5)?"OK":"NG");

	data = 0x11111111;
	src = 0xa5a5a5a5;
	asm volatile("amoadd.w	 %0, %2, %1"
			: "=r" (dest), "+A" (data)
			: "r" (src)
			: "memory");
	printf("src = 0x%08x, dest = 0x%08x, data = 0x%08x\n", src, dest, data);
	printf("%s\n", (dest == 0x11111111)?"OK":"NG");
	printf("%s\n", (data == 0xb6b6b6b6)?"OK":"NG");

	data = 0x33333333;
	src = 0xa5a5a5a5;
	asm volatile("amoand.w	 %0, %2, %1"
			: "=r" (dest), "+A" (data)
			: "r" (src)
			: "memory");
	printf("src = 0x%08x, dest = 0x%08x, data = 0x%08x\n", src, dest, data);
	printf("%s\n", (dest == 0x33333333)?"OK":"NG");
	printf("%s\n", (data == 0x21212121)?"OK":"NG");

	data = 0x33333333;
	src = 0xa5a5a5a5;
	asm volatile("amoor.w	 %0, %2, %1"
			: "=r" (dest), "+A" (data)
			: "r" (src)
			: "memory");
	printf("src = 0x%08x, dest = 0x%08x, data = 0x%08x\n", src, dest, data);
	printf("%s\n", (dest == 0x33333333)?"OK":"NG");
	printf("%s\n", (data == 0xb7b7b7b7)?"OK":"NG");

	data = 0x33333333;
	src = 0xa5a5a5a5;
	asm volatile("amoxor.w	 %0, %2, %1"
			: "=r" (dest), "+A" (data)
			: "r" (src)
			: "memory");
	printf("src = 0x%08x, dest = 0x%08x, data = 0x%08x\n", src, dest, data);
	printf("%s\n", (dest == 0x33333333)?"OK":"NG");
	printf("%s\n", (data == 0x96969696)?"OK":"NG");

	/// signed max/min
	data = 0xffffffff;
	src = 0x00000001;
	asm volatile("amomax.w	 %0, %2, %1"
			: "=r" (dest), "+A" (data)
			: "r" (src)
			: "memory");
	printf("src = 0x%08x, dest = 0x%08x, data = 0x%08x\n", src, dest, data);
	printf("%s\n", (dest == 0xffffffff)?"OK":"NG");
	printf("%s\n", (data == 0x00000001)?"OK":"NG");

	data = 0x00000000;
	src = 0x80000000;
	asm volatile("amomax.w	 %0, %2, %1"
			: "=r" (dest), "+A" (data)
			: "r" (src)
			: "memory");
	printf("src = 0x%08x, dest = 0x%08x, data = 0x%08x\n", src, dest, data);
	printf("%s\n", (dest == 0x00000000)?"OK":"NG");
	printf("%s\n", (data == 0x00000000)?"OK":"NG");

	data = 0xffffffff;
	src = 0x00000001;
	asm volatile("amomin.w	 %0, %2, %1"
			: "=r" (dest), "+A" (data)
			: "r" (src)
			: "memory");
	printf("src = 0x%08x, dest = 0x%08x, data = 0x%08x\n", src, dest, data);
	printf("%s\n", (dest == 0xffffffff)?"OK":"NG");
	printf("%s\n", (data == 0xffffffff)?"OK":"NG");

	data = 0x00000000;
	src = 0x80000000;
	asm volatile("amomin.w	 %0, %2, %1"
			: "=r" (dest), "+A" (data)
			: "r" (src)
			: "memory");
	printf("src = 0x%08x, dest = 0x%08x, data = 0x%08x\n", src, dest, data);
	printf("%s\n", (dest == 0x00000000)?"OK":"NG");
	printf("%s\n", (data == 0x80000000)?"OK":"NG");

	/// unsigned max/min
	data = 0xffffffff;
	src = 0x00000001;
	asm volatile("amomaxu.w	 %0, %2, %1"
			: "=r" (dest), "+A" (data)
			: "r" (src)
			: "memory");
	printf("src = 0x%08x, dest = 0x%08x, data = 0x%08x\n", src, dest, data);
	printf("%s\n", (dest == 0xffffffff)?"OK":"NG");
	printf("%s\n", (data == 0xffffffff)?"OK":"NG");

	data = 0x80000001;
	src = 0x80000000;
	asm volatile("amomaxu.w	 %0, %2, %1"
			: "=r" (dest), "+A" (data)
			: "r" (src)
			: "memory");
	printf("src = 0x%08x, dest = 0x%08x, data = 0x%08x\n", src, dest, data);
	printf("%s\n", (dest == 0x80000001)?"OK":"NG");
	printf("%s\n", (data == 0x80000001)?"OK":"NG");

	data = 0x80000000;
	src = 0x80000001;
	asm volatile("amominu.w	 %0, %2, %1"
			: "=r" (dest), "+A" (data)
			: "r" (src)
			: "memory");
	printf("src = 0x%08x, dest = 0x%08x, data = 0x%08x\n", src, dest, data);
	printf("%s\n", (dest == 0x80000000)?"OK":"NG");
	printf("%s\n", (data == 0x80000000)?"OK":"NG");

	data = 0x80000001;
	src = 0x80000000;
	asm volatile("amominu.w	 %0, %2, %1"
			: "=r" (dest), "+A" (data)
			: "r" (src)
			: "memory");
	printf("src = 0x%08x, dest = 0x%08x, data = 0x%08x\n", src, dest, data);
	printf("%s\n", (dest == 0x80000001)?"OK":"NG");
	printf("%s\n", (data == 0x80000000)?"OK":"NG");

	return 0;
}
