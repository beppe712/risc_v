// UART status ((rx_error << 4) | (rx_empty << 3) | (rx_full << 2) | (tx_empty << 1) | (tx_full));
static volatile char *uart = (volatile char *)0x10000000;

void uart_init(void)
{
	//(activate) : baud_period(2), bit_count(8), stop_count(1), parity_type(3)
	*(unsigned short *)(uart + 4) = 1 | (15 << 1) | (3 << 5) | (0 << 7) | (3 << 8);
}

void uart_putch(char ch)
{
	while (*(unsigned int *)(uart + 4) & 1)
		;

	*uart = ch;
}

int puts(const char *s)
{
	char ch;

	while (ch = *s++)
		uart_putch(ch);

	return 1;
}

void _start(void)
{
	unsigned int status;

	uart_init();

	puts("OK\n");
	puts("Hello\n");

	do {
		status = *(unsigned int *)(uart + 4);
	} while ((status & (1 << 1)) == 0);

	for (;;) {
		asm("ebreak" :::);
	}
}
