#include <stdio.h>

#include "assert.h"

unsigned int inst[] = {
	0x00a00513,	// li      a0,10
	0x00008067,	// ret
};

int main(int argc, char **argv)
{
	unsigned int status;
	int (*fp)() = (int (*)())inst;

	assert(fp() == 10);

	inst[0] = 0x00b00513;	// li a0,11

	assert(fp() == 10);

	asm volatile("fence.i"::);

	assert(fp() == 11);

	return 0;
}
