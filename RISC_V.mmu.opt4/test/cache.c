#include <stdio.h>

#include "assert.h"

int array[5][64][16] __attribute__((aligned(64)));

int main(int argc, char **argv)
{
	unsigned int status;

	printf("%p \n", array);

//	asm volatile("fence"::);

	for (int i = 0; i < 16; i++)
		array[0][0][i] = i + 1;

	array[0][0][0] = 1;
	array[1][0][0] = 2;
	array[2][0][0] = 3;
	array[3][0][0] = 4;
	array[4][0][0] = 5;

//	asm volatile("fence"::);

	assert(array[0][0][0] == 1);
	assert(array[1][0][0] == 2);
	assert(array[2][0][0] == 3);
	assert(array[3][0][0] == 4);
	assert(array[4][0][0] == 5);

	return 0;
}
