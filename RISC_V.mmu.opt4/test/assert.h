#ifndef	_ASSERT_H_
#define	_ASSERT_H_

extern inline void assert(int b)
{
	printf(b?"OK\n":"NG\n");
}

#endif	// _ASSERT_H_
