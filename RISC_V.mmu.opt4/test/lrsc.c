#include <stdio.h>

int main(int argc, char **argv)
{
	int	prev;
	int	rc;
	int	counter;
	int	counter2;
	int	a;

	counter = 10;
	a = 3;

	asm volatile(
		"	lr.w	%[p], %[c]\n"
		"	add	%[rc], %[p], %[a]\n"
		"	sc.w.rl	%[rc], %[rc], %[c]\n"
		: [p] "=&r" (prev), [rc] "=&r" (rc), [c] "+A" (counter)
		: [a] "r" (a)
		: "memory");

	printf("counter = %d, rc = %d\n", counter, rc);
	printf("%s\n", (counter == 13)?"OK":"NG");
	printf("%s\n", (rc == 0)?"OK":"NG");

	counter2 = 100;

	asm volatile(
		"	lr.w	%[p], %[c]\n"
		"	add	%[rc], %[p], %[a]\n"
		"	sc.w.rl	%[rc], %[rc], %[d]\n"
		: [p] "=&r" (prev), [rc] "=&r" (rc), [c] "+A" (counter), [d] "+A" (counter2)
		: [a] "r" (a)
		: "memory");

	printf("counter2 = %d, rc = %d\n", counter2, rc);
	printf("%s\n", (counter == 13)?"OK":"NG");
	printf("%s\n", (counter2 == 100)?"OK":"NG");
	printf("%s\n", (rc == 1)?"OK":"NG");

	asm volatile(
		"	lr.w	%[p], %[c]\n"
		"	add	%[rc], %[p], %[a]\n"
		"	lw	zero, %[c]\n"
		"	sc.w.rl	%[rc], %[rc], %[c]\n"
		: [p] "=&r" (prev), [rc] "=&r" (rc), [c] "+A" (counter)
		: [a] "r" (a)
		: "memory");

	printf("counter = %d, rc = %d\n", counter, rc);
	printf("%s\n", (counter == 13)?"OK":"NG");
	printf("%s\n", (rc == 1)?"OK":"NG");

	return 0;
}
