#include <stdio.h>
#include <stdlib.h>

void mhandler(unsigned a0,
		unsigned a1,
		unsigned a2,
		unsigned a3,
		unsigned a4,
		unsigned a5,
		unsigned a6,
		unsigned a7)
{
	unsigned int mepc;
	unsigned int mcause;

	asm volatile("csrr %[old], mepc" : [old] "=&r" (mepc) : );
	asm volatile("csrr %[old], mcause" : [old] "=&r" (mcause) : );

	printf("Machine Interrupt: mepc = 0x%08x, mcause = 0x%08x\n", mepc, mcause);

	if (mcause == 9) {
		if (a7 == 0x5d)
			asm volatile("ebreak" : : );

		asm volatile("csrw mepc, %[new]" : : [new] "r" (mepc) );
	} else if (mcause == 0x80000007) {
		asm volatile("csrs mip, %[new]" : : [new] "r" (0x00000020) );
	}
}

void shandler(void)
{
	unsigned int sepc;
	unsigned int scause;

	asm volatile("csrc sip, %[new]" : : [new] "r" (0x00000020) );
	asm volatile("csrr %[old], sepc" : [old] "=&r" (sepc) : );
	asm volatile("csrr %[old], scause" : [old] "=&r" (scause) : );

	//printf("Supervisor interrupt: sepc = 0x%08x, scause = 0x%08x\n", sepc, scause);

	//printf("end\n");

	asm volatile("ebreak" : : );
}

void mcall(unsigned a0,
		unsigned a1,
		unsigned a2,
		unsigned a3,
		unsigned a4,
		unsigned a5,
		unsigned a6,
		unsigned a7)
{
	asm volatile("ecall" : : );
}

int main(int argc, char **argv)
{
	extern void _mhandler(void);
	extern void _shandler(void);
	extern void smode(void);
	extern void mmuen(void);

	printf("start\n");
	asm volatile("csrw mtvec, %[new]" : : [new] "r" (_mhandler) );
	asm volatile("csrs mie, %[new]" : : [new] "r" (0x00000000) );
	asm volatile("csrs medeleg, %[new]" : : [new] "r" (0x0000b000) );

	smode();

	mmuen();

	//printf("supervisor mode\n");
	asm volatile("csrw stvec, %[new]" : : [new] "r" (_shandler) );
	asm volatile("csrs sie, %[new]" : : [new] "r" (0x00000000) );
//	asm volatile("csrs sstatus, 0x02" : : );

	*(unsigned int *)4 = 0;

	return 0;
}
