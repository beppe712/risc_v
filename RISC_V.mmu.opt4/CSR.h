/*
 * CSR: Control and Status Register.
 */

#ifndef	_CSR_H_
#define	_CSR_H_

enum {
	// User Conter/Timers
	CSR_time    = 0xc01,
	CSR_timeh   = 0xc81,

	// Supervisor Trap Setup
	CSR_sstatus = 0x100,
	CSR_sedeleg = 0x102,
	CSR_sideleg,
	CSR_sie,
	CSR_stvec,
	CSR_scounteren,
	// Supervisor Trap Handling
	CSR_sscratch = 0x140,
	CSR_sepc,
	CSR_scause,
	CSR_stval,
	CSR_sip,
	// Supervisor Protection and Translation
	CSR_satp    = 0x180,

	CSR_mvendorid = 0xf11,
	CSR_marchid,
	CSR_mimpid,
	CSR_mhartid,

	CSR_mstatus = 0x300,
	CSR_misa,
	CSR_medeleg,
	CSR_mideleg,
	CSR_mie,
	CSR_mtvec,
	CSR_mcounteren,

	CSR_mscratch = 0x340,
	CSR_mepc,
	CSR_mcause,
	CSR_mtval,
	CSR_mip,

	CSR_mcycle = 0xb00,
	CSR_minstret = 0xb02,
	CSR_mcycleh = 0xb80,
	CSR_minstreth = 0xb82,
};

struct CSR {
	UINT32	stvec;
	UINT32	sscratch;
	UINT32	sepc;
	UINT32	scause;
	UINT32	stval;
	UINT32	satp;

	UINT32	mstatus;
	UINT32	medeleg;
	UINT32	mideleg;
	UINT32	mie;
	UINT32	mtvec;
	UINT32	mcounteren;

	UINT32	mscratch;
	UINT32	mepc;
	UINT32	mcause;
	UINT32	mtval;
	UINT32	mip;

	UINT32  mcycle;
	UINT32  minstret;
	UINT32  mcycleh;
	UINT32  minstreth;

	UINT32	read(UINT12 addr)
	{
		switch (addr) {
		case CSR_stvec:
			return stvec;
		case CSR_sscratch:
			return sscratch;
		case CSR_sepc:
			return sepc;
		case CSR_scause:
			return scause;
		case CSR_stval:
			return stval;
		case CSR_satp:
			return satp;

		case CSR_sstatus:
		case CSR_mstatus:
			return mstatus;
		case CSR_misa:
			return	(1 << ('U' - 'A')) |
				(1 << ('S' - 'A')) |
				(1 << ('M' - 'A')) |
				(1 << ('I' - 'A')) |
				(1 << ('A' - 'A'));
		case CSR_medeleg:
			return medeleg;
		case CSR_mideleg:
			return mideleg;
		case CSR_sie:
		case CSR_mie:
			return mie;
		case CSR_mtvec:
			return mtvec;
		case CSR_mcounteren:
			return mcounteren;

		case CSR_mscratch:
			return mscratch;
		case CSR_mepc:
			return mepc;
		case CSR_mcause:
			return mcause;
		case CSR_mtval:
			return mtval;
		case CSR_sip:
		case CSR_mip:
			return mip;
		case CSR_mcycle:
			return mcycle;
		case CSR_minstret:
			return minstret;
		case CSR_mcycleh:
			return mcycleh;
		case CSR_minstreth:
			return minstreth;
		default:
//printf("CSR read 0x%08x\n", addr);
			return 0;
		}
	}

	void write(UINT12 addr, UINT32 data)
	{
		switch (addr) {
		case CSR_stvec:
			stvec = data;
			break;
		case CSR_sscratch:
			sscratch = data; 
			break;
		case CSR_sepc:
			sepc = data;
			break;
		case CSR_scause:
			scause = data;
			break;
		case CSR_stval:
			stval = data;
			break;
		case CSR_satp:
			satp = data;
			break;

		case CSR_sstatus:
			mstatus &= ~0x800de133;
			mstatus |= (data & 0x800de133);
			break;
		case CSR_mstatus:
			mstatus = data;
			break;
#if	0
//		case CSR_misa:
			mcsr0[addr - CSR_mstatus] = data;
			break;
#endif
		case CSR_medeleg:
			medeleg = data;
			break;
		case CSR_mideleg:
			mideleg = data;
			break;
		case CSR_sie:
			mie &= ~0x333;
			mie |= (data & 0x333);
			break;
		case CSR_mie:
			mie = data;
			break;
		case CSR_mtvec:
			mtvec = data;
			break;
		case CSR_mcounteren:
			mcounteren = data;
			break;

		case CSR_mscratch:
			mscratch = data;
			break;
		case CSR_mepc:
			mepc = data;
			break;
		case CSR_mcause:
			mcause = data;
			break;
		case CSR_mtval:
			mtval = data;
			break;
		case CSR_sip:
			mip &= ~0x333;
			mip |= (data & 0x333);
			break;
		case CSR_mip:
			// do nothing
			mip &= ~0xbbb;
			mip |= (data & 0xbbb);
			break;
		default:
//printf("CSR write 0x%08x\n", addr);
			break;
		}
	}
};

#endif	// _CSR_H_
