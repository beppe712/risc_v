#include <stdio.h>
#if 1   /// 7/1/20
#include "../C2Rlib/utils/ELFUtil.h"
#else
#include "../CPP_utils/ELFUtil.h"
#endif

typedef ELF_Parser::Elf32_Shdr	Elf32_Shdr;

typedef struct _MEMCTLPin {	///	memory controller pins!!!
	UINT32	addr;
	UINT32	din, dout;
	UINT3	size;
	UINT4	cs;	///	4-bit chip select
	BIT		we, ras, cas;
	void set_cs(UINT4 idx) {
		switch (idx) {
		case 0:
		case 2:		cs = 1; break;
		case 1:
		case 3:		cs = 2; break;
		default:	cs = 0; break;
		}
	}
	void set_outpin(struct _MEMCTLPin *xmem_pin) {
		xmem_pin->din = dout;
		xmem_pin->addr = addr;
		xmem_pin->size = size;
		xmem_pin->cs = cs;
		xmem_pin->we = we;
		xmem_pin->cas = cas;
		xmem_pin->ras = ras;
	}
} MEMCTLPin;

typedef MEMCTLPin D_MEMCTLPin _T(direct_signal);

struct MEMCTL_AXI4L : AXI4L::SlaveFSM {	///	memory controller!!!!
	MEMCTLPin	mpin _T(state);
	ST_UINT8	readCount;
	UINT32		mraddr, mwaddr, din;
	UINT3		mrsize, mwsize;
//#if defined(ENABLE_AXI_BURST)
	ST_UINT4	burst_count_mr, burst_count_mw;
	BIT devReadSetup(UINT32 addr) {
		if (w_state != W_Init)
			return 0;
//printf("devReadSetup: 0x%08x\n", addr);
		mraddr = addr;
		readFlag = 1;
//#if defined(ENABLE_AXI_BURST)
		burst_count_mr = 1;
		return 1;
	}
	BIT devRead(UINT32 *data, UINT32 addr, UINT3 size) {
#define READ_WAIT	2//3//4//4//4
		bool readReady = (readCount >= READ_WAIT);
		*data = din;
		mraddr = addr + (burst_count_mr << size);
		{
			burst_count_mr++;
			burst_count_mr &= 0xf;
		}
		mrsize = size;
		readFlag = 1;
		return readReady;
	}
	BIT devWriteSetup(UINT32 addr) {
		if (readFlag != 0)
			return 0;

		mwaddr = addr;
		burst_count_mw = 0;
		return 1;
	}
	BIT devWrite(UINT32 data, UINT32 addr, UINT3 size) {
		mpin.dout = data;
		writeFlag = 1;
		mwaddr = addr + (burst_count_mw << size);
		burst_count_mw++;
		burst_count_mw &= 0xf;
		mwsize = size;
		return 1;
	}
	void fsmUser() {
		if (readFlag) { readCount++; }
		else { readCount = 0; }
		mpin.set_cs((readFlag) ? 0 : (writeFlag) ? 0 : 4);
		mpin.we = !readFlag;
		if (readFlag || writeFlag) {	///	these are bogus....
			mpin.cas = 0;
			mpin.ras = 0;
		}
		else {
			mpin.cas = 1;
			mpin.ras = 1;
		}
		mpin.addr = ((readFlag) ? mraddr : mwaddr) & (~MEM_DEV_ADDR_MASK);
		mpin.size = (readFlag) ? mrsize : mwsize;
	}
	_C2R_FUNC(1)
	void step(AXI4L::CH *axi, D_MEMCTLPin *mem_pin) {
		INIT_VIRTUAL_BUS_SLAVE(*this, axi)
		din = mem_pin->dout;
		mpin.set_outpin(mem_pin);
		mraddr = 0;
		fsm(axi);
	}
};

struct ExtMem {
	unsigned int mem[PMEM_SIZE + DMEM_SIZE];
	int cycle;
	void update(MEMCTLPin *mem_pin) {
		if (mem_pin->cs & 1) {
			mem_access(mem, mem_pin);
		}
	}
	// elf loader
	FILE *fp;
    Elf32_Half phnum;
    /// 7/1/20
    std::vector<ELF_Parser::Elf_Phdr> phdr;

	struct page {
		struct page *next;
		unsigned int  tag;
		unsigned int  m[4096/sizeof(unsigned int)];
	};
	struct page *bucket[1024];

	struct page *get_page(unsigned int addr) {
		unsigned int ppn1 = (addr >> 22) & 0x1ff;
		unsigned int ppn2 = (addr >> 12) & 0x3ff;

		struct page *pg = bucket[ppn2];

		for (pg = bucket[ppn2]; pg; pg = pg->next) {
			if (pg->tag == ppn1) {
				return pg;
			}
		}

		pg = (struct page *)calloc(1, sizeof(struct page));
		pg->tag = ppn1;
		pg->next = bucket[ppn2];
		bucket[ppn2] = pg;

		addr &= 0xfffff000;
        unsigned PT_LOAD_val = ELF_Parser::ptypeGroup.GetValue("LOAD")->value;
        int i;
		for (i = 0; i < phnum; i++) {
			if (phdr[i].p_type == PT_LOAD_val) {
				if ((addr < phdr[i].p_paddr + phdr[i].p_filesz) && 
				    (addr + 0x1000 > phdr[i].p_paddr)) {
					int offset;

					if (addr < phdr[i].p_paddr) {
                        /// 7/1/20
                        offset = (int)(phdr[i].p_paddr - addr);
						addr = 0;
					} else {
						offset = 0;
                        /// 7/1/20
                        addr -= (int)phdr[i].p_paddr;
					}
					int sz = 0x1000 - offset;

                    if (addr + sz > phdr[i].p_filesz) {
                        /// 7/1/20
                        sz = (int)(phdr[i].p_filesz - addr);
                    }
                    /// 7/1/20
                    fseek(fp, (long)(phdr[i].p_offset + addr), SEEK_SET);
					fread((char *)pg->m + offset, 1, sz, fp);
				}
			}
		}

		return pg;
	}

	void mem_access(unsigned int *m, MEMCTLPin *mem_pin) {
		struct page *pg = get_page(mem_pin->addr);
		unsigned int addr;

		addr = (mem_pin->addr) & 0xfff;
		if (mem_pin->we) {
			switch (mem_pin->size) {
			case 0:
				((unsigned char *)pg->m)[addr] = mem_pin->din >> ((addr & 3) * 8);
				break;
			case 1:
				((unsigned short *)pg->m)[addr >> 1] = mem_pin->din >> ((addr & 2) * 8);
				break;
			case 2:
				pg->m[addr >> 2] = mem_pin->din;
				break;
			}
		} else {
			mem_pin->dout = pg->m[addr >> 2];
		}
	}

	void load_program(const char *filename, unsigned int *pc, unsigned int *sp, int skip) {
		// cleanup
		if (fp)
			fclose(fp);
		int h;
		for (h = 0; h < 1024; h++) {
			while (bucket[h]) {
				struct page *pg = bucket[h];

				bucket[h] = pg->next;
				free(pg);
			}
		}

		//
		fp = fopen(filename, "rb");
		if (fp == NULL) {
			fprintf(stderr, "Can't open program: %s\n", filename);
			return;
		}
		
        /// 7/1/20
        ELF_Parser::Info elfInfo;
        auto &hdr = elfInfo.elfHeader;
        /// 7/1/20
        if (!elfInfo.elfHeader.Parse(fp)) {
			fprintf(stderr, "ELF header error\n");
			fclose(fp);
			fp = NULL;
			return;
		}
        /// 7/1/20
        if (!elfInfo.segmentTable.Parse(&elfInfo, fp)) {
			fprintf(stderr, "ELF pheader error\n");
			fclose(fp);
			fp = NULL;
			return;
		}
        /// 7/1/20
        phnum = elfInfo.elfHeader.ehdr.e_phnum;
        phdr.resize(phnum);
        for (int j = 0; j < phnum; ++j) {
            phdr[j] = elfInfo.segmentTable.phdr[j];
        }
		int i;
		for (i = 0; i < phnum; i++) {
			phdr[i].p_paddr &= 0x7fffffff;
		}

		// get end address.
		unsigned int end_addr = 0;

        unsigned SHF_ALLOC_val = ELF_Parser::shflagGroup.GetValue("A")->value;
        /// 7/1/20
        fseek(fp, (long)hdr.ehdr.e_shoff, SEEK_SET);
		for (int i = 0; i < hdr.ehdr.e_shnum; i++) {
			Elf32_Shdr shdr;

			fread(&shdr, sizeof(Elf32_Shdr), 1, fp);
			if (shdr.sh_flags & SHF_ALLOC_val) {
				unsigned int addr;

				addr = shdr.sh_addr + shdr.sh_size;
				if (addr > end_addr)
					end_addr = addr;
			}
		}
        /// 7/1/20
        *pc = (unsigned)(hdr.ehdr.e_entry + skip);
        *sp = (unsigned)(end_addr + 0x1000);
	}

	void load_dtb(const char *filename, unsigned int *dtb) {
		FILE *fp;

		fp = fopen(filename, "rb");
		if (fp == NULL) {
			fprintf(stderr, "Can't open dtb: %s\n", filename);
			return;
		}

		struct page *pg = get_page(0xbfc00000);
		fread(pg->m, 1, 0x1000, fp);

		*dtb = 0xbfc00000;

		fclose(fp);
	}
};
