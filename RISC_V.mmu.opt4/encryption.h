#define KEYTYPE UINT64

struct secure_mem {

	int keylen;

	KEYTYPE initKey() {
		char* keystr = getenv("ENCRYPTION_KEY_VALUE");
		char* keylenstr = getenv("ENCRYPTION_KEY_LEN");
		KEYTYPE key_mem = atoi(keystr);
		keylen = atoi(keylenstr);
		return key_mem;
	}

	KEYTYPE key = initKey();

	UINT64 keyRange(uint k, uint p) {
		// extract k bits from position p
		return (key >> (keylen - k - p)) & ((1 << k) - 1);
	}
};