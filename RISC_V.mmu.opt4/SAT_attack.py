#!/usr/bin/python3

import os
import errno
import sys
import subprocess
import filecmp
import random
import time

key_len = 2
correct_key = 1
thresh_timeout = 10 # seconds

test_list = ["amo", "cache", "csr", "hazard", "hello", "icache", "irq", "lrsc", "stimer", "test", "timer"]
key_list = range(2**key_len)
proc_list = []

def terminate_threads():
    # wait or terminate process in case the simulation reaches timeout
    while proc_list:
        for key,starttime,proc in proc_list:
            try:
                outs, errs = proc.communicate(timeout=0)
                proc_list.remove((key,starttime,proc))
            except subprocess.TimeoutExpired:
                if time.time() - starttime > thresh_timeout:
                    proc.kill()
                    print("process \"%s\" with key %d killed" % (" ".join(proc.args), key))
                    outs, errs = proc.communicate()
                    proc_list.remove((key,starttime,proc))

def run(test_name, key, test_output_dir=None, test_output_filename=None, terminal_output=False):
    # make dir for UART output
    if test_output_dir is None:
        test_output_dir = "test_output/%s" % test_name
    if not os.path.exists(test_output_dir):
        try:
            os.makedirs(test_output_dir)
        except OSError as exc:
            if exc.errno != errno.EEXIST:
                raise
    if test_output_filename is None:
        test_output_filename = "key_%d.txt" % key

    if not os.path.isfile("test/%s.out" % test_name):
        # compile test application binary
        subprocess.run(["make", "-C", "test", "%s.out" % test_name])
    
    my_env = os.environ.copy()
    my_env["ENCRYPTION_KEY_VALUE"] = "%d" % key
    my_env["ENCRYPTION_KEY_LEN"] = "%d" % key_len
    my_env["TEST_OUTPUT_FILEPATH"] = "%s/%s" % (test_output_dir,test_output_filename)

    print("running %s with key %d" % (test_name,key))
    # subprocess.run(["bin/sim/RVProc_FIFO.exe", "test/%s.out" % test_name], env=my_env, stdout=subprocess.DEVNULL, timeout=30)
    proc_list.append((key,time.time(),subprocess.Popen(["bin/sim/RVProc_FIFO.exe", "test/%s.out" % test_name], env=my_env, stdout=(None if terminal_output else subprocess.DEVNULL))))

def run_test(test_name):
    for key in key_list:
        run(test_name, key)
    run(test_name,correct_key,test_output_filename="oracle.txt")
    terminate_threads()
    total_runs = len(key_list)
    dir = "test_output/%s" % test_name
    valid_keys = [key for key in key_list if filecmp.cmp("%s/key_%d.txt" % (dir,key), "%s/oracle.txt" % (dir))]
    print("Valid keys for this test: [%s]" % " ".join([str(key) for key in valid_keys]))
    print("Correct outputs: %d/%d" % (len(valid_keys), total_runs))

def sat_attack():
    dir = "test_output/sat_ws"
    if not os.path.exists(dir):
        try:
            os.makedirs(dir)
        except OSError as exc:
            if exc.errno != errno.EEXIST:
                raise

    valid_keys = list(key_list)
    oracle_access = 0

    for test in test_list:
        for key in valid_keys:
            run(test, key, dir)
        terminate_threads()
        for i in range(len(valid_keys)-1):
            # while not subprocess.check_output(["diff", "-I", "\"[.*]\"", "%s/key_%d.txt" % (dir,eval_keys[0]), "%s/key_%d.txt" % (dir,eval_keys[1])]):
            if not (filecmp.cmp("%s/key_%d.txt" % (dir,valid_keys[i]), "%s/key_%d.txt" % (dir,valid_keys[i+1]))):
                # found DIP
                # access oracle
                run(test,correct_key,dir,"oracle.txt")
                terminate_threads()
                oracle_access += 1
                # check all possible keys
                for key in valid_keys.copy():
                    print("comparing key %d with oracle" % key)
                    if not (filecmp.cmp("%s/oracle.txt" % (dir), "%s/key_%d.txt" % (dir,key))):
                        valid_keys.remove(key)
                print("Valid keys after DIP %d: %s" % (oracle_access,valid_keys))
                # input() # to pause execution
                break # goes to the next test (input)
        if len(valid_keys) == 1:
            break
            
    print("Oracle accesses count: %d" % oracle_access)
    print("Valid keys: [%s]" % " ".join([str(key) for key in valid_keys]))

def main():
    subprocess.run(["make", "sim"], stdout=subprocess.DEVNULL)
    test_app = sys.argv[1]
    if test_app == "sat":
        sat_attack()
    elif test_app == "tests":
        for test in test_list:
            run_test(test)
            input()
    elif test_app in test_list:  
        if len(sys.argv) > 2:
            key = int(sys.argv[2])
            run(test_app,key,terminal_output=True)
            terminate_threads()
        else:
            run_test(test_app)
    else:
        print("test %s not valid" % test_app)
        return

if __name__ == "__main__":
    # execute only if run as a script
    main()

    

        


    
    