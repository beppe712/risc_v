

#include "RVProc.h"
#include "RVProc_io_ext.h"

#if 1	///	move to RVProc_io_ext.cpp
typedef unsigned char u_char;

#define EXT_FIFO_BUF_SIZE	1000
struct EXT_FIFO
{
	int cpu_cycle, cpu_mode;
	UINT32 * p_cpu_cycle;
	FIFO_PORT in, out;
	char rx_buf[EXT_FIFO_BUF_SIZE];
	int rx_pnt;
	int in_count, out_count, match_count, no_match_count, in_fifo_terminated, out_fifo_terminated;
	int in_error_count, out_error_count;
	int in_interval, out_interval;
	int prev_in_cycle, prev_out_cycle;
	//	FILE * src_img_fp, * out_jpg_fp;
	FILE * in_fp, *out_fp;
	int in_bytes, in_residue_bytes, in_residue_data;
} x_fifo = { 0 };

#define MAX_INTERVAL	1//5//8//7//141
#define RAND_SEED		0
#define IN_RAND_INTERVAL	(abs(rand()) % MAX_INTERVAL)
#define OUT_RAND_INTERVAL	(abs(rand()) % MAX_INTERVAL)

#define MAX_MSG_COUNT	20

void fifo_port_reset(FIFO_PORT * fifo)
{
	fifo->chanID = 0;
	fifo->data = 0;
	fifo->ready = 0;
	fifo->strobe = 0;
	fifo->bytes = 0;
}

void fifo_ext_set_in_interval() { x_fifo.in_interval = IN_RAND_INTERVAL;  x_fifo.in.strobe = (x_fifo.in_interval == 0); }
void fifo_ext_set_out_interval() { x_fifo.out_interval = OUT_RAND_INTERVAL; x_fifo.out.ready = (x_fifo.out_interval == 0); }

//#include "../../TEST/myJPEG5_2/image_info.h"
//#include "../../../../../images/image_info.h"
//#define OUTPUT_DATA_FILE_NAME 	"../../TEST/prime/out.dat"
//#define OUTPUT_DATA_FILE_NAME 	"../../TEST/testMemIO/out.dat"
#define OUTPUT_DATA_FILE_NAME 	"../../TEST/testRVMPSoC/out.dat"

void fifo_ext_init(FIFO_PORT * in_fifo, FIFO_PORT * out_fifo, UINT32 * p_cpu_cycle)
{
	srand(RAND_SEED);
	x_fifo.p_cpu_cycle = p_cpu_cycle;
	fifo_port_reset(&x_fifo.in);
	fifo_ext_set_in_interval();
	x_fifo.in.chanID = 0;
	x_fifo.in.data = 0;
	fifo_port_reset(&x_fifo.out);
	fifo_ext_set_out_interval();
	x_fifo.in_count = 0;
	x_fifo.in_count++;
	x_fifo.out_count = 0;
	x_fifo.match_count = 0;
	x_fifo.no_match_count = 0;
	x_fifo.in_fifo_terminated = 0;
	x_fifo.out_fifo_terminated = 0;
	x_fifo.cpu_cycle = 0;
	x_fifo.cpu_mode = 0;
	x_fifo.in_error_count = 0;
	x_fifo.out_error_count = 0;
	x_fifo.prev_in_cycle = 0;
	x_fifo.prev_out_cycle = 0;
	x_fifo.in_bytes = 0;
	x_fifo.in_residue_bytes = 0;
	x_fifo.in_residue_data = 0;

	if (x_fifo.in_fp) { fclose(x_fifo.in_fp); }
	if (x_fifo.out_fp) { fclose(x_fifo.out_fp); }
	x_fifo.in_fp = 0;// fopen(INPUT_DATA_FILE_NAME, "rb");
	x_fifo.out_fp = fopen(OUTPUT_DATA_FILE_NAME, "rb");
}

int fifo_ext_print_result()
{
	printf("x_fifo: match_count = %d, no_match_count = %d, out_fifo_count = %d, in_fifo_count = %d, out_error_count = %d\n",
		x_fifo.match_count, x_fifo.no_match_count, x_fifo.out_count, x_fifo.in_count, x_fifo.out_error_count);
	if (x_fifo.in_fp) { fclose(x_fifo.in_fp); }
	if (x_fifo.out_fp) { fclose(x_fifo.out_fp); }
	x_fifo.in_fp = 0;
	x_fifo.out_fp = 0;
	return (x_fifo.no_match_count != 0);
}

void x_out_fifo_update()
{
	int cpu_cycle = (x_fifo.cpu_mode == 1) ? x_fifo.cpu_cycle : *x_fifo.p_cpu_cycle;
	if (x_fifo.out.chanID == 1) {	///	char-data
		if (x_fifo.rx_pnt == EXT_FIFO_BUF_SIZE - 3) {
			x_fifo.rx_buf[x_fifo.rx_pnt++] = '\n';
			x_fifo.rx_buf[x_fifo.rx_pnt] = 0;
			printf("[%10d] <FIFO_X.RX.pending> msg = %s", cpu_cycle, x_fifo.rx_buf);
			x_fifo.rx_pnt = 0;
		}
		x_fifo.rx_buf[x_fifo.rx_pnt++] = x_fifo.out.data;
		x_fifo.rx_buf[x_fifo.rx_pnt] = 0;
		if (x_fifo.out.data == '\n') {
			printf("[%10d] <FIFO_X.RX> msg = %s", cpu_cycle, x_fifo.rx_buf);
			x_fifo.rx_pnt = 0;
		}
	}
	else if (!x_fifo.out_fifo_terminated) {	///	x_fifo.out.chanID == 0
		if (x_fifo.out_fp) {
			UINT32 c;
#if 1   /// 7/1/20
            size_t sz = fread(&c, 4, 1, x_fifo.out_fp);
#else
			int sz = fread(&c, 4, 1, x_fifo.out_fp);
#endif
			if (sz != 1) { x_fifo.out_fifo_terminated = 1; }
			else {
				int matched = (c == x_fifo.out.data);
				x_fifo.match_count += matched;
				x_fifo.no_match_count += !matched;
#if 1	///	1/19/17
				if (!matched) {
					printf("[%10d] <FIFO_X.RX> ----------- unmatch!! ref(%08x), fifo(%08x)\n", cpu_cycle, c, x_fifo.out.data);
				}
#endif
				if (!matched)
				{
					int jj = 0;
				}
			}
		}
		else { x_fifo.out_fifo_terminated = 1; }
		x_fifo.prev_out_cycle = cpu_cycle;
		x_fifo.out_count++;
	}
	else { x_fifo.out_error_count++; }
}

void x_in_fifo_update()
{
	int cpu_cycle = (x_fifo.cpu_mode == 1) ? x_fifo.cpu_cycle : *x_fifo.p_cpu_cycle;
	int c;
	if (!x_fifo.in_fifo_terminated) {
		x_fifo.prev_in_cycle = cpu_cycle;
		x_fifo.in.strobe = 1;
		x_fifo.in.chanID = 0;
		if (x_fifo.in_fp) {
			int rx_bytes = x_fifo.in.bytes, i, d = x_fifo.in_residue_data;
			for (i = x_fifo.in_residue_bytes; i < rx_bytes; i++) {
				c = getc(x_fifo.in_fp);
				if (c == EOF) {
					x_fifo.in_fifo_terminated = 1;
					break;
				}
				else { d |= (c & 0xff) << (i * 8); }
			}
			x_fifo.in.data = d;
			x_fifo.in_residue_bytes = 0;
			x_fifo.in_residue_data = 0;
		}
		if (!x_fifo.in_fifo_terminated) { x_fifo.in_count++; }
	}
	else {
		if (x_fifo.in_fifo_terminated == 1) {
			x_fifo.in.strobe = 1;
			x_fifo.in.chanID = 1;
			x_fifo.in.data = 0;
			x_fifo.in_fifo_terminated = 2;
			printf("[%10d] <FIFO_X.TX> : in_fifo_terminated (sending data to chan[1])\n", cpu_cycle);
		}
		else {
			x_fifo.in.strobe = 0;
		}
	}
}

void set_fifo_FW(FIFO_PORT * fifo_src, FIFO_PORT * fifo_dst);
void set_fifo_BW(FIFO_PORT * fifo_src, FIFO_PORT * fifo_dst);

void fifo_ext_input(FIFO_PORT * in_fifo, FIFO_PORT * out_fifo)
{
	set_fifo_FW(&x_fifo.in, in_fifo);
	set_fifo_BW(out_fifo, &x_fifo.out);
	if (x_fifo.in_interval > 0) { x_fifo.in.strobe = 0; x_fifo.in_interval--; }
	else { x_fifo.in.strobe = 1; }
	///	8/10/15
	if (x_fifo.out.strobe) { x_out_fifo_update(); fifo_ext_set_out_interval(); }
	if (x_fifo.cpu_mode == 1) { x_fifo.cpu_cycle++; }
}

void fifo_ext_output(FIFO_PORT * in_fifo, FIFO_PORT * out_fifo)
{
	set_fifo_BW(&x_fifo.in, in_fifo);
	set_fifo_FW(out_fifo, &x_fifo.out);
	if (x_fifo.out_interval > 0) { x_fifo.out.ready = 0; x_fifo.out_interval--; }
	else { x_fifo.out.ready = 1; }
	if (x_fifo.in.ready) { x_in_fifo_update(); fifo_ext_set_in_interval(); }

	if (x_fifo.in.bytes != x_fifo.in_bytes) {
		if (x_fifo.in.bytes > x_fifo.in_bytes) {
			int i, d = x_fifo.in.data << ((x_fifo.in.bytes - x_fifo.in_bytes) * 8);
			if (x_fifo.in_fp) {
				for (i = 0; i < (x_fifo.in.bytes - x_fifo.in_bytes); i++) {
					int c = getc(x_fifo.in_fp);
					if (c == EOF) {
						x_fifo.in_fifo_terminated = 1;
						break;
					}
					else { d |= (c & 0xff) << (i * 8); }
				}
			}
			x_fifo.in.data = d;
		}
		else {
			x_fifo.in_residue_bytes = x_fifo.in_bytes - x_fifo.in.bytes;
			x_fifo.in_residue_data = x_fifo.in.data >> ((x_fifo.in_bytes - x_fifo.in_residue_bytes) * 8);
			x_fifo.in.data &= 0xffffffff >> (x_fifo.in_residue_bytes * 8);
		}
		x_fifo.in_bytes = x_fifo.in.bytes;
	}
}

void fifo_ext_switch_mode(FIFO_PORT * in_fifo, FIFO_PORT * out_fifo)
{
	if (x_fifo.rx_pnt) { x_fifo.out.chanID = 1; x_fifo.out.data = '\n'; x_out_fifo_update(); }
	fifo_ext_init(in_fifo, out_fifo, x_fifo.p_cpu_cycle);
	x_fifo.cpu_mode = 1;
}

BIT UART::parity_bit(BIT parity) {
	switch (parity_type) {
	case IOP_Odd:	return !parity;
	case IOP_Even:	return parity;
	case IOP_High:	return 1;
	default:		return 0;
	}
}

/// 6/5/17 : C++!!!
void UART::do_tx(UINT2 io_op, UINT32 src0)
{
	IOBuf * buf = &tx.buf;
	int empty = BUF_EMPTY(buf), full = BUF_FULL(buf);
	int rp = buf->rp, wp = buf->wp;
	int wflag = (io_op == IO_WriteData) && !full, rflag = 0;
#ifndef  __LLVM_C2RTL__//__TCT_COMPILER_USED__
	int prev_state = tx.state;
#endif
	tx.stalled = (io_op == IO_WriteData) && full;

	if (tx.state == IOS_Idle) {
		if (!empty) {
			rflag = 1;
			tx.activate(buf->buf[rp]);
			tx.bit_n = !0;	///	start_bit = 0;
		}
	}
	else {
		if (tx.tick < baud_period_m1) {
			int stop_at_half_period = (tx.state == IOS_Stop && tx.bit_pos && stop_count == 3);	///	stop_count = 3 indicates 1.5 bit length
			int is_half_period = (tx.tick == (baud_period_m1 >> 1));
			tx.tick++;
			if (stop_at_half_period && is_half_period) { tx.state = IOS_Idle; }	///	stop_length = 1.5 bit
		}
		else {	///	1 baud period reached...
			int bit = tx.word & 0x1;
			tx.tick = 0;
			switch (tx.state) {
			case IOS_Start:
				tx.bit_n = !bit;
				///	bit_pos : 0, 1, 2, ..., bit_count - 1
				if (tx.bit_pos == bit_count_m1) { tx.state = (parity_type != IOP_None) ? IOS_Parity : IOS_Stop; }
				tx.bit_pos++;
				tx.parity ^= bit;
				tx.word >>= 1;
				break;
			case IOS_Parity:
				tx.bit_n = !parity_bit(tx.parity);
				tx.bit_pos = 0;
				tx.state = IOS_Stop;
				break;
			case IOS_Stop:
				if (tx.bit_pos < stop_count) {
					tx.bit_n = !1;	///	stop_bit = 1;
					tx.bit_pos++;
				}
				else {	///	stop_bit finished... ready to transmit next word
					if (!empty) {
						rflag = 1;
						tx.activate(buf->buf[rp]);
						tx.bit_n = !0;	///	start_bit = 0;
					}
					else {
						tx.state = IOS_Idle;
						tx.bit_n = !1;	///	stop_bit = 1;
					}
				}
				break;
			}
		}
	}
	if (wflag) { buf->buf[wp] = src0; }

	buf->update(rflag, wflag);
	tx.irq = rflag;
}

UINT32 UART::adjust_word()
{
	switch (bit_count_m1) {
	case 5 - 1:		return (rx.word >> 3);
	case 6 - 1:		return (rx.word >> 2);
	case 7 - 1:		return (rx.word >> 1);
	default:		return (rx.word >> 0);
	}
}

UINT32 UART::do_rx(UINT2 io_op)
{
	IOBuf * buf = &rx.buf;
	int empty = BUF_EMPTY(buf);
	int full = BUF_FULL(buf);
	int wp = buf->wp;
	int rp = buf->rp;
	int rflag = (io_op == IO_ReadData) && !empty;
	int wflag = 0;
	UINT32 ret_val = 0;
	int bit_rx = (!rx.bit_n);
	rx.stalled = (io_op == IO_ReadData) && empty;

	if (rflag)
	{
		ret_val = buf->buf[rp];
	}
	if (rx.state == IOS_Idle && bit_rx == 0)
	{
		rx.activate(0);
	}	///	start_bit = 0
	else if (rx.state != IOS_Idle) {
		if (rx.tick < baud_period_m1) {
			int stop_at_half_period = (rx.state == IOS_Stop && rx.bit_pos && stop_count == 3);	///	stop_count = 3 indicates 1.5 bit length
			int is_half_period = (rx.tick == (baud_period_m1 >> 1));
			rx.tick++;
			if (is_half_period) {	///	sample at middle of baud period
				rx_bit_samp = bit_rx;
				if (rx.state == IOS_Start) {
					rx.word = (bit_rx << 7) | (rx.word >> 1);
					rx.parity ^= bit_rx;
				}	///	ignore parity_bit/end_bit
				if (stop_at_half_period) { rx.state = IOS_Idle; }
			}
		}
		else {	///	1 baud period reached...
			rx.tick = 0;
			switch (rx.state) {
			case IOS_Start:
				if (rx.bit_pos > bit_count_m1) {	///	0, 1, 2, ..., bit_count - 1, bit_count (need to include start_bit here...)
					rx.state = (parity_type != IOP_None) ? IOS_Parity : IOS_Stop;
					if ((wflag = !full)) {
						UINT32 rx_word = adjust_word();
						buf->buf[wp] = rx_word;
					}
					rx.bit_pos = 0;
				}
				else { rx.bit_pos++; }
				break;
			case IOS_Parity:
				if (parity_bit(rx.parity) != rx_bit_samp) { rx.error = 1; }
				rx.state = IOS_Stop;
				rx.bit_pos++;
				break;
			case IOS_Stop:
				if (rx_bit_samp != 1 || full) { rx.error = 1; }
				///	9/24/14 : for rx, only assume 1 stop-bit
				if (bit_rx == 0) { rx.activate(0); }	///	sample start-bit here!!!
				else { rx.state = IOS_Idle; }
			}
		}
	}
	buf->update(rflag, wflag);
	///	4/28/15 : modified again.... 
	rx.irq = !empty;
	return ret_val;
}

static const int uart_baud_period[16] = {
	BAUD_PERIOD(50),		///	0
	BAUD_PERIOD(300),		///	1
	BAUD_PERIOD(1200),		///	2
	BAUD_PERIOD(2400),		///	3
	BAUD_PERIOD(4800),		///	4
	BAUD_PERIOD(9600),		///	5
	BAUD_PERIOD(19200),		///	6
	BAUD_PERIOD(38400),		///	7
	BAUD_PERIOD(57600),		///	8
	BAUD_PERIOD(115200),	///	9
							///	10 - 14
							BAUD_PERIOD(115200), BAUD_PERIOD(115200), BAUD_PERIOD(115200), BAUD_PERIOD(115200), BAUD_PERIOD(115200),
							///	4/27/15 : added ULTRA_FAST_MODE
							1,
};

static const int uart_data_length[4] = { 5 - 1, 6 - 1, 7 - 1, 8 - 1 };	///	include start_bit and -1
static const int uart_stop_length[4] = { 3, 2, 2, 2 };	///	-1

const char * io_parity_type_name(int parity_type)
{
	switch (parity_type) {
	case IOP_None:	return "NONE";
	case IOP_Odd:	return " ODD";
	case IOP_Even:	return "EVEN";
	case IOP_High:	return "HIGH";
	case IOP_Low:	return " LOW";
	default:		return "????";
	}
}

void UART::write_command(UINT15 io_param, int devID, UINT32 cycle)
{
#if 0
	UART:
		imm15[0:0] : 1 bit : deactivate(1), deactivate(0)
			imm15[4:1] : 4 bits : baud setting
			0 : 50 bps
			1 : 300 bps
			2 : 1, 200 bps
			3 : 2, 400 bps
			4 : 4, 800 bps
			5 : 9, 600 bps
			6 : 19, 200 bps
			7 : 38, 400 bps
			8 : 57, 600 bps
			9 : 115, 200 bps
			10 - 15 : reserved(current set to 115, 200 bps)
			imm15[6:5] : 2 bits : data length
			0 : 5 bits
			1 : 6 bits
			2 : 7 bits
			3 : 8 bits
			imm15[7:7] : 1 bit : stop_bit length
			0 : 1 bit
			1 : 2 bit(6 / 7 / 8 - bit data), 1.5 bit(5 - bit data)
			imm15[10:8] : 3 bits : parity setting
			0 / 2 / 4 / 6 : no parity		->	0 : IOP_None
			1 : odd parity(XNOR)	->	1 : IOP_Odd
			3 : even parity(XOR)	->	3 : IOP_Even
			5 : high parity(1)		->	5 : IOP_High
			7 : low parity(0)		->	7 : IOP_Low
#endif
			int activate = (io_param) & 0x1;
		activated = activate;
		if (activate) {
			int baud_rate_idx = (io_param >> 1) & 0xf;
			int data_length_idx = (io_param >> 5) & 0x3;
			int stop_length = (io_param >> 7) & 0x1;
			int parity = (io_param >> 8) & 0x7;
			///	_m1 --> minus 1
			baud_period_m1 = uart_baud_period[baud_rate_idx];
			bit_count_m1 = uart_data_length[data_length_idx];
			stop_count = (stop_length) ? uart_stop_length[data_length_idx] : 1;
			parity_type = ((parity & 0x1) == 0) ? 0 : parity;
		}
		printf("[%10d] ", cycle);
		if (devID == 0) { printf("<UART_AXI>"); }
		else { printf("<UART_EXT>"); }
		if (activate) {
			printf(" (activate) : baud_period(%d), bit_count(%d), stop_count(%d), parity_type(%d)\n",
				baud_period_m1 + 1, bit_count_m1 + 1, stop_count, parity_type);
		}
		else {
			printf(" (deactivate)\n");
		}
		///	reset all status
		rx.reset_port();
		tx.reset_port();
}

UINT32 UART::read_status()
{
	int rx_error = rx.error;
	int rx_empty = BUF_EMPTY(&rx.buf);
	int rx_full = BUF_FULL(&rx.buf);
	int tx_empty = BUF_EMPTY(&tx.buf);
	int tx_full = BUF_FULL(&tx.buf);
#if !defined(__LLVM_C2RTL__) && !defined(C2R_DISABLE_USER_DISPLAY)
	printf("[%10d] <UART_%c> (status) : rx_error(%d), rx_empty(%d), rx_full(%d), tx_empty(%d), tx_full(%d)\n",
		cpu.cycle, uart_id_char(uart), rx_error, rx_empty, rx_full, tx_empty, tx_full);
#endif
	return ((rx_error << 4) | (rx_empty << 3) | (rx_full << 2) | (tx_empty << 1) | (tx_full));
}

///////////////////////////////////////////// SPI ///////////////////////////////////////////////////////////////////////////

static const int spi_baud_period_m1[16] = {
	3 - 1,  //  0 : 3 cycles  (33MHz @ 100MHz cpu-clk)
	4 - 1,  //  1 : 4 cycles  (25MHz @ 100MHz cpu-clk)
	16 - 1, //  2 : 16 cycles (6.25MHz @ 100MHz cpu-clk)
	64 - 1, //  3 : 64 cycles (1.625MHz @ 100MHz cpu-clk)
	128 - 1 //  4 : 128 cycles (0.78125MHz @ 100MHz cpu-clk)
}; // consider SPI2X mode

void SPIM_PORT::activate(UINT32 w, UINT32 tick_offset)
{
	if (tick_offset == 0) { /// this is a consecutive word transfer 
							/// (bit[7] is the last bit transmitted, and need to be stable until the next half-baud period
		w &= 0xff;
		word = ((word) & 0x80) | ((w >> 1) & 0x7f);
		bit_in_d = (w & 1);
		bit_sck = 1; //  start new baud period
	}
	else {
		word = w & 0xff; // make 8 bits
		bit_sck = 0; //  should give half_baud period before the first sck edge
	}
	state = SPI_S_Active;    /// probably selectSlave signal should be assert here
	bit_pos = 0;
	//	u->rx_buf = 0; // should not reset!!
	tick = tick_offset;
}

void SPIM::write_command(UINT15 io_param, UINT32 cycle)
{
#if 0
	UART:
		imm15[0:0] : 1 bit : activate(1), deactivate(0)
			imm15[1:1] : 1 bit : SPI interrupt enable
			//imm15[2:2]: 1 bit: master.  (no need as always master for now)
			imm[2:2] : 1 bit : clock polarity, high_idle(1), low_idle(0)
			imm15[3:3] : 1 bit : clock phase, sample_falling_set_rising(1), sample_rising_set_falling(0)
			imm15[6:4] : 2 bits : SCK frequency setting
			0 : 4 (cpu_clk / 4)
			1 : 16 (cpu_clk / 16)
			2 : 64 (cpu_clk / 64)
			3 : 128 (cpu_clk / 128)
			imm15[7] : 1 bit : SPI double speed
#endif
			int double_speed = 0;
		int activate = (io_param) & 0x1;
		activated = activate;
		if (activate) {
			int baud_rate_idx = (io_param >> 4) & 0x3;
			///	_m1 --> minus 1
			double_speed = (io_param >> 7) & 0x1;
			baud_period_m1 = double_speed ? spi_baud_period_m1[baud_rate_idx] / 2 : spi_baud_period_m1[baud_rate_idx];
			bit_count = 8;
			clock_polarity = (io_param >> 2) & 0x1;
			clock_phase = (io_param >> 3) & 0x1;
		}
		if (activate) {
			printf("[%10d] <SPI.M> (activate) : cpu_clk_division(%d), CPOL(%d), CPHA(%d), interrupt_enable(%d), double_speed(%d)\n",
				cycle, baud_period_m1 + 1, clock_polarity, clock_phase, interrupt_enable, double_speed);
		}
		else {
			printf("[%10d] <SPI.M> (deactivate)\n", cycle);
		}
		///	reset all status
		tx_rx.reset();
}

UINT32 SPIM::read_status()
{
#if 0
	status[0:0]: tx_rx__buffer_full
		status[1:1] : tx_rx__buffer_empty
		status[2:2] : tx_rx_error
#endif
		int tx_rx_error = tx_rx.error;
	int tx_rx_empty = BUF_EMPTY(&tx_rx.tx_buf);
	int tx_rx_full = BUF_FULL(&tx_rx.tx_buf);

#if !defined(__LLVM_C2RTL__) && !defined(C2R_DISABLE_USER_DISPLAY)
	printf("[%10d] <SPI> (status) : tx_rx_error(%d), tx_rx_empty(%d), tx_rx_full(%d)\n",
		cpu.cycle, tx_rx_error, tx_rx_empty, tx_rx_full);
#endif
	return ((tx_rx_error << 2) | (tx_rx_empty << 1) | (tx_rx_full));
}
/// 6/5/17
void SPIM::do_tx(UINT2 io_op, UINT32 src0)
{
	//////////////////////////////////////////// send fsm ////////////////////////////////////////////////////////////////////////////
	IOBuf * tx_buf = &tx_rx.tx_buf;
	int tx_empty = BUF_EMPTY(tx_buf);
	int tx_full = BUF_FULL(tx_buf);
	int tx_rp = tx_buf->rp;
	int tx_wp = tx_buf->wp;
	int tx_wflag = (io_op == IO_WriteData) && !tx_full;
	int tx_rflag = 0;
	int half_period = (baud_period_m1 >> 1);
#ifndef  __LLVM_C2RTL__//__TCT_COMPILER_USED__
	int prev_state = tx_rx.state;
#endif
	tx_rx.tx_stalled = (io_op == IO_WriteData) && tx_full;

	switch (tx_rx.state)
	{
	case SPI_S_Idle:
		if (!tx_empty) {
			tx_rflag = 1;
			tx_rx.activate(tx_buf->buf[tx_rp], half_period); /// give half-buad period)
		}
		break;
	case SPI_S_Active:
		tx_rx.bit_out = (tx_rx.word >> 7) & 0x1;   /// 3/3/17 : need to update bit_out here... also, spi is MSB-first!!!
		if (tx_rx.tick < baud_period_m1) {
			int is_half_period = (tx_rx.tick == half_period);
			tx_rx.tick++;
			if (is_half_period) {
				if (tx_rx.bit_pos == 8) {
					tx_rx.state = SPI_S_Idle;  /// end the last clock pulse here...
				}
				else if (tx_rx.bit_sck) {
					tx_rx.word = (tx_rx.word << 1) | tx_rx.bit_in_d; /// shift word on falling-edge
				}
				tx_rx.bit_sck = 0;
			}
		}
		else {	///	1 baud period reached... this code is executed after each baud period clock cycles
			tx_rx.tick = 0;
			if (tx_rx.bit_pos < 8 - 1)   /// ok.. this is correct...
			{
				tx_rx.bit_in_d = tx_rx.bit_in;   /// 3/2/17 : spi should sample din at clock-edge...
				tx_rx.bit_sck = 1; // this will cause the input latch to sample the miso
				tx_rx.bit_pos++;
			}
			else // 8 cycles completed, finish
			{
				tx_rx.rx_buf = (tx_rx.word << 1) | tx_rx.bit_in;
				if (!tx_empty) {
					tx_rflag = 1;
					tx_rx.activate(tx_buf->buf[tx_rp], 0);
				}
				else {
					tx_rx.bit_sck = 1; /// still need to create the last clock edge...
					tx_rx.bit_pos++;
				}
			}
		}
		break;
	default:
		break;
	}
	/// 2/28/17 : move this at the end???
	if (tx_wflag) { tx_buf->buf[tx_wp] = src0; }
	tx_buf->update(tx_rflag, tx_wflag);
	///	2/27/15 : modified 
	tx_rx.irq = tx_rflag;
	//////////////////////////////////////////// send fsm ////////////////////////////////////////////////////////////////////////////
}

UINT32 SPIM::do_rx(UINT2 io_op, BIT signedChar) {
	int rflag = (io_op == IO_ReadData);
	int return_val = 0;
	tx_rx.rx_stalled = (rflag && (tx_rx.state != SPI_S_Idle || !BUF_EMPTY(&tx_rx.tx_buf)));

	/// 3/6/17 : tx_buf needs to be empty!!! and idle!!! (otherwise we'll be reading past responses)
	if (rflag && tx_rx.state == SPI_S_Idle && BUF_EMPTY(&tx_rx.tx_buf)) {
		unsigned char c = tx_rx.rx_buf;
		UINT32 sign_ext = ((c & 0x80) && signedChar) ? 0xffffff00 : 0;
		return_val = c | sign_ext;
	}
	return return_val;
}

/// 6/7/17 : problem on TEST_AXI_BUS : wres.ready and waddr.valid can be simultaneously switch off!!!
void AXI4L::SlaveFSM::fsmRead(AXI4L::CH *axi) {
	axi->setSLRead(out.raddr, out.rdat);
	switch (r_state) {
	case R_Init:
		if (axi->raddr.m.valid && devReadSetup(axi->raddr.m.addr)) {
			raddr = axi->raddr.m.addr;
			rsize = axi->raddr.m.size;
			burst_count_r = axi->raddr.m.len;
			burst_end_r = 0;
			out.raddr.set(1);
			raddr_end = 0;
			r_state = R_Addr;
		}
		break;
	case R_Addr: {
		out.raddr.reset();
		if (!axi->raddr.m.valid) { raddr_end = 1; }
		UINT32 d = out.rdat.data;
		if ((out.rdat.valid && burst_end_r) || devRead(&d, raddr, rsize)) {
			out.rdat.set(d, RSP_OK, 1, (burst_count_r == 0));
			if (burst_count_r == 0) {
				if (axi->rdat.m.ready) { r_state = R_End; }
				burst_end_r = 1;
			} else {
				burst_count_r--;
			}
		}
		break;
	}
	case R_End: /// make sure raddr.valid is deasserted BEFORE going back to R_Init
		out.rdat.reset();
		if (raddr_end || !axi->raddr.m.valid) { r_state = R_Init; nxt_intrFlag |= 1; }
		break;
	}
}

void AXI4L::SlaveFSM::fsmWrite(AXI4L::CH *axi) {
	switch (w_state) {
	case W_Init:
		if (axi->waddr.m.valid && devWriteSetup(axi->waddr.m.addr)) {
			waddr = axi->waddr.m.addr;
			wsize = axi->waddr.m.size;
			burst_count_w = axi->waddr.m.len;
			out.waddr.set(1);
			waddr_end = 0;
			if (axi->waddr.m.len) {
				out.wdat.set(1);
				w_state = W_AddrDataBurst;
			} else {
				w_state = W_AddrData;
			}
		}
		break;
	case W_AddrData:
		out.waddr.reset();
		if (!axi->waddr.m.valid) { waddr_end = 1; }
		if (axi->wdat.m.valid && devWrite(axi->wdat.m.data, waddr, wsize)) {
			wres_end = 0;
			out.wdat.set(1);
			w_state = W_End;
			out.wres.set((mutexError) ? RSP_SLV_ERR : RSP_OK, 1);   /// 6/23/17
			writePending = (writeFlag == 1);
		}
		break;
	case W_AddrDataBurst:
		out.waddr.reset();
		if (!axi->waddr.m.valid) { waddr_end = 1; }
		if (axi->wdat.m.valid) {
			if (devWrite(axi->wdat.m.data, waddr, wsize)) {
				wres_end = 0;
				if (burst_count_w == 0) {
					out.wdat.reset();
					w_state = W_End;
					out.wres.set((mutexError) ? RSP_SLV_ERR : RSP_OK, 1);   /// 6/23/17
				} else {
					burst_count_w--;
				}
				writePending = (writeFlag == 1);
			}
		}
		break;
	case W_End:
		out.wdat.reset();
#if 0   /// wres_end (state) is referred AFTER assignment!!! 
		/// --> error in C2R (but error message is not friendly --> RTL_gateModel.cpp:L1403) !!! : ASSERT(!ng->nodeGrp)
		/// need to work on more understandable message here...
		if (axi->wres.m.ready) { wres_end = 1; }
		if ((wres_end || axi->wres.m.ready) && (waddr_end || !axi->waddr.m.valid)) {
			out.wres.reset();
			nxt_state = W_Init;
			nxt_intrFlag |= 2;
		}
#endif
#if 1   /// fixed version
		{
			BIT nxt_wres_end = wres_end;
			if (axi->wres.m.ready) { wres_end = 1; nxt_wres_end = 1; }
			if ((nxt_wres_end || axi->wres.m.ready) && (waddr_end || !axi->waddr.m.valid)) {
				out.wres.reset();
				w_state = W_Init;
				if (writePending) { nxt_intrFlag |= 2; }
			}
		}
#endif
		break;
	}

	axi->setSLWrite(out.waddr, out.wdat, out.wres);
}

BIT UART_AXI4L::devRead(UINT32 *data, UINT32 addr, UINT3 size) {
	if (addr & 0x7) {
		*data = uart.read_status();
		return 1;
	}
	else if (uart.activated && !BUF_EMPTY(&uart.rx.buf)) {
		readFlag = 1;
		*data = uart.rx.buf.buf[uart.rx.buf.rp];
		return 1;
	}
	else { return 0; }
}

//#define DBG_UART_W

BIT UART_AXI4L::devWrite(UINT32 data, UINT32 addr, UINT3 size) {
    /// 6/22/17 : watch out for virtualBus behavior!!!
	if (UINT32 idx = (addr & 0xf) >> 2) {   /// 6/17/17 : mutex!!!
		switch (idx) {
		case UART_R_CTRL: writeFlag = 2; break;  /// write_command address
#if defined(USE_VIRTUAL_BUS)    /// 6/23/17
		case UART_R_LOCK:   return mutex.getLock((UINT4)data, "UART", cycle);
		case UART_R_UNLOCK: return mutex.releaseLock((UINT4)data, "UART", cycle);
#else
		case UART_R_LOCK:   mutexError = !mutex.getLock((UINT4)data, "UART", cycle); return 1;
		case UART_R_UNLOCK: mutexError = !mutex.releaseLock((UINT4)data, "UART", cycle); return 1;
#endif
		}
	}
	else if (uart.activated && !BUF_FULL(&uart.tx.buf))
	{
		writeFlag = 1;
#if defined(DBG_UART_W)
		if (cycle < 2000)
			//        printf("[%08d] UART-W (%3d : %c)\n", cycle, data, data);
			printf("[%08d] UART-W (%3d : %08x)\n", cycle, data, addr);
#endif
	}
	if (writeFlag) { writeData = data; }
	return (writeFlag != 0);
}

void UART_AXI4L::fsmUser() {
	if (writeFlag == 2) {
		uart.write_command((UINT15)writeData, 0, cycle);
	}
	else if (uart.activated) {
		//if (!BUF_EMPTY(&uart.rx.buf)) { nxt_intrFlag |= 1; }
		uart.do_rx((readFlag) ? IO_ReadData : IO_Idle);
		uart.do_tx((writeFlag) ? IO_WriteData : IO_Idle, writeData);
		if (uart.rx.irq) { nxt_intrFlag |= 4; }
		if (uart.tx.irq) { nxt_intrFlag |= 8; }
	}
}

void UART::set_pin(UARTPin *uart_pin)
{
	///	use 2-DFFs to latch uart rx signal
	rx.bit_n = rx_bit_d_n;
	rx_bit_d_n = !uart_pin->rx;
	uart_pin->tx = !tx.bit_n;
}

BIT SPIM_AXI4L::devRead(UINT32 *data, UINT32 addr, UINT3 size) {
	if (addr & 0x6) {
		*data = spim.read_status();
		return 1;
	}
	else if (spim.activated) {
		*data = spim.do_rx(IO_ReadData, (addr & 1));
		readFlag = !spim.tx_rx.rx_stalled;
		return readFlag;
	}
	else { return 0; }
}

BIT SPIM_AXI4L::devWrite(UINT32 data, UINT32 addr, UINT3 size) {
    /// 6/22/17 : watch out for virtualBus behavior!!!
	if (UINT32 idx = (addr & 0xf) >> 2) {   /// 6/17/17 : mutex!!!
		switch (idx) {
		case SPI_R_CTRL: writeFlag = 2; break;  /// write_command address
#if defined(USE_VIRTUAL_BUS)    /// 6/23/17
		case SPI_R_LOCK:   return mutex.getLock((UINT4)data, "SPI", cycle);
		case SPI_R_UNLOCK: return mutex.releaseLock((UINT4)data, "SPI", cycle);
#else
		case SPI_R_LOCK:   mutexError = !mutex.getLock((UINT4)data, "SPI", cycle); return 1;
		case SPI_R_UNLOCK: mutexError = !mutex.releaseLock((UINT4)data, "SPI", cycle); return 1;
#endif
		}
	}
	else if (spim.activated && !BUF_FULL(&spim.tx_rx.tx_buf)) { writeFlag = 1; }
	if (writeFlag) { writeData = data; }
	return (writeFlag != 0);
}

void SPIM_AXI4L::fsmUser() {
	if (writeFlag == 2) {
		spim.write_command((UINT15)writeData, cycle);
	}
	else if (spim.activated) {
		spim.do_tx((writeFlag) ? IO_WriteData : IO_Idle, writeData);
	}
}

void SPIM::set_pin(SPIPin *spi) {   /// 6/8/17
									/// 3/3/17 : don't use DFFs for latching miso... clock period is too short...
	tx_rx.bit_in = spi->miso;
	spi->mosi = tx_rx.bit_out;
	spi->sck = tx_rx.bit_sck;
}


/// 5/4/17
#define X_UART_BUF_SIZE	60//1000
//const char x_uart_tx_msg0[] = "testing uart_rx!!\n" "******";
const char x_uart_tx_msg[] = "testing uart_rx!!\n" "\033" "******";
//const char x_uart_tx_esc = x_uart_tx_msg[19];
#define X_UART_TX_MSG_SIZE sizeof(x_uart_tx_msg)
#if 0
static const char tx_msg[] = "testing uart_rx!!\n";
static const char tx_msg_tail[] = "******";
#endif
struct X_UART
{
	const UINT8 tx_buf[X_UART_TX_MSG_SIZE];
	ST_UINT32 cycle;
	UART uart;
	ST_UINT8 state, cpu_mode;
	ST_UINT8 rx_buf[X_UART_BUF_SIZE];
	ST_UINT10 rx_pnt, tx_pnt, trig_pnt;
	ST_BIT clear_rx_buf;
	ST_UINT32 rx_cycle;
	ST_UINT32 mode;
} x_uart = { "testing uart_rx!!\n" "\033" "******", 0 };

#if defined(DBG_BBL)
BIT dbgFlag = 0;
#endif

void x_uart_update(int rx_data, int update)
{
	//int updateCondition = (rx_data == 0) || (rx_data == '\n') || (x_uart.rx_pnt == X_UART_BUF_SIZE - 2);
	int updateCondition = 0;
#if	0
	updateCondition |= (rx_data == 0);
#endif
	updateCondition |= update;
	updateCondition |= (rx_data == '\n');
	updateCondition |= (x_uart.rx_pnt == X_UART_BUF_SIZE - 2);

	x_uart.rx_buf[x_uart.rx_pnt] = (rx_data & 0x100) ? 0 : rx_data;
#if	0
	{
		const char trigger_msg[] = "(hit keys...):";
		if (trigger_msg[x_uart.trig_pnt] == rx_data) {
			if (x_uart.trig_pnt == sizeof(trigger_msg) - 2) {   /// detected trigger_msg
				x_uart.state = 2;   /// goto state = 2;
			}
			else { ++x_uart.trig_pnt; }
		}
		else { x_uart.trig_pnt = 0; }
	}
#endif
	if (updateCondition) {
		if (x_uart.state != 2)
			printf("[%10d] <UART_X.RX> msg = %s", x_uart.cycle, x_uart.rx_buf);
		else
			printf("%s", x_uart.rx_buf);
#if defined(DBG_BBL)
#define MSG_MATCH   "[    0.000000] Memory:"
        bool match = strncmp((const char *)x_uart.rx_buf, MSG_MATCH, sizeof(MSG_MATCH) - 1) == 0;
        if (match)
            printf("\nMATCH!!!!\n");
        if (x_uart.state != 2) {
            if (dbgFlag == 0 && match) {
                dbgFlag = 1;
            }
            else if (dbgFlag == 1) {
                dbgFlag = 0;
            }
        }
#endif
		if ((x_uart.mode & 1) || (rx_data != '\n')) {
			if (rx_data == '\n') x_uart.mode |= 1;
			x_uart.state = 2;
		} else
			x_uart.state = 1;
		x_uart.rx_pnt = 0;
		x_uart.clear_rx_buf = 1;
	}
	else { x_uart.rx_pnt++; }
}

//extern CPU cpu;
#if !defined(CPU_ONLY)
_C2R_FUNC(1)
#endif
int uart_ext(BIT tx)
{
	int cur_state = x_uart.state;
	////#if !defined(__LLVM_C2RTL__)   /// 3/2/17 : move this to top
	////    if (x_uart.cpu_mode == 1 || cur_state == 3) { cpu.cycle++; }
	////#endif
	if (cur_state == 0 || cur_state == 3) {
		///	4/27/15 : NOTE : baud-rate setting must match with that of the application
		x_uart.uart.write_command((IOP_Even << 8) | (0x0 << 7) | (0x3 << 5) | (UART_BAUD_ULTRA_FAST << 1) | 1, 1, x_uart.cycle);
		x_uart.cpu_mode = (cur_state == 3) ? 1 : 0;
		x_uart.state = 1;
		x_uart.rx_pnt = 0;
		x_uart.tx_pnt = 0;
		x_uart.uart.tx.bit_n = 0;
	}
	else {
		int rx_data, buf_empty = BUF_EMPTY(&x_uart.uart.rx.buf);
		rx_data = x_uart.uart.do_rx((buf_empty) ? IO_Idle : IO_ReadData);
		/// 5/6/17 : x_uart.uart.rx.bit_n should be assigned AFTER its reference (inside uart_rx)
		x_uart.uart.rx.bit_n = !tx;
		if (!buf_empty)
		{
			x_uart_update(rx_data, 0);
			x_uart.rx_cycle = x_uart.cycle;
		}
		else if (x_uart.clear_rx_buf) {
			x_uart.clear_rx_buf = 0;
			for (int i = 0; i < X_UART_BUF_SIZE; ++i) { x_uart.rx_buf[i] = 0; }
		}
		else if ((x_uart.rx_pnt != 0) && (x_uart.rx_cycle + 1000 < x_uart.cycle)) {
			x_uart_update(0, 1);
		}
#if	0
		if (cur_state == 2) {
			int buf_full = BUF_FULL(&x_uart.uart.tx.buf);
			UINT8 tx_data = x_uart.tx_buf[x_uart.tx_pnt];
			if (tx_data == 0) {
				tx_data = x_uart.tx_buf[0];
				x_uart.tx_pnt = 0;
			}
			else if (!buf_full)
			{
				x_uart.tx_pnt++;
			}
			x_uart.uart.do_tx((buf_full) ? IO_Idle : IO_WriteData, tx_data);
		}
#endif
		if (x_uart.tx_pnt != 0) {
			int buf_full = BUF_FULL(&x_uart.uart.tx.buf);
#if 1   /// 7/1/20
            int tx_data = x_uart.trig_pnt;
#else
			UINT8 tx_data = x_uart.trig_pnt;
#endif

			if (!buf_full) {
				x_uart.tx_pnt = 0;
				x_uart.trig_pnt = 0xff;
			}

			x_uart.uart.do_tx((buf_full) ? IO_Idle : IO_WriteData, tx_data);
		} else {
			x_uart.uart.do_tx(IO_Idle, 0);
		}
	}
	x_uart.cycle++;
	return !x_uart.uart.tx.bit_n;
}

void uart_ext_tx(int ch)
{
	x_uart.tx_pnt = 1;
	x_uart.trig_pnt = ch;
}

void uart_ext_flush_rx()
{
	x_uart.rx_buf[x_uart.rx_pnt] = 0;
	printf("[%10d] <UART_X.RX> msg = %s<<<<flushed...\n", x_uart.cycle, x_uart.rx_buf);
	x_uart.rx_pnt = 0;
}

void uart_ext_switch_mode()
{
	x_uart.state = 3;
	x_uart.cycle = 0;
}

void uart_ext_set_mode(int mode)
{
	x_uart.mode = mode;
}

void spis_port_activate(SPIS_PORT *spis, UINT8 response, SPIPin *spip, UINT32 cycle) {
	spis->word = response;
	spis->res_word = response;
	spis->bit_pos = 0;
	spip->miso = (response >> 7) & 0x1;
	printf("[%10d] <SPIX-slave> (activate) : res_word(%3d:0x%02x)\n",
		cycle, spis->res_word, spis->res_word);
}

int spis_rx(SPIS_PORT *spis, SPIPin *spip) {   /// called on rising-edge of sck
	spis->nxt_word = (spis->word << 1) | spip->mosi;
	if (spis->bit_pos == 8 - 1) {
		spis->bit_pos = 0;
		return 1;   /// received word
	}
	else {
		++spis->bit_pos;
		spis->word = spis->nxt_word;
		return 0;
	}
}

#define X_SPI_BUF_SIZE  (1 << 13)
#define FINGER_IN_DATA    "../../TEST/testTCTMPSoC/finger_in.data"
#define FINGER_OUT_DATA    "../../TEST/testTCTMPSoC/finger_out.data"

struct SPIS_EXT
{
	SPIS_PORT spis;
	ST_BIT    sck_d;
	char rx_buf[X_SPI_BUF_SIZE];
	int rx_pnt, state, data_count_m1;   /// data_count_m1 : 0 - 127 (data_count : 1 - 128)
	int in_data_count, out_data_count, no_match_count, match_count, error_count;
	FILE *fp_out, *fp_in;
	UINT32 cycle;
} x_spis;

#include "../CPP_utils/bmp.h"

struct BMP_EXT
{
	BMPIMG bmp;
	unsigned char param[6];
	int pnt, blkCount, bpnt, imgIdx;
	int createGrayscale64x64xN(char *buf);
	int setGrayscale64x64(char *buf) {
		int flag = bmp.setGrayscale64x64(buf, bpnt++);
		if (flag && bpnt == blkCount) {
			char fname[20];
			sprintf(fname, "xbmp_%d.bmp", imgIdx++);
			bmp.save(fname);
		}
		return flag;
	}
} x_bmp;

int BMP_EXT::createGrayscale64x64xN(char *buf) {
	int sx = (buf[0] & 0xff) | ((buf[1] << 8) & 0xff00);
	int sy = (buf[2] & 0xff) | ((buf[3] << 8) & 0xff00);
	blkCount = (buf[4] & 0xff) | ((buf[5] << 8) & 0xff00);
	if (bmp.createGrayscale64x64xN(sx, sy))
		pnt = 6;
	bpnt = 0;
	return pnt == 6;
}

void spi_slave_ext_switch_mode() {
	x_spis.state = SPI_PS_INIT;
	if (x_spis.fp_out) { rewind(x_spis.fp_out); }
	else { x_spis.fp_out = fopen(FINGER_OUT_DATA, "rb"); }
	if (x_spis.fp_in) { rewind(x_spis.fp_in); }
	else { x_spis.fp_in = fopen(FINGER_IN_DATA, "rb"); }
	x_spis.rx_pnt = 0;
	x_spis.in_data_count = 0;
	x_spis.no_match_count = 0;
	x_spis.match_count = 0;
	x_spis.error_count = 0;
	x_spis.cycle = 0;
	x_bmp.pnt = 0;
}

#define SUPPRESS_SPI_SX_DATA_MSG

void spi_slave_ext_flush_rx(int force_flush) {
	int rx_data = x_spis.rx_buf[x_spis.rx_pnt];
	if (x_spis.state == SPI_PS_BIN_OUT) {
#if !defined(SUPPRESS_SPI_SX_DATA_MSG)
		printf("[%10d] <SPIS_X.RX> binary output (%d data)\n", x_spis.cycle, x_spis.data_count_m1 + 1);
#endif
		if (x_spis.rx_pnt == 5 && x_bmp.pnt == 0) {
			x_bmp.createGrayscale64x64xN(x_spis.rx_buf);
		}
		else if (x_spis.rx_pnt == 64 * 64 - 1 && x_bmp.pnt == 6) {
			x_bmp.setGrayscale64x64(x_spis.rx_buf);
		}
	}
	else if (x_spis.state == SPI_PS_BIN_IN) {
#if !defined(SUPPRESS_SPI_SX_DATA_MSG)
		printf("[%10d] <SPIS_X.RX> binary input (%d data)\n", x_spis.cycle, x_spis.data_count_m1 + 1);
#endif
	}
	else if (x_spis.state == SPI_PS_TEXT_OUT) {
		x_spis.rx_buf[x_spis.rx_pnt + 1] = 0;
		printf("[%10d] <SPIS_X.RX> msg = %s%s", x_spis.cycle, x_spis.rx_buf,
			(rx_data != '\n') ? "<<<continued...\n" : "");
	}
	x_spis.rx_pnt = 0;
	x_spis.state = SPI_PS_CMD;
	if (force_flush) {
		if (x_spis.fp_out) { fclose(x_spis.fp_out); }
		x_spis.fp_out = NULL;
		if (x_spis.fp_in) { fclose(x_spis.fp_in); }
		x_spis.fp_in = NULL;
		printf("[%10d] <SPIS_X.RX> in_data_count = %d, out_data_count = %d, match_count = %d, no_match_count = %d, error_count = %d\n",
			x_spis.cycle, x_spis.in_data_count, x_spis.out_data_count, x_spis.match_count, x_spis.no_match_count, x_spis.error_count);
	}
}

unsigned char x_spis_prepare_tx_data()
{
	x_spis.out_data_count++;
	if (x_spis.fp_in) {
		unsigned char d;
#if 1   /// 7/1/20
        size_t res = fread(&d, 1, 1, x_spis.fp_in);
#else
		int res = fread(&d, 1, 1, x_spis.fp_in);
#endif
		if (res != 1) { x_spis.error_count++; }
		else { x_spis.spis.word = d; }
		return d;
	}
	else { return 0; }
}

void x_spis_process_rx_data(int rx_data)
{
	x_spis.in_data_count++;
	if (x_spis.fp_out) {
		unsigned char d;
#if 1   /// 7/1/20
        size_t res = fread(&d, 1, 1, x_spis.fp_out);
#else
		int res = fread(&d, 1, 1, x_spis.fp_out);
#endif
		if (res != 1) { x_spis.error_count++; }
		else if (d == rx_data) { x_spis.match_count++; }
		else {
			x_spis.no_match_count++;
			printf("[%10d] <SPIS_X.RX> no_match !!! rx_data = %2x, expected = %2x (%d matched, %d non-matched)\n",
				x_spis.cycle, rx_data, d, x_spis.match_count, x_spis.no_match_count);
		}
	}
	else { x_spis.error_count++; }
}

unsigned char x_spis_update(int rx_data)
{
	unsigned char nxt_response = x_spis.spis.res_word;
	x_spis.rx_buf[x_spis.rx_pnt] = rx_data;
	if (x_spis.state == SPI_PS_BIN_OUT) {
		x_spis_process_rx_data(rx_data);
	}
	int flushCondition;
	if (x_spis.state == SPI_PS_BIN_IN || x_spis.state == SPI_PS_BIN_OUT) {
		flushCondition = (x_spis.rx_pnt == x_spis.data_count_m1);
	}
	else if (x_spis.state == SPI_PS_TEXT_OUT) {
		flushCondition = (rx_data == '\n' || x_spis.rx_pnt == X_SPI_BUF_SIZE - 2);
	}
	else
		flushCondition = 0;
	if (flushCondition) { spi_slave_ext_flush_rx(0/*force_flush*/); }
	else if (x_spis.state == SPI_PS_TEXT_OUT && rx_data == 0) {
		/// message is not flushed, but state goes to SPI_PS_TEXT_PENDING
		x_spis.state = SPI_PS_TEXT_PENDING;
	}
	else {
		if (x_spis.state == SPI_PS_BIN_IN) {
			nxt_response = x_spis_prepare_tx_data();
		}
		++x_spis.rx_pnt;
	}
	return nxt_response;
}

#if 0
x_spis.state:
0 : init
1 : command(determine)
#endif

const char *x_spis_state_name(int curState) {
	switch (curState) {
	case SPI_PS_INIT:       return "INIT";
	case SPI_PS_CMD:        return "CMD";
	case SPI_PS_BIN_IN:     return "BIN_IN";
	case SPI_PS_BIN_OUT:    return "BIN_OUT";
	case SPI_PS_TEXT_OUT:   return "TEXT_OUT";
	case SPI_PS_TEXT_PENDING:   return "TEXT_PENDING";
	default:            return "????";
	}
}

int decode_spi_binary_data_count_m1(int rword6) {   /// rword6 : 6 bits <-- returns (# of words) - 1
	int data_count_code = (rword6 >> 1) & 0x1f; // 5 bits
	if (rword6 & 0x1) { /// bit[0] = 1 : 2's power
		return (1 << data_count_code) - 1;  /// {1,2,4,8,...,2^31} - 1
	}
	else {  /// bit[0] = 0 : linear
		return data_count_code; /// {1,2,3,4,...,32} - 1
	}
}

void spi_ext_slave(SPIPin *spi) {
	if (x_spis.state == SPI_PS_INIT) {
		spis_port_activate(&x_spis.spis, SPI_ACK, spi, x_spis.cycle);
		x_spis.rx_pnt = 0;
		x_spis.state = SPI_PS_CMD;
		x_spis.fp_out = fopen(FINGER_OUT_DATA, "rb");
		x_spis.fp_in = fopen(FINGER_IN_DATA, "rb");
		x_spis.in_data_count = 0;
		x_spis.out_data_count = 0;
		x_spis.no_match_count = 0;
		x_spis.match_count = 0;
		x_spis.error_count = 0;
		x_bmp.pnt = 0;
	}
	int sck_edge = ((!x_spis.sck_d) & spi->sck);
	x_spis.sck_d = spi->sck;
	if (sck_edge) {
#if 0   /// 6/19/17 : debugging...
		if (x_spis.cycle < 6000) {
			printf("[%8d] x-spis(sck_edge) : word = %x, mosi = %d\n", x_spis.cycle, x_spis.spis.nxt_word, spi->mosi);
		}
#endif
		int curState = x_spis.state;
		if (spis_rx(&x_spis.spis, spi)) {
			int rword = x_spis.spis.nxt_word;
			unsigned char nxt_response = x_spis.spis.res_word;
#if 0   /// 6/19/17 : debugging...
			if (x_spis.cycle < 6000) {
				printf("[%8d] x-spis : rword = %x (%d)\n", x_spis.cycle, rword, rword);
			}
#endif
			if (x_spis.state == SPI_PS_CMD || x_spis.state == SPI_PS_TEXT_PENDING) {
				/// command : determine TEXT_OUT or BINARY_IN or BINARY_OUT mode
				/// command format : 
				/// bit[7] = 1 : BINARY MODE --> bit[6:0] indicates (# of words)[encoded]
				/// bit[7] = 0 :
				///     bit[7:0] = 't' (0x75) : TEXT MODE
				///     otherwise : invalid command (ignored)
				switch ((rword >> 6) & 0x3) {
				case 2:
					x_spis.state = SPI_PS_BIN_OUT;
					x_spis.data_count_m1 = decode_spi_binary_data_count_m1(rword & 0x3f);
					break;
				case 3:
					x_spis.state = SPI_PS_BIN_IN;
					x_spis.data_count_m1 = decode_spi_binary_data_count_m1(rword & 0x3f);
					nxt_response = x_spis_prepare_tx_data();
					break;
				default:
					if (rword == SPI_TEXT_MODE) { x_spis.state = SPI_PS_TEXT_OUT; }
					break;
				}
			}
			else {
				nxt_response = x_spis_update(rword);
			}
#if !defined(SUPPRESS_SPI_SX_DATA_MSG)
			if (curState == SPI_PS_CMD) {
				printf("[%10d] <SPIS_X.RX> command : %3d(0x%02x) [state: %s]\n",
					x_spis.cycle, x_spis.spis.nxt_word, x_spis.spis.nxt_word, x_spis_state_name(x_spis.state));
			}
#endif
			spi->miso = (nxt_response >> 7) & 0x1;
			x_spis.spis.word = nxt_response;
		}
		else {
			spi->miso = (x_spis.spis.nxt_word >> 7) & 0x1;
		}
	}
	x_spis.cycle++;
}


#endif

