#!/usr/bin/python3

import sys

def main():
    key = 192
    keylen = 8
    k = int(sys.argv[1])
    p = int(sys.argv[2])
    print("%d bits from key %d at position %d: %d" % (k,key,p,(key >> (keylen - k - p)) & ((1 << k) - 1)))
    return

if __name__ == "__main__":
    # execute only if run as a script
    main()