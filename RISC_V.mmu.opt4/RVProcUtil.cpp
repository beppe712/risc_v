
#include "RVProc.h"


#include "RVProcUtil.h"

#include <string>
using namespace std;
#include <fstream>

rv_loop_info rv_insn::loopInfo;

extern CPU cpu1;


void show_riscv_cpu() { rv_insn rv(cpu1); }

#if 0
enum RV_op_stat{
	RV_op_lui,RV_op_auipc,RV_op_jal,RV_op_jalr,
	RV_op_beq, RV_op_bne, RV_op_blt, RV_op_bge, RV_op_bltu, RV_op_bgeu,
	RV_op_lb, RV_op_lh, RV_op_lw, RV_op_lbu, RV_op_lhu,
	RV_op_sb, RV_op_sh, RV_op_sw, 
	RV_op_addi, RV_op_slti, RV_op_sltiu, RV_op_xori, RV_op_ori,RV_op_andi,
	RV_op_slli, RV_op_srli, RV_op_srai, 
	RV_op_add, RV_op_sub, RV_op_sll, RV_op_slt, RV_op_sltu, RV_op_xor, 
	RV_op_srl, RV_op_sra, RV_op_or, RV_op_and,
	RV_op_fence, RV_op_fencei, RV_op_ecall, RV_op_ebreak,
	//RV32M
	RV_op_mul, RV_op_mulh, RV_op_mulhsu, RV_op_mulhu,
	RV_op_div, RV_op_divu, RV_op_rem, RV_op_remu,
	RV_op_invalid,
	RV_op_count
};
#endif
string opnameList[RV_op_count] = {
	"lui", "auipc", "jal", "jalr", 
	"beq",  "bne",  "blt",  "bge",  "bltu",  "bgeu", 
	"lb",  "lh",  "lw",  "lbu",  "lhu", 
	"sb",  "sh",  "sw", 
	"addi",  "slti",  "sltiu",  "xori",  "ori", "andi", 
	"slli",  "srli",  "srai", 
	"add",  "sub",  "sll",  "slt",  "sltu",  "xor", 
	"srl",  "sra",  "or",  "and", 
	"fence",  "fencei",  "ecall",  "ebreak", 
	//"RV32M
	"mul",  "mulh",  "mulhsu",  "mulhu", 
	"div",  "divu",  "rem",  "remu", 
	"invalid"
};
#if !defined(__LLVM_C2RTL__) && defined(USE_RISC_V) && defined(ENABLE_CPU_STAT)
void CPU::update_cpu_stat() {
	int *opc = (dc.stall) ? op_count_stall : op_count;
	switch (insn.opcode) {
	case RVI_lui:   opc[RV_op_lui]++; break;
	case RVI_auipc: opc[RV_op_auipc]++;  break;
	case RVI_jal:	opc[RV_op_jal]++;   break;
	case RVI_jalr:  opc[RV_op_jalr]++;  break;
	case RVI_br:
		switch (insn.funct3) {
		case RVF3_beq:  opc[RV_op_beq]++;   break;
		case RVF3_bne:  opc[RV_op_bne]++;   break;
		case RVF3_blt:  opc[RV_op_blt]++;   break;
		case RVF3_bge:  opc[RV_op_bge]++;   break;
		case RVF3_bltu: opc[RV_op_bltu]++;  break;
		case RVF3_bgeu: opc[RV_op_bgeu]++;  break;
		default:		  break;
		}
		break;
	case RVI_ld:
		switch (insn.funct3) {
		case RVF3_lb:  opc[RV_op_lb]++;   break;
		case RVF3_lh:  opc[RV_op_lh]++;   break;
		case RVF3_lw:  opc[RV_op_lw]++;   break;
		case RVF3_lbu: opc[RV_op_lbu]++;  break;
		case RVF3_lhu: opc[RV_op_lhu]++;  break;
		default:        break;
		}
		break;
	case RVI_st:
		switch (insn.funct3) {
		case RVF3_sb:  opc[RV_op_sb]++;   break;
		case RVF3_sh:  opc[RV_op_sh]++;   break;
		case RVF3_sw:  opc[RV_op_sw]++;   break;
		default:        break;
		}
		break;
	case RVI_compi:
	case RVI_comp:
		if (dc.r.mdFlag) {
			switch (insn.funct3) {
			case RVF3_mul:      opc[RV_op_mul]++;       break;
			case RVF3_mulh:     opc[RV_op_mulh]++;      break;
			case RVF3_mulhsu:   opc[RV_op_mulhsu]++;    break;
			case RVF3_mulhu:    opc[RV_op_mulhu]++;     break;
			case RVF3_div:      opc[RV_op_div]++;       break;
			case RVF3_divu:     opc[RV_op_divu]++;      break;
			case RVF3_rem:      opc[RV_op_rem]++;       break;
			case RVF3_remu:     opc[RV_op_remu]++;      break;
			default:                 break;
			}
		}
		else {
			switch (insn.funct3) {
			case RVF3_add:
				if (dc.r.subFlag)
					opc[RV_op_sub]++;
				else
					opc[RV_op_add]++;
				break;
			case RVF3_shl:  opc[RV_op_sll]++;   break;
			case RVF3_slt:  opc[RV_op_slt]++;   break;
			case RVF3_sltu: opc[RV_op_sltu]++;  break;
			case RVF3_xor:  opc[RV_op_xor]++;   break;
			case RVF3_shr:
				if (dc.r.sextFlag)
					opc[RV_op_sra]++;
				else
					opc[RV_op_srl]++;
				break;
			case RVF3_or:   opc[RV_op_or]++;    break;
			case RVF3_and:  opc[RV_op_and]++;   break;
			default:        break;
			}
		}
		break;
	default:
		//printf("Invalid RISC-V insn...%d\n", cpu.insn.opcode);
		opc[RV_op_invalid]++;
		break;
	}

}
void CPU::show_cpu_stat() {
	int sum = 0;
	std::string filename = "cpu_stat.txt";
	std::ofstream writing_file;
	writing_file.open(filename.c_str(), std::ios::out);
	printf("inst    : # executed (# stalls) cycles (total%%) \n");
	printf("---------------------------------------------\n");
	for (int op = RV_op_lui; op != RV_op_count; op++) { sum += op_count[op] + op_count_stall[op]; }
	for (int op = RV_op_lui; op != RV_op_count; op++) {
		if (!op_count[op]) { continue; }
		double ave_cycles = (double)(op_count[op] + op_count_stall[op]) / (double)op_count[op];
		double ratio = (double)(op_count[op] + op_count_stall[op]) * 100.0 / (double)sum;
		printf("%-7s : %10d (%8d) %6.3f (%6.3f%%)\n",
			opnameList[op].c_str(), op_count[op], op_count_stall[op], ave_cycles, ratio);
		writing_file << opnameList[op].c_str() << '\t'
			<< op_count[op] << '\t'
			<< op_count_stall[op] << '\t' << ave_cycles << endl;
	}
	printf("*total* : %10d\n", sum);
	//writing_file << "total" << '\t' << sum << endl;
}
#endif


void sc_message(){}
void mts_message(UINT32 dst_id, UINT32 ex_out){}
//void show_cpu(){}
void exception_message(int vec){}


#define C2RTL_RTL_V_TESTBENCH	"../../CORE/RISC_V.MPSoC/C2R_ROOT/RTL_V_TB/"

#define PMEM_DUMP_0	"CPU_step_G__this_pmem_0"
#define DMEM_DUMP_0	"CPU_step_G__this_dmem_0"
#define PMEM_DUMP_1	"CPU_step_G__this_pmem_1"
#define DMEM_DUMP_1	"CPU_step_G__this_dmem_1"

#define ELF_FILE_0	"../../TEST/testRVMPSoC/test.rv.3mtx.out"
#define ELF_FILE_1	"../../TEST/testRVMPSoC/test.rv.4mtx.out"

#if 1   /// 7/1/20
const char *C2R::ProcUtil::elfFileName = ELF_FILE_0;
#elif !defined(USE_RV32I_TEST) && !defined(DISABLE_MD)
const char *elfFileName = ELF_FILE_0;
#else
const char *elfFileName = "../../TEST/testMemIO/test_rv32i.out"; ///
#endif
/// -O3 : 189,472 cycles
/// -O2 : 190,823 cycles
/// -O1 : 233,245 cycles
/// -O0 : 411,349 cycles

static int pid = 0;
void setElfFileName(int pid0, char *elfFile) {
	pid = pid0;
#if 1   /// 7/1/20
	if (pid == 0)	{ C2R::ProcUtil::elfFileName = elfFile?elfFile:ELF_FILE_0; }
	else			{ C2R::ProcUtil::elfFileName = elfFile?elfFile:ELF_FILE_1; }
#else
	if (pid == 0)	{ elfFileName = elfFile?elfFile:ELF_FILE_0; }
	else			{ elfFileName = elfFile?elfFile:ELF_FILE_1; }
#endif
}

#if 1   /// 7/1/20
void C2R::ProcUtil::openMemDumpFile(int type, FILE **fp, int *segmentCount, int *printfAddrWidth, int *printfDataWidth) {
#else
void openMemDumpFile(int type, FILE **fp, int *segmentCount, int *printfAddrWidth, int *printfDataWidth) {
#endif
    if (type == 0) {
        *segmentCount = 4;	///	separate byte-enable
		if (pid == 0) {
			fp[0] = fopen(C2RTL_RTL_V_TESTBENCH DMEM_DUMP_0 "_0" ".log", "w");
			fp[1] = fopen(C2RTL_RTL_V_TESTBENCH DMEM_DUMP_0 "_1" ".log", "w");
			fp[2] = fopen(C2RTL_RTL_V_TESTBENCH DMEM_DUMP_0 "_2" ".log", "w");
			fp[3] = fopen(C2RTL_RTL_V_TESTBENCH DMEM_DUMP_0 "_3" ".log", "w");
		}
		else {
			fp[0] = fopen(C2RTL_RTL_V_TESTBENCH DMEM_DUMP_1 "_0" ".log", "w");
			fp[1] = fopen(C2RTL_RTL_V_TESTBENCH DMEM_DUMP_1 "_1" ".log", "w");
			fp[2] = fopen(C2RTL_RTL_V_TESTBENCH DMEM_DUMP_1 "_2" ".log", "w");
			fp[3] = fopen(C2RTL_RTL_V_TESTBENCH DMEM_DUMP_1 "_3" ".log", "w");
		}
        *printfAddrWidth = DMEM_PRINTF_ADDR_SIZE;
        *printfDataWidth = 2;
    }
    else {
        *segmentCount = 1;	///	single word
		if (pid == 0) {
			fp[0] = fopen(C2RTL_RTL_V_TESTBENCH PMEM_DUMP_0 ".log", "w");
		}
		else {
			fp[0] = fopen(C2RTL_RTL_V_TESTBENCH PMEM_DUMP_1 ".log", "w");
		}
        fp[1] = 0; fp[2] = 0; fp[3] = 0;
        *printfAddrWidth = PMEM_PRINTF_ADDR_SIZE;
        *printfDataWidth = 8;
    }
}

