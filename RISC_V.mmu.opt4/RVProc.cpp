#ifdef	__linux__
#include <signal.h>
#include <sys/ioctl.h>
#include <fcntl.h>
#include <termios.h>
#define	pause	__pause__
#include <unistd.h>
#undef	pause
#else
#include <windows.h>
#include <conio.h>
#endif	// __linux__

#include "RVProc.h"

///	9/20/17 : cpp

/// 7/22/20 : cleanup fetch
void CPU::fetch(AXI4L::CH *axi)    /// TCT/RISC-V
{
    BIT sw_mode_active = FE_SW_MODE;

//#define USE_FE_BR /// 7/30/20 : C2RTL occurs --> fixed this later...
#if defined(USE_FE_BR)
    BranchStatus br; br.init(wb.br, ex.br, fe.r.br);
#else
    BIT br_active = (wb.br.active || ex.br.active || fe.r.br.active);
    UINT32 br_addr = (wb.br.active) ? wb.br.addr : (ex.br.active) ? ex.br.addr : fe.r.br.addr;
#endif

    UINT32	pc = fe.r.pc;
    BIT	mmu_en = fe.r.mmu_en;

#if defined(DBG_FE)
#define DBG_FE_CYCLE    690000//100000//50000
#define DBG_FE_RANGE    500
    //if (csr.mcycle <= 300)
    if (csr.mcycle >= DBG_FE_CYCLE && csr.mcycle <= DBG_FE_CYCLE + DBG_FE_RANGE)
        printf("[%6d] fe.pc(%8x) icache.busy(%d), br(%d : %08x)\n", csr.mcycle, fe.r.pc, icache.busy,
            br_active, br_addr);
#endif
    if (!FE_STALL) {
        if (!icache.busy && !sw_mode_active) {
            if (!fe.r.pending) {
#if defined(USE_FE_BR)
                pc = (br.active) ? br.addr : pc + 4;
#else
                pc = (br_active) ? br_addr : pc + 4;
#endif
            }
            mmu_en = wb.mmu_en;
            fe.r.mmu_en = mmu_en;
            fe.r.pc = pc;
#if defined(USE_FE_BR)
            br.active = 0;
#else
            br_active = 0;
#endif
        }
        BIT flush = sw_mode_active || (icache.busy && fe.r.flush);
        fe.r.pending = 0;
        fe.r.ready = !flush;
        fe.r.flush = flush;
    }
    else if (icache.busy) {
        fe.r.pending = 1;
        fe.r.ready = 0;
    }
    else if (fe.r.pending) {
        fe.r.pending = 0;
        fe.r.ready = 1;
    }
    fe.r.sw_mode = sw_mode_active;
#if defined(USE_FE_BR)
    fe.r.br = br;
#else
    fe.r.br.active = br_active;
    fe.r.br.addr = br_addr;
#endif

//#if defined(MERGE_PRV_INST_IN_ICACHE)
#if defined(MODIFY_DTLB_FLUSH)
    ir = icache.fsm(mmu_en, pc, axi, csr.satp, ex, fe);
#else
    ir = icache.fsm(mmu_en, pc, axi, csr.satp, dc, fe);
#endif
//    printf("FE(0x%08x): 0x%08x, 0x%08x\n", cycle, pc, ir);
}

//#if defined(USE_RVINSN_DEC)
/*    U-type  : imm31_12[31:12], rd[11:7], opcode[6:0]*/
void RVInsn::dec_U(unsigned ir, unsigned opval) {
    op = opval; RV_FLD(imm20H); RV_FLD(rd);
    imm = (imm20H << 12) & 0xffffffff;
}
/*    UJ-type : imm20.10_1.11.19_12[31:12], rd[11:7], opcode[6:0]*/
void RVInsn::dec_UJ(unsigned ir, unsigned opval) {
    op = opval; RV_FLD(imm20H); RV_FLD(rd);
    br_type = 1;
    SINT32 i20 = ir & 0x80000000;              /// sign bit
    UINT32 i10_1 = (imm20H >> 9) & 0x3ff;   /// 10 bits
    UINT32 i11 = (imm20H >> 8) & 1;       /// 1 bit
    UINT32 i19_12 = (imm20H) & 0xff;         /// 8 bits
    imm = (i20 >> 11) | (i19_12 << 12) | (i11 << 11) | (i10_1 << 1);   /// lsb = 0
}
/*    I-type : imm11_0[31:20], rs1[19:15], funct3[14:12], rd[11:7], opcode[6:0]*/
void RVInsn::dec_I(unsigned ir, unsigned opval) {
    op = opval;  RV_FLD(imm12H); RV_FLD(rs1); RV_FLD(funct3); RV_FLD(rd); RV_FLD(funct7);
    switch (op) {
    case RVO_jalr:  br_type = 2; break;
    case RVO_ld:    memFlag = 1; break;
    }
    SINT32 i11 = ir & 0x80000000;          /// sign bit
    UINT32 i10_0 = (imm12H) & 0x7ff;    /// 11 bits
    imm = (i11 >> 20) | i10_0;
}
/*    S-type : imm11_5[31:25], rs2[24:20], rs1[19:15], funct3[14:12], imm4_0[11:7], opcode[6:0]*/
void RVInsn::dec_S(unsigned ir, unsigned opval) {
    op = opval; RV_FLD(funct7); RV_FLD(rs2); RV_FLD(rs1); RV_FLD(funct3); RV_FLD(imm5L);
    memFlag = 1; rs2Flag = 1;
    /// funct7 --> imm11_5[31:25]
    SINT32 i11 = ir & 0x80000000;      /// sign bit
    UINT32 i10_5 = (funct7) & 0x3f; /// 6 bits
    imm = (i11 >> 20) | (i10_5 << 5) | imm5L;
}
/*    SB-type : imm12.10_5[31:25], rs2[24:20], rs1[19:15], funct3[14:12], imm4_1.11[11:7], opcode[6:0]*/
void RVInsn::dec_SB(unsigned ir, unsigned opval) {
    op = opval; RV_FLD(funct7); RV_FLD(rs2); RV_FLD(rs1); RV_FLD(funct3); RV_FLD(imm5L);
    br_type = 3; rs2Flag = 1;
    /// funct7 --> imm12.10_5[31:25]
    SINT32 i12 = ir & 0x80000000;      /// sign bit
    UINT32 i11 = (imm5L) & 0x1;   /// 1 bit
    UINT32 i10_5 = (funct7) & 0x3f; /// 6 bits
    UINT32 i4_1 = (imm5L) >> 1;    /// 4 bits
    imm = (i12 >> 19) | (i11 << 11) | (i10_5 << 5) | (i4_1 << 1);   /// lsb = 0
}
/*    R-type : funct7[31:25], rs2[24:20], rs1[19:15], funct3[14:12], rd[11:7], opcode[6:0]*/
void RVInsn::dec_R(unsigned ir, unsigned opval) {
    op = opval; RV_FLD(funct7); RV_FLD(rs2); RV_FLD(rs1); RV_FLD(funct3); RV_FLD(rd);
    rs2Flag = 1;
}
/* sys */
// funct3 encoding
// 000:	ecall/ebreak/xret/wfi/xfence
// 001:	csrrw
// 010:	csrrs
// 011:	csrrc
// 100:	N/A
// 101:	csrrwi
// 110:	csrrsi
// 111:	csrrci

#if defined(USE_RVINSN_DEC_2)
void RVInsn::dec_SYS(unsigned ir, unsigned opval) {
    op = opval;
    RV_FLD_2(imm12H); RV_FLD_2(rs1); RV_FLD_2(funct3); RV_FLD_2(rd); RV_FLD_2(funct7);
}
#endif

void CPU::dec_SYS(BIT &mode_change) {
#if defined(USE_RVINSN_DEC_2)
    insn.dec_SYS(ir, RVO_sys);
#else
	insn.op = RVO_sys;

    RV_FLD_2(imm12H); RV_FLD_2(rs1); RV_FLD_2(funct3); RV_FLD_2(rd);
	///	1/19/17 : need funct7 in sext decoding!!!
    RV_FLD_2(funct7);
#endif
	SINT32 i11 = ir & 0x80000000;          /// sign bit
	UINT32 i10_0 = (insn.imm12H) & 0x7ff;    /// 11 bits
	insn.imm = (i11 >> 20) | i10_0;

#if defined(DBG_SYS)
    printf("[DBG_SYS][%8d] 0x%08x: imm12H(%03x), rs1(%02x), rd(%02x), funct3(%x), funct7(%02x)\n",
        csr.mcycle, fe.r.pc, insn.imm12H, insn.rs1, insn.rd, insn.funct3, insn.funct7);
    printf("[DBG_SYS][%8d] 0x%08x: wb.mode(%x)\n",
        csr.mcycle, fe.r.pc, wb.mode);
#endif

	if (insn.funct3 == 0) {
		if (insn.imm12H == 0x000) {
			insn.op = RVO_trap;
			insn.imm12H = 8 | wb.mode;	// Environment call from X-mode
			insn.imm = 0;
            dc.sw_mode = 1;
            if ((wb.mode == 3) && (!mode_change)) {
                halted = 1;
            }
		} else if (insn.imm12H  == 0x105) {
			// WFI
			insn.op = RVO_nop;
			if ((wb.mode == 3) && (!mode_change)) {
				halted = 1;
				printf("0x%08x: WFI in M-mode\n", fe.r.pc);
			}
		} else if (insn.imm12H  == 0xfc0) {	// dcache flush
				insn.op = RVO_fence;
				insn.funct3 = 3;
		} else if (insn.imm12H  == 0xfc2) { // dcache invalidate
				insn.op = RVO_fence;
				insn.funct3 = 2;
		} else if ((insn.imm12H & 0xfe0) == 0x120) {
			// SFENCE.VMA
			insn.op = RVO_fence;
			insn.funct3 = 3;
#if !defined(MODIFY_DTLB_FLUSH)
			dc.itflush = 1;
#endif
		} else if ((insn.imm12H & 0x7e0) == 0x220) {
			// HFENCE.xVMA
			halted = 1;
			printf("0x%08x: HFENCE.xVMA\n", fe.r.pc);
		} else if ((insn.imm12H & 0x3f) != 0x02) {
			// !xRET
			halted = 1;
			printf("0x%08x: EBRAKE....\n", fe.r.pc);
		} else {
			insn.op = RVO_priv;
			dc.sw_mode = 1;
		}
	} else if (insn.funct3 == 4) {
		halted = 1;
		printf("0x%08x: Illegal inst(SYS)\n", fe.r.pc);
	} else {
		if (insn.imm12H == CSR_satp) {
			dc.sw_mode = 1;
		}
		// not ((clear/set bit) && (rs1 == x0))
        if (!((insn.funct3 & 0x2) && (insn.rs1 == 0))) {
            insn.csrwFlag = 1;
        }
		if (insn.funct3 & 0x4) {	// uimm[4:0]
			insn.csriFlag = 1;
			insn.imm5L = insn.rs1;
			insn.rs1 = 0;
		}
		// CSRRW: if rd==x0 then shall not read CSR
		if ((insn.funct3 & 0x2) || (insn.rd != 0)) {
			insn.csrFlag = 1;
		}
	}
}

BIT check_branch(UINT32 src0, UINT32 src1, UINT32 funct3) {
	BIT isSigned = !((funct3 >> 1) & 1);    /// not BLTU(110)/BGEU(111)
	BIT sign1 = isSigned & (src0 >> 31);
	BIT sign2 = isSigned & (src1 >> 31);
	BIT ultFlag = (src0 < src1);
	BIT ltFlag = sign1 ^ sign2 ^ ultFlag;
	BIT eqFlag = (src0 == src1);
	switch (funct3) {
	case RVF3_beq:  return  eqFlag;
	case RVF3_bne:  return !eqFlag;
	case RVF3_blt:  return  ltFlag;
	case RVF3_bge:  return !ltFlag;
	case RVF3_bltu: return  ultFlag;
	default:        return !ultFlag;    /// RVF3_bgeu
	}
}

//#if defined(USE_BR_DELAY)   /// 7/6/20
void CPU::decode() {
    insn.reset();   /// 7/31/20
    dc.resetSig();

    BIT sw_mode_active = DC_SW_MODE;

    ///	9/27/14
    if (!DC_STALL) {
        insn.opcode = GET_RV_FLD(opcode);
#define	OP_RVG(x)	((x >> 2) & 0x1f)
        switch (OP_RVG(insn.opcode)) {
        case OP_RVG(RVI_lui):   insn.dec_U (ir, RVO_lui);   break;
        case OP_RVG(RVI_auipc): insn.dec_U (ir, RVO_auipc); break;
        case OP_RVG(RVI_jal):   insn.dec_UJ(ir, RVO_jal);   break;
        case OP_RVG(RVI_jalr):  insn.dec_I (ir, RVO_jalr);  break;
        case OP_RVG(RVI_br):    insn.dec_SB(ir, RVO_br);    break;
        case OP_RVG(RVI_ld):    insn.dec_I (ir, RVO_ld);    break;
        case OP_RVG(RVI_st):    insn.dec_S (ir, RVO_st);    break;
        case OP_RVG(RVI_compi): insn.dec_I (ir, RVO_comp);  break;
        case OP_RVG(RVI_comp):  insn.dec_R (ir, RVO_comp);  break;
        case OP_RVG(RVI_amo):   insn.dec_R (ir, RVO_amo);
            if (insn.funct3 != 2 ||
                (((insn.funct7 & 0x30) != 0) && ((insn.funct7 & 0x0c) != 0)) ||
                (((insn.funct7 & 0x0c) == 0x08) && (insn.rs2 != 0))) {
                halted = 1;
            }
            break;
#if defined(MODIFY_DTLB_FLUSH)
        case OP_RVG(RVI_fence): insn.dec_I(ir, RVO_fence);  break;
#else
        case OP_RVG(RVI_fence): insn.dec_I(ir, RVO_fence);  dc.iflush = (insn.funct3 == 1); break;
#endif
#if !defined(NO_MMU)
        case OP_RVG(RVI_sys):   dec_SYS(sw_mode_active);  break;
#endif
        default:        insn.op = RVO_nop;      halted = 1; break;  /// halt on unsupported insn...
        }
        UINT32 csr_waddr = insn.imm12H;	// trap cause.

#if !defined(NO_MMU)
        if (icache.tlb.page_fault) {
            insn.op = RVO_trap;
            csr_waddr = 12;		// Instruction page fault
            insn.imm = fe.r.pc;
            dc.sw_mode = 1;
        }
#endif
        UINT32 rs1_val = (insn.rs1) ? (FW_DC(me, insn.rs1) : (FW_DC(wb, insn.rs1) : gpr[insn.rs1])) : 0;
        UINT32 rs2_val = (insn.rs2) ? (FW_DC(me, insn.rs2) : (FW_DC(wb, insn.rs2) : gpr[insn.rs2])) : 0;
        UINT32 csr_val = 0;
#if !defined(NO_MMU)
        if (insn.csrFlag) {
            switch (insn.imm12H) {
            case CSR_time:  csr_val = io.sysctrl.mtime_l; break;
            case CSR_timeh: csr_val = io.sysctrl.mtime_h; break;
            default:        csr_val = csr.read(insn.imm12H); break;
            }
        }
#endif
        BIT csr_we = insn.csrwFlag;
        BIT dc_hazard = detect_dc_hazard();
	// when icache/itlb flush stall fe
	BIT fence_stall = 0;
	if ((dc.r.op == RVO_fence) && (dc.r.funct3 & 1)) {
		fence_stall = 1;
	} else if ((insn.op == RVO_fence) && (insn.funct3 & 1)) {
		dc.stall = 1;
	}
        /// 7/7/20
        if (sw_mode_active || dc_hazard || fence_stall) {
            // bubble
            dc.r.op = RVO_nop;
            dc.r.mdFlag = 0;
            dc.r.dst_f = 0;
            dc.r.src_f[0] = 0;
            dc.r.src_f[1] = 0;
            dc.r.sw_mode = 0;
            csr_we = 0;
#if !defined __LLVM_C2RTL__//__TCT_COMPILER_USED__
            hazard.dc += dc_hazard;
#endif
        }
        else {
            dc.r.dst_id = insn.rd;
            dc.r.src_id[0] = insn.rs1;
            dc.r.src_id[1] = insn.rs2;
            dc.r.src[0] = (insn.op == RVO_auipc) ? fe.r.pc : insn.csriFlag ? insn.imm5L : rs1_val;
            dc.r.src[1] = (insn.linkFlag) ? fe.r.pc + 4 : (insn.rs2Flag) ? rs2_val : insn.csrFlag ? csr_val : insn.imm;
            dc.r.src[2] = insn.imm;
            dc.r.op = insn.op;
            dc.r.funct3 = insn.funct3;
            dc.r.subFlag = (insn.op == RVO_comp) && insn.rs2Flag &&
                (insn.funct3 == RVF3_add) && (insn.funct7 == 0x20);
            dc.r.sextFlag = (insn.op == RVO_comp) &&
                (insn.funct3 == RVF3_shr) && (insn.funct7 == 0x20);
            dc.r.mdFlag = (insn.op == RVO_comp) && insn.rs2Flag && (insn.funct7 == 0x01);
            dc.r.dst_f = (insn.rd != 0);
            dc.r.src_f[0] = (insn.rs1 != 0);
            dc.r.src_f[1] = (insn.rs2 != 0);

            dc.r.amo_op = (insn.funct7 >> 2) & 0x1f;
            dc.r.aq = (insn.funct7 >> 1) & 1;
            dc.r.rl = insn.funct7 & 1;

            dc.r.csrc.waddr = csr_waddr;
            dc.r.sw_mode = dc.sw_mode;
        }
        dc.r.pc = (wb.br.active) ? wb.br.addr : ((fe.r.br.active) ? fe.r.br.addr : fe.r.pc);
        dc.r.csrc.we = csr_we;
    } else {
	if ((dc.r.op == RVO_fence) && (dc.r.funct3 & 1)) {
            // bubble
            dc.r.op = RVO_nop;
            dc.r.mdFlag = 0;
            dc.r.dst_f = 0;
            dc.r.src_f[0] = 0;
            dc.r.src_f[1] = 0;
            dc.r.sw_mode = 0;
	    dc.r.csrc.we = 0;
	}
    }
#if defined(DBG_DC)
//    if (csr.mcycle >= 180 && csr.mcycle <= 200)
//    if (csr.mcycle >= 39000 && csr.mcycle <= 39500)
    if (csr.mcycle < 300)
        printf("[%6d] op(%2x:%2x) fe.pc(%8x) stall(%d:%d:%d:%d) \n", csr.mcycle,
            insn.op, insn.opcode, fe.r.pc,
            dc.stall, ex.stall, me.stall, wb.stall);
#endif
}

BIT CPU::detect_dc_hazard() {
    BIT ld_hazard = ((dc.r.op == RVO_ld) || (dc.r.op == RVO_amo)) &&
        dc.r.dst_f && (dc.r.dst_id == insn.rs1 || dc.r.dst_id == insn.rs2);
    BIT dc_hazard = 0;
    int br_delay = dc.r.br_delay;
    dc.r.disable_irq = (br_delay != 0);
    switch (br_delay) {
    case 2:
        dc.r.br_delay = 1;
        if (insn.br_type != 3) {
            insn.linkFlag = (insn.rd != 0);
        }
        dc.stall = 1;
        break;
    case 1:
        dc.r.br_delay = 0;
        dc_hazard = 1;
        break;
    default:
        dc_hazard = ld_hazard;
        if (insn.br_type) {
            dc.r.br_delay = (ld_hazard) ? 2 : 1;
            dc.stall = 1;
            if (!ld_hazard && insn.br_type != 3) {
                insn.linkFlag = (insn.rd != 0);
            }
        }
        else {
            dc.r.br_delay = 0;
            dc.stall = dc_hazard;
        }
        break;
    }
    return dc_hazard;
}

UINT8 count_effective_bits(UINT32 n)
{
#if 1   /// 6/26/20 : 16ns : 0.142ns slack (MET)
    for (int i = 31; i >= 0; --i) {
        if ((n >> i) & 1) { return i; }
    }
    return 0;
#else
    BIT z4 = (((n) >> 16) & 0xffff) != 0;	UINT32 x4 = (z4) ? ((n) >> 16) : ((n) & 0xffff);
	BIT z3 = ((x4 >> 8) & 0xff) != 0;		UINT32 x3 = (z3) ? (x4 >> 8) : (x4 & 0xff);
	BIT z2 = ((x3 >> 4) & 0xf) != 0;		UINT32 x2 = (z2) ? (x3 >> 4) : (x3 & 0xf);
	BIT z1 = ((x2 >> 2) & 0x3) != 0;		UINT32 x1 = (z1) ? (x2 >> 2) : (x2 & 0x3);
	BIT z0 = ((x1 >> 1) & 0x1) != 0;
	return (UINT8)((z4 << 4) | (z3 << 3) | (z2 << 2) | (z1 << 1) | z0);
#endif
}

UINT32 CPU::div_st_1(UINT32 src0, UINT32 src1) ///src1 : divisor, src2 : dividend
{
    BIT modulo_flag = ((dc.r.funct3 & 2) != 0);
    BIT isSigned = !(dc.r.funct3 & 1);
    /// 6/16/20
    md.last_op = isSigned; /// div (unsigned ...?
    UINT32 src0abs = (isSigned && (SINT32)src0 < 0) ? -(SINT32)src0 : src0;
    UINT32 src1abs = (isSigned && (SINT32)src1 < 0) ? -(SINT32)src1 : src1;
    md.f = src0abs;
    md.g = src1abs;
    md.modulo_flag = modulo_flag;
    md.negate_q = (isSigned) ? ((src0 >> 31) != (src1 >> 31)) : 0;
    md.negate_r = (isSigned) ? (src0 >> 31) : 0;
    md.pending = 3;
    ex.stall = 1;
    SET_EX_PENDING_OP(1, 0, 0);	///	is_pending = 1, is_interruptible = 0, resume_from_next_insn = 0;
    return 0;
}

UINT32 CPU::div_st_1b() {
    UINT8  shift0 = count_effective_bits(md.f);
    UINT8  shift1 = count_effective_bits(md.g);
    UINT8  shift = shift1 + ((~shift0) & 0x1f);
    UINT8  shift_plus1 = shift + 1;
    BIT    shift_is_larger_than_31 = ((shift_plus1 >> 5) != 0);
    UINT32 src0abs = md.f;
    UINT32 src0abs_sl1 = (src0abs << 1) | 1;
    md.f = (shift_is_larger_than_31) ? 0x80000000 : src0abs_sl1 << (shift & 0x1f);
    md.r = ((shift >> 5) != 0) ? src0abs : src0abs >> (~shift & 0x1f);
    md.q = 0;
    md.pending = 1;
    md.terminate_condition = shift_is_larger_than_31;
    /// 6/6/2020
    ex.stall = 1;
    SET_EX_PENDING_OP(1, 0, 0);	///	is_pending = 1, is_interruptible = 0, resume_from_next_insn = 0;
    return 0;
}

UINT32 CPU::div_st_2()
{
    BIT	r_le_g = (md.r >= md.g);
    UINT32 r_next = (r_le_g) ? (md.r - md.g) : md.r;
    UINT32 q_next = (md.q << 1) | r_le_g;
    UINT32 f_next = (md.f << 1);
    UINT32 div_res = 0;
    if (md.terminate_condition) {
        UINT32 res = (md.modulo_flag) ? r_next : q_next;
        /// 6/16/20
        BIT negateFlag = (md.modulo_flag) ? md.negate_r : md.negate_q;
        UINT32 res2 = ((negateFlag) ? ~res + 1 : res);
        md.pending = 0;
        div_res = res2;
        md.last_op |= 2; /// div (unsigned ...?
    }
    else {
        /// 6/6/2020
        ex.stall = 1;
        SET_EX_PENDING_OP(1, 0, 0);	///	is_pending = 1, is_interruptible = 0, resume_from_next_insn = 0;
    }
    /// 6/16/20
    md.r = (md.terminate_condition) ? ((md.negate_r) ? ~r_next + 1 : r_next) : (r_next << 1 | md.f >> 31);
    md.q = q_next;
    md.f = f_next;
    md.terminate_condition = (f_next == 0x80000000);
    return div_res;
}

UINT32 CPU::format_read_data(UINT32 dout, UINT2 byte_pos, UINT3 funct3)
{
	UINT32 shf_dout, sign_ext;
	BIT isSigned = !(funct3 & 4); /// bit-2 is 0
	switch (funct3 & 3) {
	case RVF3_lb:
		switch (byte_pos) {
		case 0:		shf_dout = dout & 0xff;			break;
		case 1:		shf_dout = (dout >> 8) & 0xff;	break;
		case 2:		shf_dout = (dout >> 16) & 0xff;	break;
		default:	shf_dout = (dout >> 24) & 0xff;	break;
		}
		sign_ext = (isSigned && (shf_dout & 0x80)) ? 0xffffff00 : 0x0;
		break;
	case RVF3_lh:
		switch (byte_pos) {
		case 0:		shf_dout = dout & 0xffff;			break;
		default:	shf_dout = (dout >> 16) & 0xffff;	break;
		}
		sign_ext = (isSigned && (shf_dout & 0x8000)) ? 0xffff0000 : 0x0;
		break;
	default:
		shf_dout = dout;
		sign_ext = 0;
		break;
	}
	return shf_dout | sign_ext;
}

UINT4 CPU::byte_enable(UINT2 byte_pos, UINT3 funct3)
{
	switch (funct3) {
	case RVF3_sb:	return (0x1 << byte_pos) & 0xf;
	case RVF3_sh:	return (0x3 << byte_pos) & 0xf;
	default:		return 0xf;
	}
}

UINT32 format_write_data(UINT32 din, UINT2 byte_pos)
{
	switch (byte_pos) {
	case 0:		return din;
	case 1:		return (FC_UINT32)(din << 8);
	case 2:		return (FC_UINT32)(din << 16);
	default:	return (FC_UINT32)(din << 24);
	}
}

void write_mem(UINT32 * mem, UINT4 byte_enable, UINT32 din)
{
	UINT8 * cmem = (UINT8 *)mem;
	UINT8 * cdin = (UINT8 *)&din;
	if (byte_enable & 0x1) { cmem[0] = cdin[0]; }
	if (byte_enable & 0x2) { cmem[1] = cdin[1]; }
	if (byte_enable & 0x4) { cmem[2] = cdin[2]; }
	if (byte_enable & 0x8) { cmem[3] = cdin[3]; }
}

/* mult:
| AH | AL |
* | BH | BL |
------------------------
|  AL*BL  |
|  AH*BL  |
|  AL*BH  |
|  AH*BH  |

unsigned(A) * unsigned(B) :
A  = sigma{i=0..31}(a[i]*2^i)
AL = sigma{i=0..15}(a[i]*2^i)
AH = sigma{i=0..15}(a[i+16]*2^i)
sigma{i=16..31}(a[i]*2^i) = AH*2^16
A = AH*2^16 + AL
B = BH*2^16 + BL
A * B = (AH*2^16 + AL)*(BH*2^16 + BL) = AH*BH*2^32 + (AH*BL+AL*BH)*2^16 + AL*BL

signed(A) * signed(B) :
sA = a[31]
SA = sA*2^31
A = sigma{i=0..30}(a[i]*2^i)-SA
AL = sigma{i=0..15}(a[i]*2^i)
AH = sigma{i=0..14}(a[i+16]*2^i)-a[31]*2^15 = (AH'-SA') (SA' = sA*2^15)
sigma{i=16..30}(a[i]*2^i)-a[31]*2^31 = AH*2^16
A = AH*2^16 + AL
B = BH*2^16 + BL
A * B = (AH*2^16 + AL)*(BH*2^16 + BL) = AH*BH*2^32 + (AH*BL+AL*BH)*2^16 + AL*BL
AH*BH = (AH'-SA')*(BH'-SB') = AH'*BH'-(SA'*BH'+SB'*AH')+SA'*SB'
= AH'*BH'-(sA'*BH'+sB'*AH')*2^15+sA'*sB'*2^30
*/
//#if !defined(DISABLE_MD)
///	1/19/17 : signed mult!!
#if 0
MUL(R) : opcode(0110011), funct3(000), funct7(0000001) <--sign does not matter here
	MULH(R) : opcode(0110011), funct3(001), funct7(0000001) <-- signed(src0), signed(src1)
	MULHSU(R) : opcode(0110011), funct3(010), funct7(0000001) <-- signed(src0), unsigned(src1)
	MULHU(R) : opcode(0110011), funct3(011), funct7(0000001) <-- unsigned(src0), unsigned(src1)
#endif
//#if defined(USE_REG_ID_FUSION)  /// 7/7/20 : fix div bug
UINT32 CPU::mul_st_1(UINT32 src0, UINT32 src1)    /// only unsigned version for now....
{
    //#if defined (ENABLE_MUL_FUSION) || defined (ENABLE_DIV_FUSION)
    /// 6/26/20
    md.last_op = 0; /// mul
    SINT32 a = (SINT32)(src0);
    SINT32 b = (SINT32)(src1);
    SINT64 aa = a;  /// sign-extension
    SINT64 bx = b & 0xffff;  /// (pending = 0) bx = bl, (pending = 2) bx = bh
    int unsigned_a = (dc.r.funct3 == 3);
    if (unsigned_a) { aa &= 0xffffffff; } ///	clear sign extension if (MULHU)
                                          /// (pending = 0)   (pending = 2)
    SINT64 p = aa * bx;         /// bits[47:0]      bits[63:16]
    md.r = (p & 0xffff);            /// bits[15:0]	: a * bl
    md.q = (UINT32)(p >> 16);       /// bits[47:16]	: a * bl
    md.f = src0;
    md.g = src1;
    md.pending = 2;
    /// 6/6/2020
    ex.stall = 1;
    SET_EX_PENDING_OP(1, 0, 0);	///	is_pending = 1, is_interruptible = 0, resume_from_next_insn = 0;
    return 0;
}
UINT32 CPU::mul_st_2()    /// only unsigned version for now....
{
    //#if defined (ENABLE_MUL_FUSION) || defined (ENABLE_DIV_FUSION)
    md.last_op = 1; /// mul
    SINT32 a = (SINT32)(md.f);
    SINT32 b = (SINT32)(md.g);
    SINT64 aa = a;  /// sign-extension
    SINT64 bx = (b >> 16);  /// (pending = 0) bx = bl, (pending = 2) bx = bh
    int unsigned_a = (dc.r.funct3 == 3);
    int unsigned_b = (dc.r.funct3 != 1);
    if (unsigned_a) { aa &= 0xffffffff; } ///	clear sign extension if (MULHU)
    if (unsigned_b) { bx &= 0xffff; }     ///	clear sign extension if (not MULH)
                                          /// (pending = 0)   (pending = 2)
    SINT64 p = aa * bx;         /// bits[47:0]      bits[63:16]
    int sign_q = (!unsigned_a) && (md.q >> 31);
    SINT64 q = (SINT32)md.q;    /// sign-extension
    if (!sign_q) { q &= 0xffffffff; }
    SINT64 sh2 = p + q;
    UINT32 ll = ((sh2 << 16) & 0xffff0000) | (md.r & 0xffff);   /// bits[31:0]
    UINT32 hh = (UINT32)(sh2 >> 16);
    md.pending = 0;
    //#if defined (ENABLE_MUL_FUSION)
    md.r = ll;
    return (dc.r.funct3 == RVF3_mul) ? ll : hh;
}

/// 7/9/20
void CPU::execute()
{
    UINT32 src0 = FW_EX(me, 0) : FW_EX(wb, 0) : dc.r.src[0];
    UINT32 src1 = FW_EX(me, 1) : FW_EX(wb, 1) : dc.r.src[1];

    UINT32 ex_out = 0;
    CSRCtrl csrc(0, dc.r.csrc.waddr, dc.r.src[2]);

    UINT32 add_out = (src0 + src1);
    UINT32 add_imm = (src0 + dc.r.src[2]);
    UINT32 add_pc = (dc.r.pc + dc.r.src[2]) & 0xfffffffe;

    UINT6 op = dc.r.op;
    BIT	mode_change = dc.r.sw_mode;

#if !defined(NO_MMU)
    UINT32 irq = _irq(wb.mode, DF_MSTATUS, csr.mip & DF_MIE);// IRQ;
#endif

    dm_addr = ex.r.dst.out;

    ex.resetSig();
    ex.sw_mode = dc.r.sw_mode;
    BIT sw_mode_active = EX_SW_MODE;

    BIT wait_flush = ex.r.wait_flush || sw_mode_active;

    SET_EX_PENDING_OP(0, 0, 0);	///	is_pending = 0, is_interruptible = 0, resume_from_next_insn = 0;

    //ex.stall = 0;
#if defined(MODIFY_DTLB_FLUSH)
    BIT fence_active = 0;
#endif
    if (!EX_STALL) {
#if !defined(NO_MMU)
        if (irq && !sw_mode_active && !dc.r.disable_irq) {
            op = RVO_trap;
            /// 7/7/20
            for (int i = 0; i <= 17; ++i, irq >>= 1) {
                if (irq & 1) {
                    csrc.waddr = i;
                    break;
                }
            }
            csrc.waddr |= 0x80000000;
            csrc.wdata = 0;
            mode_change = 1;
            ex.sw_mode = 1;   /// 7/14/20 : yoshiya
        }
#endif
        ex.br.addr = 0;
        ex.br.active = 0;
        if (dc.r.mdFlag) {
            BIT divFlag = (dc.r.funct3 & 4) != 0;
            BIT modFlag = divFlag && ((dc.r.funct3 & 2) != 0);
            BIT fusionRegMatch = (md.srcID[0] == dc.r.src_id[0] && md.srcID[1] == dc.r.src_id[1]);
            BIT fusionDiv = (md.last_op & 2) && modFlag && ((md.last_op & 1) != (dc.r.funct3 & 1));
            BIT fusionMul = (md.last_op == 1) && !divFlag && (dc.r.funct3 == RVF3_mul);
            BIT curPending = md.pending;
            if (!md.pending && fusionRegMatch && (fusionDiv || fusionMul)) {
                ex_out = md.r;
                md.last_op = 0;
            }
            else if (md.pending == 3) { ex_out = div_st_1b(); }
            else if (md.pending == 1) { ex_out = div_st_2(); }
            else if (divFlag)         { ex_out = div_st_1(src0, src1); }
            else if (md.pending)      { ex_out = mul_st_2(); }
            else                      { ex_out = mul_st_1(src0, src1); }
            if (!curPending) {
                md.srcID[0] = (dc.r.dst_id == dc.r.src_id[0]) ? 0 : dc.r.src_id[0];
                md.srcID[1] = (dc.r.dst_id == dc.r.src_id[1]) ? 0 : dc.r.src_id[1];
            }
        }
        else {
            if (dc.r.dst_f) {
                if (dc.r.dst_id == md.srcID[0]) { md.srcID[0] = 0; }
                if (dc.r.dst_id == md.srcID[1]) { md.srcID[1] = 0; }
            }
            switch (op) {
            case RVO_br:
                ex.br.active = check_branch(src0, src1, dc.r.funct3);
                ex.br.addr = add_pc;
                break;
            case RVO_nop:    break;
                /// 7/7/20
            case RVO_ld:
            case RVO_st:
                dm_addr = add_imm;
                ex_out = add_imm;
                csrc.wdata = src1;
                break;
            case RVO_fence:
                dm_addr = 0;
#if defined(MODIFY_DTLB_FLUSH)
                fence_active = 1;
                if (dc.r.funct3 == 1) {
                    ex.iflush = 1;
                }
                else if (dc.r.funct3 == 3) {
                    ex.itflush = 1;
                    ex.itflush_vpn_valid = dc.r.src_f[0];
                    ex.itflush_vpn = src0 >> 12;
                    dm_addr = src0;
                    ex_out = src0;
#if 0   /// 9/7/20
                    if (ex.itflush_vpn_valid) {
                        printf("[%8d] : ex.itflush_vpn = %08x\n", csr.mcycle, ex.itflush_vpn);
                    }
#elif 0//!defined(__LLVM_C2RTL__)
                    if (ex.itflush_vpn_valid) {
                        printf("[%8d] : ex.itflush_vpn = %08x\n", cycle, ex.itflush_vpn);
                    }
#endif
                }
#endif
                break;
            case RVO_lui:
            case RVO_auipc:
                ex_out = add_out;
                break;
            case RVO_jal:
                ex.br.active = 1;
                ex.br.addr = add_pc;
                ex_out = src1;
                break;
            case RVO_jalr:
                ex.br.active = 1;
                ex.br.addr = add_imm & 0xfffffffe;
                ex_out = src1;
                break;
            case RVO_sys:
                csrc.waddr = dc.r.csrc.waddr;
                src1 = DF_CSR(dc.r.csrc.waddr, src1);
                switch (dc.r.funct3 & 3) {
                case 2: csrc.wdata = src0 | src1;  break; // bit set
                case 3: csrc.wdata = ~src0 & src1; break; // bit clear
                default: csrc.wdata = src0; break;
                }
                ex_out = src1;
                break;
            case RVO_priv:
                csrc.waddr = dc.r.csrc.waddr;
                break;
            case RVO_amo:
                dm_addr = src0;
                csrc.wdata = src1;
                ex_out = src0;
                break;
            case RVO_comp:
                BIT ultFlag = (src0 < src1), sign0 = (src0 >> 31), sign1 = (src1 >> 31);
                BIT sraFlag = (dc.r.sextFlag && sign0);
                UINT32 shamt = (src1 & 0x1f);
                switch (dc.r.funct3) {
                case RVF3_add:  ex_out = (dc.r.subFlag) ? (src0 - src1) : add_out; break;
                case RVF3_slt:  ex_out = sign0 ^ sign1 ^ ultFlag;   break;
                case RVF3_sltu: ex_out = ultFlag;                   break;
                case RVF3_shl:  ex_out = src0 << shamt;             break;
                case RVF3_shr:  ex_out = (src0 >> shamt) | ((sraFlag) ? ~(0xffffffffu >> shamt) : 0); break;
                case RVF3_xor:  ex_out = src0 ^ src1;               break;
                case RVF3_or:   ex_out = src0 | src1;               break;
                case RVF3_and:  ex_out = src0 & src1;               break;
                }
                break;
            }
        }
#if 0
        if (csr.mcycle >= 115559560 && csr.mcycle <= 115559590) { /// 115559597
            printf("[%8d] pc(%08x) src(%08x:%08x), op(%d)\n", csr.mcycle, dc.r.pc,
                src0, src1, op);
        }
#endif
        ex.r.dst.out = ex_out;
        if (op == RVO_trap) {
            ex.r.dst.valid = 0;
            csrc.we = 0;
        }
        else if (ex.stall || wait_flush) {
            // bubble
            if (!ex.stall) { wait_flush = 0; }
            op = RVO_nop;
            ex.r.dst.valid = 0;
            csrc.we = 0;
            mode_change = 0;
        }
        else {
            ex.r.dst.valid = dc.r.dst_f;
            ex.r.dst.id = dc.r.dst_id;
            csrc.we = dc.r.csrc.we;
        }
        ex.r.pc = wb.br.active ? wb.br.addr : dc.r.pc;   /// 7/21/20
        ex.r.op = op;
        ex.r.funct3 = dc.r.funct3;
        ex.r.amo_op = dc.r.amo_op;
        ex.r.csrc = csrc;
#if defined(FORMAT_WRITE_AT_EX)
        ex_reg.wdata = format_write_data(csrc.wdata, ex_out & 3);
#endif
        ex.r.sw_mode = mode_change;
#if defined(MODIFY_DTLB_FLUSH)
        ex.r.src_f0 = dc.r.src_f[0];
#endif
    }
#if defined(MODIFY_DTLB_FLUSH)
    io.dcache.pre_fetch(dm_addr, wb.mmu_en, fence_active);
#else
    io.dcache.pre_fetch(dm_addr, wb.mmu_en);
#endif
    // update stall status.
    ex.r.wait_flush = wait_flush;
#if defined(DBG_EX)
#define DBG_CYCLE 12000//11500//11000//10000//12000//15000
#define DBG_RANGE 500
    if (csr.mcycle >= (DBG_CYCLE) && csr.mcycle <= (DBG_CYCLE + DBG_RANGE))
        //    if (csr.mcycle >= 39300 && csr.mcycle <= 39500)
    //if (csr.mcycle <= 300)
        printf("[%6d] op(%2x) dc.pc(%8x) stall(%d%d%d) src0(%08x) src1(%08x)\n", csr.mcycle, dc.r.op, dc.r.pc,
            ex.stall, me.stall, wb.stall,
            src0, src1);
#endif

}

/// 7/16/20
void CPU::memory(AXI4L::CH *axi, UINT16 intr)
{
    // memory stage
    UINT6 op = ex.r.op;
    BIT	mode_change = ex.r.sw_mode;
#if defined(FORMAT_WRITE_AT_EX)
    UINT32 wdata = ex.r.wdata;
#else
    UINT32 wdata = format_write_data(ex.r.csrc.wdata, ex.r.dst.out & 3);
#endif
    DstStatus dst = ex.r.dst;
    CSRCtrl csrc = ex.r.csrc;

    me.stall = 0;
    me.sw_mode = ex.r.sw_mode;
    BIT mode_changing = ME_SW_MODE;

    BIT	output_pending = me.r.output_pending;
    UINT32	output = me.r.output;
    BIT	amo_st = me.r.amo_st;
    BIT	amo_reserved = me.r.amo_reserved;

    UINT6	io_op = op;

#if defined(ENABLE_TLB_STAT) && !defined(NO_MMU)
    if (wb.mmu_en) { io.dcache.tlb.stat.update_dtlb(ex.r); }
#endif
#if	ENABLE_CACHE_STAT
    io.dcache.stat.update_dcache(ex.r);
#endif
    if (op == RVO_amo) {
        // TODO: bus lock.
        if (ex.r.amo_op == AMO_sc) {
            amo_st = 1;
            if (amo_reserved && (me.r.amo_reserved_address == dst.out)) {
                io_op = RVO_st;
            }
            else {
                io_op = RVO_nop;
                dst.out = 1;
            }
        }
        else {
            io_op = (amo_st == 0) ? RVO_ld : RVO_st;
        }
        switch (ex.r.amo_op) {
        case AMO_add:  wdata += me.r.amo_data; break;
        case AMO_xor:  wdata ^= me.r.amo_data; break;
        case AMO_and:  wdata &= me.r.amo_data; break;
        case AMO_or:   wdata |= me.r.amo_data; break;
        case AMO_min:  wdata = ((int)wdata < (int)me.r.amo_data) ? (int)wdata : (int)me.r.amo_data; break;
        case AMO_max:  wdata = ((int)wdata > (int)me.r.amo_data) ? (int)wdata : (int)me.r.amo_data; break;
        case AMO_minu: wdata = (wdata < me.r.amo_data) ? wdata : me.r.amo_data; break;
        case AMO_maxu: wdata = (wdata > me.r.amo_data) ? wdata : me.r.amo_data; break;
        }
    }
#if 0   /// 9/23/20 : debugging...
//    if (csr.mcycle >= 115595000 && csr.mcycle <= 115595030) {
    if (csr.mcycle >= 115559560 && csr.mcycle <= 115559600) { /// 115559597
        printf("[%8d] pc(%08x) wdata(%08x:%08x), addr(%08x:%08x), op(%d:%d:%d)\n", csr.mcycle, ex.r.pc, 
            wdata, ex.r.csrc.wdata,
            ex.r.dst.out, csr.satp, op, io_op, ex.r.amo_op);
    }
#endif
#if defined(MODIFY_DTLB_FLUSH)
    int isIOop = io.fsm(io_op, wdata, ex.r.dst.out, ex.r.funct3, &dummy_fifo_port, axi, intr,
        wb.mmu_en, csr.satp, ex.r.src_f0);
#else
    int isIOop = io.fsm(io_op, wdata, ex.r.dst.out, ex.r.funct3, &dummy_fifo_port, axi, intr,
        wb.mmu_en, csr.satp);
#endif

#if defined(DBG_ME)
    if (csr.mcycle <= 300)
        printf("[%6d] ex.pc(%8x) op(%2x:%2x) addr(%08x) dcache.{busy(%d),state(%d),data(%08x)\n",
            csr.mcycle, ex.r.pc,
            op, io_op, ex.r.dst.out, io.dcache.busy, io.dcache.fb_state, io.dcache.data);
#endif

#if !defined(NO_MMU)
    if (io.dcache.tlb.page_fault) {
        if (op == RVO_ld) { csrc.waddr = 13; }  // Load page fault
        else              { csrc.waddr = 15; }  // Store/AMO page fault
        op = RVO_trap;
        csrc.wdata = ex.r.dst.out;
        dst.valid = 0;
        csrc.we = 0;
        amo_st = 0;
        mode_change = 1;
        me.sw_mode = 1;
    }
#endif
    if (!output_pending) {
        if (io.active) {
            if (io.stalled) { me.stall = 1; }
            output = format_read_data(io.data_in, ex.r.dst.out & 3, ex.r.funct3);
            output_pending = 1;
            amo_reserved = 0;
        }
    }
    if (!io.stalled && (op == RVO_amo)) {
        if (amo_st == 0) {
            if (ex.r.amo_op == AMO_lr) {
                amo_reserved = 1;
                me.r.amo_reserved_address = ex.r.dst.out;
            }
            else {
                amo_st = 1;
                me.r.amo_data = output;
                me.stall = 1;
            }
        }
        else {
            if (ex.r.amo_op == AMO_sc) {
                output = 0;
            }
            else {
                output = me.r.amo_data;
            }
            amo_st = 0;
        }
    }
    if (!ME_STALL) {
        if (output_pending) {
            dst.out = output;
            output_pending = 0;
        }
        if (op == RVO_trap) {
            csrc.we = 0;
        }
        else if (me.stall || mode_changing) {
            op = RVO_nop;
            dst.valid = 0;
            csrc.we = 0;
        }
        me.r.pc = wb.br.active ? wb.br.addr : ex.r.pc;   /// 7/21/20
        me.r.op = op;
        me.r.dst = dst;
        me.r.csrc = csrc;
        me.r.sw_mode = mode_change;
    }
    me.op = ex.r.op;
    /// 7/6/20
    me.dst.SetCond(ex.r.dst, (ex.r.op != RVO_ld) && (ex.r.op != RVO_amo));
    me.csrc = ex.r.csrc;
    // update stall status.
    me.r.output_pending = output_pending;
    me.r.output = output;

    me.r.amo_st = amo_st;
    me.r.amo_reserved = amo_reserved;
}

void CPU::writeback()
{
    UINT32 wb_data = (me.r.op == RVO_nop) ? 0 : me.r.dst.out;
    wb.sw_mode = me.r.sw_mode;
    BIT mode_changing = me.r.sw_mode;

    CSR next_csr = csr;
    UINT2  next_mode = wb.r.mode;

    wb.br.active = 0;   /// 7/21/20

    wb.stall = 0;
#if !defined(NO_MMU)
    if (me.r.op == RVO_trap) {
        // determine mode which takes trap.
        UINT32 cause = me.r.csrc.waddr;
        UINT32 mask = 1 << (cause & 0x1f);
        if (cause & 0x80000000) { mask &= csr.mideleg; }
        else { mask &= csr.medeleg; }

        UINT2 new_mode = 3;	// machine mode
        if (mask != 0) { new_mode = 1; } // supervisor mode

        BIT xie;

        switch (new_mode) {
        case 1:
            next_csr.scause = me.r.csrc.waddr;
            next_csr.sepc = me.r.pc;
            next_csr.stval = me.r.csrc.wdata;

            xie = next_csr.mstatus & 2;
            next_csr.mstatus &= ~0x0122; // SPP, SPIE, SIE
            next_csr.mstatus |= ((wb.r.mode & 1) << 8);
            next_csr.mstatus |= (xie << 4);

            wb.br.addr = csr.stvec;   /// 7/21/20
            break;
        case 3:
        default:
            next_csr.mcause = me.r.csrc.waddr;
            next_csr.mepc = me.r.pc;
            next_csr.mtval = me.r.csrc.wdata;

            xie = next_csr.mstatus & 8;
            next_csr.mstatus &= ~0x1888; // MPP, MPIE, MIE
            next_csr.mstatus |= (wb.r.mode << 11);
            next_csr.mstatus |= (xie << 4);

            wb.br.addr = csr.mtvec;   /// 7/21/20
            break;
        }
        next_mode = new_mode;
        wb.br.active = 1;   /// 7/21/20
    }
    else if (me.r.op == RVO_priv) {
        BIT xie;
        switch (me.r.csrc.waddr) {
        case 0x102:	// sret
            xie = (next_csr.mstatus >> 5) & 1;
            next_mode = (next_csr.mstatus >> 8) & 1;
            next_csr.mstatus &= ~2;
            next_csr.mstatus |= (xie << 1);

            wb.br.active = 1;   /// 7/21/20
            wb.br.addr = next_csr.sepc;   /// 7/21/20
            break;
        case 0x302:	// mret
            xie = (next_csr.mstatus >> 7) & 1;
            next_mode = (next_csr.mstatus >> 11) & 3;
            next_csr.mstatus &= ~8;
            next_csr.mstatus |= (xie << 3);

            wb.br.active = 1;   /// 7/21/20
            wb.br.addr = next_csr.mepc;   /// 7/21/20
            break;
        }
    }
    if (me.r.csrc.we) {
        next_csr.write(me.r.csrc.waddr, me.r.csrc.wdata);
    }
#endif
    if (me.r.dst.valid) {
        gpr[me.r.dst.id & 0x1f] = wb_data;
    }
    ////if (me.r.csrc.we) {
    ////    next_csr.write(me.r.csrc.waddr, me.r.csrc.wdata);
    ////}
    wb.csrc.we = (mode_changing) ? 0 : me.r.csrc.we;
    wb.csrc.waddr = me.r.csrc.waddr;
    wb.csrc.wdata = me.r.csrc.wdata;
    // update irq
#if !defined(NO_MMU)
    next_csr.mip = (io.sysctrl.seip << 9) | (io.sysctrl.mtip << 7) | (next_csr.mip & 0x93b);

    csr = next_csr;
#endif
    wb.mode = next_mode;
    wb.r.mode = next_mode;

    wb.mmu_en = (wb.r.mode <= 1) && (csr.satp & 0x80000000);
    /// 7/9/20
    if (me.r.op != RVO_nop) {
        wb.dst.SetData(me.r.dst, wb_data);
    }

#if defined(USE_CSR_COUNTERS)
    if (csr.mcycle == 0xffffffff) {
        csr.mcycle = 0;
        csr.mcycleh++;
    }
    else { csr.mcycle++; }
    if (wb.r.cur_pc != me.r.cur_pc) {
        if (csr.minstret == 0xffffffff) {
            csr.minstret = 0;
            csr.minstreth++;
        }
        else { csr.minstret++; }
    }
#else
    csr.mcycle = cycle64 & 0xffffffff;
    csr.mcycleh = (cycle64 >> 32) & 0xffffffff;
    csr.minstret = instret64 & 0xffffffff;
    csr.minstreth = (instret64 >> 32) & 0xffffffff;

    cycle64++;
    if (wb.r.pc != me.r.pc) {
        instret64++;
    }
#endif

#if defined(DBG_WB)
    if (csr.mcycle <= 100)
        printf("[%6d] op(%2x) me.pc(%8x)\n", csr.mcycle, me.r.op, me.r.pc);
#endif
    wb.r.pc = me.r.pc;
}

#if !defined(__LLVM_C2RTL__) && defined(USE_RISC_V) && defined(ENABLE_SHOW_CPU)
void show_riscv_cpu();
#define SHOW_CPU show_riscv_cpu()
#else
#define SHOW_CPU
#endif


//kudo
#define ENABLE_CPU_STAT

#if !defined(__LLVM_C2RTL__) && defined(USE_RISC_V) && defined(ENABLE_CPU_STAT)
void update_cpu_stat();
void show_cpu_stat();
#define CPU_STAT update_cpu_stat()
#define SHOW_STAT show_cpu_stat()
#else
#define CPU_STAT
#endif


#include "RVProc_io_ext.h"
#define NUM_MASTERS 2
#if defined(NO_SPI)
#define NUM_SLAVES 2
#else
#define NUM_SLAVES 3
#endif

struct IO_PINS {
#if defined(USE_FIFO)
    D_FIFO_PORT	in_fifo;
    D_FIFO_PORT	out_fifo;
#endif	// USE_FIFO
    UARTPin		uart;
#if !defined(NO_SPI)
    SPIPin		spi;
#endif
    MEMCTLPin	mpin;
};

CPU cpu1;// = { 0 };

#if 1   /// 9/7/20
ST_UINT32 *cpu_cycle = &cpu1.cycle;
#endif


#if defined(CPU_ONLY)
AXI4L::BUS<NUM_MASTERS, NUM_SLAVES> axi_bus;
#include "AXIMasterCHSim.h"
AXIMasterCHSim m_ch[2];
#else
UART_AXI4L axi_uart;
SPIM_AXI4L axi_spim;
MEMCTL_AXI4L	axi_memctl;
AXI4L::CTRL<NUM_MASTERS, NUM_SLAVES> axi_bus_ctrl;
AXI4L::BUS<NUM_MASTERS, NUM_SLAVES> axi_bus;

#if 0
DI_INVALID  = 0xffff,
DI_UART     = 0x0000,
DI_EXT_MEM  = 0x0001,
DI_SPI      = 0x0002,
#endif

_C2R_MODULE_
int RVProcAXI(IO_PINS *io_pins, AXI4L::BUS<NUM_MASTERS, NUM_SLAVES> *axi_bus) {
	PROFILER_UPDATE
	axi_uart.step(&axi_bus->s_ch[0], &io_pins->uart);
#if 1   /// 8/3/20
    axi_memctl.step(&axi_bus->s_ch[1], &io_pins->mpin);
#if !defined(NO_SPI)
    axi_spim.step(&axi_bus->s_ch[2], &io_pins->spi);
#endif
#else
#if defined(NO_SPI)
    axi_memctl.step(&axi_bus->s_ch[1], &io_pins->mpin);
#else
    axi_spim.step(&axi_bus->s_ch[1], &io_pins->spi);
	axi_memctl.step(&axi_bus->s_ch[2], &io_pins->mpin);
#endif
#endif
#if defined(USE_FIFO) && !defined(__LLVM_C2RTL__)
	fifo_ext_input(&io_pins->in_fifo, &io_pins->out_fifo);
#endif
#if defined(USE_FIFO)
    int val = cpu1.step(&io_pins->in_fifo, &io_pins->out_fifo, &axi_bus->m_ch[0], &axi_bus->m_ch[1], axi_bus->s_ch[0].intr >> 2);
#else
	int val = cpu1.step(&axi_bus->m_ch[0], &axi_bus->m_ch[1], axi_bus->s_ch[0].intr >> 2);
#endif
#if defined(USE_FIFO) && !defined(__LLVM_C2RTL__)
	fifo_ext_output(&io_pins->in_fifo, &io_pins->out_fifo);
#endif
	axi_bus_ctrl.connectChannel(axi_bus);
#if !defined(__LLVM_C2RTL__) && !defined(NO_SPI)
	spi_ext_slave(&io_pins->spi);     /// SW model
#endif
#if !defined(SOFT_UART_X) || !defined(__LLVM_C2RTL__)
	io_pins->uart.rx = uart_ext(io_pins->uart.tx);
#endif
	return val;
}
#endif

//#endif

/*****************
simulation sequence:

(1)	set_fifo_ports(&cpu.io.fifo, in_fifo, out_fifo);

(2)	jpg_fsm(&cpu.jpg, cpu.dc_reg.op, !cpu.ex_stt.pctl.stalled_flag, src0bp, src1bp, &cpu.io.fifo);
(3)	io_fsm(!cpu.ex_stt.pctl.stalled_flag, src0bp, src1bp, &cpu.dlut);

(4)	fifo_ext(in_fifo, out_fifo);

jpg.in_pix_req@(2:dlut) -> io.fifo.in.ready_out@(3:io.fifo)
-> (+1 clk)
in_fifo->ready@(1:set_fifo_ports) -> x_fifo.in.ready@(4:fifo_ext)

*****************/

//#include "../utils/TCTUtil.h"
//#define FORCE_TEST
#if !defined(__LLVM_C2RTL__) && defined(FORCE_TEST)
#define RUN_C2R_TEST
#endif

#if defined(RUN_C2R_TEST)
#define	C2R_AUTO_OPENCLOSE
#define C2R_DISABLE_SYS_VECTOR_OUTPUT
#if defined(CPU_ONLY)
//#undef C2R_DISABLE_VECTOR_OUTPUT
#include "C2R_ROOT/RTL_C/CPU_step.RTL.c"
#else
#include "C2R_ROOT/RTL_C/RVProcAXI.RTL.SYS.h"
#endif
#endif

void pause() {
    printf("hit return : ");
#if !defined(RUN_C2R_TEST) || defined(FORCE_TEST)
    getchar();
#endif
}

/// 7/1/20
#define INSTALL_PROGRAM(dm, pm, dmsz, pmsz, pc, sp, mdf, pc_ofs, skip) \
    C2R::ProcUtil::install_program_elf(dm, pm, dmsz, pmsz, pc, sp, mdf, pc_ofs, skip)
#define INSTALL_SYMBOLS(mdf, skip) \
	C2R::ProcUtil::install_program_elf(NULL, NULL, 0, 0, NULL, NULL, mdf, 0, skip)

#include "RVProcUtil.h"

AXI4L::VirtualBUS *AXI4L::VirtualBUS::virtualBus = 0;

void setElfFileName(int pid, char *elfFile);

ExtMem	xmem;

#if defined(NO_SPI)
#define G_UART  G_uart_ext_4
#else
#define G_UART  G_uart_ext_5
#endif

#if defined(RUN_C2R_TEST)
void G_uart_ext_5_flush_rx()
{
#if !defined(CPU_ONLY)
    G_UART.x_uart.rx_buf[G_UART.x_uart.rx_pnt] = 0;
    printf("[%10d] <UART_X.RX> msg = %s<<<<flushed...\n", G_UART.x_uart.cycle, G_UART.x_uart.rx_buf);
    G_UART.x_uart.rx_pnt = 0;
#endif
}
#endif


#ifdef	__linux__
/// key input
void kb_handler(int signum)
{
	char ch;

	if (read(0, &ch, 1) == 1)
#if defined(CPU_ONLY)
		m_ch[0].KeyIn(ch);
#else
		uart_ext_tx(ch);
#endif
}

#if defined(RUN_C2R_TEST)
void kb_handler_RTL(int signum)
{
	char ch;

	if (read(0, &ch, 1) == 1) {
#if defined(CPU_ONLY)
		m_ch[0].KeyIn(ch);
#elif !defined(SOFT_UART_X)
		G_UART.x_uart.tx_pnt = 1;
		G_UART.x_uart.trig_pnt = ch;
#endif
	}
}
#endif

typedef	struct termios	KB_HANDLE;

int check_kb(KB_HANDLE *h) {
	return (tcgetattr(0, h) >= 0);
}

void start_kb(KB_HANDLE *t, void (*handler)(int))
{
	struct termios raw;

	raw = *t;
	raw.c_oflag = 0;
	raw.c_lflag &= ~(ECHO|ICANON);
	raw.c_cc[VMIN] = 0;
	raw.c_cc[VTIME] = 0;
	tcsetattr(0, TCSAFLUSH, &raw);

	setvbuf(stdout, NULL, _IONBF, BUFSIZ);

	signal(SIGIO, handler);
	int on = 1;
	ioctl(0, FIOASYNC, &on);
	fcntl(0, F_SETOWN, getpid());
}

void stop_kb(struct termios *t)
{
	setlinebuf(stdout);
	tcsetattr(0, TCSAFLUSH, t);

	fcntl(0, F_SETOWN, 0);
	int off = 0;
	ioctl(0, FIOASYNC, &off);
	signal(SIGIO, SIG_DFL);
}
#else
DWORD WINAPI kb_handler(LPVOID param)
{
	for (;;) {
		char ch;

		ch = _getch();
#if defined(CPU_ONLY)
		m_ch[0].KeyIn(ch);
#else
		uart_ext_tx(ch);
#endif
	}
	return 0;
}


#if defined(RUN_C2R_TEST)
/// 8/3/20
////#if defined(NO_SPI)
////#define G_UART  G_uart_ext_4
////#else
////#define G_UART  G_uart_ext_5
////#endif
DWORD WINAPI kb_handler_RTL(LPVOID param)
{
    for (;;) {
        char ch;

        ch = _getch();
#if defined(CPU_ONLY)
	m_ch[0].KeyIn(ch);
#elif !defined(SOFT_UART_X)
        G_UART.x_uart.tx_pnt = 1;
        G_UART.x_uart.trig_pnt = ch;
#endif
    }
    return 0;
}

////void G_uart_ext_5_flush_rx()
////{
////#if !defined(CPU_ONLY)
////    G_UART.x_uart.rx_buf[G_UART.x_uart.rx_pnt] = 0;
////    printf("[%10d] <UART_X.RX> msg = %s<<<<flushed...\n", G_UART.x_uart.cycle, G_UART.x_uart.rx_buf);
////    G_UART.x_uart.rx_pnt = 0;
////#endif
////}
#endif


typedef	HANDLE KB_HANDLE;

int check_kb(KB_HANDLE *h) {
	return 1;
}

void start_kb(KB_HANDLE *h, DWORD (WINAPI *handler)(LPVOID))
{
	*h = CreateThread(NULL, 0, handler, NULL, 0, NULL);
}

void stop_kb(KB_HANDLE *h)
{
	TerminateThread(*h, 0);
	WaitForSingleObject(h, INFINITE);
}
#endif

int main(int argc, char * argv[])
{
	int run = 1, cnt = 0;
	int fifo_error = 0;
    	int intr;

#if 0       /// 8/9/20
    char *elfFile0 = (char *)"../PROC_TESTS/testRISC_V/obj_files/test.rv.uart.short.O2.out";
#elif 1   /// 7/26/20
    char *elfFile0 = (char *)"../PROC_TESTS/testRISC_V/obj_files/test.rv.uart.O2.out";
#else
	char *elfFile0 = (char *)"../../TEST/testRVMPSoC/test.rv.mmu.out";
#endif
	char *elfFile1 = NULL;

#if defined(WRITE_FILE)
    char* filename = getenv("TEST_OUTPUT_FILEPATH");
    m_ch[0].fptr = fopen(filename, "w");
#endif

	bool skip = 0;
	bool enable_key_input = 0;
	bool no_uart_msg = 0;
	const char *dtb = "dtb/c2rtl.dtb";
	const char *disk_image = "disk_image/disk.img";

	int arg_idx = 0;
	int cur_opt = 0;

	while (--argc) {
		char *cp = *++argv;

		switch (cur_opt) {
		case 't':
			dtb = cp;
			cur_opt = 0;
			break;
		case 'd':
			disk_image = cp;
			cur_opt = 0;
			break;
		default:
			if (cp[0] == '-') {
				char ch;

				while ((ch = *++cp)) {
					switch (ch) {
					case 's':
						skip = 1;
						break;
					case 'i':
						enable_key_input = 1;
						break;
					case 'm':
						no_uart_msg = 1;
						break;
					case 't':
						if (*++cp) {
							dtb = cp;
						} else {
							cur_opt = ch;
						}
						goto contin;
					case 'd':
						if (*++cp) {
							disk_image = cp;
						} else {
							cur_opt = ch;
						}
						goto contin;
					default:
						fprintf(stderr, "Unknown option %c\n", ch);
						break;
					}
				}
			} else {
				switch (arg_idx++) {
				case 0:
					elfFile0 = cp;
					break;
				case 1:
					elfFile1 = cp;
					break;
				}
			}
			break;
		}
contin:
		;
	}

#if !defined(DISABLE_VDISK)
    extern void vdisk_init(const char *disk_image);
	vdisk_init(disk_image);
#endif

	int istty = 0;
	KB_HANDLE kb_h;

	if (enable_key_input)
		istty = check_kb(&kb_h);

    IO_PINS	io_pins = { 0 };

#if defined(RUN_C2R_TEST)
#if defined(CPU_ONLY)
    ST_CPU_step *G0 = &G_CPU_step;
#else

#if defined(NO_SPI)
    ST_CPU_step *G0 = &G_CPU_step_2;
#else
	ST_CPU_step *G0 = &G_CPU_step_3;
#endif
#endif	// defined(CPU_ONLY)
#endif	// defined(RUN_C2R_TEST)

    /// 7/1/20
    C2R::Timer tm0 = { "RVProc" }, tm1 = { "RVProcRTL" };

#if !defined __LLVM_C2RTL__
	cpu1.enableProfile = 1;
#endif

	setElfFileName(0, elfFile0);
    /// 7/1/20
    xmem.load_program(C2R::ProcUtil::elfFileName, &cpu1.fe.r.pc, &cpu1.gpr[RVR_SP], -4);
	xmem.load_dtb(dtb, &cpu1.gpr[11]);

	INSTALL_SYMBOLS(1, 0);
//#if defined(MERGE_PRV_INST_IN_ICACHE)
    cpu1.icache.prev_inst = RV_NOP_INST;
	cpu1.wb.r.mode = 3;
#if !defined(CPU_ONLY)
	uart_ext_set_mode(no_uart_msg);
#endif
#if defined(USE_FIFO)
	fifo_ext_init(&io_pins.in_fifo, &io_pins.out_fifo, &cpu1.cycle);
#endif
#if defined(ENABLE_TLB_STAT)
    TLB_STAT_CPU tlb_stat[2];
#endif
#if	ENABLE_CACHE_STAT
CACHE_STAT_SAVED<2> ic_stat, dc_stat;
#endif

    if (skip) {
		printf("Skip C mode\n");
		goto run_rtl_c;
	}

    /// 7/1/20
    tm0.Set();


	if (istty) {
		start_kb(&kb_h, kb_handler);
	}
#if ENABLE_CACHE_STAT
	ic_stat.reset();
	dc_stat.reset();
#endif
#if defined(CPU_ONLY)
    m_ch[0].Reset(no_uart_msg); m_ch[1].Reset(0);
    intr = 0;
    while (cpu1.step(
#if defined(USE_FIFO)
        &io_pins.in_fifo, &io_pins.out_fifo,
#endif
        &axi_bus.m_ch[0], &axi_bus.m_ch[1], intr) == 0) {
        PROFILER_UPDATE
        tm0.cycles++;

#if defined(USE_FIFO)
        io_pins.in_fifo.ready = 1;
        io_pins.out_fifo.ready = 1;
#endif

        m_ch[0].Read(axi_bus.m_ch[0], xmem);
        intr = m_ch[0].Write(axi_bus.m_ch[0], xmem, tm0.cycles);
        m_ch[1].Read(axi_bus.m_ch[1], xmem);
#if defined(ENABLE_TLB_STAT)
        if (cpu1.wb.mode == 0 && tlb_stat[0].itlb.ld_count == 0) {
            tlb_stat[0].cycle = cpu1.cycle;
            tlb_stat[0].itlb = cpu1.icache.tlb.stat;
            tlb_stat[0].dtlb = cpu1.io.dcache.tlb.stat;
        }
#endif
        /// 369863172
#if 0
        if (tm0.cycles == 369863172) {
            printf("csr.mcycle = %lld, csr.minstret = %lld\n",
                ((long long)cpu1.csr.mcycle) | (((long long)cpu1.csr.mcycleh) << 32),
                ((long long)cpu1.csr.minstret) | (((long long)cpu1.csr.minstreth) << 32));
        }
#endif
#if 0
        if (tm0.cycles >= 16785800) { /// 16786056
            printf("[%8ld] pc = %08x, halted = %d\n", tm0.cycles, cpu1.fe.r.pc, cpu1.halted);
        }
#endif
#if	ENABLE_CACHE_STAT
	if (cpu1.wb.r.mode == 0) {
		ic_stat.save(0, cpu1.csr.mcycle,
				cpu1.icache.stat.rw_count,
				cpu1.icache.stat.miss_count);
		dc_stat.save(0, cpu1.csr.mcycle,
				cpu1.io.dcache.stat.rw_count,
				cpu1.io.dcache.stat.miss_count);
	}
#endif
    }

#else
    while (RVProcAXI(&io_pins, &axi_bus) == 0)
	{ //run = 1 - run;
		tm0.cycles++;
		xmem.update(&io_pins.mpin);
#if defined(ENABLE_TLB_STAT)
        if (cpu1.wb.mode == 0 && tlb_stat[0].itlb.ld_count == 0) {
            tlb_stat[0].cycle = cpu1.cycle;
            tlb_stat[0].itlb = cpu1.icache.tlb.stat;
            tlb_stat[0].dtlb = cpu1.io.dcache.tlb.stat;
        }
#endif
    }	///	run (1->0) transition every 2 cycles 
#endif
	spi_slave_ext_flush_rx(1/*force_flush*/);
	uart_ext_flush_rx();
	if (istty) {
		stop_kb(&kb_h);
	}
#if defined(USE_TAG_REG) && !defined(__LLVM_C2RTL__)    /// assume NO_MMU
    printf("tag_reg_hit = %d, tag_reg_miss = %d, tag_reg_total = %d\n",
        cpu1.icache.prv_vtag_reg_hit, cpu1.icache.prv_vtag_reg_miss, 
        cpu1.icache.prv_vtag_reg_hit + cpu1.icache.prv_vtag_reg_miss);
#endif
    printf("csr.mcycle = %lld, csr.minstret = %lld\n",
        ((long long)cpu1.csr.mcycle) | (((long long)cpu1.csr.mcycleh) << 32),
        ((long long)cpu1.csr.minstret) | (((long long)cpu1.csr.minstreth) << 32));
#if	ENABLE_CACHE_STAT
	ic_stat.save(1, cpu1.csr.mcycle,
			cpu1.icache.stat.rw_count,
			cpu1.icache.stat.miss_count);
	dc_stat.save(1, cpu1.csr.mcycle,
			cpu1.io.dcache.stat.rw_count,
			cpu1.io.dcache.stat.miss_count);

	printf("\n");
	ic_stat.print(1, "Total Cache stats", ic_stat, dc_stat);
	ic_stat.print(0, "Boot Cache stats", ic_stat, dc_stat);
	ic_stat.print_delta(0, 1, "EMBench Cache stats", ic_stat, dc_stat);
#endif
    /// 7/1/20
    tm0.Set();
	fifo_error += fifo_ext_print_result();
#if !defined __LLVM_C2RTL__
	printf("hazards = %d (dc : %d, io : %d, wait : %d, jpg : %d, overlap : %d)\n",
		cpu1.hazard.dc + cpu1.hazard.io + cpu1.hazard.wait + cpu1.hazard.jpg - cpu1.hazard.overlap,
		cpu1.hazard.dc, cpu1.hazard.io, cpu1.hazard.wait, cpu1.hazard.jpg, cpu1.hazard.overlap);
    /// 7/1/20
    fprintf(C2R::ProcUtil::fp_prof, "hazards = %d (dc : %d, io : %d, wait : %d, jpg : %d, overlap : %d)\n",
        cpu1.hazard.dc + cpu1.hazard.io + cpu1.hazard.wait + cpu1.hazard.jpg - cpu1.hazard.overlap,
        cpu1.hazard.dc, cpu1.hazard.io, cpu1.hazard.wait, cpu1.hazard.jpg, cpu1.hazard.overlap);
#endif

#if !defined(__LLVM_C2RTL__) && defined(USE_RISC_V) && defined(ENABLE_CPU_STAT)
	printf("---cpu stat---\n");
	cpu1.show_cpu_stat();
#endif
#if defined(ENABLE_TLB_STAT)
    tlb_stat[1].cycle = cpu1.cycle;
    tlb_stat[1].itlb = cpu1.icache.tlb.stat;
    tlb_stat[1].dtlb = cpu1.io.dcache.tlb.stat;
    tlb_stat[1].printInfo(tlb_stat[0]);
#endif

#if defined(WRITE_FILE)
    fclose(m_ch[0].fptr);
#endif

	PROFILER_PRINT
	// pause();
run_rtl_c:
#if defined(RUN_C2R_TEST)
#if !defined __LLVM_C2RTL__
	rv_insn::loopInfo.reset();
#endif
#if defined(SOFT_UART_X)
    uart_ext_switch_mode();
#else
#if 1   /// 8/3/20
#if !defined(CPU_ONLY)
    G_UART.x_uart.mode = no_uart_msg;
#endif
#else
    G_uart_ext_5.x_uart.mode = no_uart_msg;
#endif
#endif
#if defined(USE_FIFO)
	fifo_ext_switch_mode(&io_pins.in_fifo, &io_pins.out_fifo);
#endif
    spi_slave_ext_switch_mode();
	setElfFileName(0, elfFile0);
    xmem.load_program(C2R::ProcUtil::elfFileName, &G0->_this.fe.r.pc, &G0->_this.gpr[RVR_SP], 0);
	xmem.load_dtb(dtb, &G0->_this.gpr[11]);

	INSTALL_SYMBOLS(0, 1);
//#if defined(MERGE_PRV_INST_IN_ICACHE)
    G0->_this.icache.prev_inst = RV_NOP_INST;
#if !defined(NO_MMU)
    G0->_this.wb.r.mode = 3;
#endif
	io_pins.uart.rx = 1;// uart[1].rx = 1;
#if !defined(NO_SPI)
    io_pins.spi.miso = 1;
#endif
	run = 1;
	xmem.cycle = 0;
    tm1.Set();

	if (istty) {
		start_kb(&kb_h, kb_handler_RTL);
	}
#if defined(TLB_STAT_CPU)
    tlb_stat[0].reset(); tlb_stat[1].reset();
#endif

#if ENABLE_CACHE_STAT
	ic_stat.reset();
	dc_stat.reset();
#endif

#if defined(CPU_ONLY)
    m_ch[0].Reset(no_uart_msg); m_ch[1].Reset(0);
    intr = 0;
    while (CPU_step_RTL(
#if defined(USE_FIFO)
        &io_pins.in_fifo, &io_pins.out_fifo,
#endif
        &axi_bus.m_ch[0], &axi_bus.m_ch[1], intr) == 0) {
        PROFILER_UPDATE
            tm1.cycles++;

#if defined(USE_FIFO)
        io_pins.in_fifo.ready = 1;
        io_pins.out_fifo.ready = 1;
#endif
        m_ch[0].Read_RTL(axi_bus.m_ch[0], xmem);
        intr = m_ch[0].Write_RTL(axi_bus.m_ch[0], xmem, tm1.cycles);
        m_ch[1].Read_RTL(axi_bus.m_ch[1], xmem);
#if defined(ENABLE_TLB_STAT)
        if (G0->_this.wb.r.mode == 0 && tlb_stat[0].itlb.ld_count == 0) {
            tlb_stat[0].cycle = G0->_this.cycle;
            auto &itlb = G0->_this.icache.tlb.stat;
            tlb_stat[0].itlb.miss_count = itlb.miss_count;
            tlb_stat[0].itlb.ld_count = itlb.ld_count;
            auto &dtlb = G0->_this.io.dcache.tlb.stat;
            tlb_stat[0].dtlb.miss_count = dtlb.miss_count;
            tlb_stat[0].dtlb.ld_count = dtlb.ld_count;
            tlb_stat[0].dtlb.st_count = dtlb.st_count;
        }
#endif

#if 0
        if (tm1.cycles >= 16787600) {
            printf("[%8ld] pc = %08x, halted = %d\n", tm1.cycles, G0->_this.fe.r.pc, G0->_this.halted);
        }
#endif
#if	ENABLE_CACHE_STAT
	if (G0->_this.wb.r.mode == 0) {
#if 1   /// 12/1/20 : change in C2RTL --> "base0" is now omitted...
        ic_stat.save(0, G0->_gw._this_csr_mcycle,
            G0->_this.icache.stat.rw_count,
            G0->_this.icache.stat.miss_count);
        dc_stat.save(0, G0->_gw._this_csr_mcycle,
            G0->_this.io.dcache.stat.rw_count,
            G0->_this.io.dcache.stat.miss_count);
#else
		ic_stat.save(0, G0->_gw._this_csr_mcycle,
				G0->_this.icache.base0.stat.rw_count,
				G0->_this.icache.base0.stat.miss_count);
		dc_stat.save(0, G0->_gw._this_csr_mcycle,
				G0->_this.io.dcache.base0.stat.rw_count,
				G0->_this.io.dcache.base0.stat.miss_count);
#endif
	}
#endif
    }
#else
    while (RVProcAXI_RTL(&io_pins, &axi_bus) == 0)
	{
#if defined(USE_FIFO)
		fifo_ext_input(&io_pins.in_fifo, &io_pins.out_fifo);
		fifo_ext_output(&io_pins.in_fifo, &io_pins.out_fifo);
#endif
#if defined(SOFT_UART_X)
        io_pins.uart.rx = uart_ext(io_pins.uart.tx);
#endif
#if !defined(NO_SPI)
        spi_ext_slave(&io_pins.spi);     /// SW model
#endif
        xmem.update(&io_pins.mpin);
		tm1.cycles++;
#if defined(ENABLE_TLB_STAT)
        if (G0->_this.wb.r.mode == 0 && tlb_stat[0].itlb.ld_count == 0) {
            tlb_stat[0].cycle = G0->_this.cycle;
            auto &itlb = G0->_this.icache.tlb.stat;
            tlb_stat[0].itlb.miss_count = itlb.miss_count;
            tlb_stat[0].itlb.ld_count = itlb.ld_count;
            auto &dtlb = G0->_this.io.dcache.tlb.stat;
            tlb_stat[0].dtlb.miss_count = dtlb.miss_count;
            tlb_stat[0].dtlb.ld_count = dtlb.ld_count;
            tlb_stat[0].dtlb.st_count = dtlb.st_count;
        }
#endif
    }	///	run (1->0) transition every 4 cycles
#endif
	spi_slave_ext_flush_rx(1/*force_flush*/);
#if defined(SOFT_UART_X)
    uart_ext_flush_rx();
#else
    G_uart_ext_5_flush_rx();
#endif
	if (istty) {
		stop_kb(&kb_h);
	}
#if	ENABLE_CACHE_STAT
#if 1   /// 12/1/20 : change in C2RTL --> "base0" is now omitted...
    ic_stat.save(1, G0->_gw._this_csr_mcycle,
        G0->_this.icache.stat.rw_count,
        G0->_this.icache.stat.miss_count);
    dc_stat.save(1, G0->_gw._this_csr_mcycle,
        G0->_this.io.dcache.stat.rw_count,
        G0->_this.io.dcache.stat.miss_count);
#else
    ic_stat.save(1, G0->_gw._this_csr_mcycle,
			G0->_this.icache.base0.stat.rw_count,
			G0->_this.icache.base0.stat.miss_count);
	dc_stat.save(1, G0->_gw._this_csr_mcycle,
			G0->_this.io.dcache.base0.stat.rw_count,
			G0->_this.io.dcache.base0.stat.miss_count);
#endif
	ic_stat.print(1, "Total Cache stats", ic_stat, dc_stat);
	ic_stat.print(0, "Boot Cache stats", ic_stat, dc_stat);
	ic_stat.print_delta(0, 1, "EMBench Cache stats", ic_stat, dc_stat);
#endif
    tm1.Set();
	fifo_error += fifo_ext_print_result();

#if defined(ENABLE_TLB_STAT)
    {
        tlb_stat[1].cycle = G0->_this.cycle;
        auto &itlb = G0->_this.icache.tlb.stat;
        tlb_stat[1].itlb.miss_count = itlb.miss_count;
        tlb_stat[1].itlb.ld_count = itlb.ld_count;
        auto &dtlb = G0->_this.io.dcache.tlb.stat;
        tlb_stat[1].dtlb.miss_count = dtlb.miss_count;
        tlb_stat[1].dtlb.ld_count = dtlb.ld_count;
        tlb_stat[1].dtlb.st_count = dtlb.st_count;
        tlb_stat[1].printInfo(tlb_stat[0]);
    }
#endif
	pause();
#endif
	return fifo_error;
}

#if !defined(__LLVM_C2RTL__)
	// debug func
BIT CPU::read_pmem(UINT32 addr)
{
	extern ExtMem xmem;
	ExtMem::page *page = xmem.get_page(addr);

	read_data = page->m[(addr & 0xfff) / sizeof(page->m[0])];
	return 1;
}

BIT CPU::read_cmem(UINT32 addr)
{
	return  io.dcache.read_data(addr, &read_data);
}

BIT CPU::read_vmem(UINT32 addr)
{
	// simple version. for kernel space only
	return read_cmem(addr - 0x3fc00000);
}

#endif
////#endif
