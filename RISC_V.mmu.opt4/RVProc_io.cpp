
#include "RVProc.h"

//#define DBG

int nxt_pnt(int cur_pnt){ return (cur_pnt == MAX_IO_BUF_SIZE - 1) ? 0 : cur_pnt + 1;}

#define BUF_EMPTY(buf)	(!(buf)->not_empty)
#define BUF_FULL(buf)	((buf)->full)

void IOBuf::update(int rflag, int wflag) {
	if (rflag || wflag) {
		int nrp = (rflag) ? nxt_pnt(rp) : rp;
		int nwp = (wflag) ? nxt_pnt(wp) : wp;
		not_empty = (wflag) ? 1 : (nrp != nwp);
		full = (rflag) ? 0 : (nrp == nwp);
		rp = nrp;
		wp = nwp;
	}
}


#if defined(USE_FIFO)

///	FIFO handshake protocol:
///	Master	= IP;
///	Slave	= FIFO;
///	FIFO output: src(IP) --> dst(FIFO)
///	src.strobe : 1 cycle pulse
///	dst.ready : 0 (full), 1 (non_full)
///	if(dst.ready && src.strobe){	[dst(not_full) AND src(write_data)]
///		dst(FIFO) consumes src.data;
///		dst.ready = !dst.full; [1 cycle after]
///	}
///	FIFO input: src(FIFO) --> dst(IP)
///	src.strobe = src.not_empty; 
///	dst.ready : 0 (busy), 1 (read_data)
///	if(dst.ready && src.strobe){	[dst(read_data) AND src(not_empty)]
///		dst(IP) latches src.data;
///		src(FIFO) updates FIFO status;
///		dst.ready = (dst can consume next data); [1 cycle after]
///	}

void FIFO_TX::do_tx(FIFO_PORT * dev_fifo_port, UINT2 io_op, BIT chanID, UINT32 src0)
{
	//FIFO_TX * tx = &fifo->tx;
	int wflag = (io_op == IO_WriteData || dev_fifo_port->strobe) && port.ready;	///	ready
																					///	3/22/15	///	busy OR IO_WriteData operation conflicts with dlut.out_pix
	stalled = (io_op == IO_WriteData) && (!port.ready || dev_fifo_port->strobe);
	if (wflag) {
		strobe_out = 1;
		data_out = (dev_fifo_port->strobe) ? dev_fifo_port->data : src0;
		chanID_out = (dev_fifo_port->strobe) ? 0 : chanID;	///	chanID = 0 for dlut.out_pix 
	}	/// strobe == 1 : data_valid
	else if (strobe_out) { strobe_out = 0; }									///	strobe : one clk duration per data
}

UINT32 FIFO_RX::do_rx(FIFO_PORT * dev_fifo_port, UINT2 io_op)
{
	//FIFO_RX * rx = &fifo->rx;
	int rflag = (io_op == IO_ReadData || dev_fifo_port->ready) && port.strobe;	///	strobe == 1 : data_valid
	stalled = (io_op == IO_ReadData) && !port.strobe;						///	strobe == 0 : no-data
	ready_out = rflag;
	return port.data;
}

void FIFO::write_command(UINT15 io_param)
{
	int activate = (io_param) & 0x1;
	int chanID = (io_param >> 1) & 0x1;	///	0: raw-data, 1: char-data
	activated = activate;
	rx.ready_out = 0;
	rx.bytes_out = (io_param >> 2) & 0xf;	///	0 to 15 --> 1 byte to 16 bytes
	tx.strobe_out = 0;	///	0: no-data
	tx.chanID_out = chanID;
	tx.chanID_out_default = chanID;
}

UINT32 FIFO::read_status()
{
	return ((tx.strobe_out << 2) | (tx.chanID_out << 1) | (rx.ready_out));
}

void FIFO::fsm(IO * io, FIFO_PORT * dev_fifo_port, BIT cancel_io_insn, UINT3 io_op_in, UINT15 io_param, UINT32 src0)
{
	UINT32 status = 0, rx_data = 0;
	UINT3 io_op = (cancel_io_insn) ? IO_Idle : (io_op_in | pending_op);
	stalled = 0;
	if (io_op == IO_WriteCommand) { write_command(src0); }
	else {
		if (io_op == IO_ReadStatus) { status = read_status(); }
		if (activated) {
			tx.do_tx(dev_fifo_port, io_op, io_param & 0x1, src0);
			rx_data = rx.do_rx(dev_fifo_port, io_op);
			stalled = tx.stalled | rx.stalled;
		}
#if 1	///	8/23/15
		else { rx.ready_out = 0; }
#endif
	}
	if (stalled) {
		pending_op = io_op;
		data_latched = 0;
	}
	else {
		pending_op = IO_Idle;
		data_in = status | rx_data;
		data_latched = (io_op == IO_ReadStatus) | (io_op == IO_ReadData);
	}
	active = (io_op != IO_Idle);
	rx.port.ready = rx.ready_out;
	rx.port.bytes = rx.bytes_out;
	tx.port.strobe = tx.strobe_out;
	tx.port.chanID = tx.chanID_out;
	tx.port.data = tx.data_out;
	io->stalled |= stalled;
	io->data_in |= data_in;
	io->data_latched |= data_latched;
	io->active |= active;
}
#endif

void SYSCTRL::fsm(UINT3 io_op, UINT12 addr, UINT32 data, UINT16 intr) {
	UINT32	n_ext_irq_pending = ext_irq_pending | intr;
	UINT32	n_ext_irq_enable = ext_irq_enable;

	data_in = 0;
	active = 0;
	stalled = 0;
	data_latched = 0;

	irq_set = intr;
	irq_clr = 0;

	mtip = 0;

	if (mtime_h > mtimecmp_h) {
		mtip = 1;
	} else if (mtime_h == mtimecmp_h) {
		if (mtime_l >= mtimecmp_l)
			mtip = 1;
	}

	seip = ((n_ext_irq_pending & n_ext_irq_enable) != 0);

	if (io_op == IO_ReadData) {
		active = 1;

		switch (addr) {
		// read
		case 0x0000:
			data_in = n_ext_irq_pending & n_ext_irq_enable;
			break;
		case 0x0004:
			data_in = n_ext_irq_enable;
			break;
		case 0x0010:
			data_in = mtime_l;
			break;
		case 0x0014:
			data_in = mtime_h;
			break;
		case 0x0018:
			data_in = mtimecmp_l;
			break;
		case 0x001c:
			data_in = mtimecmp_h;
			break;
		}
	} else if (io_op == IO_WriteData) {
		active = 1;
		data_latched = 1;

		switch (addr) {
		// write
		case 0x0000:
			n_ext_irq_pending &= ~data;	// clear bits
			break;
		case 0x0004:
			n_ext_irq_enable = data;
			break;
		case 0x0018:
			mtimecmp_l = data;
			break;
		case 0x001c:
			mtimecmp_h = data;
			break;
		}
	}

	ext_irq_pending = n_ext_irq_pending;
	ext_irq_enable = n_ext_irq_enable;

	if (divider == 0)
		incr_timer();
	divider = (divider + 1) & 0xf;
}

#if 0
#define		IO_DEV_ADDR_MASK	0xFFFFFFF0
#define		DEV_ADDR_MASK		0xF0000000
#define		UART_MASK			0x10000000
#define		FIFO0_MASK			0x10000020
#define		FIFO1_MASK			0x10000030

#define		SPI_MASK			0x10000060 // Mustafa 7/11/2016
#define		SYSCTL_MASK			0x10001000
#endif

BIT IO::decode_addr(UINT6 op, UINT32 addr, UINT2 *io_id, UINT3 *io_op, UINT12 *io_param)
{
	if (op == RVO_ld || op == RVO_st) {
		int id_io, op_io, p = 0;
		///	id_io : IO_DEV_TYPE	{ AXI, FIFO, SYSCTL }
		///	op_io : IO_OP_TYPE	{ IO_ReadData(4), IO_WriteData(5), IO_ReadStatus(6), IO_WriteCommand(7)}
		int is_write = (op == RVO_st);
		///	9/19/17
#if defined(SIMPLIFY_IO_DECODE)
        BIT devFlag = ((addr >> 28) == 0x1);
        UINT32 devAddr = ((addr >> 4) & 0xfff);
        id_io = IO_AXI;
        if (devFlag) {
            switch (devAddr) {
#if defined(USE_FIFO)
            case 0x002: /// FIFO0_MASK
            case 0x003: /// FIFO1_MASK
                id_io = IO_FIFO;
                op_io = ((addr >> 1) & 0x2) | (is_write);
                p = devAddr & 0x1;
                break;
#endif
            case 0x100: /// SYSCTL_MASK
            case 0x101: /// SYSCTL_MASK + 0x10
                id_io = IO_SYSCTRL;
                op_io = is_write;
                p = addr & 0xfff;
                break;
            }
        }
        if (id_io == IO_AXI) {
            op_io = is_write;
            p = 0;
        }

#else
		switch (addr & IO_DEV_ADDR_MASK) {
		case FIFO0_MASK:
		case FIFO1_MASK:
			id_io = IO_FIFO;
			op_io = ((addr >> 1) & 0x2) | (is_write);
			p = (addr >> 4) & 0x1;
			break;
		case SYSCTL_MASK:
		case SYSCTL_MASK + 0x10:
			id_io = IO_SYSCTRL;
			op_io = is_write;
			p = addr & 0xfff;
			break;
		default:
			id_io = IO_AXI;
			op_io = is_write;
			p = 0;
			break;
		}
#endif
		*io_id = id_io;
		*io_op = op_io | 0x4;
		*io_param = p;
		return 1;
	} if (op == RVO_fence) {
		*io_id = IO_AXI;
		*io_op = IO_WriteCommand;
		*io_param = 0;
		return 1;
	} else {
		*io_id = 0;
		*io_op = IO_Idle;
		*io_param = 0;
		return 0;
	}
}

//#define USE_DTLB_STATE

/// 7/23/20
#if defined(MODIFY_DTLB_FLUSH)
BIT IO::fsm(UINT6 op, UINT32 src0, UINT32 add_out, UINT3 funct3, FIFO_PORT * dev_fifo_port, AXI4L::CH *axi,
    UINT16 intr, BIT mmu_en, UINT32 pg_dir, BIT src_f)
#else
BIT IO::fsm(UINT6 op, UINT32 src0, UINT32 add_out, UINT3 funct3, FIFO_PORT * dev_fifo_port, AXI4L::CH *axi,
    UINT16 intr, BIT mmu_en, UINT32 pg_dir)
#endif
{
    UINT2	io_id;
    UINT3	io_op;
    UINT12	io_param;
    UINT32	paddr = add_out;

#define USE_TLB_ST_STATE
#if !defined(NO_MMU)
#if !defined(CHECK_TLB_TAG_IN_PREFETCH)
    dcache.tlb.check_tag(mmu_en, add_out >> 12);
#endif
#endif
#if 0//!defined(__LLVM_C2RTL__)
    {
        extern ST_UINT32 *cpu_cycle;
        if (*cpu_cycle >= 390400 && *cpu_cycle <= 390545) {
            printf("[%8d] op(%x), add_out(%08x), DTLB.hit(%d)\n", *cpu_cycle, op, add_out, dcache.tlb.hit);
        }
    }
#endif
#if !defined(NO_MMU)
    if (mmu_en) {
        paddr = dcache.tlb.get_phy_daddr(pg_dir, paddr);   /// 7/24/20
    }
#endif
    BIT isIOop = decode_addr(op, paddr, &io_id, &io_op, &io_param);

    // check pte
#if !defined(NO_MMU)
    if ((io_op == IO_WriteData) && mmu_en && !dcache.tlb.hit)
        io_op = IO_ReadData;
#endif
//#if defined(MERGE_DTLB_FSM_INTO_DCACHE)
#if defined(MODIFY_DTLB_FLUSH)
    dcache.fsm(op, (io_id == IO_AXI) ? io_op : IO_Idle, paddr, add_out, src0, funct3, axi, mmu_en, src_f);
#else
    dcache.fsm(op, (io_id == IO_AXI) ? io_op : IO_Idle, paddr, add_out, src0, funct3, axi, mmu_en);
#endif
#if !defined(NO_MMU)
    if (dcache.tlb.page_fault) {
        io_op = IO_Idle;
    }
#endif

#if defined(NO_MMU)
    if ((io_id == IO_AXI) && (io_op != IO_Idle)) {
        stalled = dcache.busy;
        data_in = dcache.data;
        data_latched = 1;
        active = 1;
    }
    else {
        stalled = 0;
        data_in = 0;
        data_latched = 0;
        active = 0;
    }
#else
    if ((io_op == IO_ReadData || io_op == IO_WriteData) && mmu_en && !dcache.tlb.hit) {
        stalled = 1;
        data_in = 0;
        data_latched = 0;
        active = 1;
    }
    else if ((io_id == IO_AXI) && (io_op != IO_Idle)) {
        stalled = dcache.busy;
        data_in = dcache.data;
        data_latched = 1;
        active = 1;
    }
    else {
        stalled = 0;
        data_in = 0;
        data_latched = 0;
        active = 0;
    }
//#if defined(MERGE_DTLB_FSM_INTO_DCACHE)
#if defined(MODIFY_DTLB_FLUSH)
    if (dcache.tlb.fb_state == TLB_ST_FLUSH || dcache.tlb.fb_state == TLB_ST_FLUSH_ALL) {
#else
    if (dcache.tlb.fb_state == TLB_ST_FLUSH) {
#endif
        stalled |= 1;
        active |= 1;
    }
#endif

#if defined(USE_FIFO)
    fifo.fsm(this, dev_fifo_port, 0/*cancel_io_insn*/, (io_id == IO_FIFO) ? io_op : IO_Idle, io_param, src0);
#endif
    sysctrl.fsm((io_id == IO_SYSCTRL) ? io_op : IO_Idle, io_param, src0, intr);
    data_in |= sysctrl.data_in;
    data_latched |= sysctrl.data_latched;
    active |= sysctrl.active;

    return isIOop;
}

///	10/26/14
UINT32 CPU::io_pipe_control()
{
	if (io.stalled) {
		ex.stall = 1;
		SET_EX_PENDING_OP(1, 1, 0);	///	is_pending = 1, is_interruptible = 1, resume_from_next_insn = 0;
#if !defined __LLVM_C2RTL__
		hazard.io++;
		if (dc.stall) { hazard.overlap++; }
#endif
	}
	return io.data_in;
}

void set_fifo_FW(FIFO_PORT * fifo_src, FIFO_PORT * fifo_dst)
{
	fifo_dst->strobe	= fifo_src->strobe;
	fifo_dst->chanID	= fifo_src->chanID;
	fifo_dst->data		= fifo_src->data;
}
void set_fifo_BW(FIFO_PORT * fifo_src, FIFO_PORT * fifo_dst)
{
	fifo_src->ready		= fifo_dst->ready;
	fifo_src->bytes		= fifo_dst->bytes;
}
void FIFO::set_input_ports(FIFO_PORT * in_fifo, FIFO_PORT * out_fifo)
{
	set_fifo_FW(in_fifo, &rx.port);
	set_fifo_BW(&tx.port, out_fifo);
}
void FIFO::set_output_ports(FIFO_PORT * in_fifo, FIFO_PORT * out_fifo)
{
	set_fifo_BW(in_fifo, &rx.port);
	set_fifo_FW(&tx.port, out_fifo);
}

int test_local(int a, int b)
{
	int k = a * b + 10;
	if(a < b){
		int k = a - b;
		a = k * a * b;
	}
	return k / a + b;
}

void RV_AXI4L::fsm(BIT cancel_io_insn, UINT3 io_op_in, UINT32 src0, AXI4L::CH *axi) {
	UINT3 io_op = (cancel_io_insn) ? IO_Idle : (io_op_in | pending_op);
	int isRead = (io_op == IO_ReadData || io_op == IO_ReadStatus);
	int isWrite = (io_op == IO_WriteData || io_op == IO_WriteCommand);
	AXIOp axiOp = (isRead) ? AXI_Read : (isWrite) ? AXI_Write : AXI_Idle;
	data_latched = 0;
	data_in = 0;
	stalled = !MasterFSM::fsm(axiOp, axi, addr_out, transfer_size, src0, &data_in, &data_latched);
	pending_op = (stalled) ? io_op : IO_Idle;
	active = (io_op != IO_Idle);
}

#if defined(USE_VIRTUAL_BUS)
bool AXI4L::MasterFSM::fsm(UINT2 op, CH *axi, UINT32 addr, UINT3 size, UINT32 dout, UINT32 *din, BIT *data_latched) {
	if (op == AXI_Read) { return VirtualBUS::virtualBus->slaveDevRead(din, addr) != 0; }
	if (op == AXI_Write) { return VirtualBUS::virtualBus->slaveDevWrite(dout, addr) != 0; }
	return true;
}
#else
//#define DBG_MFSMW
#if defined(DBG_MFSMW)
extern CPU cpu1, cpu2;
#endif
#define SUPRESS_LOCK_ERROR_MSG
bool AXI4L::MasterFSM::fsm(UINT2 op, CH *axi, UINT32 addr, UINT3 size, UINT32 dout, UINT32 *din, BIT *data_latched) {
	BIT stalled = 0;
#ifdef	__LLVM_C2RTL__
	axi->setMARead(out.raddr, out.rdat);
	axi->setMAWrite(out.waddr, out.wdat, out.wres);
#endif
	switch (rw_state) {
	case RW_Init:
		if (op == AXI_Read) {
			out.setRead(addr, size, 1, 1, burst_length, mem_prot);    /// ra_addr, ra_size, ra_valid, rd_ready
			burst_count = burst_length;
			in.resetRead();
			stalled = 1;
			rw_state = RW_RAddr;
		}
		else if (op == AXI_Write) {  /// isWrite
			out.setWrite(addr, size, 1, dout, 0xf, 1, (burst_length == 0), 1, burst_length, mem_prot); /// wa_addr, wa_size, wa_valid,wd_data,wd_strobe,wd_valid,wr_ready
			burst_count = burst_length;
			in.resetWrite();
			stalled = 1;
			*data_latched = 1;
			rw_state = RW_WAddr;
#if defined(DBG_MFSMW)
			int myID = (this == &cpu1.io.axim) ? 0 : 1;
			UINT32 cycle = (this == &cpu1.io.axim) ? cpu1.cycle : cpu2.cycle;
			printf("[%8d] AXIM-Write(cpu.%d) : addr(%08x), data(%08x)\n", cycle, myID, addr, dout);
#endif
		}
		break;
	case RW_RAddr: {
		int n_rflag = (in.rdat.valid << 1) | in.raddr.ready;
		if (axi->raddr.s.ready) {
			n_rflag |= 1;
			in.raddr.set(axi->raddr.s);
			out.raddr.reset();
		}
		if (axi->rdat.s.valid) {
			if (burst_count == 0) {
				n_rflag |= 2; in.rdat.set(axi->rdat.s);   out.rdat.reset();
			} else {
				burst_count--;
			}
			*din = axi->rdat.s.data;
			*data_latched = 1;
			if (axi->rdat.s.resp != RSP_OK) {
				printf("[AXI::Master] bad rdat.resp from slave!!!\n");
			}
		}
		if (n_rflag == 3) { rw_state = RW_Init; }
		else { stalled = 1; }
		break;
	}
	case RW_WAddr: {
		int n_wflag = (in.wres.valid << 2) | (in.wdat.ready << 1) | in.waddr.ready;
		int bad_resp = (in.wres.resp != RSP_OK);
		if (axi->waddr.s.ready) {
			n_wflag |= 1;
			in.waddr.set(axi->waddr.s);
			out.waddr.reset();
		}
		if (axi->wdat.s.ready) {
			if (burst_count == 0) {
				n_wflag |= 2;
				out.wdat.reset();
			} else {
				out.wdat.last = (burst_count == 1);
				out.wdat.data = dout;
				burst_count--;
				*data_latched = 1;
			}
			in.wdat.set(axi->wdat.s);
		}
		if (axi->wres.s.valid) {
			n_wflag |= 4; in.wres.set(axi->wres.s);   out.wres.reset();
			if (axi->wres.s.resp != RSP_OK) {
#if !defined(SUPRESS_LOCK_ERROR_MSG)
				printf("[AXI::Master] bad wres.resp from slave!!! (lock/unlock error?)\n");
#endif
				bad_resp = 1;
			}
		}
#if 1   /// 6/23/17 : if (bad response) { try again; }
		if (n_wflag == 7) {
			rw_state = RW_Init;
			if (bad_resp) { stalled = 1; }  /// try again
		}
#else
		if (n_wflag == 7) { rw_state = RW_Init; }
#endif
		else { stalled = 1; }
		break;
	}
	}
#ifndef	__LLVM_C2RTL__
	axi->setMARead(out.raddr, out.rdat);
	axi->setMAWrite(out.waddr, out.wdat, out.wres);
#endif
	return !stalled;// != 0;
}
#endif
