
#if !defined(RV_PROC_UTIL_H)
#define RV_PROC_UTIL_H
#if 0   /// 7/1/20
#define C2R_ASSERT(n) do { if (!(n)) { abort(#n); } } while(0)
#endif

#if !defined(__LLVM_C2RTL__) && defined(USE_RISC_V)

#if defined(RTL_C_HEADER)
#include RTL_C_HEADER
#endif
#endif

struct rv_loop_info {
    unsigned startAddr, endAddr, loopCount;
    rv_loop_info() { reset(); }
    void reset() { startAddr = 0; endAddr = 0; loopCount = 0; }
    void init(UINT32 br_active, UINT32 br_addr, UINT32 pc) {
        if (!loopCount && br_active && br_addr < pc) {   /// backward branch
            endAddr = pc;
            startAddr = br_addr;
            loopCount = 1;
        }
    }
    void pause() { printf(" <hit ENTER>:"); getchar(); }
    void printLoopMsg() { printf("---- loop [%x -- %x] : iterated %d times ----\n", startAddr, endAddr, loopCount); }
    bool skipPrint(UINT32 dc_stall, UINT32 pc, UINT32 br_active) {
        if (!dc_stall) {
            if (pc == endAddr) {
                if (br_active) { ++loopCount; }
                else { printLoopMsg(); reset(); }  /// exit loop
            }
            else if (pc < startAddr || pc > endAddr) {
                if (loopCount > 1) { printLoopMsg(); }
                reset();
            }
        }
        return loopCount > 10;
    }
};

//#define IGNORE_UNTIL	190000//185000

struct rv_insn {
    static rv_loop_info loopInfo;
    const char *name;
    UINT32 *imm, *rd, *rs1, *rs2, *rs3;
    int br_type, mem_flag, invalid;
    UINT32 br_active, br_addr, pc, ir, cycle, opcode, io_active;
    UINT32 dc_stall, ex_stall;
    UINT32 funct3, mdFlag, subFlag, sextFlag, dst_id, src_id[2], *src, *gpr, ex_out;
    rv_insn(CPU &cpu) : name(0), imm(0), rd(0), rs1(0), rs2(0), rs3(0), br_type(0), mem_flag(0), invalid(0) {
#if defined(IGNORE_UNTIL)
		if (cpu.cycle < IGNORE_UNTIL) { return; }
#endif
//#if defined(USE_BRANCH_EX)
        br_active = cpu.ex.br.active;
        br_addr = cpu.ex.br.addr;
        pc = cpu.dc.r.pc;
        dc_stall = cpu.dc.stall;
        if (loopInfo.skipPrint(dc_stall, pc, br_active)) { return; }
        ex_stall = cpu.ex.stall;
        ir = cpu.ir;
        cycle = cpu.cycle;
		opcode = cpu.insn.opcode;
        io_active = cpu.io.active;
        funct3 = cpu.dc.r.funct3;
        mdFlag = cpu.dc.r.mdFlag;
        subFlag = cpu.dc.r.subFlag;
        sextFlag = cpu.dc.r.sextFlag;
        dst_id = cpu.dc.r.dst_id;
        src_id[0] = cpu.dc.r.src_id[0];
        src_id[1] = cpu.dc.r.src_id[1];
        src = cpu.dc.r.src;
        gpr = cpu.gpr;
#if defined(USE_DST_STATUS_REG)   /// 7/10/20
        ex_out = cpu.ex.r.dst.out;
#else
        ex_out = cpu.ex_reg.out;
#endif
        print();
    }
#if defined(RTL_C_HEADER)
    rv_insn(UINT32 _br_active, UINT32 _br_addr, UINT32 _dc_stall, UINT32 _ex_stall, UINT32 _ir, UINT32 _io_active)
        : name(0), imm(0), rd(0), rs1(0), rs2(0), rs3(0), br_type(0), mem_flag(0), invalid(0) {
        ST_RVProc_FIFO &G = G_RVProc_FIFO;
        br_active = _br_active;
        pc = G.cpu.fe_reg.cur_pc;
        dc_stall = _dc_stall;
        if (loopInfo.skipPrint(dc_stall, pc, br_active)) { return; }
        br_addr = _br_addr;
        ex_stall = _ex_stall;
        ir = _ir;
        cycle = G.cpu.cycle;
        opcode = _ir & 0x7f;
        io_active = _io_active;
        funct3 = G.cpu.dc_reg.funct3;
#if !defined(DISABLE_MD)
        mdFlag = G.cpu.dc_reg.mdFlag;
#else
        mdFlag = 0;
#endif
        subFlag = G.cpu.dc_reg.subFlag;
        sextFlag = G.cpu.dc_reg.sextFlag;
        dst_id = G.cpu.dc_reg.dst_id;
        src_id[0] = G.cpu.dc_reg.src_id[0];
        src_id[1] = G.cpu.dc_reg.src_id[1];
        src = G.cpu.dc_reg.src;
        gpr = G.cpu.gpr;
        ex_out = G.cpu.ex_reg.out;
        print();
    }
#endif
    static void abort(const char *n) { printf("ABORT!!(%s)...", n); getchar(); exit(-1); }
    void setNAME(const char *n) { name = n; }
    void setJALNAME() { name = (!dst_id) ? "j" : "jal"; }
    void setJALRNAME() { name = (!dst_id) ? "jr" : "jalr"; }
    void setBRNAME() {
        switch (funct3) {
        case RVF3_beq:  name = "beq";   break;
        case RVF3_bne:  name = "bne";   break;
        case RVF3_blt:  name = "blr";   break;
        case RVF3_bge:  name = "bge";   break;
        case RVF3_bltu: name = "bltu";  break;
        case RVF3_bgeu: name = "bgeu";  break;
        default:        name = "b???";  break;
        }
    }
    void setLDNAME() {
        switch (funct3) {
        case RVF3_lb:  name = "lb";   break;
        case RVF3_lh:  name = "lh";   break;
        case RVF3_lw:  name = "lw";   break;
        case RVF3_lbu: name = "lbu";  break;
        case RVF3_lhu: name = "lhu";  break;
        default:       name = "l???"; break;
        }
    }
    void setSTNAME() {
        switch (funct3) {
        case RVF3_sb:  name = "sb";   break;
        case RVF3_sh:  name = "sh";   break;
        case RVF3_sw:  name = "sw";   break;
        default:       name = "s???"; break;
        }
    }
    void setCOMPNAME() {
        if (mdFlag) {
            switch (funct3) {
            case RVF3_mul:      name = "mul";       break;
            case RVF3_mulh:     name = "mulh";      break;
            case RVF3_mulhsu:   name = "mulhsu";    break;
            case RVF3_mulhu:    name = "mulhu";     break;
            case RVF3_div:      name = "div";       break;
            case RVF3_divu:     name = "divu";      break;
            case RVF3_rem:      name = "rem";       break;
            case RVF3_remu:     name = "remu";      break;
            default:            name = "md???";     break;
            }
        }
        else {
            switch (funct3) {
            case RVF3_add:  name = (subFlag) ? "sub" : "add";    break;
            case RVF3_shl:  name = "sll";   break;
            case RVF3_slt:  name = "slt";   break;
            case RVF3_sltu: name = "sltu";  break;
            case RVF3_xor:  name = "xor";   break;
            case RVF3_shr:  name = (sextFlag) ? "sra" : "srl";   break;
            case RVF3_or:   name = "or";    break;
            case RVF3_and:  name = "and";   break;
            default:        name = "c???";  break;
            }
        }
    }
    void setIMM() { imm = &src[2]; }
    void setRD() { rd = &dst_id; }
    void setRS(int idx) {
        switch (idx) {
        case 1: rs1 = &src_id[0]; break;
        case 2: rs2 = &src_id[1]; break;
            //        case 3: rs3 = &src_id[0]; break;
        }
    }
#if 1
#define PAUSE   if (((opcode == RVI_jal || opcode == RVI_jalr) && rd && *rd &&\
                    !(dc_stall | ex_stall)) \
                    || io_active || invalid) \
                { loopInfo.pause(); } else { printf("\n"); }
#elif 1
#define PAUSE   if (cpu.fe_stt.pctl.stalled_flag) { getchar(); } else { printf("\n"); }
#elif 1
#define PAUSE   getchar()
#else
#define PAUSE   printf("\n");
#endif
    void printReg(UINT32 idx, UINT32 val) {
        if (idx <= 9) { printf(" "); }
        printf("r%d(%8x)", idx, val);
    }
    void printSpace(int sp) { while (sp--) { printf(" "); } }
    void print() {
        set();
        printf("[%6d] %8x [%08x] ", cycle, pc, ir);
        if (dc_stall | ex_stall) {
            printf(" -------- ");
            if (dc_stall) { printf("(dc)"); }
            if (ex_stall) { printf("(ex)"); }
            printf(" stalled --------");
        }
        else {
            int flag = 0;
#define COMMA   if (flag) { printf(","); } flag++
            printf("%-6s ", name);
            if (mem_flag) {
                C2R_ASSERT(rs1 && imm);
                if (mem_flag == 1) {
                    C2R_ASSERT(rd && *rd && !rs2);
                    printReg(*rd, gpr[*rd]);
                    printf(" <- *(");
                }
                else {
                    C2R_ASSERT(!rd && rs1);
                    printReg(*rs2, src[1]);
                    printf(" -> *(");
                }
                printReg(*rs1, src[0]);
                printf("+%8x) = *(%08x) ", *imm, src[0] + *imm);
                if (*rs1 == RVR_SP) { printf("[STACK]"); }
                else if (*rs1 == RVR_GP) { printf("[GLOBAL]"); }
                if ((rd && *rd == RVR_LK) || (rs2 && *rs2 == RVR_LK)) { printf("[LK]"); }
                if (io_active) { printf("[IO-ACTIVE]"); if (rs2) { printf("[DATA:%d]", gpr[*rs2]); } }
            }
            else if (br_type) {
                C2R_ASSERT(imm);
                if (rs1) { COMMA; printReg(*rs1, (br_type) ? gpr[*rs1] : src[0]); }
                if (rs2) { COMMA; printReg(*rs2, (br_type) ? gpr[*rs2] : src[1]); }
                COMMA; printf("%8x+pc", *imm);
                printf(" (br:%8x)", br_addr);
                if (rd && *rd) { printf("(LK:r%d:%8x) [CALL]", *rd, gpr[*rd]); }
                if (rs1 && *rs1 == RVR_LK && *imm == 0) { printf(" [RETURN]"); }
            }
            else {
                if (rd) { printReg(*rd, gpr[*rd]); printf(" <- "); }
                if (rs1) { COMMA; printReg(*rs1, src[0]); }
                if (rs2) { COMMA; printReg(*rs2, src[1]); }
                if (rs3) { COMMA; printReg(*rs3, gpr[*rs3]); }
                if (imm) { COMMA; printf("%8x", *imm); }
                if (opcode == RVI_auipc) { printf("+pc"); }
                if (rd && *rd == RVR_SP) { printf(" [SP]"); }
            }
        }
        printf(" (ex_out:%08x)", ex_out);
        PAUSE;
    }
    void set() {
        switch (opcode) {
        case RVI_lui:   setNAME("lui");  setIMM(); setRD(); break;
        case RVI_auipc: setNAME("auipc"); setIMM(); setRD();  break;
        case RVI_jal:   setJALNAME();  setIMM(); setRD(); br_type = 1; break;
        case RVI_jalr:  setJALRNAME(); setIMM(); setRS(1); setRD(); br_type = 2; break;
        case RVI_br:    setBRNAME();     setIMM(); setRS(1); setRS(2); br_type = 3;
            loopInfo.init(br_active, br_addr, pc);
            break;
        case RVI_ld:    setLDNAME();     setIMM(); setRS(1); setRD(); mem_flag = 1;  break;
        case RVI_st:    setSTNAME();     setIMM(); setRS(1); setRS(2); mem_flag = 2; break;
        case RVI_compi: setCOMPNAME();   setIMM(); setRS(1); setRD();  break;
        case RVI_comp:  setCOMPNAME();   setRS(1); setRS(2); setRD();  break;
        default:    setNAME("Invalid RISC-V insn..."); invalid = 1; break;
        }
    }
    //EmitC2RInitReg("M0.M0.G_cpu_fe_reg_nxt_pc", G->cpu.fe_reg.nxt_pc);
    static const char* GetC2RegName(const char *regName) {
        C2R_ASSERT(strlen(regName) < 100);
        static char tmpStr[106];
        bool isGlobal = false;
        const char *p = regName;
        while (*p) { if (*p == '-' && *(p + 1) == '>') { isGlobal = true; break; } ++p; }
        if (!isGlobal) { return regName; }
        strcpy(tmpStr, "INIT_");
        char *pp = tmpStr + strlen(tmpStr);
        p = regName;
        while (*p) {
            if (*p == '-' && *(p + 1) == '>')   { *(pp++) = '_'; p += 2; }
            else if (*p == '.' || *p == '[')    { *(pp++) = '_'; ++p; }
            else if (*p == ']')                 { ++p; }
            else                                { *(pp++) = *(p++); }
        }
        C2R_ASSERT(pp - tmpStr < 106);
        *pp = 0;
        return tmpStr;
    }
#undef PAUSE
#undef COMMA
};

#define EMIT_C2R_INIT_REG(reg)  EmitC2RInitReg(rv_insn::GetC2RegName(#reg), reg)


#endif // !defined(RV_PROC_UTIL_H)

#if 0   /// add below to RTL-C
#include "../../RVProcUtil.h"   /// before int RVProc_FIFO_RTL_Core (D_FIFO_PORT* in_fifo, D_FIFO_PORT* out_fifo, ST_RVProc_FIFO *G) 
  {
      rv_insn rv(W_G_cpu_dc_stt_br_active, W_G_cpu_dc_stt_br_addr, W_G_cpu_dc_stt_pctl_stall_prev,
          W_G_cpu_ex_stt_pctl_stall_prev, G_cpu_ir, G_cpu_io_active);
  }

#endif
