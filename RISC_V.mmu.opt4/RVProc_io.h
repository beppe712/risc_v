

enum IO_OP_TYPE
{
	IO_Idle			= 0x0,	///	000
	///	2/23/15 : changed bit patterns
	IO_ReadData		= 0x4,	///	100
	IO_WriteData	= 0x5,	///	101
	IO_ReadStatus	= 0x6,	///	110
	IO_WriteCommand	= 0x7,	///	111
};

enum IO_Enum
{
	///	9/12/14
///	enum InsnEnum
	I_io		= 0x08,

	SO_io_r	= 0x0,
	SO_io_s	= 0x1,
	SO_io_w	= 0x2,
	SO_io_c	= 0x3,
	///	9/12/14
///	enum OpEnum
	O_io_r	= 0x30,
	O_io_s	= 0x31,
	O_io_w	= 0x32,
	O_io_c	= 0x33,

///	device ID
	IO_AXI		= 0x0,
	IO_FIFO		= 0x1,
	IO_SYSCTRL	= 0x2,
	IO_TLB		= 0x3,
};

#define		IO_DEV_ADDR_MASK	0xFFFFFFF0
#define		DEV_ADDR_MASK		0xF0000000
#define		UART_MASK			0x10000000
#define		FIFO0_MASK			0x10000020
#define		FIFO1_MASK			0x10000030

#define		SPI_MASK			0x10000060 // Mustafa 7/11/2016
#define		SYSCTL_MASK			0x10001000

#if	0
#define		MEM_DEV_ADDR_MASK	0xF0000000
#define		EXT_MEM_MASK		0x00000000
#else
#define		MEM_DEV_ADDR_MASK	0x80000000
#define		EXT_MEM_MASK		0x80000000
#endif


//#define		IO_R_DATA			0	///	qemu: #define		UART_R_RXTX				0
//#define		IO_R_CTRL			1	///	qemu: #define		UART_R_DIV				1

#define		SPI_R_DATA				0
#define		SPI_R_CTRL				1
#define     SPI_R_LOCK              2
#define     SPI_R_UNLOCK            3
#define		SPI_R_MAX				4

#define		UART_R_DATA				0	///	qemu: #define		UART_R_RXTX				0
#define		UART_R_CTRL				1	///	qemu: #define		UART_R_DIV				1
#define     UART_R_LOCK             2
#define     UART_R_UNLOCK           3
#define		UART_R_MAX				4

#define		FIFO_R_DATA			0	///	qemu: #define		UART_R_RXTX				0
#define		FIFO_R_CTRL			1	///	qemu: #define		UART_R_DIV				1

enum SPI_COMMAND_ENUM
{
	SPI_ACK = 'a',
	SPI_NACK = 'n',
	SPI_TEXT_MODE = 't',
	SPI_BINARY_MOD = 'b',
};
enum IO_Parity
{
	IOP_None = 0,
	IOP_Odd = 1,
	IOP_Even = 3,
	IOP_High = 5,
	IOP_Low = 7,
};

enum UART_BIT
{
	UART_BIT5 = 0,
	UART_BIT6 = 1,
	UART_BIT7 = 2,
	UART_BIT8 = 3,
};

enum UART_BAUD
{
	UART_BAUD_50 = 0,
	UART_BAUD_300 = 1,
	UART_BAUD_1200 = 2,
	UART_BAUD_2400 = 3,
	UART_BAUD_4800 = 4,
	UART_BAUD_9600 = 5,
	UART_BAUD_19200 = 6,
	UART_BAUD_38400 = 7,
	UART_BAUD_57600 = 8,
	UART_BAUD_115200 = 9,
	///	4/27/15 : added UART_BAUD_ULTRA_FAST
	UART_BAUD_ULTRA_FAST = 15,
};

#if !defined(IGNORE_ARCH_INFO)

#define MAX_IO_BUF_SIZE	2

int nxt_pnt(int cur_pnt);

struct IOBuf
{
	ST_BIT		not_empty, full;
	ST_UINT4	rp, wp;
	RF_UINT32	buf[MAX_IO_BUF_SIZE];	///	reg-file
	void reset() { rp = 0; wp = 0; full = 0; not_empty = 0; }
	void update(int rflag, int wflag);
};

typedef UINT32		FIFO_DATA;
typedef ST_UINT32	ST_FIFO_DATA;

struct FIFO_PORT
{
	BIT			ready;		///	0: busy,		1: ready
	BIT			strobe;		///	0: no-data,		1: data-valid
	BIT			chanID;		///	0: raw-data,	1: char-data
	UINT4		bytes;		///	# of bytes to transmit (may be ignored)
	FIFO_DATA	data;		///	24-bit data
};

typedef FIFO_PORT	D_FIFO_PORT _T(direct_signal);	///	direct port signals (not latched)

struct FIFO_RX
{
	FIFO_PORT		port;
	BIT				stalled;
	ST_BIT			ready_out;	///	9/22/17 : changed from BIT (make this register)
	ST_UINT4		bytes_out;
	UINT32 do_rx(FIFO_PORT * dev_fifo_port, UINT2 io_op);
};

struct FIFO_TX
{
	FIFO_PORT		port;
	BIT				stalled;
	ST_BIT			strobe_out;	///	(output) 0: no-data,	1: data-valid
	ST_BIT			chanID_out;	///	(output) 0: raw-data,	1: char-data
	ST_BIT			chanID_out_default;
	ST_FIFO_DATA	data_out;	///	(output) data-output
	void do_tx(FIFO_PORT * dev_fifo_port, UINT2 io_op, BIT chanID, UINT32 src0);
};

struct IO;

struct FIFO
{
	FIFO_RX		rx;
	FIFO_TX		tx;
	ST_BIT		activated;
	ST_UINT3	pending_op;
	UINT32		data_in;
	BIT			active, stalled, data_latched;
	void write_command(UINT15 io_param);
	UINT32 read_status();
	void fsm(IO * io, FIFO_PORT * dev_fifo_port, BIT cancel_io_insn, UINT3 io_op_in, UINT15 io_param, UINT32 src0);
	void set_input_ports(FIFO_PORT * in_fifo, FIFO_PORT * out_fifo);
	void set_output_ports(FIFO_PORT * in_fifo, FIFO_PORT * out_fifo);
};

//#define USE_VOLATILE    /// or may volatile is needed....
/// it seems that volatile is not needed... 
#if defined(USE_VOLATILE)
#define VT  volatile void
#else
#define VT void
#endif

/// 6/9/17 : redo design...
#if !defined(__LLVM_C2RTL__)
//#define USE_VIRTUAL_BUS /// !!!
#endif
struct AXI4L
{
	enum TransferSize { // in bytes
		TRANSFER_SIZE_1   = 0,
		TRANSFER_SIZE_2   = 1,
		TRANSFER_SIZE_4   = 2,
		TRANSFER_SIZE_8   = 3,
		TRANSFER_SIZE_16  = 4,
		TRANSFER_SIZE_32  = 5,
		TRANSFER_SIZE_64  = 6,
		TRANSFER_SIZE_128 = 7,
	};
	enum RespEnum { RSP_OK = 0, RSP_EXOK = 1, RSP_SLV_ERR = 2, RSP_DEC_ERR = 3, };
	struct CH { // channel
		struct ADDR { /// addr-read, addr-write
			struct MA {
				UINT32 addr; UINT3 size; BIT valid; UINT4 len; UINT3 prot;
				VT set(UINT32 a, UINT3 s, BIT v, UINT4 l, UINT3 p) { addr = a; size = s; valid = v; len = l; prot = p; }
				VT set(MA &m) { set(m.addr, m.size, m.valid, m.len, m.prot); }
				VT reset() { set(0, TRANSFER_SIZE_4, 0, 0, 7); }
			} m;
			struct SL {
				BIT ready;
				VT set(BIT r) { ready = r; }
				VT set(SL &s) { set(s.ready); }
				VT reset() { set(0); }
			} s;
		} raddr, waddr;
		struct RDAT {  /// read-data
			struct MA {
				BIT ready;
				VT set(BIT r) { ready = r; }
				VT set(MA &m) { set(m.ready); }
				VT reset() { set(0); }
			} m;
			struct SL {
				UINT32 data; UINT2 resp; BIT valid, last;
				VT set(UINT32 d, UINT2 r, BIT v, BIT l) { data = d; valid = v; resp = r; last = l; }
				VT set(SL &s) { set(s.data, s.resp, s.valid, s.last); }
				VT reset() { set(0, 0, 0, 0); }
			} s;
		} rdat;
		struct WDAT {  /// write-data
			struct MA {
				UINT32 data; UINT4 strobe; BIT valid, last;
				VT set(UINT32 d, UINT4 s, BIT v, BIT l) { data = d; strobe = s; valid = v; last = l; }
				VT set(MA &m) { set(m.data, m.strobe, m.valid, m.last); }
				VT reset() { set(0, 0, 0, 0); }
			} m;
			struct SL {
				BIT ready;
				VT set(BIT r) { ready = r; }
				VT set(SL &s) { set(s.ready); }
				VT reset() { set(0); }
			} s;
		} wdat;
		struct WRES {  /// write-resp
			struct MA {
				BIT ready;
				VT set(BIT r) { ready = r; }
				VT set(MA &m) { set(m.ready); }
				VT reset() { set(0); }
			} m;
			struct SL {
				UINT2 resp; BIT valid;
				VT set(UINT2 r, BIT v) { resp = r; valid = v; }
				VT set(SL &s) { set(s.resp, s.valid); }
				VT reset() { set(0, 0); }
			} s;
		} wres;
		UINT32 intr;
		VT setMARead(ADDR::MA &ra, RDAT::MA &rd) { raddr.m.set(ra); rdat.m.set(rd); }
		VT setMAWrite(ADDR::MA &wa, WDAT::MA &wd, WRES::MA &wr) { waddr.m.set(wa); wdat.m.set(wd); wres.m.set(wr); }
		VT setMARead(CH &c) { raddr.m.set(c.raddr.m); rdat.m.set(c.rdat.m); }
		VT setMAWrite(CH &c) { waddr.m.set(c.waddr.m); wdat.m.set(c.wdat.m); wres.m.set(c.wres.m); }
		VT resetMA() { resetMARead(); resetMAWrite(); }
		VT resetMARead() { raddr.m.reset(); rdat.m.reset(); }
		VT resetMAWrite() { waddr.m.reset(); wdat.m.reset(); wres.m.reset(); }
		VT resetSL() { resetSLRead(); resetSLWrite(); }
		VT setSLRead(ADDR::SL &ra, RDAT::SL &rd) { raddr.s.set(ra); rdat.s.set(rd); }
		VT setSLWrite(ADDR::SL &wa, WDAT::SL &wd, WRES::SL &wr) { waddr.s.set(wa); wdat.s.set(wd); wres.s.set(wr); }
		VT setSLRead(CH &c) { raddr.s.set(c.raddr.s); rdat.s.set(c.rdat.s); }
		VT setSLWrite(CH &c) { waddr.s.set(c.waddr.s); wdat.s.set(c.wdat.s); wres.s.set(c.wres.s); }
		VT resetSLRead() { raddr.s.reset(); rdat.s.reset(); }
		VT resetSLWrite() { waddr.s.reset(); wdat.s.reset(); wres.s.reset(); }
		static VT connectRCh(CH &mc, CH &sc) { sc.setMARead(mc);  mc.setSLRead(sc); }
		static VT connectWCh(CH &mc, CH &sc) { sc.setMAWrite(mc); mc.setSLWrite(sc); }
	} _T(direct_signal);    /// struct CH
	struct PORT {   /// for AXI-FSM IOs : split each MA/SL channels and merge as MA/SL PORTs
		struct MA { /// for AXI-Master-FSM outPorts
			CH::ADDR::MA raddr, waddr; CH::RDAT::MA rdat; CH::WDAT::MA wdat; CH::WRES::MA wres;
			VT setRead(UINT32 ra_addr, UINT3 size, BIT ra_valid, BIT rd_ready, UINT4 len, UINT3 prot)
			{
				raddr.set(ra_addr, size, ra_valid, len, prot); rdat.set(rd_ready);
			}
			VT setWrite(UINT32 wa_addr, UINT2 size, BIT wa_valid, UINT32 wd_data, UINT4 wd_strobe, BIT wd_valid, BIT wd_last, BIT wr_ready, UINT4 len, UINT3 prot)
			{
				waddr.set(wa_addr, size, wa_valid, len, prot); wdat.set(wd_data, wd_strobe, wd_valid, wd_last); wres.set(wr_ready);
			}
		};
		struct SL { /// for AXI-Slave-FSM outPorts, AXI-Master-FSM inPorts
			CH::ADDR::SL raddr, waddr; CH::RDAT::SL rdat; CH::WDAT::SL wdat; CH::WRES::SL wres;
			VT resetRead() { raddr.reset(); rdat.reset(); }
			VT resetWrite() { waddr.reset(); wdat.reset(); wres.reset(); }
		};
	};
	enum DeviceID {
		DI_INVALID = 0xffff,
		DI_UART = 0x0000,
#if 1   /// 8/3/20
        DI_EXT_MEM = 0x0001,
        DI_SPI = 0x0002,
#else
		DI_SPI = 0x0001,
		DI_EXT_MEM = 0x0002,
#endif
	};
	static UINT32 decodeAddr(UINT32 addr) {
		switch (addr & IO_DEV_ADDR_MASK) {
		case UART_MASK: return DI_UART;
		case SPI_MASK:  return DI_SPI;
		default:
			if ((addr & MEM_DEV_ADDR_MASK) == EXT_MEM_MASK)
				return DI_EXT_MEM;
			else
				return DI_EXT_MEM;
		}
	}
	struct MasterFSM;
	struct SlaveFSM;
	struct VirtualBUS {
		static VirtualBUS *virtualBus;
		int dummy;  /// sorry... we need at least 1 member data in base class....
#if defined(USE_VIRTUAL_BUS)
		virtual MasterFSM **getMasterDevices() = 0;
		virtual SlaveFSM  **getSlaveDevices() = 0;
		virtual CH *getMasterChannels() = 0;
		virtual CH *getSlaveChannels() = 0;
		virtual int getMasterCount() = 0;
		virtual int getSlaveCount() = 0;
		void registerSlaveDevice(CH *ch, SlaveFSM *sdev) {
			int scount = getSlaveCount();
			SlaveFSM **sdevArray = getSlaveDevices();
			CH *schan = getSlaveChannels();
			for (int i = 0; i < scount; ++i) { if (&schan[i] == ch) { sdevArray[i] = sdev; return; } }
		}
		virtual BIT slaveDevRead(UINT32 *data, UINT32 addr) = 0;
		virtual BIT slaveDevWrite(UINT32 data, UINT32 addr) = 0;
		void showInterruptCount() {
			int scount = getSlaveCount();
			SlaveFSM **sdevArray = getSlaveDevices();
			for (int i = 0; i < scount; ++i) {
				printf("AXI-slave[%7d] : (%6d Reads, %7d Writes)...\n",
					i, sdevArray[i]->readCount, sdevArray[i]->writeCount);
			}
		}
#else
		void showInterruptCount() {}
#endif
	};
	struct MasterFSM {
		PORT::MA out _T(state); /// for driving MA-outputs
		PORT::SL in _T(state);  /// for latching MA-inputs
		enum RWState { RW_Init, RW_RAddr, RW_WAddr, };
		enum AXIOp { AXI_Read, AXI_Write, AXI_Idle, };
		ST_UINT3    rw_state;
		ST_UINT4	burst_count;
		UINT4		burst_length;
		UINT3		mem_prot;
		bool fsm(UINT2 op, CH *axi, UINT32 addr, UINT3 size, UINT32 dout, UINT32 *din, BIT *data_latched);
	};
	#define ENABLE_MUTEX_MSG
	/// 6/22/17 : change to counting semaphore!!!
	struct Mutex {
		ST_UINT4    lockCount;
		ST_UINT4    ownerID;
		ST_UINT32   lockErrorCount;
		BIT getLock(UINT4 ID, const char *name, UINT32 cycle) {
			if (lockCount && ownerID != ID) {
#if defined(USE_VIRTUAL_BUS)
				if ((lockErrorCount & 0xfff) == 0)
#else
				if ((lockErrorCount & 0x7ff) == 0)
#endif
					printf("[%8d] [getLock:%s:%d] ERROR (already locked by owner(%d))!! (total lockErrors = %d)\n",
						cycle, name, ID, ownerID, lockErrorCount);
				lockErrorCount++;
				return 0;
			}   /// already locked!!!
#if defined(ENABLE_MUTEX_MSG)
			if (cycle < 2000)
				printf("[%8d] [getLock:%s] locked by owner(%d) : lockCount(%d)!!\n", cycle, name, ID, lockCount + 1);
#endif
			ownerID = ID; lockCount++; return 1;
		}
		BIT releaseLock(UINT4 ID, const char *name, UINT32 cycle) {
			if (!lockCount || ID != ownerID) {
				if (!lockCount) { printf("[%8d] [releaseLock:%s:%d] ERROR (not locked)!!\n", cycle, name, ID); }
				else { printf("[%8d] [releaseLock:%s:%d] ERROR (ownerID(%d) != ID(%d))!!\n", cycle, name, ID, ownerID, ID); }
				return 0; /// not locked or not owner
			}
#if defined(ENABLE_MUTEX_MSG)
			if (cycle < 2000)
				printf("[%8d] [releaseLock:%s] released by owner(%d) : lockCount(%d)!!\n", cycle, name, ID, lockCount - 1);
#endif
			lockCount--; return 1;
		}
	};
#define TOTAL_INTERRUPTS    5   /// # of interrupts per CPU
#define INTERRUPT_MASK      ((1 << TOTAL_INTERRUPTS) - 1)
	struct SlaveFSM {
#if defined(USE_VIRTUAL_BUS)
#define INIT_VIRTUAL_BUS_SLAVE(dev, axi) \
    if (!(dev).device_is_registered) { (dev).registerDevice(axi); (dev).device_is_registered = 1; }
		/// assuem SlaveFSM is declared as global/static data (zero initialized)
		int device_is_registered;
		void registerDevice(CH *axi) { VirtualBUS::virtualBus->registerSlaveDevice(axi, this); }
#else
#define INIT_VIRTUAL_BUS_SLAVE(dev, axi)    /// nothing...
#endif
		PORT::SL out _T(state);
		enum ReadState { R_Init, R_Addr, R_End, };
		enum WriteState { W_Init, W_AddrData, W_AddrDataBurst, W_End, };
		/// regs (states)
		ST_UINT3 r_state, w_state;
		ST_UINT32 raddr, waddr;
		ST_UINT3 rsize, wsize;
		ST_BIT raddr_end, waddr_end, wres_end; /// indicate that raddr/waddr are released by master
		ST_BIT writePending;   /// need to indicate difference of write-operation and mutex
		ST_UINT32 intrFlag;
		Mutex mutex;
		/// wires (no-states)
		UINT32 nxt_intrFlag;
		int readFlag, writeFlag;
		UINT32 writeData;
		ST_UINT32   cycle;
		BIT mutexError;
		ST_UINT4	burst_count_r, burst_count_w;
		ST_BIT		burst_end_r;
		/// methods
		void fsmRead(CH *axi);
		void fsmWrite(CH *axi);
#if defined(USE_VIRTUAL_BUS)
		int readCount, writeCount;
		void fsm(CH *axi) {
			fsmUser();
#if defined(TEST_MPSOC)
			axi->intr = (nxt_intrFlag | (readFlag == 1) | ((writeFlag == 1) << 1)) << (TOTAL_INTERRUPTS * mutex.ownerID);
#else
			axi->intr = nxt_intrFlag | (readFlag == 1) | ((writeFlag == 1) << 1);
#endif
			readCount += (readFlag != 0);
			writeCount += (writeFlag != 0);
			/// reset wires for NEXT cycle
			nxt_intrFlag = 0;
			preFsmUser();
			cycle++;
		}
#else
		void fsm(CH *axi) {
			axi->intr = intrFlag;
			nxt_intrFlag = 0;
			preFsmUser();
			fsmRead(axi);
			fsmWrite(axi);
			fsmUser();
#if defined(TEST_MPSOC)
			intrFlag = (nxt_intrFlag) << (TOTAL_INTERRUPTS * mutex.ownerID);
#else
			intrFlag = nxt_intrFlag;
#endif
			cycle++;
		}
#endif
		virtual BIT devRead(UINT32 *data, UINT32 addr, UINT3 size) = 0;
		virtual BIT devReadSetup(UINT32 addr) { return 1; }
		virtual BIT devWrite(UINT32 data, UINT32 addr, UINT3 size) = 0;
		virtual BIT devWriteSetup(UINT32 addr) { return 1; }
		virtual void preFsmUser() { readFlag = 0; writeFlag = 0; writeData = 0; mutexError = 0; };
		virtual void fsmUser() {};
	};
	template <int MC, int SC> struct BUS : VirtualBUS {
		CH m_ch[MC], s_ch[SC];
#if defined(USE_VIRTUAL_BUS)
		MasterFSM *mdev[MC];
		SlaveFSM  *sdev[SC];
		BUS() {
			virtualBus = this;
			resetAllChannelSinks();
			resetAllChannelSources();
			for (unsigned i = 0; i < SC; ++i) { sdev[i] = 0; }
			for (unsigned i = 0; i < MC; ++i) { mdev[i] = 0; }
		}
		void resetAllChannelSources() {
			for (unsigned i = 0; i < SC; ++i) { s_ch[i].resetSL(); }
			for (unsigned i = 0; i < MC; ++i) { m_ch[i].resetMA(); }
		}
		MasterFSM **getMasterDevices() { return mdev; }
		SlaveFSM  **getSlaveDevices() { return sdev; }
		CH *getMasterChannels() { return m_ch; }
		CH *getSlaveChannels() { return s_ch; }
		int getMasterCount() { return MC; }
		int getSlaveCount() { return SC; }
		~BUS() { virtualBus = 0; }
		virtual BIT slaveDevRead(UINT32 *data, UINT32 addr) {
			int slaveID = decodeAddr(addr);
			return (slaveID == DI_INVALID) ? 0 : sdev[slaveID]->devRead(data, addr);
		}
		virtual BIT slaveDevWrite(UINT32 data, UINT32 addr) {
			int slaveID = decodeAddr(addr);
			return (slaveID == DI_INVALID) ? 0 : sdev[slaveID]->devWrite(data, addr);
		}
#endif
		void resetAllChannelSinks() {
			for (unsigned i = 0; i < SC; ++i) { s_ch[i].resetMA(); }
			for (unsigned i = 0; i < MC; ++i) { m_ch[i].resetSL(); }
		}
		void connectInterrupts() { // hard-coded for now... : no intr for spi
#if 0//defined (TEST_MPSOC)
			m_ch[0].intr = (((s_ch[DI_UART].intr) | (s_ch[DI_SYSCTL].intr << 2))) & INTERRUPT_MASK;
			m_ch[1].intr = (((s_ch[DI_UART].intr) | (s_ch[DI_SYSCTL].intr << 2)) >> TOTAL_INTERRUPTS) & INTERRUPT_MASK;
#else
			m_ch[0].intr = (s_ch[DI_UART].intr);// | (s_ch[DI_SYSCTL].intr << 2);
#endif
		}
	};
	struct MasterStatus {   /// ok, another problem... C2R crashes if MasterStatus is declared under BUS
							/// put _T(state) inside MasterStatus/SlaveStatus
		ST_UINT8   slaveID; ////    0xffff : invalid device
		ST_BIT     active;
		BIT        granted;     /// wire
		BIT        requested;   /// wire
		UINT8      reqSlaveID;  /// wire
		VT set(UINT8 sID, BIT a) { slaveID = sID; active = a; }
		VT set(MasterStatus &ms) { set(ms.slaveID, ms.active); }
		VT reset() { slaveID = 0; active = 0; }
		VT resetFlags() { granted = 0; requested = 0; reqSlaveID = 0; }
	};
	template <int MC> struct SlaveStatus {
		ST_UINT8    masterID;
		ST_BIT      active;
	};
	template <int MC, int SC> struct CTRL {
		/// first try... assume MA_COUNT == 1; (no arbitration)
		/// put _T(state) inside MasterStatus/SlaveStatus
		MasterStatus MRStat[MC], MWStat[MC];
		SlaveStatus<MC> SRStat[SC], SWStat[SC];
		ST_UINT4     roundRobinRVal, roundRobinWVal;
#if defined(USE_VIRTUAL_BUS)
		void connectChannel(BUS<MC, SC> *bus) { bus->connectInterrupts(); }
#else
//#if defined(TEST_MPSOC)
#if 1
		void checkMRReq(BUS<MC, SC> *bus, int midx) {
			CH &mc = bus->m_ch[midx];
			MasterStatus &rs = MRStat[midx];
			rs.resetFlags();
			if (!rs.active && mc.raddr.m.valid) {
				UINT32 sidx = decodeAddr(mc.raddr.m.addr);
				if (!SRStat[sidx].active && !bus->s_ch[sidx].rdat.s.valid) {
					rs.requested = 1;
					rs.reqSlaveID = sidx;
				}
			}
		}
		void checkMWReq(BUS<MC, SC> *bus, int midx) {
			CH &mc = bus->m_ch[midx];
			MasterStatus &ws = MWStat[midx];
			ws.resetFlags();
			if (!ws.active && mc.waddr.m.valid) {
				UINT32 sidx = decodeAddr(mc.waddr.m.addr);
				if (!SWStat[sidx].active && !bus->s_ch[sidx].wres.s.valid) {
					ws.requested = 1;
					ws.reqSlaveID = sidx;
				}
			}
		}
		BIT arbitrate(UINT4 sidx, MasterStatus *ms, UINT4 rrVal) {
			BIT flag = 0;
			for (int i = 0; i < MC; ++i) {  /// 1st round
				if (i >= rrVal && !flag && ms[i].requested && ms[i].reqSlaveID == sidx) {
					ms[i].granted = 1; flag = 1;
				}
			}
			for (int i = 0; i < MC; ++i) {  /// 2nd round
				if (!flag && ms[i].requested && ms[i].reqSlaveID == sidx) {
					ms[i].granted = 1; flag = 1;
				}
			}
			return flag;
		}
		void arbitrateMR() {
			int granted = 0;
			for (int i = 0; i < SC; ++i) { granted |= arbitrate(i, MRStat, roundRobinRVal); }
			if (granted) { roundRobinRVal = (roundRobinRVal < MC - 1) ? roundRobinRVal + 1 : 0; }
		}
		void arbitrateMW() {
			int granted = 0;
			for (int i = 0; i < SC; ++i) { granted |= arbitrate(i, MWStat, roundRobinWVal); }
			if (granted) { roundRobinWVal = (roundRobinWVal < MC - 1) ? roundRobinWVal + 1 : 0; }
		}
		//#define DBG_MW
#if defined(DBG_MW)
		ST_UINT32 dbg_cycle;
#endif
#if defined(TEST_VIRTUAL_METHOD_C2R_FUNC)
		_C2R_FUNC(1)
#endif
		void connectChannel(BUS<MC, SC> *bus) {
			bus->resetAllChannelSinks();
			bus->connectInterrupts();
#if defined(DBG_MW)
			if (dbg_cycle < 2000) {
				if (bus->m_ch[0].intr) { printf("[%8d] m[0].intr = %08x\n", dbg_cycle, bus->m_ch[0].intr); }
				if (bus->m_ch[1].intr) { printf("[%8d] m[1].intr = %08x\n", dbg_cycle, bus->m_ch[1].intr); }
			}
#endif
			int i, j;
			for (i = 0; i < MC; ++i) { checkMRReq(bus, i); checkMWReq(bus, i); }
			arbitrateMR(); arbitrateMW();
			for (i = 0; i < MC; ++i) {  /// i : const (due to loop-unrolling), j : variable
				if (updateMRStat(bus, i, &j)) { CH::connectRCh(bus->m_ch[i], bus->s_ch[j]); }
			}
			for (i = 0; i < MC; ++i) {
				if (updateMWStat(bus, i, &j)) { CH::connectWCh(bus->m_ch[i], bus->s_ch[j]); }
			}
#if defined(DBG_MW)
			++dbg_cycle;
#endif
		}
		BIT updateMRStat(BUS<MC, SC> *bus, int midx, int *sidx) {
			CH &mc = bus->m_ch[midx];
			MasterStatus &rs = MRStat[midx];
			*sidx = rs.slaveID;
			if (rs.granted) {
				*sidx = rs.reqSlaveID;
				rs.set(*sidx, 1);
				SRStat[*sidx].active = 1;
				SRStat[*sidx].masterID = midx;
				return 1;
			}
			if (rs.active && mc.rdat.m.ready && bus->s_ch[*sidx].rdat.s.last) {
				rs.reset();  SRStat[*sidx].active = 0; return 1;
			}
			else { return rs.active; }
		}
		BIT updateMWStat(BUS<MC, SC> *bus, int midx, int *sidx) {
			CH &mc = bus->m_ch[midx];
			MasterStatus &ws = MWStat[midx];
			*sidx = ws.slaveID;
			if (ws.granted) {
				*sidx = ws.reqSlaveID;
				ws.set(*sidx, 1);
				SWStat[*sidx].active = 1;
				SWStat[*sidx].masterID = midx;
#if defined(DBG_MW)
				if (dbg_cycle < 2000)
					printf("[%8d] W-granted : M[%d]->S[%d]\n", dbg_cycle, midx, *sidx);
#endif
				return 1;
			}
			if (ws.active && mc.wres.m.ready && bus->s_ch[*sidx].wres.s.valid) {
				ws.reset(); SWStat[*sidx].active = 0;
#if defined(DBG_MW)
				if (dbg_cycle < 2000)
					printf("[%8d] W-end     : M[%d]->S[%d]\n", dbg_cycle, midx, *sidx);
#endif
				return 1;
			}
			else { return ws.active; }
		}
#else
#if defined(TEST_VIRTUAL_METHOD_C2R_FUNC)
	_C2R_FUNC(1)
#endif
	void connectChannel(BUS<MC, SC> *bus) {
			bus->resetAllChannelSinks();
			bus->connectInterrupts();
			int i, j;
			for (i = 0; i < MC; ++i) {  /// i : const (due to loop-unrolling), j : variable
				if (updateMRStat(bus, i, &j)) { CH::connectRCh(bus->m_ch[i], bus->s_ch[j]); }
			}
			for (i = 0; i < MC; ++i) {
				if (updateMWStat(bus, i, &j)) { CH::connectWCh(bus->m_ch[i], bus->s_ch[j]); }
			}
		}
		BIT updateMRStat(BUS<MC, SC> *bus, int midx, int *sidx) {
			CH &mc = bus->m_ch[midx];
			MasterStatus &rs = MRStat[midx];
			*sidx = rs.slaveID;
			if (!rs.active && mc.raddr.m.valid) {
				*sidx = decodeAddr(mc.raddr.m.addr);
				rs.set(*sidx, 1); return 1;
			}
			if (rs.active && mc.rdat.m.ready && bus->s_ch[*sidx].rdat.valid) { rs.reset(); return 1; }
			else { return rs.active; }
		}
		BIT updateMWStat(BUS<MC, SC> *bus, int midx, int *sidx) {
			CH &mc = bus->m_ch[midx];
			MasterStatus &ws = MWStat[midx];
			*sidx = ws.slaveID;
			if (!ws.active && mc.waddr.m.valid) {
				*sidx = decodeAddr(mc.waddr.m.addr);
				ws.set(*sidx, 1); return 1;
			}
			if (ws.active && mc.wres.m.ready && bus->s_ch[*sidx].wres.valid) { ws.reset(); return 1; }
			else { return ws.active; }
		}
#endif
#endif
	};
};

struct RV_AXI4L : AXI4L::MasterFSM {
	UINT32		addr_out;   /// from master-core
	UINT32		data_in;    /// from slave
	UINT3		transfer_size;
	ST_UINT3	pending_op;
	BIT		active, stalled, data_latched;
	void fsm(BIT cancel_io_insn, UINT3 io_op_in, UINT32 src0, AXI4L::CH *axi);
};

struct SYSCTRL {
	UINT32	ext_irq_pending _T(state);
	UINT32	ext_irq_enable _T(state);

	UINT16	irq_pending;
	UINT16	irq_set;
	UINT16	irq_clr;

	BIT	seip;
	BIT	mtip;
	// timer
	ST_UINT32	mtime_l;
	ST_UINT32	mtime_h;
	ST_UINT32	mtimecmp_l;
	ST_UINT32	mtimecmp_h;

	ST_UINT7	divider;
	//
	UINT32	data_in;
	BIT	active, stalled, data_latched;
	void fsm(UINT3 io_op, UINT12 addr, UINT32 data, UINT16 intr);
	void incr_timer() {
		UINT32	n_mtime_l = mtime_l;
		UINT32	n_mtime_h = mtime_h;

		if (n_mtime_l == 0xffffffff) {
			n_mtime_l = 0;
			n_mtime_h++;
		} else {
			n_mtime_l++;
		}

		mtime_l = n_mtime_l;
		mtime_h = n_mtime_h;
	}
};

//#define USE_FIFO

#include "Cache.h"

struct IO
{
	SYSCTRL	sysctrl;
#if defined(USE_FIFO)
	FIFO	fifo;
#endif
//	RV_AXI4L axim;
//#if defined(USE_CACHE_TEMPLATE)
//#if defined(MERGE_DTLB_INTO_DCACHE)
    DCache<DTLB_WAY_BITS, DC_WAY_BITS, DC_OFFSET_BITS, DC_INDEX_BITS, DC_TAG_BITS> dcache;
	UINT32	data_in;
	BIT	active, stalled, data_latched;

	static BIT decode_addr(UINT6 op, UINT32 addr, UINT5 *io_id, UINT3 *io_op, UINT12 *io_param);
	void pre_fetch(UINT32 addr);
	BIT fsm(UINT6 dc_reg_op, UINT32 src0, UINT32 io_com, UINT3 funct3,
		FIFO_PORT * dev_fifo_port, AXI4L::CH *axi, UINT16 intr,
#if defined(MODIFY_DTLB_FLUSH)
        BIT mmu_en, UINT32 pg_dir, BIT src_f);
#else
        BIT mmu_en, UINT32 pg_dir);
#endif
};

void dec_io();
UINT32 io_pipe_control();
void set_fifo_input_ports(FIFO * fifo, FIFO_PORT * in_fifo, FIFO_PORT * out_fifo);
void set_fifo_output_ports(FIFO * fifo, FIFO_PORT * in_fifo, FIFO_PORT * out_fifo);
void fifo_ext_init(FIFO_PORT * in_fifo, FIFO_PORT * out_fifo, UINT32 * p_cpu_cycle);
void fifo_ext_input(FIFO_PORT * in_fifo, FIFO_PORT * out_fifo);
void fifo_ext_output(FIFO_PORT * in_fifo, FIFO_PORT * out_fifo);
void fifo_ext_switch_mode(FIFO_PORT * in_fifo, FIFO_PORT * out_fifo);
int fifo_ext_print_result();

const char * insn_name_IO();

#endif
