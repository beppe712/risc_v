//#if defined(USE_CACHE_TEMPLATE)

#if !defined(_CACHE_H_)

#define _CACHE_H_

#define	USE_SINGLE_PORT_MEMORY

/*
* Psuedo LRU
*
* LRU update:
*
*	   access | 0
*      -------+------
*       way 0 | 1 
*       way 1 | 0 

*	   access | 2 | 1 | 0
*      -------+---+---+------
*       way 0 | 1 | 1 | - (no change)
*       way 1 | 1 | 0 | -
*       way 2 | 0 | - | 1
*       way 3 | 0 | - | 0
*
*	   access | 6 | 5 | 4 | 3 | 2 | 1 | 0
*      -------+---+---+---+---+---+---+------
*       way 0 | 1 | 1 | - | 1 | - | - | - 
*       way 1 | 1 | 1 | - | 0 | - | - | - 
*       way 2 | 1 | 0 | - | - | 1 | - | - 
*       way 3 | 1 | 0 | - | - | 0 | - | - 
*       way 4 | 0 | - | 1 | - | - | 1 | -
*       way 5 | 0 | - | 1 | - | - | 0 | -
*       way 6 | 0 | - | 0 | - | - | - | 1
*       way 7 | 0 | - | 0 | - | - | - | 0
*
* Select victim:

*  if (bit2 == 1)
*	// select victim from way 2/3.
*	if (bit0 == 1)
*		select way 3
*	else
*		select way 2
*  else
*	// select victim from way 0/1.
*	if (bit1 == 1)
*		select way 1
*	else
*		select way 0
*/

#define	N_WAY		(1 << WAY_BITS)
#define	WAY_MASK	(N_WAY - 1)
#define	BPW_BITS	2
#define	TAG_BITS	(32 - INDEX_BITS - OFFSET_BITS - BPW_BITS)

#define	N_BPW		(1 << BPW_BITS)
#define	N_OFFSET	(1 << OFFSET_BITS)
#define	N_INDEX		(1 << INDEX_BITS)

#define	BPW_MASK	(N_BPW - 1)
#define	OFFSET_MASK	(N_OFFSET - 1)
#define	INDEX_MASK	(N_INDEX - 1)
#define	TAG_MASK	((1 << TAG_BITS) - 1)

#define	OFFSET_BPW_MASK	((OFFSET_MASK << BPW_BITS) | BPW_MASK)

#define	VALID_BIT	(1 << TAG_BITS)
#define	DIRTY_BIT	(1 << (TAG_BITS + 1))

#define	WORD_OFFSET(i, o)	(((i) << OFFSET_BITS) | (o))

/// 9/1/20
template <int WAY_BITS, int INDEX_BITS> struct PseudoLRU {
    uint64_t	lru[N_INDEX] _T(state) _BWT((1 << WAY_BITS) - 1);
    template<unsigned NBITS> uint64_t update(uint64_t lru_bits, unsigned access_way) {
        uint64_t ret;
        const unsigned NBITS2 = NBITS - 1;
        const unsigned SHIFT2 = (1 << NBITS2) - 1;
        const uint64_t MASK2 = ((uint64_t)1 << SHIFT2) - 1;
        unsigned access_way2 = access_way & SHIFT2;

        if ((access_way & (1 << NBITS2)) == 0) {
            ret = update<NBITS2>((lru_bits >> SHIFT2) & MASK2, access_way2);
            ret <<= SHIFT2;
            ret |= lru_bits & MASK2;
            ret |= (uint64_t)1 << (((uint64_t)1 << NBITS) - 2);
        }
        else {
            ret = update<NBITS2>(lru_bits & MASK2, access_way2);
            ret |= lru_bits & (MASK2 << SHIFT2);
        }
        return ret;
    }
    template<> uint64_t update<1>(uint64_t lru_bits, unsigned access_way) {
        return (uint64_t)(access_way ^ 1);
    }
    void update(unsigned idx, unsigned access_way) {
        lru[idx] = update<WAY_BITS>(lru[idx], access_way);
    }
    template<unsigned NBITS> unsigned select_victim(uint64_t lru_bits) {
        const unsigned NBITS2 = NBITS - 1;
        const unsigned SHIFT2 = (1 << NBITS2) - 1;
        const uint64_t MASK2 = ((uint64_t)1 << SHIFT2) - 1;

        if (lru_bits & ((uint64_t)1 << (((uint64_t)1 << NBITS) - 2))) {
            return select_victim<NBITS2>(lru_bits & MASK2) + (1 << NBITS2);
        }
        else {
            return select_victim<NBITS2>((lru_bits >> SHIFT2) & MASK2);
        }
    }
    template<> unsigned select_victim<1>(uint64_t lru_bits) { return (unsigned)lru_bits; }
    unsigned select_victim(unsigned idx) {
        return select_victim<WAY_BITS>(lru[idx]);
    }
};

template <int INDEX_BITS> struct PseudoLRU<0, INDEX_BITS> {
    void update(unsigned idx, unsigned access_way) {}
    unsigned select_victim(unsigned idx) { return 0; }
};

template <int WAY_BITS, int INDEX_BITS> struct RandomReplacement {
    uint64_t randWayIdx _T(state) _BWT(WAY_BITS);
    void update(unsigned idx, unsigned access_way) {
        randWayIdx = (randWayIdx + 1) & (WAY_MASK);
    }
    unsigned select_victim(unsigned idx) { return randWayIdx; }
};

struct TLB_STAT {
    unsigned prev_addr, prev_op, ld_count, st_count, miss_count;
    void update_itlb(unsigned pc) {
        if (pc != prev_addr) {
            pc = prev_addr;
            ld_count++;
        }
    }
    void update_dtlb(EXState::Reg &ex_r) {
        if ((prev_addr != ex_r.pc) || (prev_op != ex_r.op)) {
            prev_addr = ex_r.pc;
            prev_op = ex_r.op;
            if (ex_r.op == RVO_ld)
                ld_count++;
            else if ((ex_r.op == RVO_st) || (ex_r.op == RVO_amo))
                st_count++;
        }
    }
    void reset() {
        prev_addr = 0;
        prev_op = 0;
        ld_count = 0;
        st_count = 0;
        miss_count = 0;
    }
    unsigned total() { return ld_count + st_count; }
    unsigned totalNZ() { return (ld_count + st_count) ? ld_count + st_count : 1; }
} _T(state);

struct TLB_STAT_CPU {
    unsigned cycle;
    TLB_STAT itlb, dtlb;
    TLB_STAT_CPU() { reset(); }
    void reset() { cycle = 0; itlb.reset(); dtlb.reset(); }
    void printInfo(TLB_STAT_CPU & stat1) {
        printf("Total TLB stats: %d\n", cycle);
        printf("I-TLB miss: %d/%d = %f%%\n",
            itlb.miss_count, itlb.total(),
            (float)itlb.miss_count * 100 / itlb.totalNZ());
        printf("D-TLB miss: %d/%d = %f%%\n",
            dtlb.miss_count, dtlb.total(),
            (float)dtlb.miss_count * 100 / dtlb.totalNZ());
        printf("Boot TLB stats: %d\n", stat1.cycle);
        printf("I-TLB miss: %d/%d = %f%%\n",
            stat1.itlb.miss_count, stat1.itlb.total(),
            (float)stat1.itlb.miss_count * 100 / stat1.itlb.totalNZ());
        printf("D-TLB miss: %d/%d = %f%%\n",
            stat1.dtlb.miss_count, stat1.dtlb.total(),
            (float)stat1.dtlb.miss_count * 100 / stat1.dtlb.totalNZ());
        printf("Normal TLB stats: %d\n", cycle - stat1.cycle);
        unsigned miss, total, totalNZ;
        miss = itlb.miss_count - stat1.itlb.miss_count;
        total = itlb.total() - stat1.itlb.total();
        totalNZ = (total) ? total : 1;
        printf("I-TLB miss: %d/%d = %f%%\n",
            miss, total, (float)miss * 100 / totalNZ);
        miss = dtlb.miss_count - stat1.dtlb.miss_count;
        total = dtlb.total() - stat1.dtlb.total();
        totalNZ = (total) ? total : 1;
        printf("D-TLB miss: %d/%d = %f%%\n",
            miss, total, (float)miss * 100 / totalNZ);
    }
};

enum {
    TLB_ST_INIT = 0, TLB_ST_IDLE = 1, TLB_ST_DIR = 2, TLB_ST_PTP = 3, TLB_ST_FLUSH = 4,
    TLB_ST_FLUSH_ALL = 5 /// TLB_ST_IDLE not used in DTLB
};

#define TLB_TAG_BITS    (20 - INDEX_BITS)
#define	TLB_VALID_BIT	(1 << TLB_TAG_BITS)
#define TLB_TAG_MASK    (TLB_VALID_BIT - 1)

template <int WAY_BITS, int INDEX_BITS, int bits>
struct TLB_DATA {
    unsigned data[N_WAY][N_INDEX] _T(memory) _BWT(bits);
	unsigned fwd[N_WAY];

    void     set(int w, int i, unsigned val) { data[w][i] = val; fwd[w] = val; }

	void fetch(unsigned idx) {
        for (int w = 0; w < N_WAY; w++) {
            fwd[w] = data[w][idx];
        }
	}
	void clear_fwd() {
        for (int w = 0; w < N_WAY; w++) {
            fwd[w] = 0;
        }
	}
	unsigned get(unsigned w) { return fwd[w]; }
};

template <int WAY_BITS, int bits>
struct TLB_DATA<WAY_BITS, 0, bits> {
    unsigned data0[N_WAY] _T(state) _BWT(bits);
    void     set(int w, int i, unsigned val) { data0[w] = val; }

	void fetch(unsigned idx) {}
	void clear_fwd() {}
	unsigned get(unsigned w) { return data0[w]; }
};

template <int WAY_BITS, int INDEX_BITS>
struct TLB {
    /*
    * tttt tttt tttt ttii iiii ---- ---- ----
    */
    // TLB
    //protected:
    /// 9/1/20
    TLB_DATA<WAY_BITS, INDEX_BITS, 20 - INDEX_BITS + 1> m_tag;
    TLB_DATA<WAY_BITS, INDEX_BITS, 32>                  m_pte;

    ST_UINT3	st_state;
    UINT3       fb_state;

    UINT32      fb_pte_addr;

    UINT32		pte;
    UINT32      fb_pte;
    ST_UINT32	ptp;
    unsigned    st_index _T(state) _BWT(INDEX_BITS);
#if defined(USE_RANDOM_REPLACEMENT_TLB)
    RandomReplacement<WAY_BITS, INDEX_BITS> crp; /// cache replacement policy
#else
    PseudoLRU<WAY_BITS, INDEX_BITS> crp; /// cache replacement policy
#endif
    unsigned    victim _BWT(WAY_BITS);

    BIT		page_fault;
    BIT     flush_flag;
    unsigned    flush_way, flush_index;
    /// 7/23/20 : remove tlb.miss
    BIT		hit;
    BIT     flush_done, fb_update;
#if defined(ENABLE_TLB_STAT)
    TLB_STAT    stat;
#endif
    // initialize
    int init() {
        for (int w = 0; w < N_WAY; w++) {
            m_tag.set(w, st_index, 0);
        }
        unsigned nxt_index = (st_index + 1) & INDEX_MASK;
        st_index = nxt_index;
        return nxt_index;
    }
    void fetch(UINT20 vpn) {
        unsigned idx = vpn & INDEX_MASK;

		m_tag.fetch(idx);
		m_pte.fetch(idx);
    }
protected:
    void select_victim(unsigned idx) {
        if (WAY_BITS > 0) {
			int w;

            for (w = 0; w < N_WAY; w++) {
                if ((m_tag.get(w) & TLB_VALID_BIT) == 0) {   /// 9/1/20
					victim = w;
                    return;
                }
            }
            victim = crp.select_victim(idx);
        }
        else { victim = 0; }
    }
public:
    void check_tag(BIT mmu_en, UINT20 vpn) {
        unsigned idx = vpn & INDEX_MASK;
        unsigned	hit_way = 0;
        select_victim(idx);
        unsigned vt = ((vpn >> INDEX_BITS) & TLB_TAG_MASK) | TLB_VALID_BIT;	// valid 
        /// 9/9/20 : feedback wire C2RTL bug fixed...
        hit = 0;
        for (int w = 0; w < N_WAY; w++) {
            if (vt == m_tag.get(w)) {
                hit = 1;
                hit_way = w;
                pte = m_pte.get(w);
            }
        }
        if (mmu_en && hit) {
            crp.update(idx, hit_way);
        }
    }
    void update_pte(UINT20 vpn, UINT32 wdata) {
        unsigned	idx = vpn & INDEX_MASK;
        m_pte.set(victim, idx, wdata);
    }
    void update_mtag(UINT20 vpn) {   /// 9/1/20
        UINT6	idx = vpn & INDEX_MASK;
        unsigned vt = ((vpn >> INDEX_BITS) & TLB_TAG_MASK) | TLB_VALID_BIT;
        m_tag.set(victim, idx, vt);
    }
    void update_tag(UINT20 vpn) {
        UINT6	idx = vpn & INDEX_MASK;
        unsigned vt = ((vpn >> INDEX_BITS) & TLB_TAG_MASK) | TLB_VALID_BIT;
        vt |= TLB_VALID_BIT;	// valid 
        m_tag.set(victim, idx, vt);
    }
//#if defined(MODIFY_DTLB_FLUSH)
    BIT get_flush_entry(unsigned vp) {
        unsigned tlb_vt = ((vp >> INDEX_BITS) & TLB_TAG_MASK) | TLB_VALID_BIT;
        /// 9/9/20 : feedback wire C2RTL bug fixed...
        flush_flag = 0;
        BIT tag_matched = 0;
        for (int w = 0; w < N_WAY; w++) {
            if (tlb_vt == m_tag.get(w)) {
//#if defined(MODIFY_CACHE_HIT_LOGIC)
                flush_flag = 1;
                flush_way = w;
                flush_index = vp & INDEX_MASK;
            }
        }
        return flush_flag;
    }
    void i_fsm(BIT mmu_en, EXState &ex, unsigned vp, unsigned pc, RV_AXI4L &axim) {
#if defined(ENABLE_TLB_STAT)
        if (stat.prev_addr != pc) {
            stat.prev_addr = pc;
            if (mmu_en) { stat.ld_count++; }
        }
#endif
#define REPAIR_FLUSH_PATH   /// 12/1/20 : error occurs in llvm.11.0.0
        BIT	do_fetch = 1;
#if !defined(REPAIR_FLUSH_PATH)   /// 12/1/20 : error occurs in llvm.11.0.0
        BIT do_flush = 0;
#endif
        unsigned cur_pte = axim.data_in;
        switch (st_state) {
        case TLB_ST_INIT:
            do_fetch = 0;
            if (init() == 0) {
                st_state = TLB_ST_IDLE;
            }
            break;
        case TLB_ST_FLUSH:
            if (flush_flag) {
                do_fetch = 0;
#if defined(REPAIR_FLUSH_PATH)   /// 12/1/20 : error occurs in llvm.11.0.0
                m_tag.set(flush_way, flush_index, 0); /// do_flush!!
#else
                do_flush = 1;
#endif
            }
            else {
                st_state = TLB_ST_IDLE;
            }
            break;
        case TLB_ST_IDLE:
            if (ex.itflush) {
                if (ex.itflush_vpn_valid) {
                    vp = ex.itflush_vpn;
                    st_state = TLB_ST_FLUSH;
                }
                else {
                    do_fetch = 0;
                    init();
                    st_state = TLB_ST_INIT;
                }
            }
            else if (mmu_en && !hit) {   /// 7/23/20 : remove tlb.miss
#if defined(ENABLE_TLB_STAT)
                stat.miss_count++;
#endif
                st_state = TLB_ST_DIR;
            }
            break;
        case TLB_ST_DIR:
            if (axim.data_latched) {
                if ((cur_pte & 0xf) != 1) {
                    // leaf pte
                    if (cur_pte & 0x000ffc00)
                        cur_pte = 0;
                    else
                        cur_pte |= ((pc & 0x003ff000) >> 2);

                    m_pte.set(victim, vp & INDEX_MASK, cur_pte);   /// 9/2/20
                }
                ptp = cur_pte;
            }
            else {
                cur_pte = ptp;
            }
            if (!axim.stalled) {
                if ((cur_pte & 0xf) != 1) {
                    update_mtag(vp);   /// 9/1/20
                    st_state = TLB_ST_IDLE;
                }
                else {
                    // pointer pte
                    st_state = TLB_ST_PTP;
                }
            }
            break;
        case TLB_ST_PTP:
            if (axim.data_latched) {
                m_pte.set(victim, vp & INDEX_MASK, cur_pte);   /// 9/2/20
            }
            if (!axim.stalled) {
                update_mtag(vp);   /// 9/1/20
                st_state = TLB_ST_IDLE;
            }
            break;
        }
        if (do_fetch) { fetch(vp); }
        else {
#if !defined(REPAIR_FLUSH_PATH)
            if (do_flush) { m_tag.set(flush_way, flush_index, 0); }
#endif
			m_tag.clear_fwd();
			m_pte.clear_fwd();
        }
        // ======= stage/clock boundary =========

        page_fault = 0;
        hit = 1;
        /// 7/23/20 : remove tlb.miss
        if (st_state == TLB_ST_FLUSH) {
            get_flush_entry(ex.itflush_vpn);
        }
        else if (st_state != TLB_ST_INIT && mmu_en) {
            check_tag(mmu_en, vp);
            if (hit) {
                /// 9/3/20 : changed page_fault condition
                page_fault = ((pte & 0x49) != 0x49) || ((pte & 6) == 4);
            }
        }
    }

    	static inline
	UINT32 get_pte_addr(unsigned pg_dir, unsigned ptp, unsigned vp, int root) {
		if (root) {
			return (pg_dir << 12) | ((vp >> 8) & 0xffc);
		} else {
			return ((ptp & 0x3ffffc00) << 2) | ((vp << 2) & 0xffc);
		}
	}

    UINT32 get_phy_pc(unsigned pg_dir, unsigned vp, unsigned low_pc) {   /// 9/2/20
        if (!hit) {
	    return get_pte_addr(pg_dir, ptp, vp, st_state == TLB_ST_IDLE);
        } else { return ((pte & 0xfffffc00) << 2) | low_pc; }   /// 9/2/20
    }
    UINT32 get_phy_daddr(unsigned pg_dir, unsigned daddr) {
        if (!hit) {
	    fb_pte_addr = get_pte_addr(pg_dir, ptp, daddr >> 12,
			    (st_state == TLB_ST_IDLE || st_state == TLB_ST_DIR));
	    return fb_pte_addr;
        } else { return ((pte << 2) & 0xfffff000) | (daddr & 0x00000fff); }
    }
    BIT d_update_page_fault(BIT mmu_en, UINT6 op) {
        page_fault = 0;
        if (st_state != TLB_ST_FLUSH && st_state != TLB_ST_FLUSH_ALL && mmu_en && hit) {
            /// 9/3/20 : changed page_fault condition
            if ((op == RVO_ld && (((pte & 0x43) != 0x43) || (pte & 6) == 4))
                || (op == RVO_st && ((pte & 0xc7) != 0xc7))) {
                page_fault = 1;
            }
        }
        return page_fault;
    }
/***
DTLB states : changes from mmu.v3 to mmu.opt3
mmu.v3 : 	enum { TLB_ST_DIR = 0, TLB_ST_PTP = 1, TLB_ST_PTE = 2, TLB_ST_INIT = 3, TLB_ST_FLUSH };
mmu.opt3 : enum { TLB_ST_INIT = 0, TLB_ST_IDLE = 1, TLB_ST_DIR = 2, TLB_ST_PTP = 3, TLB_ST_FLUSH = 4,
                TLB_ST_FLUSH_ALL = 1 };
mmu.v3          ->  mmu.opt3
TLB_ST_DIR(0)   ->  TLB_ST_DIR(2)
TLB_ST_PTP(1)   ->  TLB_ST_PTP(3)
TLB_ST_PTE(2) : not used
TLB_ST_INIT(3)  ->  TLB_ST_FLUSH_ALL(1)
TLB_ST_FLUSH(4) ->  TLB_ST_FLUSH(4)

***/
    void d_fsm(UINT6 op, BIT fence_active, BIT src_f, BIT pte_search, BIT data_ready, UINT32 data, UINT32 vaddr) {
        fb_update = 0;

        UINT3	next_tlb_state = st_state;

	switch (st_state) {
	case TLB_ST_INIT:
		next_tlb_state = TLB_ST_IDLE;
		break;
	case TLB_ST_IDLE:
		if (fence_active) {
		    if (src_f) {
			int flag = get_flush_entry(vaddr >> 12);
			if (flag) {
			    next_tlb_state = TLB_ST_FLUSH;
			}
		    } else {
			next_tlb_state = TLB_ST_FLUSH_ALL;
		    }
		} else if (pte_search) {
			next_tlb_state = TLB_ST_DIR;
		}
		break;
	case TLB_ST_DIR:
		if (data_ready) {
		    UINT32 nxt_pte = data;

			if ((data & 0xf) != 1) { // leaf pte
			    if (data & 0x000ffc00)
				nxt_pte = 0;
			    else
				nxt_pte = data | ((vaddr & 0x003ff000) >> 2);
			    fb_update = 1;
			    next_tlb_state = TLB_ST_IDLE;
			}
			else { // pointer pte
			    next_tlb_state = TLB_ST_PTP;
			}

		    fb_pte = nxt_pte;
		    ptp = nxt_pte;

		    fb_pte_addr = ((data  & 0x3ffffc00) << 2) |
			          ((vaddr & 0x003ff000) >> 10);
		}
		break;
	case TLB_ST_PTP:
		if (data_ready) {
			UINT32 nxt_pte = data;   /// 7/23/20 : use tlb.ptp instead of tlb.fb_pte
			fb_update = 1;
			next_tlb_state = TLB_ST_IDLE; /// TLB_ST_DIR : same

			fb_pte = nxt_pte;
			ptp = nxt_pte;
		}
		break;
	case TLB_ST_FLUSH:
		next_tlb_state = TLB_ST_IDLE;       /// TLB_ST_DIR : same
		break;
	case TLB_ST_FLUSH_ALL:
		if (flush_done) {
			next_tlb_state = TLB_ST_IDLE;   /// TLB_ST_DIR : same
		}
		break;
	}
        fb_state = next_tlb_state;   /// referenced in EX-stage
        st_state = next_tlb_state;
    }
    UINT32 d_pre_fetch(UINT32 addr, BIT mmu_en) {
        UINT20	vpn = addr >> 12 & 0xfffff;

        flush_done = 0;

	switch (fb_state) {
	case TLB_ST_FLUSH_ALL:
		if (init() == 0)
			flush_done = 1;
		break;
	case TLB_ST_FLUSH:
		m_tag.set(flush_way, flush_index, 0);
		break;
	case TLB_ST_DIR:
	case TLB_ST_PTP:
		return fb_pte_addr;
	default:
		if (fb_update) {
#if defined(ENABLE_TLB_STAT)
			stat.miss_count++;
#endif
			update_pte(vpn, fb_pte);
			update_tag(vpn);
		} else {
			fetch(vpn);
		}
		break;
	}
#if defined(CHECK_TLB_TAG_IN_PREFETCH)
        check_tag(mmu_en, vpn);
#endif
	return addr;
    }
};

///////////////// CACHE ///////////////

#if	ENABLE_CACHE_STAT
struct CACHE_STAT {
    unsigned prev_addr, prev_op, rw_count, miss_count;

    void update_icache(unsigned pc) {
	if (prev_addr != pc) {
		prev_addr = pc;
		rw_count++;
	}
    }
    void update_dcache(EXState::Reg &ex_r) {
	if ((prev_addr != ex_r.pc) || (prev_op != ex_r.op)) {
	    prev_addr = ex_r.pc;
	    prev_op = ex_r.op;
            if ((ex_r.op == RVO_ld) || (ex_r.op == RVO_st) || (ex_r.op == RVO_amo))
		rw_count++;
	}
    }
} _T(state);

template <int SIZE>
struct CACHE_STAT_SAVED {
	unsigned long cycle[SIZE], rw_count[SIZE], miss_count[SIZE];
	void reset() { memset(cycle, 0, sizeof(cycle)); }
	void save(int id, unsigned long cyc, unsigned long rw, unsigned long miss) {
		if (cycle[id] == 0) {
			cycle[id] = cyc;
			rw_count[id] = rw;
			miss_count[id] = miss;
		}
	}

	void print(int i, const char *msg) {
		print(msg, rw_count[i], miss_count[i]);
	}

	void print_delta(int i, int j, const char *msg) {
		print(msg, rw_count[j] - rw_count[i], miss_count[j] - miss_count[i]);
	}

	void print(const char *msg, unsigned long rw, unsigned long miss) {
		printf("%s: %ld/%ld = %f%%\n", msg, miss, rw, (float)miss/rw);
	}

	static void print(int i, const char *msg,
			  CACHE_STAT_SAVED<SIZE> i_stat,
			  CACHE_STAT_SAVED<SIZE> d_stat) {
		printf("%s: %ld\n", msg, i_stat.cycle[i]);
		i_stat.print(i, "I$ miss");
		d_stat.print(i, "D$ miss");
	}
	static void print_delta(int i, int j, const char *msg,
			  CACHE_STAT_SAVED<SIZE> i_stat,
			  CACHE_STAT_SAVED<SIZE> d_stat) {
		printf("%s: %ld\n", msg, i_stat.cycle[j] - i_stat.cycle[i]);
		i_stat.print_delta(i, j, "I$ miss");
		d_stat.print_delta(i, j, "D$ miss");
	}
};
#endif

template <int WAY_BITS, int OFFSET_BITS, int INDEX_BITS, int TAG_BITS0>
struct Cache {
    UINT32		data;
    RV_AXI4L	axim;

protected:
    unsigned	word[N_WAY][N_INDEX * N_OFFSET] _T(memory) _BW(32);
    unsigned	vtag[N_WAY][N_INDEX] _T(memory) _BWT(2 + TAG_BITS0); /// _BWT(1 + ((2 + TAG_BITS0) * 2) / 2 - (TAG_BITS0 != 0));

    unsigned    fwd_word[N_WAY] _BW(32);
    unsigned    fwd_vtag[N_WAY] _BWT(2 + TAG_BITS0);

    PseudoLRU<WAY_BITS, INDEX_BITS> crp; /// cache replacement policy
    unsigned	reg_index _T(state) _BWT(INDEX_BITS);
    ST_BIT		reg_initialized;

public:
    BIT		busy;

protected:
    BIT init_vtag() {
        unsigned index = reg_index;

        for (int w = 0; w < N_WAY; w++) {
            vtag[w][index] = 0;
        }
        index = (index + 1) & INDEX_MASK;
        if (index == 0) {
            reg_initialized = 1;
        }
        reg_index = index;
        return index == 0;
    }
    unsigned select_victim(unsigned idx) {
        unsigned	victim;

        for (victim = 0; victim < N_WAY; victim++) {
            if ((fwd_vtag[victim] & VALID_BIT) == 0) {
                return victim;
            }
        }
        return crp.select_victim(idx);
    }
#define USE_CACHE_SUB_STRUCT
    struct AddressField {
        unsigned tag, idx, ofs;
    } adf;
    void extract_addr_fields(unsigned addr) {
        adf.tag = (addr >> (INDEX_BITS + OFFSET_BITS + BPW_BITS)) & TAG_MASK;
        adf.idx = (addr >> (OFFSET_BITS + BPW_BITS)) & INDEX_MASK;
        adf.ofs = (addr >> BPW_BITS) & OFFSET_MASK;
    }
    struct TagStatus {
        unsigned hit_way;
        BIT hit, dirty;
    } tstt;
    BIT check_vtag() {
        tstt.hit = 0;
        tstt.hit_way = 0;
        tstt.dirty = 0;

        for (int w = 0; w < N_WAY; w++) {
            if (adf.tag == (fwd_vtag[w] & ~DIRTY_BIT)) {
                tstt.dirty = (fwd_vtag[w] >> (TAG_BITS + 1)) & 1;
//#if defined(MODIFY_CACHE_HIT_LOGIC)
                tstt.hit = 1;
                tstt.hit_way = w;
            }
        }
        return tstt.hit;
    }
    // for debugger
public:
    BIT read_data(UINT32 addr, UINT32 *data) {
        int index = (addr >> 6) & 0x3f;
        int vt = ((addr >> 12) & 0xfffff) | 0x100000;
        for (int w = 0; w < N_WAY; w++) {
            if ((vtag[w][index] & 0x1fffff) == vt) {
                *data = word[w][(addr & 0xfff) / sizeof(word[0][0])];
                return 1;
            }
        }
        return 0;
    }
#if	ENABLE_CACHE_STAT
    CACHE_STAT	stat;
#endif
};

template <int TLB_WAY_BITS, int WAY_BITS, int OFFSET_BITS, int INDEX_BITS, int TAG_BITS0>
struct ICache : Cache<WAY_BITS, OFFSET_BITS, INDEX_BITS, TAG_BITS0> {
#if !defined(NO_MMU)
#if defined(FIX_TLB_SIZE)   /// 9/1/20
    TLB<TLB_WAY_BITS, 6 - TLB_WAY_BITS>		tlb;
#else
    TLB<TLB_WAY_BITS, 6>		tlb;
#endif
#endif

    unsigned	st_offset _T(state) _BWT(OFFSET_BITS);
    ST_UINT32	reg_axim_out;

    enum { ST_INIT = 0, ST_IDLE = 1, ST_FILL = 2 };   /// 8/7/20
    ST_UINT3	st_state;

    unsigned    victim _BWT(WAY_BITS);

    ST_UINT32 prev_inst;

#if defined(USE_TAG_REG)
    //unsigned    prv_vtag _T(state) _BWT(2 + TAG_BITS0);
    unsigned    prv_vtag _T(state), prv_idx _T(state);
    BIT      prv_vtag_hit;
    UINT8    prv_vtag_hit_way;
#if !defined(__LLVM_C2RTL__)
    unsigned    prv_vtag_reg_hit, prv_vtag_reg_miss;
#endif
#endif

#define TRASLATE_ADDR_INSIDE_UPDATE
    void update_state(BIT mmu_en, BIT flush) {
#if defined(USE_TAG_REG)    /// assume NO_MMU
        unsigned cur_tag = this->adf.tag;
#endif
#if !defined(NO_MMU)
        if (mmu_en) { this->adf.tag = ((tlb.pte & 0x3ffffc00) >> 10) | VALID_BIT; }
        else        { this->adf.tag |= VALID_BIT; }
#else
        this->adf.tag |= VALID_BIT;
#endif
        BIT	vtag_fetch = 1;
        BIT	word_fetch = 1;

        UINT32 axim_out = reg_axim_out;

        switch (st_state) {
        case ST_INIT:
            vtag_fetch = 0;
            if (this->init_vtag()) {
                st_state = ST_IDLE;
            }
            break;
        /// 8/7/20 : ST_INIT_END not used
        case ST_IDLE:
            if (flush) {
                vtag_fetch = 0;
                this->init_vtag();
                st_state = ST_INIT;
            }
#if defined(NO_MMU)
            else if (this->busy) {
                st_state = ST_FILL;
            }
#else
            else if (tlb.hit && (tlb.st_state == TLB_ST_IDLE) && this->busy) {   /// 7/23/20 : remove tlb.miss
                st_state = ST_FILL;
            }
#endif
            break;
        case ST_FILL:
            if (this->axim.data_latched) {
                word_fetch = 0;
                this->word[victim][WORD_OFFSET(this->adf.idx, st_offset)] = this->axim.data_in;
                if (st_offset == this->adf.ofs) {
                    axim_out = this->axim.data_in;
                }
                st_offset = (st_offset + 1) & OFFSET_MASK;
            }
            if (!this->axim.stalled) {
                this->vtag[victim][this->adf.idx] = this->adf.tag;
                st_state = ST_IDLE;
#if	ENABLE_CACHE_STAT
		this->stat.miss_count++;
#endif
            }
            break;
        }
        reg_axim_out = axim_out;
#if defined(USE_TAG_REG)
        if (vtag_fetch) {
            prv_vtag = cur_tag;
            prv_idx = this->adf.idx;
        }
//        prv_vtag_active = vtag_fetch;
#endif
        // ======= stage/clock boundary =========
        unsigned taddr = this->adf.idx;   /// 8/7/20
        unsigned waddr = WORD_OFFSET(this->adf.idx, this->adf.ofs);
        for (int w = 0; w < N_WAY; w++) {
            this->fwd_vtag[w] = vtag_fetch ? this->vtag[w][taddr] : 0;
            this->fwd_word[w] = word_fetch ? this->word[w][waddr] : axim_out;
        }
        victim = this->select_victim(this->adf.idx);
    }
    void access_axi(BIT mmu_en, AXI4L::CH *axi, UINT32 pc, unsigned pg_dir, unsigned vp) {
        UINT3	mem_op;
        /// 8/7/20
#if defined(NO_MMU)
        if (!this->busy || st_state == ST_INIT) { /// ST_INIT_END not used
#else
        if (!this->busy || st_state == ST_INIT || tlb.st_state == TLB_ST_INIT) { /// ST_INIT_END not used
#endif
            mem_op = IO_Idle;
        }
        else {
            mem_op = IO_ReadData;
        }

        this->axim.burst_length = N_OFFSET - 1;

#if !defined(NO_MMU)
        if (mmu_en) { /// 7/24/20
            /// 9/2/20
            this->axim.addr_out = tlb.get_phy_pc(pg_dir, vp, (pc & (INDEX_MASK << (OFFSET_BITS + BPW_BITS))));
            if (!tlb.hit) { this->axim.burst_length = 0; }
        }
        else { this->axim.addr_out = (pc & ~OFFSET_BPW_MASK); }
#else
        this->axim.addr_out = (pc & ~OFFSET_BPW_MASK);
#endif
        this->axim.transfer_size = AXI4L::TRANSFER_SIZE_4;
        this->axim.mem_prot = 7;
        this->axim.fsm(0, mem_op, 0, axi);
    }
/// 8/2/20 : cleanup
    UINT32 fsm(BIT mmu_en, UINT32 pc, AXI4L::CH *axi, unsigned pg_dir, EXState &ex, FEState &fe) {
#if	ENABLE_CACHE_STAT
	this->stat.update_icache(pc);
#endif
        this->extract_addr_fields(pc);
        unsigned vp = this->adf.tag;
        access_axi(mmu_en, axi, pc, pg_dir, vp);

#if defined(USE_TAG_REG)    /// assume NO_MMU
        if (prv_vtag_hit && prv_vtag == this->adf.tag && prv_idx == this->adf.idx) {
            this->tstt.hit = prv_vtag_hit;
            this->tstt.hit_way = prv_vtag_hit_way;
            unsigned waddr = WORD_OFFSET(this->adf.idx, this->adf.ofs);
            for (int w = 0; w < N_WAY; w++) {
                this->fwd_word[w] = (w == prv_vtag_hit_way) ? this->word[w][waddr] : 0;
            }
#if !defined(__LLVM_C2RTL__)
            prv_vtag_reg_hit++;
#endif
        }
        else {
            update_state(mmu_en, dc.iflush);
#if !defined(NO_MMU)
            tlb.i_fsm(mmu_en, dc.itflush, vp, pc, this->axim);
            if (tlb.st_state != TLB_ST_INIT && mmu_en) {
                this->adf.tag = (tlb.pte >> 10) | VALID_BIT;
            }
#endif
            this->check_vtag();
            prv_vtag_hit = this->tstt.hit;
            prv_vtag_hit_way = this->tstt.hit_way;
#if !defined(__LLVM_C2RTL__)
            if (this->tstt.hit) { prv_vtag_reg_miss++; }
#endif
        }
#else
        update_state(mmu_en, ex.iflush);
        //////////// TLB FSM
#if !defined(NO_MMU)
        tlb.i_fsm(mmu_en, ex, vp, pc, this->axim);
        if (tlb.st_state != TLB_ST_INIT && mmu_en) {
            this->adf.tag = (tlb.pte >> 10) | VALID_BIT;
        }
#endif
        this->check_vtag();
#endif

#if defined(NO_MMU)
        if (st_state == ST_INIT) {
            this->busy = 1;
        }
        else {
            this->busy = !this->tstt.hit;
        }
#else
        if (tlb.st_state == TLB_ST_INIT || tlb.st_state == TLB_ST_FLUSH || !tlb.hit) {
            this->busy = 1;
        }
        else if (tlb.page_fault) {
            this->busy = 0;
        }
        else {
            this->busy = !this->tstt.hit;
        }
#endif
        UINT32	data_out = RV_NOP_INST;	// NOP
        if (!this->busy && !tlb.page_fault) {  /// 9/21/20
            data_out = this->fwd_word[this->tstt.hit_way];
            this->crp.update(this->adf.idx, this->tstt.hit_way);
        }
#if !defined(NO_MMU)
        if (!fe.r.ready || fe.r.sw_mode) { tlb.page_fault = 0; }
#endif
        UINT32 ir = (fe.r.ready) ? data_out : prev_inst;
        prev_inst = ir;
        return (fe.r.sw_mode) ? RV_NOP_INST : ir;
    }
};

template <int TLB_WAY_BITS, int WAY_BITS, int OFFSET_BITS, int INDEX_BITS, int TAG_BITS0>
struct DCache : Cache<WAY_BITS, OFFSET_BITS, INDEX_BITS, TAG_BITS0> {
#if !defined(NO_MMU)
#if defined(FIX_TLB_SIZE)   /// 9/1/20
    TLB<TLB_WAY_BITS, 6 - TLB_WAY_BITS>		tlb;
#else
    TLB<TLB_WAY_BITS, 6>	tlb;
#endif
#endif

    BIT	do_update_vt;
    unsigned	update_vt _BWT(TAG_BITS0);

    BIT	do_update_word;
    UINT32	update_word;
    unsigned	update_offset _BWT(OFFSET_BITS);

    // state
    enum {
        ST_IDLE = 0,
        ST_DONE,
        ST_RW,
        ST_FILL,
        ST_FLUSH,
        ST_EVICT,
    };
    ST_UINT3    st_state;   /// ME-stage update
    UINT3       fb_state;   /// EX-stage reference

    // for flush/invalidate
    unsigned    fb_way _BWT(WAY_BITS);
    unsigned    fb_index _BWT(INDEX_BITS);
    unsigned	fb_offset _BWT(OFFSET_BITS);

    UINT32		data_in _T(state);

    BIT	updating_vt;
    BIT	updating_word;

//#define DBG_DCACHE
#if defined(DBG_DCACHE)
    ST_UINT32 dcycle;
#endif

    void pre_fetch(UINT32 addr, BIT mmu_en, BIT fence_active) {
#if !defined(NO_MMU)
        addr = tlb.d_pre_fetch(addr, mmu_en);
#endif
        updating_vt = 0;
        updating_word = 0;

        for (int w = 0; w < N_WAY; w++) {
            this->fwd_vtag[w] = 0;
            this->fwd_word[w] = 0;
        }
        if (!this->reg_initialized) {
            this->init_vtag();
        }
        else {
            unsigned i = (addr >> (OFFSET_BITS + BPW_BITS)) & INDEX_MASK;
            unsigned o = (addr >> BPW_BITS) & OFFSET_MASK;

            if (do_update_vt) {
                this->vtag[fb_way][fb_index] = update_vt;
                this->fwd_vtag[fb_way] = update_vt;
                updating_vt = 1;
            }
            else if (fb_state == ST_FLUSH) {
                this->fwd_vtag[fb_way] = this->vtag[fb_way][fb_index];
            }
            else {
                for (int w = 0; w < N_WAY; w++) {
                    this->fwd_vtag[w] = this->vtag[w][i];
                }
            }

            if (do_update_word) {
                int data_offset = WORD_OFFSET(fb_index, update_offset);
                this->word[fb_way][data_offset] = update_word;
                updating_word = 1;
            }
            else if (fb_state > ST_FILL) {
                this->fwd_word[fb_way] = this->word[fb_way][WORD_OFFSET(fb_index, fb_offset)];
            }
            else {
                int data_offset = WORD_OFFSET(i, o);

                for (int w = 0; w < N_WAY; w++) {
                    this->fwd_word[w] = this->word[w][data_offset];
                }
            }
        }
    }

    // ======= stage/clock boundary =========

    BIT check_updating(UINT3 op, UINT2 size) {
        if (updating_vt)
            return true;
        if (op == IO_ReadData) {
            return updating_word;
        }
        else {
            return (updating_word && (size != 2));
        }
    }

    void access_axi(UINT3 op, UINT32 addr, UINT32 wdata, AXI4L::CH *axi, BIT cacheable, unsigned size,
        BIT victim_invalid, unsigned victim_vtag, unsigned victim_data) {
        BIT hit = this->tstt.hit;

        unsigned axim_addr_out = addr & ~BPW_MASK;
        unsigned axim_transfer_size = AXI4L::TRANSFER_SIZE_4;
        unsigned axim_burst_length = 0;

        UINT3 mem_op = IO_Idle;
        BIT fill = 0;

        switch (st_state) {
        case ST_IDLE:
            if ((op == IO_ReadData) || (op == IO_WriteData)) {
                if (cacheable) {
                    if (!victim_invalid && !hit && !(victim_vtag & DIRTY_BIT)) {
                        fill = 1;
                    }
                }
                else {
                    mem_op = op;
                    axim_transfer_size = size;
                }
            }
            break;
        case ST_FILL:
            fill = 1;
            break;
        case ST_FLUSH:
        case ST_EVICT:
            if ((victim_vtag & DIRTY_BIT) != 0)
                mem_op = IO_WriteData;
            axim_addr_out = (victim_vtag << (INDEX_BITS + OFFSET_BITS + BPW_BITS)) +
                (fb_index << (OFFSET_BITS + BPW_BITS));
            axim_burst_length = N_OFFSET - 1;
            wdata = victim_data;
            break;
        }

        if (fill) {
            mem_op = IO_ReadData;
            axim_addr_out = (addr & ~OFFSET_BPW_MASK);
            axim_burst_length = N_OFFSET - 1;
        }
        this->axim.addr_out = axim_addr_out;
        this->axim.burst_length = axim_burst_length;
        this->axim.transfer_size = axim_transfer_size;

#if defined(DBG_DCACHE)   /// 9/21/20 : debugging...
        if (dcycle >= 115595000 && dcycle <= 115595030) {
            printf("d$(%8d): mem_op(%d) state(%d:%d) addr(%08x) data(%08x)\n", dcycle, mem_op,
                st_state, tlb.st_state, axim_addr_out, wdata);
        }
#endif
        this->axim.mem_prot = 3;
        this->axim.fsm(0, mem_op, wdata, axi);

        update_word = this->axim.data_in;
    }

    void update_state(UINT3 op, UINT32 addr, UINT32 wdata, UINT3 funct3,
        BIT victim_invalid, unsigned victim, unsigned victim_vtag,
	BIT cacheable, unsigned size) {
        unsigned    flush_index = fb_index;
        unsigned    flush_way = fb_way;
        unsigned	flush_offset = fb_offset;

        unsigned nxt_state = st_state;
        switch (st_state) {
        case ST_FLUSH:
            //printf("YYYYYYYYYYY mem_op = %d, flush_offset = %d\n", mem_op, flush_offset);
            if ((funct3 & 1) == 0) {
                if (victim_vtag & (DIRTY_BIT | VALID_BIT)) {
                    do_update_vt = 1;
                    update_vt = victim_vtag & ~(DIRTY_BIT | VALID_BIT);
                }
            }
            else {
                if (victim_vtag & DIRTY_BIT) {
                    do_update_vt = 1;
                    update_vt = victim_vtag & ~DIRTY_BIT;
                }
            }

            if (this->axim.data_latched)
                flush_offset = (flush_offset + 1) & OFFSET_MASK;

            if (!this->axim.stalled && !do_update_vt) {
                flush_way = (flush_way + 1) & WAY_MASK;
                if (flush_way == 0) {
                    flush_index = (flush_index + 1) & INDEX_MASK;

                    if (flush_index == 0)
                        nxt_state = ST_DONE;
                }
            }
            this->busy = 1;
            break;
        case ST_DONE:
            nxt_state = ST_IDLE;
            this->data = data_in;
            this->busy = 0;
            break;
        case ST_EVICT:
            if (this->axim.data_latched) {
                flush_offset = (flush_offset + 1) & OFFSET_MASK;
            }
            if (!this->axim.stalled) {
                nxt_state = ST_FILL;
            }
            this->busy = 1;
            break;
        case ST_FILL:
            update_offset = flush_offset;

            if (this->axim.data_latched) {
                do_update_word = 1;
                flush_offset = (flush_offset + 1) & OFFSET_MASK;
            }
            if (!this->axim.stalled) {
                do_update_vt = 1;
                update_vt = this->adf.tag;
                if (op == IO_WriteData)
                    update_vt |= DIRTY_BIT;
                nxt_state = ST_IDLE;
            }
            this->busy = 1;
            break;
        case ST_RW:
            if (this->axim.data_latched) {
                data_in = this->axim.data_in;
            }
            if (!this->axim.stalled)
                nxt_state = ST_DONE;
            this->busy = 1;
            break;
        default:    /// IDLE
            if (op == IO_WriteCommand && (funct3 != 0)) {   /// 9/3/20 : added : && (funct3 != 0)
                flush_index = 0;
                flush_way = 0;
                flush_offset = 0;
                nxt_state = ST_FLUSH;
                this->busy = 1;
            }
            else if ((op == IO_ReadData) || (op == IO_WriteData)) {
                flush_index = this->adf.idx;
                flush_way = victim;
                flush_offset = 0;

		if (cacheable) {
                    if (!victim_invalid && !this->tstt.hit) {
#if	ENABLE_CACHE_STAT
		        this->stat.miss_count++;
#endif
                        nxt_state = (victim_vtag & DIRTY_BIT) ? ST_EVICT : ST_FILL;
                    }
                }
                else {
                    nxt_state = ST_RW;
                }
                this->busy = (!cacheable) || (victim_invalid) || (!this->tstt.hit);
            }
            else { this->busy = 0; }

                UINT32	current_word = this->fwd_word[this->tstt.hit_way];

                this->data = current_word;
            if (!this->busy) {
                update_vt = DIRTY_BIT | this->adf.tag;
                update_word = make_write_data(current_word, wdata, addr & 3, size);
                if (op == IO_WriteData) {
                    do_update_vt = !this->tstt.dirty;
                    do_update_word = 1;
                    flush_index = this->adf.idx;
                    flush_way = this->tstt.hit_way;
                    update_offset = this->adf.ofs;
                }
                if (op != IO_Idle) {
                    this->crp.update(this->adf.idx, this->tstt.hit_way);
                }
            }
            break;
        }
        fb_state = nxt_state;
        st_state = nxt_state;
        fb_index = flush_index;
        fb_way = flush_way;
        fb_offset = flush_offset;
    }
    void fsm(UINT6 op, UINT3 io_op, UINT32 paddr, UINT32 tlb_addr, UINT32 wdata, UINT3 funct3,
        AXI4L::CH *axi, BIT mmu_en, BIT src_f) {
        /// 7/24/20
        BIT prv_busy = this->busy;
#if !defined(NO_MMU)
        if (tlb.d_update_page_fault(mmu_en, op)) {
            io_op = IO_Idle;
        }
#endif
        BIT cacheable = ((paddr & MEM_DEV_ADDR_MASK) == EXT_MEM_MASK);

        this->extract_addr_fields(paddr);
        this->adf.tag |= VALID_BIT;

        unsigned	victim = (st_state == ST_FLUSH) ? fb_way : this->select_victim(this->adf.idx);
        /// 7/20/20 : this works
        unsigned victim_vtag = 0, victim_data = 0;
        for (int i = 0; i < N_WAY; ++i) {
            victim_vtag |= (victim == i) ? this->fwd_vtag[i] : 0;
            victim_data |= (victim == i) ? this->fwd_word[i] : 0;
        }
        do_update_vt = 0;
        do_update_word = 0;

        this->check_vtag();

        /// 7/22/20
        unsigned size = funct3 & 3;
	BIT victim_invalid = check_updating(io_op, size);
#if !defined(NO_MMU)
	if (mmu_en && !tlb.hit && (tlb.st_state == TLB_ST_IDLE))
		victim_invalid = 1;
#endif
        access_axi(io_op, paddr, wdata, axi, cacheable, size,
			victim_invalid, victim_vtag, victim_data);
        /// 7/22/20
        update_state(io_op, paddr, wdata, funct3,
			victim_invalid, victim, victim_vtag, cacheable, size);

#if !defined(NO_MMU)
        /// 7/24/20
        tlb.d_fsm(op, (op == RVO_fence && !prv_busy && (funct3 & 2)), src_f,  /// fence_active
            ((io_op == IO_ReadData || io_op == IO_WriteData) && mmu_en && !tlb.hit), /// pte_search
	    this->tstt.hit && !updating_word, // !this->busy,
            this->data, tlb_addr);
#endif
#if defined(DBG_DCACHE)   /// 9/21/20 : debugging...
        dcycle++;
#endif
    }
    UINT32 make_write_data(UINT32 data, UINT32 wdata, UINT2 offset, UINT2 size) {
        UINT32	result;

        switch (size) {
        case 0:
            switch (offset & 3) {
            case 0:
                result = data & ~0xff;
                result |= wdata & 0xff;
                break;
            case 1:
                result = data & ~0xff00;
                result |= wdata & 0xff00;
                break;
            case 2:
                result = data & ~0xff0000;
                result |= wdata & 0xff0000;
                break;
            default:
                result = data & ~0xff000000;
                result |= wdata & 0xff000000;
                break;
            }
            break;
        case 1:
            switch (offset & 2) {
            case 0:
                result = data & ~0xffff;
                result |= wdata & 0xffff;
                break;
            default:
                result = data & ~0xffff0000;
                result |= wdata & 0xffff0000;
                break;
            }
            break;
        default:
            result = wdata;
            break;
        }

        return result;
    }
};

#endif  /// (_CACHE_H_)

